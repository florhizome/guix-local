(define-module (guix-local packages gnome-xyz)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages acl)
  #:use-module (gnu packages attr)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages iso-codes)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages language)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages image)
  #:use-module (gnu packages ibus)
  #:use-module (gnu packages node)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages freedesktop))

(define-public python-magic-wormhole-transit-relay
  (package
    (name "python-magic-wormhole-transit-relay")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "magic-wormhole-transit-relay" version))
              (sha256
               (base32
                "0ppsx2s1ysikns1h053x67z2zmficbn3y3kf52bzzslhd2s02j6b"))))
    (build-system python-build-system)
    (propagated-inputs (list ;;python-pypiwin32
                        python-twisted))
    (native-inputs (list python-mock python-pyflakes python-tox))
    (home-page "https://github.com/warner/magic-wormhole-transit-relay")
    (synopsis "Transit Relay server for Magic-Wormhole")
    (description "Transit Relay server for Magic-Wormhole")
    (license license:expat)))

(define-public python-magic-wormhole-mailbox-server
  (package
    (name "python-magic-wormhole-mailbox-server")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "magic-wormhole-mailbox-server" version))
              (sha256
               (base32
                "1yw8i8jv5iv1kkz1aqimskw7fpichjn6ww0fq0czbalwj290bw8s"))))
    (build-system python-build-system)
    (propagated-inputs (list python-attrs python-autobahn ;;python-pywin32
                             python-six python-twisted))
    (native-inputs (list python-mock python-pyflakes python-tox python-treq))
    (home-page "https://github.com/warner/magic-wormhole-mailbox-server")
    (synopsis "Securely transfer data between computers")
    (description "Securely transfer data between computers")
    (license license:expat)))

(define-public python-magic-wormhole
(package
  (name "python-magic-wormhole")
  (version "0.12.0")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "magic-wormhole" version))
            (sha256
             (base32
              "0q41j99718y7m95zg1vaybnsp31lp6lhyqkbv4yqz5ys6jixh3qv"))))
  (build-system python-build-system)
  (propagated-inputs (list python-attrs
                           python-autobahn
                           python-automat
                           python-click
                           python-hkdf
                           python-humanize
                           python-pynacl
                           ;;python-pywin32
                           python-six
                           python-spake2
                           python-tqdm
                           python-twisted
                           python-txtorcon))
  (native-inputs (list python-magic-wormhole-mailbox-server
                       python-magic-wormhole-transit-relay python-mock
                       python-pyflakes python-tox))
  (home-page "https://github.com/warner/magic-wormhole")
  (synopsis "Securely transfer data between computers")
  (description "Securely transfer data between computers")
  (license license:expat)))

(define-public flatseal
  (package
    (name "flatseal")
    (version "1.8.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/tchx84/Flatseal")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1vf63wsdkm7a1vg0ligwk6w3hjby10q96j2mzfknxgcp5xyc4scc"))))
    (build-system meson-build-system)
    (arguments
     (list
      #:glib-or-gtk? #t
      ;; tests fail trying to download data (pngs) from the gihub repo
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'build 'setenvvars
            (lambda _
              (setenv "HOME" (getcwd)))))))
    (native-inputs
     (list appstream-glib desktop-file-utils gettext-minimal `(,glib "bin") `(,gtk+ "bin") gobject-introspection pkg-config))
    (inputs (list flatpak gtk+ gjs libhandy webkitgtk))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))



;;hydrapaper

;; more extensions: openweather, arcmenu (see techhut video), multiple-monitors ddcutil, onboard, toggle vpn, custom vpn,
;; upgrade: gnome-shell-extensions, gnome-desktop, libadwaita;; appindicator
;; improved osk touch ux, gesture-improvements, screen-autorotate, extended hot corners, espresso, task...
;; ddterm, material-shell; more tiling extensions, flypie
(define* (make-gnome-shell-extension name uuid repo version hash #:optional (more-inputs '()) (synopsis "")(definition "") license)
  (package
    (name (string-append "gnome-shell-extension-" name))
    (version version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url repo)
             (commit (string-append "v" version ""))))
       (file-name (git-file-name name version))
       (sha256
        (base32 hash))
       ;;(snippet
       ;; '(begin (delete-file "src/schemas/gschemas.compiled")))
       ))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("." ,(string-append
                "share/gnome-shell/extensions/"
                uuid)
          #:include-regexp ("\\.js(on)?$" "\\.css$" "\\.ui$" "\\.png$"
                            "\\.xml$" "\\.compiled$")))
       #:phases
       (modify-phases %standard-phases
         ;;(add-after 'unpack 'cd-src
         ;;  (lambda _ (chdir "src")))
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list (list glib "bin"))) ; for glib-compile-schemas
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))


(define-public gnome-shell-extension-coverflowalttab
  (package
    (name "gnome-shell-extension-coverflowalttab")
    (version "1.13")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dmo60/CoverflowAltTab.git")
             (commit "ffd2f3747387a069ea6b58fb198c18d270cc5397")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0hzafsqjdgdcqfsy76vl3bakzbd19070hr5ff0y1dcm2nzxnlcwx"))))
    (build-system gnu-build-system)
    (native-inputs
     (list `(,glib "bin")))
    (arguments
     (list
      #:tests? #f
       #:phases
       #~(modify-phases %standard-phases
           (delete 'configure)
           (add-before 'build 'fake-home-env-var
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (mkdir-p (string-append out "/.local/share/gnome-shell/extensions"))
                 (setenv "HOME" out))))
           (add-after 'install 'move
            (lambda _
              (copy-recursively (string-append #$output "/.local/share")
                                (string-append #$output "/share"))
            (delete-file-recursively (string-append #$output "/.local")))))))
    (home-page "https://github.com/dmo60/CoverflowAltTab")
    (synopsis "Coverflow like Alt-Tab replacement for Gnome-Shell and Cinnamon")
    (description
     "CoverflowAltTab is an Alt-Tab replacement available as an extension for Gnome-Shell.
It lets you Alt-Tab through your windows in a cover-flow manner.")
    (license license:gpl2+)))

;;ddcutil only works with --force-slave-address

(define-public ddcutil-udev-rules
  (package
    (inherit ddcutil)
    (name "ddcutil-udev-rules")
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
       #~'(("data/etc/udev/" "/etc/udev/"))))))

(define-public gnome-shell-extension-gnome-display-brightness-ddcutil
  (package
    (name "gnome-shell-extension-gnome-display-brightness-ddcutil")
    ;;30 not compatible with our version of gnome shell
    (version "33")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/daitj/gnome-display-brightness-ddcutil.git")
             (commit "8997d60e7383a400bd3753cfd6595be2ceb52e20")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1iqws2h068k54fhrfj6vxffwjjdmgkv90y0w5ap6dqq8vpin8286"))))
    (build-system gnu-build-system)
    (native-inputs
     (list `(,glib "bin") gettext-minimal gnome-shell))
    (propagated-inputs (list ddcutil))
    (arguments
     (list
      #:tests? #f
      #:make-flags #~(list (string-append "HOME="
                                          #$output))
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-before 'build 'move
             (lambda* (#:key inputs #:allow-other-keys)
               (let ((ddcutil (assoc-ref inputs "ddcutil")))
                 (substitute* (find-files "extension.js")
                   (("/usr/bin/ddcutil")
                    (string-append ddcutil "/bin/ddcutil"))))))
          (add-after 'install 'move
                     (lambda _
                       (copy-recursively
                        (string-append #$output "/.local/share")
                        (string-append #$output "/share"))
                       (for-each delete-file-recursively
                                 (list (string-append #$output "/.local")
                                       (string-append #$output "/.cache"))))))))
    (home-page "https://github.com/daitj60/gnome-display-brightness-ddcutil")
    (synopsis "Display brightness slider for gnome shell using ddcutil backend ")
    (description "")
    (license license:gpl2+)))

(define-public gnome-menus-fix
  (package
    (inherit gnome-menus)
    (name "gnome-menus")
    (native-inputs
     (list gettext-minimal pkg-config))
    (inputs (list gobject-introspection glib))
    (arguments
     (list #:make-flags #~(list (string-append "INTROSPECTION_GIRDIR="
                                          #$output
                                          "/share/gir-1.0/")
                         (string-append "INTROSPECTION_TYPELIBDIR="
                                          #$output
                                          "/lib/girepository-1.0"))))))

(define-public gnome-shell-extension-arcmenu
  (package
    (name "gnome-shell-extension-arcmenu")
    (version "38")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/arcmenu/ArcMenu.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1f0pl7m9xkvkjnav06n5h8y25sndaq3khz1i31khbcsyyibiqa2r"))
       (patches
        (search-patches "fix_gmenu.patch"))))
    (build-system gnu-build-system)
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    (propagated-inputs (list libadwaita xdg-utils xdg-user-dirs gnome-menus-fix))
    (arguments
     (list
      #:tests? #f
      #:make-flags #~(list (string-append "INSTALLBASE="
                                          #$output
                                          "/share/gnome-shell/extensions"))
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-before 'install 'fix-gmenu-path
            (lambda* (#:key inputs #:allow-other-keys)
              (let ((gmenu-typelib (search-input-directory inputs "/lib/girepository-1.0")))
                (substitute*  (find-files "." "extension.js")
                  (("'@gmenu_path@'")
                   (string-append "'" gmenu-typelib "'") ))))))))
    (home-page "")
    (synopsis "")
    (description "ArcMenu is an application menu for GNOME Shell,
designed to provide a more familiar user experience and workflow.
 This extension has many features, including various menu layout styles,
 GNOME search, quick access to system shortcuts, and much more!")
    (license license:gpl2+)))

(define-public gnome-shell-extension-task-widget
  (package
    (name "gnome-shell-extension-task-widget")
    (version "14")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/jmiskinis/gnome-shell-extension-task-widget")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1yv1pr38kx3d30fi9akv0bshw2n6jqpawz79b5wvyzb3f79ldckr"))))
    (build-system meson-build-system)
    (native-inputs
     (list `(,glib "bin") pkg-config node gettext-minimal libxml2))
    (arguments
     (list
      #:tests? #f
      #:configure-flags #~(list (string-append "-Dgschema_dir="
                                               #$output
                                               "/share/gnome-shell/extensions"))))
    (home-page "https://gitlab.com/jmiskinis/gnome-shell-extension-task-widget")
    (synopsis "Display tasks next to GNOME's calendar widget")
    (description
     "Task Widget is an extension for GNOME that displays tasks next to the calendar widget. It integrates with GNOME Online Accounts and a number of GNOME applications, such as Evolution and Endeavour. With Task Widget you can:

Access your task lists in the top menu
Merge task lists
Group tasks by due date
Mark tasks as (un)completed
Hide completed and empty task lists
Hide completed tasks
Toggle task list visibility and change its display order
And more...")
    (license license:gpl2+)))


(define-public gnome-shell-extension-media-controls
  (package
    (name "gnome-shell-extension-media-controls")
    (version "20")
    (source (origin
              (method git-fetch)
              (uri
               (git-reference
                (url "https://github.com/cliffniff/media-controls")
                (commit "54c23a93811297d85f6b093f0fbc370a8e79fbc4")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0h0hjlvbh9pzl6a0fhpjn31kpfp63wp65914zv2yk7h385ck843k"))
              (modules '((guix build utils)))
              (snippet
               ;; Remove pre-compiled settings schemas and translations from
               ;; source, as they are generated as part of build. Upstream
               ;; includes them for people who want to run the software
               ;; directly from source tree.
               '(begin
                  (for-each delete-file (find-files "schemas/gschemas.compiled"))
                  (for-each delete-file
                            (find-files "locale" "\\.mo$"))))
              (patches (search-patches "media-controls-42.patch"))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("." "share/gnome-shell/extensions/mediacontrols@cliffniff.github.com"
          #:include-regexp ("\\.css$" "\\.compiled$" "\\.ui$" "\\.js(on)?$"
                            "\\.mo$" "\\.xml$" "\\.png$" "\\.svg$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    (home-page "https://github.com/cliffniff/media-controls")
    (synopsis "A media indicator for the Gnome shell")
    (description
     "Show controls and information of the currently playing media in the panel.

Features

Customize how the extension looks
Disable elements you don't want
Invoke different actions in many ways through mouse actions
Basic media controls (play/pause/next/prev)
Other media controls (loop/shuffle)
And more...")
    (license license:expat)))

(define-public gnome-shell-extension-multi-monitors-add-on
  (package
    (name "gnome-shell-extension-multi-monitors-add-on")
    (version "25")
    (source (origin
              (method git-fetch)
              (uri
               (git-reference
                (url "https://github.com/realh/multi-monitors-add-on")
                (commit "b47418b7a098eb33524116d04e5d563c7e81907d")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ndx54n62qwjp7q4mnbkxn3wk6zfhvc3sw64vncci0905rmawmb8"))
              (modules '((guix build utils)))
              (snippet
               ;; Remove pre-compiled settings schemas and translations from
               ;; source, as they are generated as part of build. Upstream
               ;; includes them for people who want to run the software
               ;; directly from source tree.
               '(begin
                  (for-each delete-file (find-files "schemas/gschemas.compiled"))
                  (for-each delete-file
                            (find-files "locale" "\\.mo$"))))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("multi-monitors-add-on@spin83" "share/gnome-shell/extensions/multi-monitors-add-on@spin83"
          #:include-regexp ("\\.css$" "\\.compiled$" "\\.ui$" "\\.js(on)?$" "\\.mo$" "\\.xml$" "\\.png$" "\\.svg$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./multi-monitors-add-on@spin83/schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    (home-page "https://github.com/realh/multi-monitors-add-on")
    (synopsis "Add panels including AppMenu to each additional monitor")
    (description
     "Extension inspired by https://github.com/darkxst/multiple-monitor-panels and rewritten from scratch for gnome-shell version 3.10.4. Adds panels and thumbnails for additional monitors. Settings changes are applied in dynamic fashion, no restart needed.")
    (license license:expat)))

(define-public gnome-shell-extension-vitals
  (package
    (name "gnome-shell-extension-vitals")
    (version "55.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     (string-append
                      "https://github.com/corecoding/vitals"))
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1901a85pqgsass40agf7fs00gk0d27k8w8xfb2y3215kscgs1pf4"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  (for-each delete-file (find-files "schemas/gschemas.compiled"))
                  (for-each delete-file
                            (find-files "locale" "\\.mo$"))))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("." "share/gnome-shell/extensions/Vitals@CoreCoding.com"
          #:include-regexp ("\\.css$" "\\.compiled$" "\\.ui$" "\\.js(on)?$" "\\.mo$" "\\.xml$" "\\.png$" "\\.svg$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    (propagated-inputs (list libgtop adwaita-icon-theme lm-sensors))
    (home-page "")
    (synopsis "A glimpse into your computer's temperature, voltage, fan speed, memory usage and CPU load")
    (description
     "Vitals is a GNOME Shell extension for displaying your computer's temperature, voltage, fan speed, memory usage, processor load, system resources, network speed and storage stats in your GNOME Shell's top menu bar. This is a one stop shop to monitor all of your vital sensors. Vitals uses asynchronous polling to provide a smooth user experience.")
    (license license:expat)))


(define-public gnome-shell-extension-improved-osk
  (package
    (name "gnome-shell-extension-improved-osk")
    (version "8")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     (string-append
                      "https://github.com/nick-shmyrev/improved-osk-gnome-ext"))
                    (commit "63b5629ad08cf7d5d3008aa75c376730b87ba716")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0w0j559bvbm6avg5kn3kpq0mwlyi298rgjwyjf0phqjqizj0mayz"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  (for-each delete-file (find-files "schemas/gschemas.compiled"))
                  (for-each delete-file
                            (find-files "locale" "\\.mo$"))))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("." "share/gnome-shell/extensions/improvedosk@nick-shmyrev.dev"
          #:include-regexp ("\\.css$" "\\.compiled$" "\\.ui$" "\\.js(on)?$" "\\.mo$" "\\.xml$" "\\.png$" "\\.svg$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    ;; need symbola probably
    (propagated-inputs (list font-google-material-design-icons ibus))
    (home-page "https://extensions.gnome.org/extension/4413/improved-osk/")
    (synopsis "Improved On Screen Keyboard for Gnome Shell ")
    (description
     "Makes Gnome's onscreen keyboard more useable.

Features:

    More buttons like CTRL, F-Keys, Arrow Keys...
    Configurable keyboard size (landscape/portrait)
    Toggle auto keyboard popup on touch input
    Works in gnome password modals
    Statusbar indicator to toggle keyboard")
    (license license:expat)))

(define-public gnome-shell-extension-tiling-assistant
  (package
    (name "gnome-shell-extension-tiling-assistant")
    (version "36")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     (string-append
                      "https://github.com/Leleat/Tiling-Assistant"))
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1cii7l66sysials6p713aklij31pq47bzncl1l0ixx29g90p08py"))
              (modules '((guix build utils)))
              (snippet
               ;; Remove pre-compiled settings schemas and translations from
               ;; source, as they are generated as part of build. Upstream
               ;; includes them for people who want to run the software
               ;; directly from source tree.
               '(begin
                  (for-each delete-file (find-files "schemas/gschemas.compiled"))
                  (for-each delete-file
                            (find-files "locale" "\\.mo$"))))))
    (build-system copy-build-system)
    (arguments
     '(#:install-plan
       '(("tiling-assistant@leleat-on-github" "share/gnome-shell/extensions/tiling-assistant@leleat-on-github"
          #:include-regexp ("\\.css$" "\\.compiled$" "\\.ui$" "\\.js(on)?$" "\\.mo$" "\\.xml$" "\\.png$" "\\.svg$")))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'compile-schemas
           (lambda _
             (with-directory-excursion "./tiling-assistant@leleat-on-github/schemas"
               (invoke "glib-compile-schemas" ".")))))))
    (native-inputs
     (list `(,glib "bin") gettext-minimal))
    (home-page "https://github.com/Leleat/Tiling-Assistant")
    (synopsis "GNOME extension which adds a Windows-like snap assist to the
 GNOME desktop environment.")
    (description "")
    (license license:expat)))



(define-public gnome-shell-extension-pop-shell
  (package
    (name "gnome-shell-extension-pop-shell")
    (version "2.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pop-os/shell")
             (commit "4cf9e144abf0c9775d3dabcd6ac35af70a5b96b4")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1afwc7x3any4kqy5ly54y2bsrxqlqr6aivnlifv642r348zwny20"))))
    (build-system gnu-build-system)
    (native-inputs
     (list `(,glib "bin") gettext-minimal ))
    (propagated-inputs (list gjs))
    (arguments
     (list
      #:tests? #f
      #:make-flags #~(list (string-append "XDG_DATA_HOME="
                                          #$output
                                          "/share/"))

       #:phases
       #~(modify-phases %standard-phases
         (delete 'configure))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl2+)))
