(define-module (guix-local packages spotify)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages man)
  #:use-module (gnu packages web)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system qt)
  #:use-module (guix-local packages librespot))

(define-public rust-gtk4-0.4
  (package
    (name "rust-gtk4")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gtk4" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15kkdx6x63gk6w8f7mqg9bvzqgqrmidsvpgb7zf9ks407lm0qky6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.15)
                       ("rust-field-offset" ,rust-field-offset-0.3)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.15)
                       ("rust-gdk4" ,rust-gdk4-0.4)
                       ("rust-gio" ,rust-gio-0.15)
                       ("rust-glib" ,rust-glib-0.15)
                       ("rust-graphene-rs" ,rust-graphene-rs-0.15)
                       ("rust-gsk4" ,rust-gsk4-0.4)
                       ("rust-gtk4-macros" ,rust-gtk4-macros-0.4)
                       ("rust-gtk4-sys" ,rust-gtk4-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pango" ,rust-pango-0.15))
       #:cargo-development-inputs (("rust-gir-format-check" ,rust-gir-format-check-0.1))))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GTK 4 library")
    (description "Rust bindings of the GTK 4 library")
    (license license:expat)))

(define-public rust-libadwaita-sys-0.2
  (package
    (name "rust-libadwaita-sys")
    (version "0.2.0-alpha.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libadwaita-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1izdywvz711lw02nb1wh04bll4i1knfsf31cvcyz9rznjadzjdg8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gdk4-sys" ,rust-gdk4-sys-0.4)
                       ("rust-gio-sys" ,rust-gio-sys-0.15)
                       ("rust-glib-sys" ,rust-glib-sys-0.15)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.15)
                       ("rust-gtk4-sys" ,rust-gtk4-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/")
    (synopsis "FFI bindings for libadwaita")
    (description "FFI bindings for libadwaita")
    (license license:expat)))

(define-public rust-libadwaita-0.2
  (package
    (name "rust-libadwaita")
    (version "0.2.0-alpha.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libadwaita" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ik0r211gppr9q2brbjciyp89han0fyq3lqy3ndn5j5bqwc27ba9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.15)
                       ("rust-gdk4" ,rust-gdk4-0.4)
                       ("rust-gio" ,rust-gio-0.15)
                       ("rust-glib" ,rust-glib-0.15)
                       ("rust-gtk4" ,rust-gtk4-0.4)
                       ("rust-libadwaita-sys" ,rust-libadwaita-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pango" ,rust-pango-0.15))))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/")
    (synopsis "Rust bindings for libadwaita")
    (description "Rust bindings for libadwaita")
  (license license:expat)))




(define-public rust-gtk4-sys-0.4
  (package
    (name "rust-gtk4-sys")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gtk4-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qqgxfbmygsl3xd3qal37cdz4ibfc0j9xxrzv9r7qjv3x9p01j2v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.15)
                       ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.15)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.4)
                       ("rust-gio-sys" ,rust-gio-sys-0.15)
                       ("rust-glib-sys" ,rust-glib-sys-0.15)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.15)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.15)
                       ("rust-gsk4-sys" ,rust-gsk4-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.15)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (home-page "http://gtk-rs.org/")
    (synopsis "FFI bindings of GTK 4")
    (description "FFI bindings of GTK 4")
    (license license:expat)))
(define-public rust-gtk4-macros-0.4
  (package
    (name "rust-gtk4-macros")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gtk4-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "19zp8mfnf30rf3srdibr4fifb78b0kkm6j0ngmyngszl1a9cryzs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
                       ("rust-proc-macro-error" ,rust-proc-macro-error-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quick-xml" ,rust-quick-xml-0.22)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://gtk-rs.org/")
    (synopsis "Macros helpers for GTK 4 bindings")
    (description "Macros helpers for GTK 4 bindings")
    (license license:expat)))
(define-public rust-gsk4-sys-0.4
  (package
    (name "rust-gsk4-sys")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gsk4-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1sizv9dy5ch1nxmfmdb3xm35q10zr7fa4hw6hf650y00yv63kpbs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.15)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.4)
                       ("rust-glib-sys" ,rust-glib-sys-0.15)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.15)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.15)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.15)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (home-page "http://gtk-rs.org/")
    (synopsis "FFI bindings of GSK 4")
    (description "FFI bindings of GSK 4")
    (license license:expat)))
(define-public rust-gsk4-0.4
  (package
    (name "rust-gsk4")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gsk4" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1r0vnrgdpkavxkq67bgixcp72l4vz9dlk5nl72mb701j6c6h5s85"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.15)
                       ("rust-gdk4" ,rust-gdk4-0.4)
                       ("rust-glib" ,rust-glib-0.15)
                       ("rust-graphene-rs" ,rust-graphene-rs-0.15)
                       ("rust-gsk4-sys" ,rust-gsk4-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango" ,rust-pango-0.15))))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GSK 4 library")
    (description "Rust bindings of the GSK 4 library")
    (license license:expat)))
(define-public rust-graphene-sys-0.15
  (package
    (name "rust-graphene-sys")
    (version "0.15.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "graphene-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12h2qcdhvzxhkc75fqkky6rz212wp2yc6mgvk9cxz8bv6g3iysgs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.15)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libgraphene-1.0")
    (description "FFI bindings to libgraphene-1.0")
    (license license:expat)))
(define-public rust-graphene-rs-0.15
  (package
    (name "rust-graphene-rs")
    (version "0.15.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "graphene-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0w2mz098dr8mlz18ssmlnln1x6c3byizqbc9kz4n5nzgpvxzjm3w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib" ,rust-glib-0.15)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.15)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the Graphene library")
    (description "Rust bindings for the Graphene library")
    (license license:expat)))


(define-public rust-gdk4-sys-0.4
  (package
    (name "rust-gdk4-sys")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gdk4-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1wnfv62n9dmpzg9rpy3hj1aldpkkavyans9zzymsw02w9ysdrrzg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.15)
                       ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.15)
                       ("rust-gio-sys" ,rust-gio-sys-0.15)
                       ("rust-glib-sys" ,rust-glib-sys-0.15)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.15)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.15)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (home-page "http://gtk-rs.org/")
    (synopsis "FFI bindings of GDK 4")
    (description "FFI bindings of GDK 4")
    (license license:expat)))

(define-public rust-gdk4-0.4
  (package
    (name "rust-gdk4")
    (version "0.4.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gdk4" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xh8b3ms20xmmp2gkvrfmsljggy0s2avp2nnln2v09iwhk7vgasg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.15)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.15)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.4)
                       ("rust-gio" ,rust-gio-0.15)
                       ("rust-glib" ,rust-glib-0.15)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango" ,rust-pango-0.15))
       #:cargo-development-inputs (("rust-gir-format-check" ,rust-gir-format-check-0.1))))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GDK 4 library")
    (description "Rust bindings of the GDK 4 library")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "protobuf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0zd7ajch8rpwnyv8r5gsd1phgrc7hsh289i4ri5sz3qlclffdnpi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytes" ,rust-bytes-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license (list license:expat license:asl2.0))))

(define-public rust-test-case-macros-2
  (package
    (name "rust-test-case-macros")
    (version "2.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "test-case-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0m1y1f1sbhgckcqlyfdvar65xv3q9wkg9q10371gbi3gvkp6hnf9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-proc-macro-error" ,rust-proc-macro-error-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/frondeus/test-case")
    (synopsis
     "Provides #[test_case(...)] procedural macro attribute for generating parametrized test cases easily")
    (description
     "This package provides #[test_case(...)] procedural macro attribute for
generating parametrized test cases easily")
    (license license:expat)))

(define-public rust-test-case-2
  (package
    (name "rust-test-case")
    (version "2.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "test-case" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1md6czjifaqjcj0zbqy2067kj82nzqllqhfw9av9i2a8x4lskbh7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-regex" ,rust-regex-1)
                       ("rust-test-case-macros" ,rust-test-case-macros-2))))
    (home-page "https://github.com/frondeus/test-case")
    (synopsis
     "Provides #[test_case(...)] procedural macro attribute for generating parametrized test cases easily")
    (description
     "This package provides #[test_case(...)] procedural macro attribute for
generating parametrized test cases easily")
    (license license:expat)))
(define-public rust-sluice-0.5
  (package
    (name "rust-sluice")
    (version "0.5.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sluice" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1d9ywr5039ibgaby8sc72f8fs5lpp8j5y6p3npya4jplxz000x3d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-channel" ,rust-async-channel-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3))))
    (home-page "https://github.com/sagebind/sluice")
    (synopsis
     "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (description
     "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (license license:expat)))
(define-public rust-rustls-ffi-0.8
  (package
    (name "rust-rustls-ffi")
    (version "0.8.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rustls-ffi" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06kqrvm1d5ps9pml26zdd2hm8hh20j6svwvqibpnx7m5rh3jg9cx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-num-enum" ,rust-num-enum-0.5)
                       ("rust-rustls" ,rust-rustls-0.20)
                       ("rust-rustls-pemfile" ,rust-rustls-pemfile-0.2)
                       ("rust-sct" ,rust-sct-0.7)
                       ("rust-webpki" ,rust-webpki-0.22))))
    (home-page "https://github.com/rustls/rustls-ffi")
    (synopsis "C-to-rustls bindings")
    (description "C-to-rustls bindings")
    (license (list license:asl2.0 license:isc license:expat))))
(define-public rust-curl-sys-0.4
  (package
    (name "rust-curl-sys")
    (version "0.4.56+curl-7.83.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "curl-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vyjydzavbbp0qdvzr8zr18mbkfd26pgnjd6ix39xqjdvmly34v0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libnghttp2-sys" ,rust-libnghttp2-sys-0.1)
                       ("rust-libz-sys" ,rust-libz-sys-1)
                       ("rust-openssl-sys" ,rust-openssl-sys-0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-rustls-ffi" ,rust-rustls-ffi-0.8)
                       ("rust-vcpkg" ,rust-vcpkg-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Native bindings to the libcurl library")
    (description "Native bindings to the libcurl library")
    (license license:expat)))
(define-public rust-curl-0.4
  (package
    (name "rust-curl")
    (version "0.4.44")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "curl" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "08hsq6ssy228df56adv2wbgam05f5rw1f2wzs7mhkb678qbx36sh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-curl-sys" ,rust-curl-sys-0.4)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-openssl-probe" ,rust-openssl-probe-0.1)
                       ("rust-openssl-sys" ,rust-openssl-sys-0.9)
                       ("rust-schannel" ,rust-schannel-0.1)
                       ("rust-socket2" ,rust-socket2-0.4)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Rust bindings to libcurl for making HTTP requests")
    (description "Rust bindings to libcurl for making HTTP requests")
    (license license:expat)))

(define-public rust-castaway-0.1
  (package
    (name "rust-castaway")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "castaway" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xhspwy477qy5yg9c3jp713asxckjpx0vfrmz5l7r5zg7naqysd2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/sagebind/castaway")
    (synopsis
     "Safe, zero-cost downcasting for limited compile-time specialization.")
    (description
     "Safe, zero-cost downcasting for limited compile-time specialization.")
    (license license:expat)))
(define-public rust-isahc-1
  (package
    (name "rust-isahc")
    (version "1.7.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "isahc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1scfgyv3dpjbkqa9im25cd12cs6rbd8ygcaw67f3dx41sys08kik"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-async-channel" ,rust-async-channel-1)
                       ("rust-castaway" ,rust-castaway-0.1)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8)
                       ("rust-curl" ,rust-curl-0.4)
                       ("rust-curl-sys" ,rust-curl-sys-0.4)
                       ("rust-encoding-rs" ,rust-encoding-rs-0.8)
                       ("rust-event-listener" ,rust-event-listener-2)
                       ("rust-futures-lite" ,rust-futures-lite-1)
                       ("rust-http" ,rust-http-0.2)
                       ("rust-httpdate" ,rust-httpdate-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mime" ,rust-mime-0.3)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-polling" ,rust-polling-2)
                       ("rust-publicsuffix" ,rust-publicsuffix-2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-slab" ,rust-slab-0.4)
                       ("rust-sluice" ,rust-sluice-0.5)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracing-futures" ,rust-tracing-futures-0.2)
                       ("rust-url" ,rust-url-2)
                       ("rust-waker-fn" ,rust-waker-fn-1))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-flate2" ,rust-flate2-1)
                                   ("rust-indicatif" ,rust-indicatif-0.15)
                                   ("rust-rayon" ,rust-rayon-1)
                                   ("rust-serde-json" ,rust-serde-json-1)
                                   ("rust-static-assertions" ,rust-static-assertions-1)
                                   ("rust-structopt" ,rust-structopt-0.3)
                                   ("rust-tempfile" ,rust-tempfile-3)
                                   ("rust-test-case" ,rust-test-case-2)
                                   ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3))))
    (home-page "https://github.com/sagebind/isahc")
    (synopsis "The practical HTTP client that is fun to use.")
    (description "The practical HTTP client that is fun to use.")
    (license license:expat)))


(define-public spot
  (package
    (name "spot")
    (version "0.3.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/xou816/spot")
         (commit "4f4a20c68b5d11046283a7de282c26efd3682072")))
       (file-name (git-file-name name version))
        (sha256
         (base32 "0prbn8x42xqjni6bz3jb9cdd0hgvfh59ih396s23syspind8sk8c"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (propagated-inputs (list libevdev))
    ;;FIXME this feels hacky 
    (arguments
     `(;;#:skip-build? #t
       ;;#:cargo-build-flags '("--debug")
       #:cargo-inputs
       (("rust-tokio" ,rust-tokio-1)
        ("rust-env-logger" ,rust-env-logger-0.9)
        ("rust-protobuf" ,rust-protobuf-2)
        ("rust-gio" ,rust-gio-0.15)
        ("rust-gtk4" ,rust-gtk4-0.4)
        ("rust-gdk4" ,rust-gdk4-0.4)
        ("rust-gio" ,rust-gio-0.15)
        ("rust-glib" ,rust-glib-0.15)
        ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.15)
        ("rust-libadwaita" ,rust-libadwaita-0.2)
        ("rust-librespot-0.3" ,rust-librespot-0.3)
        ("rust-log" ,rust-log-0.4)
        ("rust-zbus" ,rust-zbus-1);;need 2
        ("rust-zvariant" ,rust-zvariant-2);;need 3
        ("rust-secret-service" ,rust-secret-service-2)
        ("rust-futures" ,rust-futures-0.1)
        ("rust-serde-derive" ,rust-serde-derive-1)
        ("rust-serde-json" ,rust-serde-json-1)
        ("rust-thiserror" ,rust-thiserror-1)
        ("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-form-urlencoded" ,rust-form-urlencoded-1)
        ("rust-regex" ,rust-regex-1)
        ("rust-async-std" ,rust-async-std-1)
        ("rust-isahc" ,rust-isahc-1)
        ("rust-rand" ,rust-rand-0.8)
        ("rust-gettext-rs" ,rust-gettext-rs-0.7)
        ("rust-ref-filter-map" ,rust-ref-filter-map-1)
        ("rust-percent-encoding" ,rust-percent-encoding-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:expat))))


(define-public spotify-qt
  (package
   (name "spotify-qt")
   (version "3.8")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/kraxarn/spotify-qt")
       (commit  (string-append "v" version))))
     (sha256
      (base32 "1aa0smj4m37xgy7rivklxbys7f44spf7n27j3h0qbwfcgbx702s6"))))
   (arguments `(#:tests? #f))
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("extra-cmake-modules" ,extra-cmake-modules)
      ("qttools" ,qttools-5)))
   (inputs (list qtbase-5  libxcb qtsvg-5))
   (build-system qt-build-system)
   (synopsis "Lightweight Spotify client using Qt")
   (description "An unofficial Spotify client using Qt as a
 simpler, lighter alternative to the official client, inspired by
 @code{spotify-tui}. Much like spotify-tui, you need an actual Spotify client
 running, for example @code{spotifyd}, which can be configured from
 within the app. Also, like other clients, controlling music playback
 requires Spotify Premium.")
   (home-page "https://github.com/kraxarn/spotify-qt")
   (license license:expat)))

(define-public rust-spmc-0.2
  (package
    (name "rust-spmc")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "spmc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nhbjc65avbb4nffk6b49spbv7rsmmnrppj2qnx39mhwi57spgiw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/seanmonstar/spmc")
    (synopsis "Simple SPMC channel")
    (description "Simple SPMC channel")
    (license (list license:expat license:asl2.0))))

(define-public rust-pretty-env-logger-0.2
  (package
    (name "rust-pretty-env-logger")
    (version "0.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pretty_env_logger" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0a0c53plsr4abw0y1iyjxs0d64f0a6dn48464a2rp21f0iiix3gd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.11)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-env-logger" ,rust-env-logger-0.5)
                       ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/seanmonstar/pretty-env-logger")
    (synopsis "a visually pretty env_logger")
    (description "a visually pretty env_logger")
    (license (list license:expat license:asl2.0))))

(define-public rust-want-0.0.4
  (package
    (name "rust-want")
    (version "0.0.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "want" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l9mbh4a0r2m3s8nckhy1vz9qm6lxsswlgxpimf4pyjkcyb9spd0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-try-lock" ,rust-try-lock-0.1))))
    (home-page "https://github.com/seanmonstar/want")
    (synopsis "Detect when another Future wants a result.")
    (description "Detect when another Future wants a result.")
    (license license:expat)))

(define-public rust-tokio-service-0.1
  (package
    (name "rust-tokio-service")
    (version "0.1.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-service" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qjinhax0y164kxp887mj8c5ih9829kdrnrb2ramzwg0fz825ni4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1))))
    (home-page "https://github.com/tokio-rs/tokio-service")
    (synopsis "The core `Service` trait for Tokio.
")
    (description "The core `Service` trait for Tokio.")
    (license (list license:expat license:asl2.0))))

(define-public rust-smallvec-0.2
  (package
    (name "rust-smallvec")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "smallvec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "04z0bv5pcnwnvij8kfzw56lnib9mjq8bafp120i7q48yvzbbr32c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/servo/rust-smallvec")
    (synopsis
     "'Small vector' optimization: store up to a small number of items on the stack")
    (description
     "Small vector optimization: store up to a small number of items on the stack")
    (license license:mpl2.0)))

(define-public rust-tokio-proto-0.1
  (package
    (name "rust-tokio-proto")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-proto" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "12833cckniq3y83zjhk2ayv6qpr99d4mj1h3hz266g1mh6p4gfwg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-log" ,rust-log-0.3)
                       ("rust-net2" ,rust-net2-0.2)
                       ("rust-rand" ,rust-rand-0.3)
                       ("rust-slab" ,rust-slab-0.3)
                       ("rust-smallvec" ,rust-smallvec-0.2)
                       ("rust-take" ,rust-take-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-service" ,rust-tokio-service-0.1))))
    (home-page "https://tokio.rs")
    (synopsis
     "A network application framework for rapid development and highly scalable
production deployments of clients and servers.
")
    (description
     "This package provides a network application framework for rapid development and
highly scalable production deployments of clients and servers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-hyper-0.11
  (package
    (name "rust-hyper")
    (version "0.11.27")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hyper" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1msrx9fgiiv7vl4kryn2zgahbqndph5szrgqvm6fjhfk1759199l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-base64" ,rust-base64-0.9)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-futures-cpupool" ,rust-futures-cpupool-0.1)
                       ("rust-http" ,rust-http-0.1)
                       ("rust-httparse" ,rust-httparse-1)
                       ("rust-iovec" ,rust-iovec-0.1)
                       ("rust-language-tags" ,rust-language-tags-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mime" ,rust-mime-0.3)
                       ("rust-net2" ,rust-net2-0.2)
                       ("rust-percent-encoding" ,rust-percent-encoding-1)
                       ("rust-relay" ,rust-relay-0.1)
                       ("rust-time" ,rust-time-0.1)
                       ("rust-tokio-core" ,rust-tokio-core-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1)
                       ("rust-tokio-proto" ,rust-tokio-proto-0.1)
                       ("rust-tokio-service" ,rust-tokio-service-0.1)
                       ("rust-unicase" ,rust-unicase-2)
                       ("rust-want" ,rust-want-0.0.4))
       #:cargo-development-inputs (("rust-num-cpus" ,rust-num-cpus-1)
                                   ("rust-pretty-env-logger" ,rust-pretty-env-logger-0.2)
                                   ("rust-spmc" ,rust-spmc-0.2)
                                   ("rust-url" ,rust-url-1))))
    (home-page "https://hyper.rs")
    (synopsis "A fast and correct HTTP library.")
    (description "This package provides a fast and correct HTTP library.")
    (license license:expat)))

(define-public rust-nalgebra-0.18
  (package
    (name "rust-nalgebra")
    (version "0.18.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "nalgebra" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18i1npny8s45ff67p5qpdwwsn36fp23mal8847fkb32cqgdzvada"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-abomonation" ,rust-abomonation-0.7)
                       ("rust-alga" ,rust-alga-0.9)
                       ("rust-approx" ,rust-approx-0.3)
                       ("rust-generic-array" ,rust-generic-array-0.12)
                       ("rust-matrixmultiply" ,rust-matrixmultiply-0.2)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-num-complex" ,rust-num-complex-0.2)
                       ("rust-num-rational" ,rust-num-rational-0.2)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-pest" ,rust-pest-2)
                       ("rust-pest-derive" ,rust-pest-derive-2)
                       ("rust-quickcheck" ,rust-quickcheck-0.8)
                       ("rust-rand" ,rust-rand-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-typenum" ,rust-typenum-1))
       #:cargo-development-inputs (("rust-rand-xorshift" ,rust-rand-xorshift-0.1)
                                   ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://nalgebra.org")
    (synopsis
     "General-purpose linear algebra library with transformations and statically-sized or dynamically-sized matrices.")
    (description
     "General-purpose linear algebra library with transformations and statically-sized
or dynamically-sized matrices.")
    (license license:bsd-3)))

(define-public rust-synstructure-0.10
  (package
    (name "rust-synstructure")
    (version "0.10.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "synstructure" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0grirdkgh2wl4hf9a3nbiazpgccxgq54kn52ms0xrr6njvgkwd82"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-0.4)
                       ("rust-quote" ,rust-quote-0.6)
                       ("rust-syn" ,rust-syn-0.15)
                       ("rust-unicode-xid" ,rust-unicode-xid-0.1))
       #:cargo-development-inputs (("rust-synstructure-test-traits" ,rust-synstructure-test-traits-0.1))))
    (home-page "https://github.com/mystor/synstructure")
    (synopsis "Helper methods and macros for custom derives")
    (description "Helper methods and macros for custom derives")
    (license license:expat)))

(define-public rust-serde-xml-rs-0.4
  (package
    (name "rust-serde-xml-rs")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "serde-xml-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ykx1xkfd59gf0ijnp93xhpd457xy4zi8xv2hrr0ikvcd6h1pgzh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-xml-rs" ,rust-xml-rs-0.8))
       #:cargo-development-inputs (("rust-docmatic" ,rust-docmatic-0.1)
                                   ("rust-serde-derive" ,rust-serde-derive-1)
                                   ("rust-simple-logger" ,rust-simple-logger-1))))
    (home-page "https://github.com/RReverser/serde-xml-rs")
    (synopsis "xml-rs based deserializer for Serde (compatible with 0.9+)")
    (description "xml-rs based deserializer for Serde (compatible with 0.9+)")
    (license license:expat)))

(define-public rust-enumflags2-derive-0.6
  (package
    (name "rust-enumflags2-derive")
    (version "0.6.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "enumflags2_derive" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kkcwi4n76bi1c16ms00dyk4d393gdf29kpr4k9zsn5z7m7fjvll"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/NieDzejkob/enumflags2")
    (synopsis
     "Do not use directly, use the reexport in the `enumflags2` crate. This allows for better compatibility across versions.")
    (description
     "Do not use directly, use the reexport in the `enumflags2` crate.  This allows
for better compatibility across versions.")
    (license (list license:expat license:asl2.0))))

(define-public rust-enumflags2-0.6
  (package
    (name "rust-enumflags2")
    (version "0.6.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "enumflags2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "182xd6cxxmadx1axnz6x73d12pzgwkc712zq2lxd4z1k48lxij43"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-enumflags2-derive" ,rust-enumflags2-derive-0.6)
                       ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3))))
    (home-page "https://github.com/NieDzejkob/enumflags2")
    (synopsis "Enum-based bit flags")
    (description "Enum-based bit flags")
    (license (list license:expat license:asl2.0))))

(define-public rust-pbkdf2-0.3
  (package
    (name "rust-pbkdf2")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pbkdf2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1na2fmmfcmksz4xk7m0ihl778501c1krx88dcylrand48f506v00"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-base64" ,rust-base64-0.9)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-crypto-mac" ,rust-crypto-mac-0.7)
                       ("rust-hmac" ,rust-hmac-0.7)
                       ("rust-rand" ,rust-rand-0.5)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-sha2" ,rust-sha2-0.8)
                       ("rust-subtle" ,rust-subtle-1))
       #:cargo-development-inputs (("rust-hmac" ,rust-hmac-0.7)
                                   ("rust-sha-1" ,rust-sha-1-0.8)
                                   ("rust-sha2" ,rust-sha2-0.8))))
    (home-page
     "https://github.com/RustCrypto/password-hashes/tree/master/pbkdf2")
    (synopsis "Generic implementation of PBKDF2")
    (description "Generic implementation of PBKDF2")
    (license (list license:expat license:asl2.0))))

(define-public rust-rpassword-3
  (package
    (name "rust-rpassword")
    (version "3.0.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rpassword" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vkifbbs160d7i7wy3kb0vw9mbf3pf470hg8f623rjkzmsyafky3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.2))))
    (home-page "https://github.com/conradkleinespel/rpassword")
    (synopsis "Read passwords in console applications.")
    (description "Read passwords in console applications.")
    (license license:asl2.0)))

(define-public rust-dbus-0.6
  (package
    (name "rust-dbus")
    (version "0.6.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbus" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "068qyxvaam34sjmhjgxz6iikklvylxly7gp6n00yksqydzrz1da8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-libdbus-sys" ,rust-libdbus-sys-0.2))
       #:cargo-development-inputs (("rust-tempdir" ,rust-tempdir-0.3))))
    (home-page "https://github.com/diwic/dbus-rs")
    (synopsis
     "Bindings to D-Bus, which is a bus commonly used on Linux for inter-process communication.")
    (description
     "Bindings to D-Bus, which is a bus commonly used on Linux for inter-process
communication.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tracing-error-0.1
  (package
    (name "rust-tracing-error")
    (version "0.1.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tracing-error" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "092y3357af6058mdw7nmr7sysqdka8b4cyaqz940fl2a7nwc1mxl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.2))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities for enriching errors with `tracing`.
")
    (description "Utilities for enriching errors with `tracing`.")
    (license license:expat)))

(define-public rust-owo-colors-1
  (package
    (name "rust-owo-colors")
    (version "1.4.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "owo-colors" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rybl2lvhaycpkpaq45099idp5ny7nv4sqsafz0cvfqw1wjfy9vz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atty" ,rust-atty-0.2))))
    (home-page "https://github.com/jam1garner/owo-colors")
    (synopsis "Zero-allocation terminal colors that'll make people go owo")
    (description "Zero-allocation terminal colors that'll make people go owo")
    (license license:expat)))

(define-public rust-color-spantrace-0.1
  (package
    (name "rust-color-spantrace")
    (version "0.1.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "color-spantrace" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1lb2li71zvpxp80nck98gcqbqm3dnmp43pnlvm52z9x8livy9vmn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-owo-colors" ,rust-owo-colors-1)
                       ("rust-tracing-core" ,rust-tracing-core-0.1)
                       ("rust-tracing-error" ,rust-tracing-error-0.1))))
    (home-page "https://github.com/yaahc/color-spantrace")
    (synopsis
     "A pretty printer for tracing_error::SpanTrace based on color-backtrace")
    (description
     "This package provides a pretty printer for tracing_error::SpanTrace based on
color-backtrace")
    (license (list license:expat license:asl2.0))))

(define-public rust-color-eyre-0.5
  (package
    (name "rust-color-eyre")
    (version "0.5.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "color-eyre" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1dspj58bk57f9hiqlvbz25rik92i4a95iwa2dl4pg8g8grlqa60z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-backtrace" ,rust-backtrace-0.3)
                       ("rust-color-spantrace" ,rust-color-spantrace-0.1)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-indenter" ,rust-indenter-0.3)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-owo-colors" ,rust-owo-colors-1)
                       ("rust-tracing-error" ,rust-tracing-error-0.1)
                       ("rust-url" ,rust-url-2))
       #:cargo-development-inputs (("rust-ansi-parser" ,rust-ansi-parser-0.6)
                                   ("rust-pretty-assertions" ,rust-pretty-assertions-0.6)
                                   ("rust-thiserror" ,rust-thiserror-1)
                                   ("rust-tracing" ,rust-tracing-0.1)
                                   ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.2)
                                   ("rust-wasm-bindgen-test" ,rust-wasm-bindgen-test-0.3))))
    (home-page "https://github.com/yaahc/color-eyre")
    (synopsis
     "An error report handler for panics and eyre::Reports for colorful, consistent, and well formatted error reports for all kinds of errors.")
    (description
     "An error report handler for panics and eyre::Reports for colorful, consistent,
and well formatted error reports for all kinds of errors.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pasts-0.4
  (package
    (name "rust-pasts")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pasts" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11rdczdhpazclhkbbjafv5nd9ybll9a110crhh67si0p5rdc6mz7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/AldaronLau/pasts/blob/stable/CHANGELOG.md")
    (synopsis "Minimal and simpler alternative to the futures crate.")
    (description "Minimal and simpler alternative to the futures crate.")
    (license (list license:asl2.0 license:zlib))))

(define-public rust-cala-core-0.1
  (package
    (name "rust-cala-core")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cala_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17939zm80lxi0mqsvi98wv2hjasbbh132j5i2m201x30j8dkx4wx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pasts" ,rust-pasts-0.4)
         ("rust-stdweb" ,rust-stdweb-0.4)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "https://github.com/libcala/cala_core/blob/master/CHANGELOG.md")
    (synopsis "Low-level platform glue for Cala")
    (description "Low-level platform glue for Cala")
    (license (list license:asl2.0 license:zlib))))

(define-public rust-whoami-0.9
  (package
    (name "rust-whoami")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "whoami" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "012mw2q72gpmf354yw2qc5w105ziac75shpqp1f62x4hnqx7g13q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cala-core" ,rust-cala-core-0.1))))
    (home-page "https://github.com/libcala/whoami/blob/main/CHANGELOG.md")
    (synopsis "Retrieve the current user and environment.")
    (description "Retrieve the current user and environment.")
    (license (list license:expat license:boost1.0))))

(define-public rust-tokio-signal-0.1
  (package
    (name "rust-tokio-signal")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-signal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zlz5dwbh8lr0a9zar9459wcbfciqcg74wyiab7hb6hg4dinix78"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mio" ,rust-mio-0.6)
         ("rust-mio-uds" ,rust-mio-uds-0.6)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/tokio-rs/tokio")
    (synopsis
      "An implementation of an asynchronous Unix signal handling backed futures.
")
    (description
      "An implementation of an asynchronous Unix signal handling backed futures.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rspotify-0.8
  (package
    (name "rust-rspotify")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rspotify" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14xdic0zhalmvk32y1ffanvgwdqki91qw549kj6mqcdirxka2959"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.10)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-derive-builder" ,rust-derive-builder-0.7)
         ("rust-dotenv" ,rust-dotenv-0.13)
         ("rust-env-logger" ,rust-env-logger-0.6)
         ("rust-failure" ,rust-failure-0.1)
         ("rust-itertools" ,rust-itertools-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-percent-encoding" ,rust-percent-encoding-1)
         ("rust-rand" ,rust-rand-0.6)
         ("rust-random" ,rust-random-0.12)
         ("rust-reqwest" ,rust-reqwest-0.10)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-url" ,rust-url-1)
         ("rust-webbrowser" ,rust-webbrowser-0.5))))
    (home-page "https://github.com/ramsayleung/rspotify")
    (synopsis "Spotify API wrapper")
    (description "Spotify API wrapper")
    (license license:expat)))

(define-public rust-zerocopy-derive-0.1
  (package
    (name "rust-zerocopy-derive")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "198g500f9j1z1crq2j6m60jmk0kkmi1x61b4i9p04906rmz4d45h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-0.4)
         ("rust-syn" ,rust-syn-0.15)
         ("rust-synstructure" ,rust-synstructure-0.10))))
    (home-page
      "https://fuchsia.googlesource.com/fuchsia/+/HEAD/src/lib/zerocopy/zerocopy-derive")
    (synopsis "Custom derive for traits from the zerocopy crate")
    (description "Custom derive for traits from the zerocopy crate")
    (license license:bsd-3)))

(define-public rust-zerocopy-0.2
  (package
    (name "rust-zerocopy")
    (version "0.2.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mj0scnsm2skj3gh3sk8qdn73mj3raw7ky03z5ks3m0gz0qrnawr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-zerocopy-derive" ,rust-zerocopy-derive-0.1))))
    (home-page
      "https://fuchsia.googlesource.com/fuchsia/+/HEAD/src/lib/zerocopy")
    (synopsis "Utilities for zero-copy parsing and serialization")
    (description "Utilities for zero-copy parsing and serialization")
    (license license:bsd-3)))

(define-public rust-unidiff-0.3
  (package
    (name "rust-unidiff")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unidiff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b13vhp2x7jlvmkm44h5niqcxklyrmz6afmppvykp4zimhcjg9nq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/messense/unidiff-rs")
    (synopsis "Unified diff parsing/metadata extraction library for Rust")
    (description "Unified diff parsing/metadata extraction library for Rust")
    (license license:expat)))

(define-public rust-proc-macro2-0.3
  (package
    (name "rust-proc-macro2")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m0ksg6hbm46zblq0dpkwrg3n1h7n90yq1zcgwc6vpbfmr9pr6bp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-xid" ,rust-unicode-xid-0.1))))
    (home-page "https://github.com/dtolnay/proc-macro2")
    (synopsis
      "A ute implementation of the compiler's `proc_macro` API to decouple
token-based libraries from the procedural macro use case.
")
    (description
      "This package provides a substitute implementation of the compiler's `proc_macro`
API to decouple token-based libraries from the procedural macro use case.")
    (license (list license:expat license:asl2.0))))

(define-public rust-bindgen-0.42
  (package
    (name "rust-bindgen")
    (version "0.42.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bindgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p14hgi90wd4zbd8j1s4xrxf770cx0s6a2d32fg9ypmzpb69kwg0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-cexpr" ,rust-cexpr-0.3)
         ("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-clang-sys" ,rust-clang-sys-0.26)
         ("rust-clap" ,rust-clap-2)
         ("rust-env-logger" ,rust-env-logger-0.5)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-0.3)
         ("rust-quote" ,rust-quote-0.5)
         ("rust-regex" ,rust-regex-1)
         ("rust-which" ,rust-which-1))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-sdl2-sys-0.32
  (package
    (name "rust-sdl2-sys")
    (version "0.32.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sdl2-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11kz2bqkpcywpyd5hyqflbszpgdmh64zxb61wibpsabx0wji3rrl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.42)
         ("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-cmake" ,rust-cmake-0.1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-unidiff" ,rust-unidiff-0.3))))
    (home-page "https://github.com/rust-sdl2/rust-sdl2")
    (synopsis "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (description "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (license license:expat)))

(define-public rust-c-vec-1
  (package
    (name "rust-c-vec")
    (version "1.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "c_vec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c3wgb15h97k6lzfm9qgkwk35ij2ad7w8fb5rbqvalyf3n8ii8zq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/GuillaumeGomez/c_vec-rs.git")
    (synopsis "Structures to wrap C arrays")
    (description "Structures to wrap C arrays")
    (license (list license:asl2.0 license:expat))))

(define-public rust-sdl2-0.32
  (package
    (name "rust-sdl2")
    (version "0.32.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sdl2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0yyx7sl08y241ddyyfkk9ysxbxllfdpwny6s37vza0z365ra0lfh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-c-vec" ,rust-c-vec-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num" ,rust-num-0.1)
         ("rust-rand" ,rust-rand-0.6)
         ("rust-sdl2-sys" ,rust-sdl2-sys-0.32))))
    (home-page "https://github.com/Rust-SDL2/rust-sdl2")
    (synopsis "SDL2 bindings for Rust")
    (description "SDL2 bindings for Rust")
    (license license:expat)))

(define-public rust-slice-deque-0.3
  (package
    (name "rust-slice-deque")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slice-deque" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "098gvqjw52qw4gac567c9hx3y6hw9al7hjqb5mnvmvydh3i6xvri"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gnzlbg/slice_deque")
    (synopsis "A double-ended queue that Deref's into a slice.")
    (description
      "This package provides a double-ended queue that Deref's into a slice.")
    (license (list license:expat license:asl2.0))))

(define-public rust-minimp3-sys-0.3
  (package
    (name "rust-minimp3-sys")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimp3-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "144vmf3s89kad0smjprzigcp2c9r5dm95n4ydilrbp399irp6772"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-minimp3-0.3
  (package
    (name "rust-minimp3")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimp3" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mfj2vg9vx0gvn5qhlsyqifccfynvnxij21mnavgilxzl3vczq6w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-minimp3-sys" ,rust-minimp3-sys-0.3)
         ("rust-slice-deque" ,rust-slice-deque-0.3))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-hound-3
  (package
    (name "rust-hound")
    (version "3.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hound" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jbm25p2nc8758dnfjan1yk7hz2i85y89nrbai14zzxfrsr4n5la"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/hound")
    (synopsis "A wav encoding and decoding library")
    (description "This package provides a wav encoding and decoding library")
    (license license:asl2.0)))

(define-public rust-claxon-0.4
  (package
    (name "rust-claxon")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "claxon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/claxon#readme")
    (synopsis "A FLAC decoding library")
    (description "This package provides a FLAC decoding library")
    (license license:asl2.0)))

(define-public rust-rodio-0.9
  (package
    (name "rust-rodio")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rodio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b9s1w55l3zx2ys38yl0gp4k929h065wfl5mlx3x2rjf4ldrc3sx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-claxon" ,rust-claxon-0.4)
         ("rust-cpal" ,rust-cpal-0.8)
         ("rust-hound" ,rust-hound-3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lewton" ,rust-lewton-0.9)
         ("rust-minimp3" ,rust-minimp3-0.3)
         ("rust-nalgebra" ,rust-nalgebra-0.18))))
    (home-page "https://github.com/RustAudio/rodio")
    (synopsis "Audio playback library")
    (description "Audio playback library")
    (license (list license:expat license:asl2.0))))

(define-public rust-portaudio-sys-0.1
  (package
    (name "rust-portaudio-sys")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "portaudio-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xdpywirpr1kqkbak7hnny62gmsc93qgc3ij3j2zskrvjpxa952i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "Bindings to PortAudio")
    (description "Bindings to PortAudio")
    (license license:expat)))

(define-public rust-portaudio-rs-0.3
  (package
    (name "rust-portaudio-rs")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "portaudio-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qnmc7amk0fzbcs985ixv0k4955f0fmpkhrl9ps9pk3cz7pvbdnd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-portaudio-sys" ,rust-portaudio-sys-0.1))))
    (home-page "")
    (synopsis "PortAudio bindings for Rust")
    (description "PortAudio bindings for Rust")
    (license license:expat)))

(define-public rust-linear-map-1
  (package
    (name "rust-linear-map")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "linear-map" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vh3sczl4xb5asdlpafdf3y4g9bp63fgs8y2a2sjgmcsn7v21bmz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1) ("rust-serde-test" ,rust-serde-test-1))))
    (home-page "https://github.com/contain-rs/linear-map")
    (synopsis "A map implemented by searching linearly in a vector.")
    (description
      "This package provides a map implemented by searching linearly in a vector.")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-metadata-0.1
  (package
    (name "rust-librespot-metadata")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-metadata" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13dswphpaayccdrvxkj86cfnpv3rj4gl67711gp1qb5j8q7d2hvn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-librespot-core" ,rust-librespot-core-0.1)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.1)
         ("rust-linear-map" ,rust-linear-map-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The metadata logic for librespot")
    (description "The metadata logic for librespot")
    (license license:expat)))

(define-public rust-libpulse-sys-0.0.0
  (package
    (name "rust-libpulse-sys")
    (version "0.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libpulse-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b74hfq24jzqycfs0ywwvzlkfxvc7r2z8p323c6510zqz831pccv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis "FFI bindings for the PulseAudio libpulse system library.")
    (description "FFI bindings for the PulseAudio libpulse system library.")
    (license #f)))

(define-public rust-jack-sys-0.2
  (package
    (name "rust-jack-sys")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jack-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h9c9za19nyr1prx77gkia18ia93f73lpyjdiyrvmhhbs79g54bv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libloading" ,rust-libloading-0.6)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/RustAudio/rust-jack/tree/main/jack-sys")
    (synopsis "Low-level binding to the JACK audio API.")
    (description "Low-level binding to the JACK audio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-0.5
  (package
    (name "rust-jack")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jack" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pr2fkjh181b6qjx940vp8hamcadq21p0l7z0nhp8nif5rczq58y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-0.7)
         ("rust-jack-sys" ,rust-jack-sys-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK.")
    (description "Real time audio and midi with JACK.")
    (license license:expat)))

(define-public rust-gstreamer-base-0.15
  (package
    (name "rust-gstreamer-base")
    (version "0.15.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-base" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04hwa85j3w959i025il908bvsx6dyiawkmc0w45hn9kcrisjyma2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-glib" ,rust-glib-0.9)
         ("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gobject-sys" ,rust-gobject-sys-0.9)
         ("rust-gstreamer" ,rust-gstreamer-0.15)
         ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.8)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rustdoc-stripper" ,rust-rustdoc-stripper-0.1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer Base library")
    (description "Rust bindings for GStreamer Base library")
    (license (list license:expat license:asl2.0))))

(define-public rust-gstreamer-base-sys-0.8
  (package
    (name "rust-gstreamer-base-sys")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-base-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v9v09jqjrwz87c4r7za3yb6g7had112d8zwjdjmhg2b2x94yf5s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gobject-sys" ,rust-gobject-sys-0.9)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstbase-1.0")
    (description "FFI bindings to libgstbase-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-sys-0.8
  (package
    (name "rust-gstreamer-app-sys")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-app-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rw1sjbjhlsp31rrkm6l505hsxbzni323dhsfrfwlfy2abhrr1mz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.8)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstapp-1.0")
    (description "FFI bindings to libgstapp-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-0.15
  (package
    (name "rust-gstreamer-app")
    (version "0.15.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-app" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1afnc6kvj7cpiwafdfjn0zfj37nhwbvzjp4n3qgdsnigskl895vq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-glib" ,rust-glib-0.9)
         ("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gobject-sys" ,rust-gobject-sys-0.9)
         ("rust-gstreamer" ,rust-gstreamer-0.15)
         ("rust-gstreamer-app-sys" ,rust-gstreamer-app-sys-0.8)
         ("rust-gstreamer-base" ,rust-gstreamer-base-0.15)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rustdoc-stripper" ,rust-rustdoc-stripper-0.1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer App library")
    (description "Rust bindings for GStreamer App library")
    (license (list license:expat license:asl2.0))))

(define-public rust-muldiv-0.2
  (package
    (name "rust-muldiv")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "muldiv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "014jlry2l2ph56mp8knw65637hh49q7fmrraim2bx9vz0a638684"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/sdroege/rust-muldiv")
    (synopsis
      "Provides a trait for numeric types to perform combined multiplication and
division with overflow protection
")
    (description
      "This package provides a trait for numeric types to perform combined
multiplication and division with overflow protection")
    (license license:expat)))

(define-public rust-gstreamer-sys-0.8
  (package
    (name "rust-gstreamer-sys")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nsk802vlcyi9p93sg60wv8fzb2mq7j52lfdda4va2kxp40xl60x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gobject-sys" ,rust-gobject-sys-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstreamer-1.0")
    (description "FFI bindings to libgstreamer-1.0")
    (license license:expat)))

(define-public rust-gstreamer-0.15
  (package
    (name "rust-gstreamer")
    (version "0.15.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qx1fhr9ajms0128ixmi2ncr35llwppdb0z7ximw2vnd2jhn91nf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-glib" ,rust-glib-0.9)
         ("rust-glib-sys" ,rust-glib-sys-0.9)
         ("rust-gobject-sys" ,rust-gobject-sys-0.9)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-muldiv" ,rust-muldiv-0.2)
         ("rust-num-rational" ,rust-num-rational-0.2)
         ("rust-paste" ,rust-paste-0.1)
         ("rust-rustdoc-stripper" ,rust-rustdoc-stripper-0.1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-bytes" ,rust-serde-bytes-0.11)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer")
    (description "Rust bindings for GStreamer")
    (license (list license:expat license:asl2.0))))

(define-public rust-stdweb-0.1
  (package
    (name "rust-stdweb")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gjk7ch31a3kgdc39kj4zqinf10yqaf717wanh9kwwbbwg430m7g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clippy" ,rust-clippy-0.0)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/koute/stdweb")
    (synopsis "A standard library for the client-side Web")
    (description
      "This package provides a standard library for the client-side Web")
    (license (list license:expat license:asl2.0))))

(define-public rust-bindgen-0.56
  (package
    (name "rust-bindgen")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bindgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fajmgk2064ca1z9iq1jjkji63qwwz38z3d67kv6xdy0xgdpk8rd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-cexpr" ,rust-cexpr-0.4)
         ("rust-clang-sys" ,rust-clang-sys-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-env-logger" ,rust-env-logger-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lazycell" ,rust-lazycell-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-shlex" ,rust-shlex-0.1)
         ("rust-which" ,rust-which-3))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-coreaudio-sys-0.2
  (package
    (name "rust-coreaudio-sys")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coreaudio-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "12r4icmi931jp6dvaf22499r8fqnq7ldy4n0ckq1b35xknjpjina"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.56))))
    (home-page "https://github.com/RustAudio/coreaudio-sys")
    (synopsis
      "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (description
      "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (license license:expat)))

(define-public rust-coreaudio-rs-0.9
  (package
    (name "rust-coreaudio-rs")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coreaudio-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14g4yqsbhif2bqdk4qk0lixfy78gl1p8lrl122qyklysclcpcagj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-coreaudio-sys" ,rust-coreaudio-sys-0.2))))
    (home-page "https://github.com/RustAudio/coreaudio-rs")
    (synopsis "A friendly rust interface for Apple's CoreAudio API.")
    (description
      "This package provides a friendly rust interface for Apple's CoreAudio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-core-foundation-sys-0.5
  (package
    (name "rust-core-foundation-sys")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1yiyi30bnlnh29i21gp5f411b4qaj05vc8zp8j1y9b0khqg2fv3i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/servo/core-foundation-rs")
    (synopsis "Bindings to Core Foundation for macOS")
    (description "Bindings to Core Foundation for macOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpal-0.8
  (package
    (name "rust-cpal")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17l298jyp4lanl0igxp30m6xnv84gacvdbp3ylrv5c9ncpny32nm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa-sys" ,rust-alsa-sys-0.1)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.5)
         ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.9)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-stdweb" ,rust-stdweb-0.1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/rustaudio/cpal")
    (synopsis "Low-level cross-platform audio I/O library in pure Rust.")
    (description "Low-level cross-platform audio I/O library in pure Rust.")
    (license license:asl2.0)))

(define-public rust-nix-0.9
  (package
    (name "rust-nix")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nix" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cpvygha6bak7apkp9cm5snmld3lnm26crlxavl7pv4q07mszid2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-0.9)
         ("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-void" ,rust-void-1))))
    (home-page "https://github.com/nix-rust/nix")
    (synopsis "Rust friendly bindings to *nix APIs")
    (description "Rust friendly bindings to *nix APIs")
    (license license:expat)))

(define-public rust-alsa-0.2
  (package
    (name "rust-alsa")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cpb9bz4r4x564gp0yclb1589bab7ghsxjcvvv2l2c5jr3mx985l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa-sys" ,rust-alsa-sys-0.1)
         ("rust-bitflags" ,rust-bitflags-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.9))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-librespot-playback-0.1
  (package
    (name "rust-librespot-playback")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-playback" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mpczkj967wv51srcjlqaz3r9b2lyqdmn62cd4n1bxgcidhwlj8j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa" ,rust-alsa-0.2)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-cpal" ,rust-cpal-0.8)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-glib" ,rust-glib-0.9)
         ("rust-gstreamer" ,rust-gstreamer-0.15)
         ("rust-gstreamer-app" ,rust-gstreamer-app-0.15)
         ("rust-jack" ,rust-jack-0.5)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libpulse-sys" ,rust-libpulse-sys-0.0.0)
         ("rust-librespot-audio" ,rust-librespot-audio-0.1)
         ("rust-librespot-core" ,rust-librespot-core-0.1)
         ("rust-librespot-metadata" ,rust-librespot-metadata-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-portaudio-rs" ,rust-portaudio-rs-0.3)
         ("rust-rodio" ,rust-rodio-0.9)
         ("rust-sdl2" ,rust-sdl2-0.32)
         ("rust-shell-words" ,rust-shell-words-0.1)
         ("rust-zerocopy" ,rust-zerocopy-0.2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The audio playback logic for librespot")
    (description "The audio playback logic for librespot")
    (license license:expat)))

(define-public rust-if-addrs-sys-0.3
  (package
    (name "rust-if-addrs-sys")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if-addrs-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1skrzs79rafv185064p44r0k1va9ig4bfnpbwlvyhxh4g3fvjx6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1) ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "if_addrs sys crate")
    (description "if_addrs sys crate")
    (license (list license:expat license:bsd-3))))

(define-public rust-if-addrs-0.6
  (package
    (name "rust-if-addrs")
    (version "0.6.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if-addrs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pkkkwm9znn07xq9s6glf8lxzn2rdxvy8kwkw6czrw64ywhy8wr2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-if-addrs-sys" ,rust-if-addrs-sys-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "Return interface IP addresses on Posix and windows systems")
    (description "Return interface IP addresses on Posix and windows systems")
    (license (list license:expat license:bsd-3))))

(define-public rust-libmdns-0.2
  (package
    (name "rust-libmdns")
    (version "0.2.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libmdns" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0kirdfq8i9r5irnxrhsnhc0pqli4kdqar0n47dim6v3kfk0q51ax"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-hostname" ,rust-hostname-0.3)
         ("rust-if-addrs" ,rust-if-addrs-0.6)
         ("rust-log" ,rust-log-0.4)
         ("rust-multimap" ,rust-multimap-0.8)
         ("rust-net2" ,rust-net2-0.2)
         ("rust-quick-error" ,rust-quick-error-1)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-tokio-core" ,rust-tokio-core-0.1))))
    (home-page "https://github.com/librespot-org/libmdns")
    (synopsis
      "mDNS Responder library for building discoverable LAN services in Rust")
    (description
      "mDNS Responder library for building discoverable LAN services in Rust")
    (license license:expat)))

(define-public rust-dns-sd-0.1
  (package
    (name "rust-dns-sd")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dns-sd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11r0jymjshfnn3sh2nqjhrikk4r5rr1g36sip9iqy8i0xafm0j6p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/plietar/rust-dns-sd")
    (synopsis "Rust binding for dns-sd")
    (description "Rust binding for dns-sd")
    (license license:expat)))

(define-public rust-block-modes-0.3
  (package
    (name "rust-block-modes")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "block-modes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0af562hvwgbvn38npmrw5rybvma819rvb7wh6avzsfay14889aii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-block-cipher-trait" ,rust-block-cipher-trait-0.6)
         ("rust-block-padding" ,rust-block-padding-0.1))))
    (home-page "")
    (synopsis
      "This crate is deprecated. Use crates from https://github.com/RustCrypto/block-modes instead.")
    (description
      "This crate is deprecated.  Use crates from
https://github.com/RustCrypto/block-modes instead.")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-connect-0.1
  (package
    (name "rust-librespot-connect")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-connect" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r82z5ggs5rsih7kch5kpdyb0ajjb8h88fkg97r5na6xy23mmapx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-ctr" ,rust-aes-ctr-0.3)
         ("rust-base64" ,rust-base64-0.10)
         ("rust-block-modes" ,rust-block-modes-0.3)
         ("rust-dns-sd" ,rust-dns-sd-0.1)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-hmac" ,rust-hmac-0.7)
         ("rust-hyper" ,rust-hyper-0.11)
         ("rust-libmdns" ,rust-libmdns-0.2)
         ("rust-librespot-core" ,rust-librespot-core-0.1)
         ("rust-librespot-playback" ,rust-librespot-playback-0.1)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.2)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha-1" ,rust-sha-1-0.8)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-url" ,rust-url-1))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The discovery and Spotify Connect logic for librespot")
    (description "The discovery and Spotify Connect logic for librespot")
    (license license:expat)))

(define-public rust-vorbisfile-sys-0.0.8
  (package
    (name "rust-vorbisfile-sys")
    (version "0.0.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vorbisfile-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1la2j2zbzdjd93byz21ij58c540bfn1r9pi0bssrjimcw7bhchsg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gcc" ,rust-gcc-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-vorbis-sys" ,rust-vorbis-sys-0.1))))
    (home-page "")
    (synopsis "FFI for the vorbisfile library")
    (description "FFI for the vorbisfile library")
    (license license:expat)))

(define-public rust-vorbis-sys-0.1
  (package
    (name "rust-vorbis-sys")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vorbis-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zgv7lwa4b2z091g25h83zil8bawk4frc1f0ril5xa31agpxd7mx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "FFI for the libvorbis library")
    (description "FFI for the libvorbis library")
    (license license:expat)))

(define-public rust-vorbis-0.0.14
  (package
    (name "rust-vorbis")
    (version "0.0.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vorbis" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xn7diq8qz2zmsmwzg3rcsxmpmm2gj7wgnl2gdan0lq7ax21k2jy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
         ("rust-vorbis-sys" ,rust-vorbis-sys-0.1)
         ("rust-vorbisfile-sys" ,rust-vorbisfile-sys-0.0.8))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "High-level bindings for the official libvorbis library.")
    (description "High-level bindings for the official libvorbis library.")
    (license license:asl2.0)))

(define-public rust-ogg-sys-0.0.9
  (package
    (name "rust-ogg-sys")
    (version "0.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ogg-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cpx6n5ndh2d59g43l6rj3myzi5jsc0n6rldpx0impqp5qbqqnx9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gcc" ,rust-gcc-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/tomaka/ogg-sys")
    (synopsis "FFI for libogg, the media container.")
    (description "FFI for libogg, the media container.")
    (license license:expat)))

(define-public rust-librespot-tremor-0.2
  (package
    (name "rust-librespot-tremor")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-tremor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zmld16zawvn7ayrf318lwdr2d7awn4bk9s0d6kpim0mz6zjbxcp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "Rust bindings to tremor")
    (description "Rust bindings to tremor")
    (license license:expat)))

(define-public rust-shannon-0.2
  (package
    (name "rust-shannon")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "shannon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qa52zs4y1i87ysr11g9p6shpdagl14bb340gfm6rd97jhfb99by"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "Shannon cipher implementation")
    (description "Shannon cipher implementation")
    (license license:expat)))

(define-public rust-protobuf-codegen-pure-2
  (package
    (name "rust-protobuf-codegen-pure")
    (version "2.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen-pure" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0h34gfqlb7bqmgqv1mfgy5wk35z5r2h5ki3p3pdcmw1vqzmly6id"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen" ,rust-protobuf-codegen-2))))
    (home-page
      "https://github.com/stepancheg/rust-protobuf/tree/master/protobuf-codegen-pure/")
    (synopsis
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP
")
    (description
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP")
    (license license:expat)))

(define-public rust-protobuf-codegen-2
  (package
    (name "rust-protobuf-codegen")
    (version "2.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "031bx325lsgcx7wc76vc2cqph6q0b34jgc8nz0g2rkwcfnx3n4fy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e. g. from `build.rs`) and `protoc-gen-rust` binary.
")
    (description
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e.  g.  from `build.rs`) and
`protoc-gen-rust` binary.")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11bl8hf522s9mbkckivnn9n8s3ss4g41w6jmfdsswmr5adqd71lf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license license:expat)))

(define-public rust-librespot-protocol-0.1
  (package
    (name "rust-librespot-protocol")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-protocol" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1n8gr8g4yrjp6i0rbsm7vpq00x9yh3lbjkzy1m5d8sgkkss048yl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glob" ,rust-glob-0.3)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen" ,rust-protobuf-codegen-2)
         ("rust-protobuf-codegen-pure" ,rust-protobuf-codegen-pure-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The protobuf logic for communicating with Spotify servers")
    (description "The protobuf logic for communicating with Spotify servers")
    (license license:expat)))

(define-public rust-tokio-tls-0.1
  (package
    (name "rust-tokio-tls")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-tls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04yrdscn8m9qza8ms09pqipbmj6x2q64jgm5n3ipy4b0wl24nbvp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-native-tls" ,rust-native-tls-0.1)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-tokio-proto" ,rust-tokio-proto-0.1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Deprecated in favor of `tokio-naitve-tls`.

An implementation of TLS/SSL streams for Tokio giving an implementation of TLS
for nonblocking I/O streams.
")
    (description
      "Deprecated in favor of `tokio-naitve-tls`.

An implementation of TLS/SSL streams for Tokio giving an implementation of TLS
for nonblocking I/O streams.")
    (license (list license:expat license:asl2.0))))

(define-public rust-hyper-tls-0.1
  (package
    (name "rust-hyper-tls")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-tls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y13a98grzvgza9wpql0gmghwhp48jzxn5dk1a26ac4da5gbvcgz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-hyper" ,rust-hyper-0.11)
         ("rust-native-tls" ,rust-native-tls-0.1)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-tokio-service" ,rust-tokio-service-0.1)
         ("rust-tokio-tls" ,rust-tokio-tls-0.1))))
    (home-page "https://hyper.rs")
    (synopsis "Default TLS implementation for use with hyper")
    (description "Default TLS implementation for use with hyper")
    (license (list license:expat license:asl2.0))))

(define-public rust-hyper-proxy-0.4
  (package
    (name "rust-hyper-proxy")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-proxy" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kmy7xybj9sdw2162bzhcxap93syq89d4za7dqg4hzklw9fr5w24"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-0.4)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-hyper" ,rust-hyper-0.11)
         ("rust-hyper-tls" ,rust-hyper-tls-0.1)
         ("rust-native-tls" ,rust-native-tls-0.1)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-tokio-tls" ,rust-tokio-tls-0.1))))
    (home-page "https://github.com/tafia/hyper-proxy")
    (synopsis "A proxy connector for Hyper-based applications")
    (description
      "This package provides a proxy connector for Hyper-based applications")
    (license license:expat)))

(define-public rust-librespot-core-0.1
  (package
    (name "rust-librespot-core")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02mzh76xx014bvffbzqnybr5s8c7ycyphg1qn71c6lhiyvqay7i7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes" ,rust-aes-0.3)
         ("rust-base64" ,rust-base64-0.10)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-0.4)
         ("rust-error-chain" ,rust-error-chain-0.12)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-hmac" ,rust-hmac-0.7)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-hyper" ,rust-hyper-0.11)
         ("rust-hyper-proxy" ,rust-hyper-proxy-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.2)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-pbkdf2" ,rust-pbkdf2-0.3)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha-1" ,rust-sha-1-0.8)
         ("rust-shannon" ,rust-shannon-0.2)
         ("rust-tokio-codec" ,rust-tokio-codec-0.1)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-url" ,rust-url-1)
         ("rust-uuid" ,rust-uuid-0.7)
         ("rust-vergen" ,rust-vergen-3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The core functionality provided by librespot")
    (description "The core functionality provided by librespot")
    (license license:expat)))

(define-public rust-ogg-0.7
  (package
    (name "rust-ogg")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ogg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ldnq9hccrsaqyzp6yb6w2nn1mpd4wd5fqsckmrf3ybsa71p3r8k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-0.4)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/ogg")
    (synopsis "Ogg container decoder and encoder written in pure Rust")
    (description "Ogg container decoder and encoder written in pure Rust")
    (license license:bsd-3)))

(define-public rust-lewton-0.9
  (package
    (name "rust-lewton")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lewton" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l4bc88cpr8p94dfycykn8gajg20kp611kx159fc8dkh64d2qm4d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-ogg" ,rust-ogg-0.7)
         ("rust-smallvec" ,rust-smallvec-0.6)
         ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/lewton")
    (synopsis "Pure Rust vorbis decoder")
    (description "Pure Rust vorbis decoder")
    (license (list license:expat license:asl2.0))))

(define-public rust-ctr-0.3
  (package
    (name "rust-ctr")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ctr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dk7ik2pjr10q8b3bm64af10k0x0xkl6y02xs9kxz4a4f28xcb02"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-block-cipher-trait" ,rust-block-cipher-trait-0.6)
         ("rust-stream-cipher" ,rust-stream-cipher-0.3))))
    (home-page "https://github.com/RustCrypto/block-modes")
    (synopsis "CTR block modes of operation")
    (description "CTR block modes of operation")
    (license (list license:expat license:asl2.0))))

(define-public rust-aes-ctr-0.3
  (package
    (name "rust-aes-ctr")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-ctr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vnib0h0ala3dg07iks60vwi13jgar33j3wc3l6sxgm3ir2v1rfj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-soft" ,rust-aes-soft-0.3)
         ("rust-aesni" ,rust-aesni-0.6)
         ("rust-ctr" ,rust-ctr-0.3)
         ("rust-stream-cipher" ,rust-stream-cipher-0.3))))
    (home-page "https://github.com/RustCrypto/block-ciphers/tree/master/aes")
    (synopsis "DEPRECATED: replaced by the `aes` crate")
    (description "DEPRECATED: replaced by the `aes` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-audio-0.1
  (package
    (name "rust-librespot-audio")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-audio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kpk83m67ahb65q4h1f2b0k4pa1rdlj73gxfmpzdrn4c2m0icr4l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-ctr" ,rust-aes-ctr-0.3)
         ("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-0.4)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-lewton" ,rust-lewton-0.9)
         ("rust-librespot-core" ,rust-librespot-core-0.1)
         ("rust-librespot-tremor" ,rust-librespot-tremor-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.2)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-vorbis" ,rust-vorbis-0.0.14))))
    (home-page "")
    (synopsis "The audio fetching and processing logic for librespot")
    (description "The audio fetching and processing logic for librespot")
    (license license:expat)))

(define-public rust-librespot-0.1
  (package
    (name "rust-librespot")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rn53hamcbknh3k1h9l5cy62gq78wwsq915xqng8ddr0x8ny575b"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (inputs (list alsa-lib openssl pulseaudio dbus))    
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.10)
         ("rust-env-logger" ,rust-env-logger-0.6)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-getopts" ,rust-getopts-0.2)
         ("rust-hex" ,rust-hex-0.3)
         ("rust-hyper" ,rust-hyper-0.11)
         ("rust-librespot-audio" ,rust-librespot-audio-0.1)
         ("rust-librespot-connect" ,rust-librespot-connect-0.1)
         ("rust-librespot-core" ,rust-librespot-core-0.1)
         ("rust-librespot-metadata" ,rust-librespot-metadata-0.1)
         ("rust-librespot-playback" ,rust-librespot-playback-0.1)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.2)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-rpassword" ,rust-rpassword-3)
         ("rust-sha-1" ,rust-sha-1-0.8)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-tokio-process" ,rust-tokio-process-0.2)
         ("rust-tokio-signal" ,rust-tokio-signal-0.2)
         ("rust-url" ,rust-url-1))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis
      "An open source client library for Spotify, with support for Spotify Connect")
    (description
      "An open source client library for Spotify, with support for Spotify Connect")
    (license license:expat)))

(define-public rust-oid-registry-0.1
  (package
    (name "rust-oid-registry")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oid-registry" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05a7ldcq043w3dzdjj9r4bcn6gg053ipj8dfv2nbx0sg8wzfgapn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-der-parser" ,rust-der-parser-5))))
    (home-page "https://github.com/rusticata/oid-registry")
    (synopsis "Object Identifier (OID) database")
    (description "Object Identifier (OID) database")
    (license (list license:expat license:asl2.0))))

(define-public rust-der-oid-macro-0.4
  (package
    (name "rust-der-oid-macro")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "der-oid-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mjgpd0c9glranngv2gjbfjkzr6hmsagi0d52nhzrh4qpdhczk54"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nom" ,rust-nom-6)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/rusticata/der-parser")
    (synopsis "Macro to encode DER oids at compile time")
    (description "Macro to encode DER oids at compile time")
    (license (list license:expat license:asl2.0))))

(define-public rust-der-parser-5
  (package
    (name "rust-der-parser")
    (version "5.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "der-parser" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w1pl92scy553qm32529bybc5vclg378b5i0pha43fr5fpdxwzid"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cookie-factory" ,rust-cookie-factory-0.3)
         ("rust-der-oid-macro" ,rust-der-oid-macro-0.4)
         ("rust-nom" ,rust-nom-6)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rusticata-macros" ,rust-rusticata-macros-3))))
    (home-page "https://github.com/rusticata/der-parser")
    (synopsis "Parser/encoder for ASN.1 BER/DER data")
    (description "Parser/encoder for ASN.1 BER/DER data")
    (license (list license:expat license:asl2.0))))

(define-public rust-x509-parser-0.10
  (package
    (name "rust-x509-parser")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x509-parser" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nbq8w0q906saan2jghpnisdrcvkabnr6f7wcwmd9s9h0bk2gnwz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-der-parser" ,rust-der-parser-5)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-nom" ,rust-nom-6)
         ("rust-oid-registry" ,rust-oid-registry-0.1)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-rusticata-macros" ,rust-rusticata-macros-3)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/rusticata/x509-parser")
    (synopsis "Parser for the X.509 v3 format (RFC 5280 certificates)")
    (description "Parser for the X.509 v3 format (RFC 5280 certificates)")
    (license (list license:expat license:asl2.0))))

(define-public rust-core-foundation-sys-0.8
  (package
    (name "rust-core-foundation-sys")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p5r2wckarkpkyc4z83q08dwpvcafrb1h6fxfa3qnikh8szww9sq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/servo/core-foundation-rs")
    (synopsis "Bindings to Core Foundation for macOS")
    (description "Bindings to Core Foundation for macOS")
    (license (list license:expat license:asl2.0))))


(define-public rust-security-framework-2.4
  (package
    (name "rust-security-framework")
    (version "2.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11vwvw2qnrf1z8i0ncbj9456vxwnwq9wyi9c2n6rkqd2znmw2nsj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1.2)
         ("rust-core-foundation" ,rust-core-foundation-0.9)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-security-framework-sys" ,rust-security-framework-sys-2))
        #:cargo-development-inputs
        (("rust-env-logger" ,rust-env-logger-0.9)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-tempdir" ,rust-tempdir-0.3)
         ("rust-x509-parser" ,rust-x509-parser-0.10))))
    (home-page "https://lib.rs/crates/security_framework")
    (synopsis "Security.framework bindings for macOS and iOS")
    (description "Security.framework bindings for macOS and iOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-security-framework-2
  (package
    (name "rust-security-framework")
    (version "2.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p0hgqba3h2glm7mgp5d45l2gpmh28kn5vddlfa032mg5wblzh9d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-core-foundation" ,rust-core-foundation-0.9)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-security-framework-sys" ,rust-security-framework-sys-2))
        #:cargo-development-inputs
        (("rust-env-logger" ,rust-env-logger-0.9)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-tempdir" ,rust-tempdir-0.3)
         ("rust-x509-parser" ,rust-x509-parser-0.12))))
    (home-page "https://lib.rs/crates/security_framework")
    (synopsis "Security.framework bindings for macOS and iOS")
    (description "Security.framework bindings for macOS and iOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-security-framework-sys-2
  (package
    (name "rust-security-framework-sys")
    (version "2.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "security-framework-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mn5lm0jip9nm6ydqm6qd9alyiwq15c027777jsbyibs2wxa2q01"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://lib.rs/crates/security-framework-sys")
    (synopsis "Apple `Security.framework` low-level FFI bindings")
    (description "Apple `Security.framework` low-level FFI bindings")
    (license (list license:expat license:asl2.0))))

(define-public rust-core-foundation-sys-0.8
  (package
    (name "rust-core-foundation-sys")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p5r2wckarkpkyc4z83q08dwpvcafrb1h6fxfa3qnikh8szww9sq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/servo/core-foundation-rs")
    (synopsis "Bindings to Core Foundation for macOS")
    (description "Bindings to Core Foundation for macOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-core-foundation-0.9
  (package
    (name "rust-core-foundation")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ii1ihpjb30fk38gdikm5wqlkmyr8k46fh4k2r8sagz5dng7ljhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uuid" ,rust-uuid-0.5))))
    (home-page "https://github.com/servo/core-foundation-rs")
    (synopsis "Bindings to Core Foundation for macOS")
    (description "Bindings to Core Foundation for macOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-zvariant-derive-2
  (package
    (name "rust-zvariant-derive")
    (version "2.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zvariant_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1s9xk9c4p9vl0j2vr1abqc12mgv500sjc3fnh8ij3d1yb4i5xjp4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://gitlab.freedesktop.org/dbus/zbus/")
    (synopsis "D-Bus & GVariant encoding & decoding")
    (description "D-Bus & GVariant encoding & decoding")
    (license license:expat)))

(define-public rust-zvariant-2
  (package
    (name "rust-zvariant")
    (version "2.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zvariant" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0995d59vl8409mk3qrbshqrz5d76dq52szg0x2vqji07y9app356"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayvec" ,rust-arrayvec-0.5)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-enumflags2" ,rust-enumflags2-0.6)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-bytes" ,rust-serde-bytes-0.11)
         ("rust-static-assertions" ,rust-static-assertions-1)
         ("rust-zvariant-derive" ,rust-zvariant-derive-2))))
    (home-page "https://gitlab.freedesktop.org/dbus/zbus/")
    (synopsis "D-Bus & GVariant encoding & decoding")
    (description "D-Bus & GVariant encoding & decoding")
    (license license:expat)))


;;redundant? 

(define-public rust-zbus-macros-1
  (package
    (name "rust-zbus-macros")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zbus_macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c093vxdly5v7hy8z7i9z5l3j79ris0g3bcv6pplnf3jv8f19k47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://gitlab.freedesktop.org/dbus/zbus/")
    (synopsis "proc-macros for zbus")
    (description "proc-macros for zbus")
    (license license:expat)))


(define-public rust-ntest-0.7
  (package
    (name "rust-ntest")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ntest" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i4xsvx52hmcnga2xbjl74hdylz4jy8bc2swcichlvw1di4lwm2w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ntest-proc-macro-helper" ,rust-ntest-proc-macro-helper-0.7)
         ("rust-ntest-test-cases" ,rust-ntest-test-cases-0.7)
         ("rust-ntest-timeout" ,rust-ntest-timeout-0.7))))
    (home-page "https://github.com/becheran/ntest")
    (synopsis
      "Testing framework for rust which enhances the built-in library with some useful features.")
    (description
      "Testing framework for rust which enhances the built-in library with some useful
features.")
    (license license:expat)))

(define-public rust-ntest-timeout-0.7
  (package
    (name "rust-ntest-timeout")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ntest_timeout" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08v3r6hggh43qabl887pkz88k6lg6hrc62mppxyabb0pw44v03di"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ntest-proc-macro-helper" ,rust-ntest-proc-macro-helper-0.7)
         ("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/becheran/ntest")
    (synopsis "Timeout attribute for the ntest framework.")
    (description "Timeout attribute for the ntest framework.")
    (license license:expat)))

(define-public rust-ntest-test-cases-0.7
  (package
    (name "rust-ntest-test-cases")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ntest_test_cases" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ghal2rb03cnj7ciqgdq0dvifdf8qp2hnmi9z1ip1j5b02s1xa4r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/becheran/ntest")
    (synopsis "Test cases for ntest framework.")
    (description "Test cases for ntest framework.")
    (license license:expat)))

(define-public rust-ntest-proc-macro-helper-0.7
  (package
    (name "rust-ntest-proc-macro-helper")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ntest_proc_macro_helper" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lkyfx97aynbm7cnhzyc9cr0rpq1xzng1hwmzizbf1a6855y6llg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/becheran/ntest")
    (synopsis
      "Provide helper functions for the procedural macros used in ntest.")
    (description
      "Provide helper functions for the procedural macros used in ntest.")
    (license license:expat)))

(define-public rust-zbus-polkit-1
  (package
    (name "rust-zbus-polkit")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zbus_polkit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1yzbs8sga4s3h97vb6n5nvdvlnmhws2vj46bn44hbncfm25f51mc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-enumflags2" ,rust-enumflags2-0.6)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-repr" ,rust-serde-repr-0.1)
         ("rust-zbus" ,rust-zbus-1)
         ("rust-zvariant" ,rust-zvariant-2))))
    (home-page "https://gitlab.freedesktop.org/dbus/zbus/")
    (synopsis "PolicyKit binding")
    (description "PolicyKit binding")
    (license license:expat)))

(define-public rust-zbus-1
  (package
    (name "rust-zbus")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zbus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1a023ykgvavp28p5zzgxl14lj639p4vhqpypmqr4xvfs76md9d20"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-async-io" ,rust-async-io-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-derivative" ,rust-derivative-2)
         ("rust-enumflags2" ,rust-enumflags2-0.6)
         ("rust-fastrand" ,rust-fastrand-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-nb-connect" ,rust-nb-connect-1)
         ("rust-nix" ,rust-nix-0.17)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-polling" ,rust-polling-2)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-xml-rs" ,rust-serde-xml-rs-0.4)
         ("rust-serde-repr" ,rust-serde-repr-0.1)
         ("rust-zbus-macros" ,rust-zbus-macros-1)
         ("rust-zvariant" ,rust-zvariant-2))
        #:cargo-development-inputs
        (("rust-doc-comment" ,rust-doc-comment-0.3)
         ("rust-ntest" ,rust-ntest-0.7)
         ("rust-zbus-polkit" ,rust-zbus-polkit-1))))
    (home-page "https://gitlab.freedesktop.org/dbus/zbus/")
    (synopsis "API for D-Bus communication")
    (description "API for D-Bus communication")
    (license license:expat)))

(define-public rust-num-rational-0.3
  (package
    (name "rust-num-rational")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-rational" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01sgiwny9iflyxh2xz02sak71v2isc3x608hfdpwwzxi3j5l5b0j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-num-bigint" ,rust-num-bigint-0.3)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-num/num-rational")
    (synopsis "Rational numbers implementation for Rust")
    (description "Rational numbers implementation for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-complex-0.3
  (package
    (name "rust-num-complex")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-complex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1igjwm5kk2df9mxmpb260q6p40xfnkrq4smymgdqg2sm1hn66zbl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-num/num-complex")
    (synopsis "Complex numbers implementation for Rust")
    (description "Complex numbers implementation for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-0.3
  (package
    (name "rust-num")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13vsnqr0kasn7rwfq5r1vqdd0sy0y5ar3x4xhvzy4fg0wndqwylb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-bigint" ,rust-num-bigint-0.3)
         ("rust-num-complex" ,rust-num-complex-0.3)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-iter" ,rust-num-iter-0.1)
         ("rust-num-rational" ,rust-num-rational-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/rust-num/num")
    (synopsis
      "A collection of numeric types and traits for Rust, including bigint,
complex, rational, range iterators, generic integers, and more!
")
    (description
      "This package provides a collection of numeric types and traits for Rust,
including bigint, complex, rational, range iterators, generic integers, and
more!")
    (license (list license:expat license:asl2.0))))

(define-public rust-secret-service-2
  (package
    (name "rust-secret-service")
    (version "2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "secret-service" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v2ncsk4jyvdyqmrwzj7554xwalk9wllcj90v8x30yx8y8dzn014"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes" ,rust-aes-0.6)
         ("rust-block-modes" ,rust-block-modes-0.7)
         ("rust-hkdf" ,rust-hkdf-0.10)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-num" ,rust-num-0.3)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-zbus" ,rust-zbus-1)
         ("rust-zbus-macros" ,rust-zbus-macros-1)
         ("rust-zvariant" ,rust-zvariant-2)
         ("rust-zvariant-derive" ,rust-zvariant-derive-2))))
    (home-page "https://github.com/hwchen/secret-service-rs.git")
    (synopsis "Library to interface with Secret Service API")
    (description "Library to interface with Secret Service API")
    (license (list license:expat license:asl2.0))))



(define-public rust-keyring-0.10
  (package
    (name "rust-keyring")
    (version "0.10.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "keyring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xs73nygvd6gb5mnisdxngqdh0i5vmbg0id8k1l0nfv6d8aqp6m4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-secret-service" ,rust-secret-service-2)
         ("rust-security-framework" ,rust-security-framework-2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/hwchen/keyring-rs")
    (synopsis "Cross-platform library for managing passwords/credentials")
    (description "Cross-platform library for managing passwords/credentials")
    (license (list license:expat license:asl2.0))))

(define-public rust-dbus-tokio-0.2
  (package
    (name "rust-dbus-tokio")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dbus-tokio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b87p6gzfycl2hrpcsnv7dak93m1nrmiyirh02g3mmmk7sjqm9f4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dbus" ,rust-dbus-0.6)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-log" ,rust-log-0.3)
         ("rust-mio" ,rust-mio-0.6)
         ("rust-tokio-core" ,rust-tokio-core-0.1))))
    (home-page "https://github.com/diwic/dbus-rs")
    (synopsis
      "Makes it possible to use Tokio with D-Bus, which is a bus commonly used on Linux for inter-process communication.")
    (description
      "Makes it possible to use Tokio with D-Bus, which is a bus commonly used on Linux
for inter-process communication.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-alsa-sys-0.1
  (package
    (name "rust-alsa-sys")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0n3xr2msblmqlsx313b2y2v9hamqh0hp43v23fp1b3znkszwpvdh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/diwic/alsa-sys")
    (synopsis
      "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (description
      "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (license license:expat)))

(define-public rust-alsa-0.3
  (package
    (name "rust-alsa")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ljwyg1ckyglkjf3axhpfs4hspw2pxzr4qcis6w7r7c7ni75wspy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa-sys" ,rust-alsa-sys-0.1)
         ("rust-bitflags" ,rust-bitflags-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.14))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-version-compare-0.0.10
  (package
    (name "rust-version-compare")
    (version "0.0.10")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "version-compare" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18ack6rx18rp700h1dncljmpzchs3p2dfh76a8ds6vmfbfi5cdfn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://timvisee.com/projects/version-compare/")
    (synopsis
     "Rust library to easily compare version numbers with no specific format, and test against various comparison operators.")
    (description
     "Rust library to easily compare version numbers with no specific format, and test
against various comparison operators.")
    (license license:expat)))

(define-public rust-vcpkg-0.2
  (package
    (name "rust-vcpkg")
    (version "0.2.15")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vcpkg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/mcgoo/vcpkg-rs")
    (synopsis "A library to find native dependencies in a vcpkg tree at build
time in order to be used in Cargo build scripts.
")
    (description
     "This package provides a library to find native dependencies in a vcpkg tree at
build time in order to be used in Cargo build scripts.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unidiff-0.3
  (package
    (name "rust-unidiff")
    (version "0.3.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "unidiff" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0b13vhp2x7jlvmkm44h5niqcxklyrmz6afmppvykp4zimhcjg9nq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-encoding-rs" ,rust-encoding-rs-0.8)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/messense/unidiff-rs")
    (synopsis "Unified diff parsing/metadata extraction library for Rust")
    (description "Unified diff parsing/metadata extraction library for Rust")
    (license license:expat)))

(define-public rust-sdl2-sys-0.34
  (package
    (name "rust-sdl2-sys")
    (version "0.34.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sdl2-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0x4g141ais0k16frypykc1bfxlg5smraavg2lr0mlnqp3yi9m8j1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.53)
                       ("rust-cfg-if" ,rust-cfg-if-0.1)
                       ("rust-cmake" ,rust-cmake-0.1)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-unidiff" ,rust-unidiff-0.3)
                       ("rust-vcpkg" ,rust-vcpkg-0.2)
                       ("rust-version-compare" ,rust-version-compare-0.0.10))))
    (home-page "https://github.com/rust-sdl2/rust-sdl2")
    (synopsis "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (description "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (license license:expat)))

(define-public rust-c-vec-2
  (package
    (name "rust-c-vec")
    (version "2.0.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "c_vec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1s765fviy10q27b0wmkyk4q728z9v8v5pdlxv5k564y0mlks9mzx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/GuillaumeGomez/c_vec-rs.git")
    (synopsis "Structures to wrap C arrays")
    (description "Structures to wrap C arrays")
    (license (list license:asl2.0 license:expat))))

(define-public rust-sdl2-0.34
  (package
    (name "rust-sdl2")
    (version "0.34.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "sdl2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0jymfs8ibf1xli4vn562l05bl6zknmff0qz5l7swy2j6m4zvrv6y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-c-vec" ,rust-c-vec-2)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
                       ("rust-sdl2-sys" ,rust-sdl2-sys-0.34))))
    (home-page "https://github.com/Rust-SDL2/rust-sdl2")
    (synopsis "SDL2 bindings for Rust")
    (description "SDL2 bindings for Rust")
    (license license:expat)))

(define-public rust-slice-deque-0.3
  (package
    (name "rust-slice-deque")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "slice-deque" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "098gvqjw52qw4gac567c9hx3y6hw9al7hjqb5mnvmvydh3i6xvri"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-mach" ,rust-mach-0.3)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gnzlbg/slice_deque")
    (synopsis "A double-ended queue that Deref's into a slice.")
    (description
     "This package provides a double-ended queue that Deref's into a slice.")
    (license (list license:expat license:asl2.0))))

(define-public rust-minimp3-sys-0.3
  (package
    (name "rust-minimp3-sys")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minimp3-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "144vmf3s89kad0smjprzigcp2c9r5dm95n4ydilrbp399irp6772"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-minimp3-0.5
  (package
    (name "rust-minimp3")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "minimp3" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0wj3nzj1swnvwsk3a4a3hkfj1d21jsi7babi40wlrxzbbzvkhm4q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-minimp3-sys" ,rust-minimp3-sys-0.3)
                       ("rust-slice-deque" ,rust-slice-deque-0.3)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-hound-3
  (package
    (name "rust-hound")
    (version "3.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hound" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cadkxzdsb3bxwzri6r6l78a1jy9j0jxrfwmh34gjadvbnyws4sd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/hound")
    (synopsis "A wav encoding and decoding library")
    (description "This package provides a wav encoding and decoding library")
    (license license:asl2.0)))

(define-public rust-claxon-0.4
  (package
    (name "rust-claxon")
    (version "0.4.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "claxon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/claxon#readme")
    (synopsis "A FLAC decoding library")
    (description "This package provides a FLAC decoding library")
    (license license:asl2.0)))

(define-public rust-rodio-0.13
  (package
    (name "rust-rodio")
    (version "0.13.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rodio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03sjp37vb7ss1dwf76sf6yavmndqklx35shjpg8zd49ickd2wp5n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-claxon" ,rust-claxon-0.4)
                       ("rust-cpal" ,rust-cpal-0.13)
                       ("rust-hound" ,rust-hound-3)
                       ("rust-lewton" ,rust-lewton-0.10)
                       ("rust-minimp3" ,rust-minimp3-0.5))))
    (home-page "https://github.com/RustAudio/rodio")
    (synopsis "Audio playback library")
    (description "Audio playback library")
    (license (list license:expat license:asl2.0))))

(define-public rust-portaudio-sys-0.1
  (package
    (name "rust-portaudio-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "portaudio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xdpywirpr1kqkbak7hnny62gmsc93qgc3ij3j2zskrvjpxa952i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "Bindings to PortAudio")
    (description "Bindings to PortAudio")
    (license license:expat)))

(define-public rust-portaudio-rs-0.3
  (package
    (name "rust-portaudio-rs")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "portaudio-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qnmc7amk0fzbcs985ixv0k4955f0fmpkhrl9ps9pk3cz7pvbdnd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-portaudio-sys" ,rust-portaudio-sys-0.1))))
    (home-page "")
    (synopsis "PortAudio bindings for Rust")
    (description "PortAudio bindings for Rust")
    (license license:expat)))

(define-public rust-librespot-metadata-0.2
  (package
    (name "rust-librespot-metadata")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-metadata" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0ixg23ck98rjnshp6xil4lbig1sxpbg0blvyy6pdrnq1v7jpq5c4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-librespot-core" ,rust-librespot-core-0.2)
                       ("rust-librespot-protocol" ,rust-librespot-protocol-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The metadata logic for librespot")
    (description "The metadata logic for librespot")
    (license license:expat)))

(define-public rust-vorbisfile-sys-0.0.8
  (package
    (name "rust-vorbisfile-sys")
    (version "0.0.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbisfile-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1la2j2zbzdjd93byz21ij58c540bfn1r9pi0bssrjimcw7bhchsg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-vorbis-sys" ,rust-vorbis-sys-0.1))))
    (home-page "")
    (synopsis "FFI for the vorbisfile library")
    (description "FFI for the vorbisfile library")
    (license license:expat)))

(define-public rust-vorbis-sys-0.1
  (package
    (name "rust-vorbis-sys")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zgv7lwa4b2z091g25h83zil8bawk4frc1f0ril5xa31agpxd7mx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "FFI for the libvorbis library")
    (description "FFI for the libvorbis library")
    (license license:expat)))

(define-public rust-vorbis-0.0.14
  (package
    (name "rust-vorbis")
    (version "0.0.14")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "vorbis" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0xn7diq8qz2zmsmwzg3rcsxmpmm2gj7wgnl2gdan0lq7ax21k2jy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-vorbis-sys" ,rust-vorbis-sys-0.1)
                       ("rust-vorbisfile-sys" ,rust-vorbisfile-sys-0.0.8))))
    (home-page "https://github.com/tomaka/vorbis-rs")
    (synopsis "High-level bindings for the official libvorbis library.")
    (description "High-level bindings for the official libvorbis library.")
    (license license:asl2.0)))

(define-public rust-ogg-sys-0.0.9
  (package
    (name "rust-ogg-sys")
    (version "0.0.9")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cpx6n5ndh2d59g43l6rj3myzi5jsc0n6rldpx0impqp5qbqqnx9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gcc" ,rust-gcc-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/tomaka/ogg-sys")
    (synopsis "FFI for libogg, the media container.")
    (description "FFI for libogg, the media container.")
    (license license:expat)))

(define-public rust-librespot-tremor-0.2
  (package
    (name "rust-librespot-tremor")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-tremor" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1zmld16zawvn7ayrf318lwdr2d7awn4bk9s0d6kpim0mz6zjbxcp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-ogg-sys" ,rust-ogg-sys-0.0.9)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "Rust bindings to tremor")
    (description "Rust bindings to tremor")
    (license license:expat)))

(define-public rust-ogg-0.8
  (package
    (name "rust-ogg")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ogg" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vjxmqcv9252aj8byy70iy2krqfjknfcxg11lcyikj11pzlb8lb9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-0.4)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/ogg")
    (synopsis "Ogg container decoder and encoder written in pure Rust")
    (description "Ogg container decoder and encoder written in pure Rust")
    (license license:bsd-3)))

(define-public rust-lewton-0.10
  (package
    (name "rust-lewton")
    (version "0.10.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "lewton" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c60fn004awg5c3cvx82d6na2pirf0qdz9w3b93mbcdakbglhyvp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-futures" ,rust-futures-0.1)
                       ("rust-ogg" ,rust-ogg-0.8)
                       ("rust-tinyvec" ,rust-tinyvec-1)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/lewton")
    (synopsis "Pure Rust vorbis decoder")
    (description "Pure Rust vorbis decoder")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-audio-0.2
  (package
    (name "rust-librespot-audio")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-audio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1h25cinnyfw15farjv7lwcyqppd6gv0h5fkyac00dn06w79qj3ps"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-aes-ctr" ,rust-aes-ctr-0.6)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-lewton" ,rust-lewton-0.10)
                       ("rust-librespot-core" ,rust-librespot-core-0.2)
                       ("rust-librespot-tremor" ,rust-librespot-tremor-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ogg" ,rust-ogg-0.8)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-vorbis" ,rust-vorbis-0.0.14)
                       ("rust-zerocopy" ,rust-zerocopy-0.3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The audio fetching logic for librespot")
    (description "The audio fetching logic for librespot")
    (license license:expat)))

(define-public rust-libpulse-simple-sys-1
  (package
    (name "rust-libpulse-simple-sys")
    (version "1.20.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libpulse-simple-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0hiqnhpygrhmkwvkjjccbfjnqn5bcl70h5j9kg70ij8rqvcj7r44"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libpulse-sys" ,rust-libpulse-sys-1)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis
     "FFI bindings for the PulseAudio libpulse-simple system library.")
    (description
     "FFI bindings for the PulseAudio libpulse-simple system library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pkg-config-0.3
  (package
    (name "rust-pkg-config")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pkg-config" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0q2i61dhqvawc51zfzl3jich57w0cjgfa894hn6ings7ffgsbjba"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/rust-lang/pkg-config-rs")
    (synopsis
     "A library to run the pkg-config system tool at build time in order to be used in
Cargo build scripts.
")
    (description
     "This package provides a library to run the pkg-config system tool at build time
in order to be used in Cargo build scripts.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-sys-1
  (package
    (name "rust-libpulse-sys")
    (version "1.20.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libpulse-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1nza6ywj5g7sv9f2ljysl0msb9g7vj8qxb1fyx6dzl8q124fd491"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis "FFI bindings for the PulseAudio libpulse system library.")
    (description "FFI bindings for the PulseAudio libpulse system library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-binding-2
  (package
    (name "rust-libpulse-binding")
    (version "2.27.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libpulse-binding" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1438xsd9dm0bg9ba67r8jglpyjidxkq49w98z0pc2jhrzh5v4i8p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libpulse-sys" ,rust-libpulse-sys-1)
                       ("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis "A Rust language binding for the PulseAudio libpulse library.")
    (description
     "This package provides a Rust language binding for the PulseAudio libpulse
library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-simple-binding-2
  (package
    (name "rust-libpulse-simple-binding")
    (version "2.27.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libpulse-simple-binding" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c8fvypbyraa4in7syzvxfbhc7sfyd1l0kip64s16i3fkqcr9vaw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libpulse-binding" ,rust-libpulse-binding-2)
                       ("rust-libpulse-simple-sys" ,rust-libpulse-simple-sys-1)
                       ("rust-libpulse-sys" ,rust-libpulse-sys-1))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis
     "A Rust language binding for the PulseAudio libpulse-simple library.")
    (description
     "This package provides a Rust language binding for the PulseAudio libpulse-simple
library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-0.6
  (package
    (name "rust-jack")
    (version "0.6.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jack" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0sk8rxiq3h2y33hdq15hnf915l8rv09zl9sgg2vjysvypms4ksrd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-jack-sys" ,rust-jack-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK.")
    (description "Real time audio and midi with JACK.")
    (license license:expat)))

(define-public rust-gstreamer-base-0.16
  (package
    (name "rust-gstreamer-base")
    (version "0.16.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-base" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kxgwvv0qh1dgvd08nxfhhkjp36z9fxrf3x1nps11jsrdz2h3zds"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-glib" ,rust-glib-0.10)
                       ("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.10)
                       ("rust-gstreamer" ,rust-gstreamer-0.16)
                       ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.9)
                       ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
                       ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer Base library")
    (description "Rust bindings for GStreamer Base library")
    (license (list license:expat license:asl2.0))))

(define-public rust-gstreamer-base-sys-0.9
  (package
    (name "rust-gstreamer-base-sys")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-base-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xgf5dl7507hn9mvz46ffjj3y2shpl1gc4l6w8d0l5kf5pfbddx4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.10)
                       ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstbase-1.0")
    (description "FFI bindings to libgstbase-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-sys-0.9
  (package
    (name "rust-gstreamer-app-sys")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-app-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gbyp1jsqqs3x2bfjkbdfsb5kfb4zafwzvxr52w36ywybhkn8gw1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.9)
                       ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstapp-1.0")
    (description "FFI bindings to libgstapp-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-0.16
  (package
    (name "rust-gstreamer-app")
    (version "0.16.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-app" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03mg4ywgba0q02zdqfamzxv7887bab2az32xhzg3x31kf618i06c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-sink" ,rust-futures-sink-0.3)
                       ("rust-glib" ,rust-glib-0.10)
                       ("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.10)
                       ("rust-gstreamer" ,rust-gstreamer-0.16)
                       ("rust-gstreamer-app-sys" ,rust-gstreamer-app-sys-0.9)
                       ("rust-gstreamer-base" ,rust-gstreamer-base-0.16)
                       ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
                       ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer App library")
    (description "Rust bindings for GStreamer App library")
    (license (list license:expat license:asl2.0))))

(define-public rust-pretty-hex-0.2
  (package
    (name "rust-pretty-hex")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pretty-hex" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0c91f9sdwmn3mz2d414dp1xk4iw0k1nsif7lyqvhklzh57arjp5w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/wolandr/pretty-hex")
    (synopsis "Pretty hex dump of bytes slice in the common style.")
    (description "Pretty hex dump of bytes slice in the common style.")
    (license license:expat)))

(define-public rust-muldiv-0.2
  (package
    (name "rust-muldiv")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "muldiv" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "014jlry2l2ph56mp8knw65637hh49q7fmrraim2bx9vz0a638684"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/sdroege/rust-muldiv")
    (synopsis
     "Provides a trait for numeric types to perform combined multiplication and
division with overflow protection
")
    (description
     "This package provides a trait for numeric types to perform combined
multiplication and division with overflow protection")
    (license license:expat)))

(define-public rust-gstreamer-sys-0.9
  (package
    (name "rust-gstreamer-sys")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07b09f2acaiczjl3725dhraym935yns8x2jziiqza6nhh901a7zw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.10)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstreamer-1.0")
    (description "FFI bindings to libgstreamer-1.0")
    (license license:expat)))

(define-public rust-gstreamer-rs-lgpl-docs-0.16
  (package
    (name "rust-gstreamer-rs-lgpl-docs")
    (version "0.16.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer-rs-lgpl-docs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06k4mr6478463q7hhsl4a252nhzf0b2qjqla3xhlh20ma0hz8912"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-rustdoc-stripper" ,rust-rustdoc-stripper-0.1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "LGPL-licensed docs for gstreamer-rs crates")
    (description "LGPL-licensed docs for gstreamer-rs crates")
    (license license:lgpl2.0)))

(define-public rust-gstreamer-0.16
  (package
    (name "rust-gstreamer")
    (version "0.16.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "gstreamer" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0crghv0qh0lys26712j3dshdwnvq2znnsyxldrzf72ihzzvx1xcz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-glib" ,rust-glib-0.10)
                       ("rust-glib-sys" ,rust-glib-sys-0.10)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.10)
                       ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
                       ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-muldiv" ,rust-muldiv-0.2)
                       ("rust-num-rational" ,rust-num-rational-0.3)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-pretty-hex" ,rust-pretty-hex-0.2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-bytes" ,rust-serde-bytes-0.11)
                       ("rust-serde-derive" ,rust-serde-derive-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer")
    (description "Rust bindings for GStreamer")
    (license (list license:expat license:asl2.0))))

(define-public rust-stdweb-0.1
  (package
    (name "rust-stdweb")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "stdweb" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gjk7ch31a3kgdc39kj4zqinf10yqaf717wanh9kwwbbwg430m7g"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clippy" ,rust-clippy-0.0)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/koute/stdweb")
    (synopsis "A standard library for the client-side Web")
    (description
     "This package provides a standard library for the client-side Web")
    (license (list license:expat license:asl2.0))))

(define-public rust-fetch-unroll-0.3
  (package
    (name "rust-fetch-unroll")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "fetch_unroll" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1l3cf8fhcrw354hdmjf03f5v4bxgn2wkjna8n0fn8bgplh8b3666"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libflate" ,rust-libflate-1)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-ureq" ,rust-ureq-2))))
    (home-page "https://github.com/katyo/fetch_unroll")
    (synopsis "Simple utilities for fetching and unrolling .tar.gz archives")
    (description
     "Simple utilities for fetching and unrolling .tar.gz archives")
    (license license:asl2.0)))

(define-public rust-oboe-sys-0.4
  (package
    (name "rust-oboe-sys")
    (version "0.4.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "oboe-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1gcl494yy880h2gfgsbdd32g2h0s1n94v58j5hil9mrf6yvsnw1k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.59)
                       ("rust-cc" ,rust-cc-1)
                       ("rust-fetch-unroll" ,rust-fetch-unroll-0.3))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
     "Unsafe bindings for oboe an android library for low latency audio IO")
    (description
     "Unsafe bindings for oboe an android library for low latency audio IO")
    (license license:asl2.0)))

(define-public rust-oboe-0.4
  (package
    (name "rust-oboe")
    (version "0.4.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "oboe" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1hd5626s8qkpgrl2alwz73i8rh1rzifbxj6pxz7zp82gicskrxi7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jni" ,rust-jni-0.19)
                       ("rust-ndk" ,rust-ndk-0.6)
                       ("rust-ndk-context" ,rust-ndk-context-0.1)
                       ("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-oboe-sys" ,rust-oboe-sys-0.4))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
     "Safe interface for oboe an android library for low latency audio IO")
    (description
     "Safe interface for oboe an android library for low latency audio IO")
    (license license:asl2.0)))

(define-public rust-ndk-glue-0.6
  (package
    (name "rust-ndk-glue")
    (version "0.6.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-glue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pz6cdmmlzsb2jhrfvkma5d5vw2i331dlghqnkk2c0l6hdxll30d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-android-logger" ,rust-android-logger-0.10)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ndk" ,rust-ndk-0.6)
                       ("rust-ndk-context" ,rust-ndk-context-0.1)
                       ("rust-ndk-macro" ,rust-ndk-macro-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.3))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Startup code for android binaries")
    (description "Startup code for android binaries")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-sys-0.3
  (package
    (name "rust-ndk-sys")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "15zsq4p6k5asf4mc0rknd8cz9wxrwvi50qdspgf87qcfgkknlnkf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jni-sys" ,rust-jni-sys-0.3))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "FFI bindings for the Android NDK")
    (description "FFI bindings for the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-0.6
  (package
    (name "rust-ndk")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "ndk" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1m1dfjw35qpys1hr4qib6mm3zacd01k439l7cx5f7phd0dzcfci0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-jni" ,rust-jni-0.18)
                       ("rust-jni-glue" ,rust-jni-glue-0.0)
                       ("rust-jni-sys" ,rust-jni-sys-0.3)
                       ("rust-ndk-sys" ,rust-ndk-sys-0.3)
                       ("rust-num-enum" ,rust-num-enum-0.5)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Safe Rust bindings to the Android NDK")
    (description "Safe Rust bindings to the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-sys-0.2
  (package
    (name "rust-jack-sys")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jack-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1h9c9za19nyr1prx77gkia18ia93f73lpyjdiyrvmhhbs79g54bv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libloading" ,rust-libloading-0.6)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/RustAudio/rust-jack/tree/main/jack-sys")
    (synopsis "Low-level binding to the JACK audio API.")
    (description "Low-level binding to the JACK audio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-0.8
  (package
    (name "rust-jack")
    (version "0.8.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jack" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0lz10s0n2gy128m65pf96is9ip00vfgvnkfja0y9ydmv24pw2ajx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-jack-sys" ,rust-jack-sys-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK.")
    (description "Real time audio and midi with JACK.")
    (license license:expat)))

(define-public rust-bindgen-0.61
  (package
    (name "rust-bindgen")
    (version "0.61.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bindgen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16phlka8ykx28jlk7l637vlr9h01j8mh2s0d6km6z922l5c2w0la"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cexpr" ,rust-cexpr-0.6)
                       ("rust-clang-sys" ,rust-clang-sys-1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-lazycell" ,rust-lazycell-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-shlex" ,rust-shlex-1)
                       ("rust-syn" ,rust-syn-1)
                       ("rust-which" ,rust-which-4))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
     "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
     "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-coreaudio-sys-0.2
  (package
    (name "rust-coreaudio-sys")
    (version "0.2.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1yvqsz7h0hknj0f25074zrmy5hb9diqaj0cyqbngw9409fwl950s"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.61))))
    (home-page "https://github.com/RustAudio/coreaudio-sys")
    (synopsis
     "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (description
     "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (license license:expat)))

(define-public rust-coreaudio-rs-0.10
  (package
    (name "rust-coreaudio-rs")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "coreaudio-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "125d4zr3n363ybga4629p41ym7iqjfb2alnwrc1zj7zyxch4p28i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-coreaudio-sys" ,rust-coreaudio-sys-0.2))))
    (home-page "https://github.com/RustAudio/coreaudio-rs")
    (synopsis "A friendly rust interface for Apple's CoreAudio API.")
    (description
     "This package provides a friendly rust interface for Apple's CoreAudio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-bindgen-0.56
  (package
    (name "rust-bindgen")
    (version "0.56.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "bindgen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fajmgk2064ca1z9iq1jjkji63qwwz38z3d67kv6xdy0xgdpk8rd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cexpr" ,rust-cexpr-0.4)
                       ("rust-clang-sys" ,rust-clang-sys-1)
                       ("rust-clap" ,rust-clap-2)
                       ("rust-env-logger" ,rust-env-logger-0.8)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-lazycell" ,rust-lazycell-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-shlex" ,rust-shlex-0.1)
                       ("rust-which" ,rust-which-3))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
     "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
     "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-asio-sys-0.2
  (package
    (name "rust-asio-sys")
    (version "0.2.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "asio-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16lbavksj2aasadyxbdnbrll6a1m8cwl4skbxgbvr1ma2wpwv82c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.56)
                       ("rust-cc" ,rust-cc-1)
                       ("rust-num-derive" ,rust-num-derive-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/RustAudio/cpal/")
    (synopsis
     "Low-level interface and binding generation for the steinberg ASIO SDK.")
    (description
     "Low-level interface and binding generation for the steinberg ASIO SDK.")
    (license license:asl2.0)))

(define-public rust-alsa-0.6
  (package
    (name "rust-alsa")
    (version "0.6.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1sicg23vc00fnr3zsib1b5wz20p5w54mpdlvk5msh5jn68wlfqzm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.24))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cpal-0.13
  (package
    (name "rust-cpal")
    (version "0.13.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cpal" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "05j11vz8rw19gqqvpd48i7wvm6j77v8fwx5lwhlkckqjllv7h4bl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-alsa" ,rust-alsa-0.6)
                       ("rust-asio-sys" ,rust-asio-sys-0.2)
                       ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
                       ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.10)
                       ("rust-jack" ,rust-jack-0.8)
                       ("rust-jni" ,rust-jni-0.19)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-mach" ,rust-mach-0.3)
                       ("rust-ndk" ,rust-ndk-0.6)
                       ("rust-ndk-glue" ,rust-ndk-glue-0.6)
                       ("rust-nix" ,rust-nix-0.23)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-oboe" ,rust-oboe-0.4)
                       ("rust-parking-lot" ,rust-parking-lot-0.11)
                       ("rust-stdweb" ,rust-stdweb-0.1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/rustaudio/cpal")
    (synopsis "Low-level cross-platform audio I/O library in pure Rust.")
    (description "Low-level cross-platform audio I/O library in pure Rust.")
    (license license:asl2.0)))

(define-public rust-alsa-sys-0.3
  (package
    (name "rust-alsa-sys")
    (version "0.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "09qmmnpmlcj23zcgx2xsi4phcgm5i02g9xaf801y7i067mkfx3yv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/diwic/alsa-sys")
    (synopsis
     "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (description
     "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (license license:expat)))

(define-public rust-alsa-0.5
  (package
    (name "rust-alsa")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "alsa" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03nmld6vbpxqg22fy07p51x2rmwl7bzsc7rszhd03gyknd5ldaqb"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-alsa-sys" ,rust-alsa-sys-0.3)
                       ("rust-bitflags" ,rust-bitflags-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.21))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-librespot-playback-0.2
  (package
    (name "rust-librespot-playback")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-playback" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1mksjvbwq7y01x54i7aqdbl7g48m7s060xki10qa9qd7hvwkvjjr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-alsa" ,rust-alsa-0.5)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-cpal" ,rust-cpal-0.13)
                       ("rust-futures-executor" ,rust-futures-executor-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-glib" ,rust-glib-0.10)
                       ("rust-gstreamer" ,rust-gstreamer-0.16)
                       ("rust-gstreamer-app" ,rust-gstreamer-app-0.16)
                       ("rust-jack" ,rust-jack-0.6)
                       ("rust-libpulse-binding" ,rust-libpulse-binding-2)
                       ("rust-libpulse-simple-binding" ,rust-libpulse-simple-binding-2)
                       ("rust-librespot-audio" ,rust-librespot-audio-0.2)
                       ("rust-librespot-core" ,rust-librespot-core-0.2)
                       ("rust-librespot-metadata" ,rust-librespot-metadata-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-portaudio-rs" ,rust-portaudio-rs-0.3)
                       ("rust-rodio" ,rust-rodio-0.13)
                       ("rust-sdl2" ,rust-sdl2-0.34)
                       ("rust-shell-words" ,rust-shell-words-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-zerocopy" ,rust-zerocopy-0.3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The audio playback logic for librespot")
    (description "The audio playback logic for librespot")
    (license license:expat)))

(define-public rust-slab-0.4
  (package
    (name "rust-slab")
    (version "0.4.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "slab" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1vyw3rkdfdfkzfa1mh83s237sll8v5kazfwxma60bq4b59msf526"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/tokio-rs/slab")
    (synopsis "Pre-allocated storage for a uniform data type")
    (description "Pre-allocated storage for a uniform data type")
    (license license:expat)))

(define-public rust-tokio-util-0.7
  (package
    (name "rust-tokio-util")
    (version "0.7.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-util" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cp6yx4789j6gvbp4xnbk7lpd7q0j2a2qd4g1pg2b4q0afadh9sl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytes" ,rust-bytes-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-sink" ,rust-futures-sink-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hashbrown" ,rust-hashbrown-0.12)
                       ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
                       ("rust-slab" ,rust-slab-0.4)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Additional utilities for working with Tokio.
")
    (description "Additional utilities for working with Tokio.")
    (license license:expat)))

(define-public rust-tokio-stream-0.1
  (package
    (name "rust-tokio-stream")
    (version "0.1.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "tokio-stream" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ki3aafl33qyqmahhp4i5da1ig0im2a89cpqr5xwsg270h27fq6n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-util" ,rust-tokio-util-0.7))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities to work with `Stream` and `tokio`.
")
    (description "Utilities to work with `Stream` and `tokio`.")
    (license license:expat)))

(define-public rust-shannon-0.2
  (package
    (name "rust-shannon")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "shannon" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qa52zs4y1i87ysr11g9p6shpdagl14bb340gfm6rd97jhfb99by"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "Shannon cipher implementation")
    (description "Shannon cipher implementation")
    (license license:expat)))

(define-public rust-priority-queue-1
  (package
    (name "rust-priority-queue")
    (version "1.3.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "priority-queue" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rv2r1ww2hr1rvd8hjmcgx22icz4hcnnrf3f2cdd52frf2zcdaaw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1)
                       ("rust-indexmap" ,rust-indexmap-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/garro95/priority-queue")
    (synopsis
     "A Priority Queue implemented as a heap with a function to efficiently change the priority of an item.")
    (description
     "This package provides a Priority Queue implemented as a heap with a function to
efficiently change the priority of an item.")
    (license (list license:lgpl3 license:mpl2.0))))

(define-public rust-password-hash-0.2
  (package
    (name "rust-password-hash")
    (version "0.2.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "password-hash" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1rr4kd52ld978a2xhcvlc54p1d92yhxl9kvbajba7ia6rs5b5q3p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64ct" ,rust-base64ct-1)
                       ("rust-rand-core" ,rust-rand-core-0.6)
                       ("rust-subtle" ,rust-subtle-2))))
    (home-page
     "https://github.com/RustCrypto/traits/tree/master/password-hash")
    (synopsis
     "Traits which describe the functionality of password hashing algorithms,
as well as a `no_std`-friendly implementation of the PHC string format
(a well-defined subset of the Modular Crypt Format a.k.a. MCF)
")
    (description
     "Traits which describe the functionality of password hashing algorithms, as well
as a `no_std`-friendly implementation of the PHC string format (a well-defined
subset of the Modular Crypt Format a.k.a.  MCF)")
    (license (list license:expat license:asl2.0))))

(define-public rust-pbkdf2-0.8
  (package
    (name "rust-pbkdf2")
    (version "0.8.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "pbkdf2" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1ykgicvyjm41701mzqhrfmiz5sm5y0zwfg6csaapaqaf49a54pyr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64ct" ,rust-base64ct-1)
                       ("rust-crypto-mac" ,rust-crypto-mac-0.11)
                       ("rust-hmac" ,rust-hmac-0.11)
                       ("rust-password-hash" ,rust-password-hash-0.2)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-sha-1" ,rust-sha-1-0.9)
                       ("rust-sha2" ,rust-sha2-0.9))))
    (home-page
     "https://github.com/RustCrypto/password-hashes/tree/master/pbkdf2")
    (synopsis "Generic implementation of PBKDF2")
    (description "Generic implementation of PBKDF2")
    (license (list license:expat license:asl2.0))))

(define-public rust-protobuf-codegen-pure-2
  (package
    (name "rust-protobuf-codegen-pure")
    (version "2.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "protobuf-codegen-pure" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0h34gfqlb7bqmgqv1mfgy5wk35z5r2h5ki3p3pdcmw1vqzmly6id"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2)
                       ("rust-protobuf-codegen" ,rust-protobuf-codegen-2))))
    (home-page
     "https://github.com/stepancheg/rust-protobuf/tree/master/protobuf-codegen-pure/")
    (synopsis "Pure-rust codegen for protobuf using protobuf-parser crate

WIP
")
    (description
     "Pure-rust codegen for protobuf using protobuf-parser crate WIP")
    (license license:expat)))

(define-public rust-protobuf-codegen-2
  (package
    (name "rust-protobuf-codegen")
    (version "2.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "protobuf-codegen" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "031bx325lsgcx7wc76vc2cqph6q0b34jgc8nz0g2rkwcfnx3n4fy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis
     "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e. g. from `build.rs`) and `protoc-gen-rust` binary.
")
    (description
     "Code generator for rust-protobuf.  Includes a library to invoke programmatically
(e.  g.  from `build.rs`) and `protoc-gen-rust` binary.")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.14.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "protobuf" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11bl8hf522s9mbkckivnn9n8s3ss4g41w6jmfdsswmr5adqd71lf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytes" ,rust-bytes-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license license:expat)))

(define-public rust-librespot-protocol-0.2
  (package
    (name "rust-librespot-protocol")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-protocol" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0pa3j2csmimf0l2yfdxp9ijmgm8ngxadim801jrh43xxqgj3nx8w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glob" ,rust-glob-0.3)
                       ("rust-protobuf" ,rust-protobuf-2)
                       ("rust-protobuf-codegen" ,rust-protobuf-codegen-2)
                       ("rust-protobuf-codegen-pure" ,rust-protobuf-codegen-pure-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The protobuf logic for communicating with Spotify servers")
    (description "The protobuf logic for communicating with Spotify servers")
    (license license:expat)))

(define-public rust-hyper-proxy-0.9
  (package
    (name "rust-hyper-proxy")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "hyper-proxy" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1k3mpq6d4rhz58dam1757sav14j32n39q8x37wjgpz943f4mm0fa"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytes" ,rust-bytes-1)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-headers" ,rust-headers-0.3)
                       ("rust-http" ,rust-http-0.2)
                       ("rust-hyper" ,rust-hyper-0.14)
                       ("rust-hyper-rustls" ,rust-hyper-rustls-0.22)
                       ("rust-hyper-tls" ,rust-hyper-tls-0.5)
                       ("rust-native-tls" ,rust-native-tls-0.2)
                       ("rust-openssl" ,rust-openssl-0.10)
                       ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.5)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
                       ("rust-tokio-openssl" ,rust-tokio-openssl-0.6)
                       ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
                       ("rust-tower-service" ,rust-tower-service-0.3)
                       ("rust-webpki" ,rust-webpki-0.21)
                       ("rust-webpki-roots" ,rust-webpki-roots-0.21))))
    (home-page "https://github.com/tafia/hyper-proxy")
    (synopsis "A proxy connector for Hyper-based applications")
    (description
     "This package provides a proxy connector for Hyper-based applications")
    (license license:expat)))

(define-public rust-librespot-core-0.2
  (package
    (name "rust-librespot-core")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0j1jjqf89dlsrlfjj9yy111dvlfi9zq3789ck0f57x9iw7f70gnz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-aes" ,rust-aes-0.6)
                       ("rust-base64" ,rust-base64-0.13)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-form-urlencoded" ,rust-form-urlencoded-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hmac" ,rust-hmac-0.11)
                       ("rust-http" ,rust-http-0.2)
                       ("rust-httparse" ,rust-httparse-1)
                       ("rust-hyper" ,rust-hyper-0.14)
                       ("rust-hyper-proxy" ,rust-hyper-proxy-0.9)
                       ("rust-librespot-protocol" ,rust-librespot-protocol-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-num-bigint" ,rust-num-bigint-0.4)
                       ("rust-num-integer" ,rust-num-integer-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pbkdf2" ,rust-pbkdf2-0.8)
                       ("rust-priority-queue" ,rust-priority-queue-1)
                       ("rust-protobuf" ,rust-protobuf-2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha-1" ,rust-sha-1-0.9)
                       ("rust-shannon" ,rust-shannon-0.2)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-stream" ,rust-tokio-stream-0.1)
                       ("rust-tokio-util" ,rust-tokio-util-0.6)
                       ("rust-url" ,rust-url-2)
                       ("rust-uuid" ,rust-uuid-0.8)
                       ("rust-vergen" ,rust-vergen-3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The core functionality provided by librespot")
    (description "The core functionality provided by librespot")
    (license license:expat)))

(define-public rust-if-addrs-sys-0.3
  (package
    (name "rust-if-addrs-sys")
    (version "0.3.2")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "if-addrs-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1skrzs79rafv185064p44r0k1va9ig4bfnpbwlvyhxh4g3fvjx6y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "if_addrs sys crate")
    (description "if_addrs sys crate")
    (license (list license:expat license:bsd-3))))

(define-public rust-if-addrs-0.6
  (package
    (name "rust-if-addrs")
    (version "0.6.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "if-addrs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1pkkkwm9znn07xq9s6glf8lxzn2rdxvy8kwkw6czrw64ywhy8wr2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-if-addrs-sys" ,rust-if-addrs-sys-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "Return interface IP addresses on Posix and windows systems")
    (description "Return interface IP addresses on Posix and windows systems")
    (license (list license:expat license:bsd-3))))

(define-public rust-libmdns-0.6
  (package
    (name "rust-libmdns")
    (version "0.6.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libmdns" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n1ymkv5246c5yj2m70ql07z38hrz6b2l16lg3wpb98vz7mbqd59"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-byteorder" ,rust-byteorder-1)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hostname" ,rust-hostname-0.3)
                       ("rust-if-addrs" ,rust-if-addrs-0.6)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-multimap" ,rust-multimap-0.8)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-socket2" ,rust-socket2-0.4)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/librespot-org/libmdns")
    (synopsis
     "mDNS Responder library for building discoverable LAN services in Rust")
    (description
     "mDNS Responder library for building discoverable LAN services in Rust")
    (license license:expat)))

(define-public rust-dns-sd-0.1
  (package
    (name "rust-dns-sd")
    (version "0.1.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dns-sd" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11r0jymjshfnn3sh2nqjhrikk4r5rr1g36sip9iqy8i0xafm0j6p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/plietar/rust-dns-sd")
    (synopsis "Rust binding for dns-sd")
    (description "Rust binding for dns-sd")
    (license license:expat)))

(define-public rust-aes-ctr-0.6
  (package
    (name "rust-aes-ctr")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "aes-ctr" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0qspjxzrclnb83501595y01dhc0km1ssrbjnwlxhcrsdwp6w6abp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-aes-soft" ,rust-aes-soft-0.6)
                       ("rust-aesni" ,rust-aesni-0.10)
                       ("rust-cipher" ,rust-cipher-0.2)
                       ("rust-ctr" ,rust-ctr-0.6))))
    (home-page "https://github.com/RustCrypto/block-ciphers/tree/master/aes")
    (synopsis "DEPRECATED: replaced by the `aes` crate")
    (description "DEPRECATED: replaced by the `aes` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-connect-0.2
  (package
    (name "rust-librespot-connect")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "librespot-connect" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1272nnzk9xhyc2jvakfrl3451pql65mnxcc6xkqpjiypr3b5yhk9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-aes-ctr" ,rust-aes-ctr-0.6)
                       ("rust-base64" ,rust-base64-0.13)
                       ("rust-dns-sd" ,rust-dns-sd-0.1)
                       ("rust-form-urlencoded" ,rust-form-urlencoded-1)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hmac" ,rust-hmac-0.11)
                       ("rust-hyper" ,rust-hyper-0.14)
                       ("rust-libmdns" ,rust-libmdns-0.6)
                       ("rust-librespot-core" ,rust-librespot-core-0.2)
                       ("rust-librespot-playback" ,rust-librespot-playback-0.2)
                       ("rust-librespot-protocol" ,rust-librespot-protocol-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-protobuf" ,rust-protobuf-2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha-1" ,rust-sha-1-0.9)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-stream" ,rust-tokio-stream-0.1)
                       ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The discovery and Spotify Connect logic for librespot")
    (description "The discovery and Spotify Connect logic for librespot")
    (license license:expat)))

(define-public rust-cc-1
  (package
    (name "rust-cc")
    (version "1.0.79")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "cc" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "07x93b8zbf3xc2dggdd460xlk1wg8lxm6yflwddxj8b15030klsh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jobserver" ,rust-jobserver-0.1))))
    (home-page "https://github.com/rust-lang/cc-rs")
    (synopsis
     "A build-time dependency for Cargo build scripts to assist in invoking the native
C compiler to compile native C code into a static archive to be linked into Rust
code.
")
    (description
     "This package provides a build-time dependency for Cargo build scripts to assist
in invoking the native C compiler to compile native C code into a static archive
to be linked into Rust code.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libdbus-sys-0.2
  (package
    (name "rust-libdbus-sys")
    (version "0.2.4")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "libdbus-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "182qbyd48qcb4sn51hqksbgqq2cvyn1fdr8ahif85jz1a7kpm3cz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/diwic/dbus-rs")
    (synopsis "FFI bindings to libdbus.")
    (description "FFI bindings to libdbus.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-dbus-0.9
  (package
    (name "rust-dbus")
    (version "0.9.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbus" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06vdv4aarjs4w6byg9nqajr67c8qvlhk3153ic2i65pvp63ikchv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-executor" ,rust-futures-executor-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libdbus-sys" ,rust-libdbus-sys-0.2)
                       ("rust-winapi" ,rust-winapi-0.3))
       #:cargo-development-inputs (("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/diwic/dbus-rs")
    (synopsis
     "Bindings to D-Bus, which is a bus commonly used on Linux for inter-process communication.")
    (description
     "Bindings to D-Bus, which is a bus commonly used on Linux for inter-process
communication.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-jni-0.19
  (package
    (name "rust-jni")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "jni" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1v0pn0i1wb8zp4wns4l8hz9689hqsplv7iba7hylaznvwg11ipy6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cesu8" ,rust-cesu8-1)
                       ("rust-combine" ,rust-combine-4)
                       ("rust-jni-sys" ,rust-jni-sys-0.3)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/jni-rs/jni-rs")
    (synopsis "Rust bindings to the JNI")
    (description "Rust bindings to the JNI")
    (license (list license:expat license:asl2.0))))

(define-public rust-webbrowser-0.6
  (package
    (name "rust-webbrowser")
    (version "0.6.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "webbrowser" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0m1m9dkmyywndbdbk03428j0gjgb7xg64n3kch10ni3qd9mqphpr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-jni" ,rust-jni-0.19)
                       ("rust-ndk-glue" ,rust-ndk-glue-0.5)
                       ("rust-url" ,rust-url-2)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-widestring" ,rust-widestring-0.5)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/amodm/webbrowser-rs")
    (synopsis "Open URLs in web browsers available on a platform")
    (description "Open URLs in web browsers available on a platform")
    (license (list license:expat license:asl2.0))))

(define-public rust-strum-macros-0.24
  (package
    (name "rust-strum-macros")
    (version "0.24.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "strum_macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0naxz2y38kwq5wgirmia64vvf6qhwy8j367rw966n62gsbh5nf0y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-heck" ,rust-heck-0.4)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-rustversion" ,rust-rustversion-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/Peternator7/strum")
    (synopsis "Helpful macros for working with enums and strings")
    (description "Helpful macros for working with enums and strings")
    (license license:expat)))

(define-public rust-strum-0.24
  (package
    (name "rust-strum")
    (version "0.24.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "strum" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0gz6cjhlps5idwasznklxdh2zsas6mxf99vr0n27j876q12n0gh6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-phf" ,rust-phf-0.10)
                       ("rust-strum-macros" ,rust-strum-macros-0.24))))
    (home-page "https://github.com/Peternator7/strum")
    (synopsis "Helpful macros for working with enums and strings")
    (description "Helpful macros for working with enums and strings")
    (license license:expat)))

(define-public rust-enum-dispatch-0.3
  (package
    (name "rust-enum-dispatch")
    (version "0.3.11")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "enum_dispatch" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1qlxlxjvy92s0fwcwlnd2cdkkyml1755xap2lq8v4812hsanxwqi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://gitlab.com/antonok/enum_dispatch")
    (synopsis
     "Near drop-in replacement for dynamic-dispatched method calls with up to 10x the speed")
    (description
     "Near drop-in replacement for dynamic-dispatched method calls with up to 10x the
speed")
    (license (list license:expat license:asl2.0))))

(define-public rust-rspotify-model-0.11
  (package
    (name "rust-rspotify-model")
    (version "0.11.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rspotify-model" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kc33dxajlgmqj04xmr9yvnx1f8kdlazhp33javwji7w09mjj0rv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-enum-dispatch" ,rust-enum-dispatch-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-strum" ,rust-strum-0.24)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/ramsayleung/rspotify")
    (synopsis "Model for RSpotify")
    (description "Model for RSpotify")
    (license license:expat)))

(define-public rust-rspotify-macros-0.11
  (package
    (name "rust-rspotify-macros")
    (version "0.11.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rspotify-macros" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0iqc7b1srhn9hanjh6s6qsmzhrvyn7gi803zjcsm5isiqwgm51hc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/ramsayleung/rspotify")
    (synopsis "Macros for RSpotify")
    (description "Macros for RSpotify")
    (license license:expat)))

(define-public rust-rspotify-http-0.11
  (package
    (name "rust-rspotify-http")
    (version "0.11.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rspotify-http" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "018i3jb8h5fzq2742pbrkd8pwj456f6i951zlyhf67jx2nrq2x5a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-maybe-async" ,rust-maybe-async-0.2)
                       ("rust-reqwest" ,rust-reqwest-0.11)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-ureq" ,rust-ureq-2))))
    (home-page "https://github.com/ramsayleung/rspotify")
    (synopsis "HTTP compatibility layer for RSpotify")
    (description "HTTP compatibility layer for RSpotify")
    (license license:expat)))

(define-public rust-maybe-async-0.2
  (package
    (name "rust-maybe-async")
    (version "0.2.7")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "maybe-async" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "01gksgxmzgl8hvg831vv993fvrwz8hjwgcln99ilp08zrc9qq6qg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/fMeow/maybe-async-rs")
    (synopsis "A procedure macro to unify SYNC and ASYNC implementation")
    (description
     "This package provides a procedure macro to unify SYNC and ASYNC implementation")
    (license license:expat)))

(define-public rust-getrandom-0.2
  (package
    (name "rust-getrandom")
    (version "0.2.8")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "getrandom" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cbb766pcyi7sws0fnp1pxkz0nhiya0ckallq502bxmq49mfnnn0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cfg-if" ,rust-cfg-if-1)
                       ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-wasi" ,rust-wasi-0.11)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "https://github.com/rust-random/getrandom")
    (synopsis
     "A small cross-platform library for retrieving random data from system source")
    (description
     "This package provides a small cross-platform library for retrieving random data
from system source")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-macro-0.3
  (package
    (name "rust-futures-macro")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-macro" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0w3ahy69varlfw57rb2pag7jwngy771vvzmcag7mlfx3gpw3m9wm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The futures-rs procedural macro implementations.
")
    (description "The futures-rs procedural macro implementations.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-io-0.3
  (package
    (name "rust-futures-io")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-io" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cc5s3qdgls25rlm3zpdf9fdk6gwmfp0fiiph39b5bmjdwdkgf5z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
     "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the futures-rs library.
")
    (description
     "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the
futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-util-0.3
  (package
    (name "rust-futures-util")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-util" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1lbvdf6hq62yczd87glm6ih8h5qkagsl7xdiwhmqvwzymkins7cw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures" ,rust-futures-0.1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-macro" ,rust-futures-macro-0.3)
                       ("rust-futures-sink" ,rust-futures-sink-0.3)
                       ("rust-futures-task" ,rust-futures-task-0.3)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
                       ("rust-pin-utils" ,rust-pin-utils-0.1)
                       ("rust-slab" ,rust-slab-0.4)
                       ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
     "Common utilities and extension traits for the futures-rs library.
")
    (description
     "Common utilities and extension traits for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-task-0.3
  (package
    (name "rust-futures-task")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-task" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0rk3jg6lri1rrn03ns89cmw8lircbaf2i2d4mr10zc8hyqdrmxyw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Tools for working with tasks.
")
    (description "Tools for working with tasks.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-executor-0.3
  (package
    (name "rust-futures-executor")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-executor" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "03mm37yv235i2ifjfaacw5cl8cmiyirj8ap3d64fr5xblqshmpp8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-task" ,rust-futures-task-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-num-cpus" ,rust-num-cpus-1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
     "Executors for asynchronous tasks based on the futures-rs library.
")
    (description
     "Executors for asynchronous tasks based on the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-sink-0.3
  (package
    (name "rust-futures-sink")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-sink" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0r43djzf0caz89c724ishpzxy59y6nw7ykfvh1nd9kz8nc5q447k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The asynchronous `Sink` trait for the futures-rs library.
")
    (description "The asynchronous `Sink` trait for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-portable-atomic-1
  (package
    (name "rust-portable-atomic")
    (version "1.0.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "portable-atomic" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "06p5dglnqj43m4fj70dwcqhv9rmy9amn9pdpgpzd8fx0hf30rh1r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-critical-section" ,rust-critical-section-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/taiki-e/portable-atomic")
    (synopsis
     "Portable atomic types including support for 128-bit atomics, atomic float, etc.
")
    (description
     "Portable atomic types including support for 128-bit atomics, atomic float, etc.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-futures-core-0.3
  (package
    (name "rust-futures-core")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-core" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "02467z5mv0219hkrgmpvsb3h7vb8pg31s1j901h7vxg11x6zz47c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-portable-atomic" ,rust-portable-atomic-1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The core traits and types in for the `futures` library.
")
    (description "The core traits and types in for the `futures` library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-channel-0.3
  (package
    (name "rust-futures-channel")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-channel" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1xadcvj4hi6278hq6i0vnrsa231fyiylh2n03rx7d2ch79k1flrf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-sink" ,rust-futures-sink-0.3))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Channels for asynchronous communication using futures-rs.
")
    (description "Channels for asynchronous communication using futures-rs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-0.3
  (package
    (name "rust-futures")
    (version "0.3.26")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "115z5bqihd2jq75s8n7jxy4k83kpv67vhic4snch6d7h1wmpkqhk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-executor" ,rust-futures-executor-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-sink" ,rust-futures-sink-0.3)
                       ("rust-futures-task" ,rust-futures-task-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
     "An implementation of futures and streams featuring zero allocations,
composability, and iterator-like interfaces.
")
    (description
     "An implementation of futures and streams featuring zero allocations,
composability, and iterator-like interfaces.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rspotify-0.11
  (package
    (name "rust-rspotify")
    (version "0.11.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rspotify" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1fx0688n5xqarl2wg0nx6wsrd306vmjnvbysp93ncdsjpgm0rqpi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-async-stream" ,rust-async-stream-0.3)
                       ("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-base64" ,rust-base64-0.13)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-dotenv" ,rust-dotenv-0.15)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-getrandom" ,rust-getrandom-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-maybe-async" ,rust-maybe-async-0.2)
                       ("rust-rspotify-http" ,rust-rspotify-http-0.11)
                       ("rust-rspotify-macros" ,rust-rspotify-macros-0.11)
                       ("rust-rspotify-model" ,rust-rspotify-model-0.11)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-url" ,rust-url-2)
                       ("rust-webbrowser" ,rust-webbrowser-0.6))
       #:cargo-development-inputs (("rust-env-logger" ,rust-env-logger-0.9)
                                   ("rust-futures-util" ,rust-futures-util-0.3)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/ramsayleung/rspotify")
    (synopsis "Spotify API wrapper")
    (description "Spotify API wrapper")
    (license license:expat)))

(define-public rust-dbus-crossroads-0.3
  (package
    (name "rust-dbus-crossroads")
    (version "0.3.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbus-crossroads" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nlrffpi4acgry0czv70h0zv3rbf6iw2fvzsrhdpnb1qwf56x08a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-dbus" ,rust-dbus-0.9))))
    (home-page "https://github.com/diwic/dbus-rs/")
    (synopsis "Framework for writing D-Bus method handlers")
    (description "Framework for writing D-Bus method handlers")
    (license (list license:asl2.0 license:expat))))

(define-public rust-dbus-tokio-0.7
  (package
    (name "rust-dbus-tokio")
    (version "0.7.3")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbus-tokio" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "172hnk86r2ws6cvs0xp5sjjknp66wb6c866gm8m06x6k7anq6h4b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-dbus" ,rust-dbus-0.9)
                       ("rust-dbus-crossroads" ,rust-dbus-crossroads-0.3)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-tokio" ,rust-tokio-1))
       #:cargo-development-inputs (("rust-dbus-tree" ,rust-dbus-tree-0.9)
                                   ("rust-futures" ,rust-futures-0.3)
                                   ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/diwic/dbus-rs")
    (synopsis
     "Makes it possible to use Tokio with D-Bus, which is a bus commonly used on Linux for inter-process communication.")
    (description
     "Makes it possible to use Tokio with D-Bus, which is a bus commonly used on Linux
for inter-process communication.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-dbus-crossroads-0.4
  (package
    (name "rust-dbus-crossroads")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "dbus-crossroads" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "18wcwqm1qp8yisxj62vn81fdbh07kq4vxba9823fcd4zv6zinn64"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-dbus" ,rust-dbus-0.9))))
    (home-page "https://github.com/diwic/dbus-rs/")
    (synopsis "Framework for writing D-Bus method handlers")
    (description "Framework for writing D-Bus method handlers")
    (license (list license:asl2.0 license:expat))))


(define-public spotifyd
  (package
    (name "spotifyd")
    (version "0.3.4")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/Spotifyd/spotifyd")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
       (sha256
          (base32 "179sgwq7nanpaiv21x8r07gncybwxh45lsy7zn8lvg6x7820fg7p"))))
    (build-system cargo-build-system)    
    (native-inputs (list pkg-config))
    (inputs (list alsa-lib openssl pulseaudio dbus))
    (arguments
      `(#:cargo-inputs
        (("rust-alsa" ,rust-alsa-0.5)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-color-eyre" ,rust-color-eyre-0.5)
         ("rust-daemonize" ,rust-daemonize-0.4)
         ("rust-dbus-crossroads" ,rust-dbus-crossroads-0.4)
         ("rust-dbus" ,rust-dbus-0.9)
         ("rust-dbus-tokio" ,rust-dbus-tokio-0.7)
         ("rust-fern" ,rust-fern-0.6)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-gethostname" ,rust-gethostname-0.2)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-keyring" ,rust-keyring-0.10)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-librespot-core" ,rust-librespot-core-0.2)
         ("rust-librespot-connect" ,rust-librespot-connect-0.2)
         ("rust-librespot-audio" ,rust-librespot-audio-0.2)
         ("rust-librespot-playback" ,rust-librespot-playback-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-rspotify" ,rust-rspotify-0.11)
         ("rust-serde" ,rust-serde-1)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-structopt" ,rust-structopt-0.3)
         ("rust-syslog" ,rust-syslog-4)
         ("rust-tokio-core" ,rust-tokio-core-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-tokio-signal" ,rust-tokio-signal-0.1)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-url" ,rust-url-1)
         ("rust-whoami" ,rust-whoami-0.9)
         ("rust-xdg" ,rust-xdg-2))
        #:cargo-development-inputs
        (("rust-env-logger" ,rust-env-logger-0.7))))
    (home-page "https://github.com/Spotifyd/spotifyd")
    (synopsis "Open source Spotify client running as a UNIX daemon")
    (description "Spotifyd streams music just like the official
 client, but is more lightweight and supports more platforms. Spotifyd
 also supports the Spotify Connect protocol, which makes it show up as
 a device that can be controlled from the official clients.")
    (license license:gpl3)))
