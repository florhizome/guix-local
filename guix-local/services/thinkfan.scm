(define-module (guix-local services thinkfan)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services configuration)
  #:use-module (gnu services linux)
  #:use-module (gnu services dbus)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages linux)
  #:export (thinkfan-service-type
            thinkfan-configuration
            ;;%default-thinkfan-configuration-file
            %tf-config-tpx230))



(define %tf-config-tpx230
  (plain-file "thinkfan.conf"
              "sensors:
- hwmon: /sys/class/thermal/thermal_zone0/temp
fans:
- tpacpi: /proc/acpi/ibm/fan
#- hwmon: /sys/devices/platform/thinkpad_hwmon/hwmon/hwmon5/pwm1 we can't use
levels when using this interface
- [0, 0, 60]
- [1, 53, 65]
- [2, 55, 66]
- [3, 57, 68]
- [4, 61, 70]
- [5, 64, 71]
- [7, 68, 32767]
- [\"level full-speed\", 68, 32767]"))


;;(define (%default-thinkfan-config-file thinkfan-package)
;;  (file-append thinkfan-package "share/doc/thinkfan/thinkfan.yaml"))

(define-record-type* <thinkfan-configuration>
  thinkfan-configuration make-thinkfan-configuration
  thinkfan-configuration?
  (thinkfan-package   thinkfan-configuration-thinkfan ;file-like
                      (default thinkfan))
  (config-file         thinkfan-configuration-file ;string 
                          (default %tf-config-tpx230)))


;;this places a file in /etc/modprobe.d that enables fan control
;;another method is setting "thinkpad_acpi.fan_control=1" via kernel-command-line
;;(TODO test if that's actually the only one)
(define (thinkfan-etc-service config)  
   `(("modprobe.d/thinkfan.conf"
      ,(plain-file "thinkfan.conf" "options thinkpad_acpi fan_control=1"))))

(define (thinkfan-shepherd-service thinkfan-configuration)
  (match-record thinkfan-configuration <thinkfan-configuration>
    (thinkfan-package config-file)
    (list
     (shepherd-service
      (documentation "Run thinkfan daemon.")
      (provision '(thinkfan))
      (requirement '(file-systems udev))
      (start
       #~(make-forkexec-constructor
          (list #$(file-append thinkfan-package "/sbin/thinkfan") "-c" #$config-file)))
      (stop #~(make-kill-destructor))
;;      (actions
;;       (list
;;        (shepherd-action
;;         (name 'test)
;;         (documentation "Start thinkfan with chosen configuration in non-daemon mode, logging to console.")
;;         (procedure
;;          #~(lambda _
;;              (invoke
;;               #$(file-append thinkfan-package "/sbin/thinkfan" "-c" #$config-file "-n")))))))
      ))))


(define thinkfan-service-type
  (service-type
   (name 'thinkfan)
   (extensions
    (list
     (service-extension shepherd-root-service-type thinkfan-shepherd-service)
     (service-extension etc-service-type thinkfan-etc-service)
     (service-extension profile-service-type
                        (compose list thinkfan-configuration-thinkfan))))
   (description "Run and control thinkfan")
   (default-value (thinkfan-configuration))))
