(define-module (guix-local packages  nemo)
  #:use-module (gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages file)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pantheon)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages xfce)
  #:use-module (gnu packages cinnamon)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages flex))

(define-public cinnamon-desktop-5.4
  (package
    (inherit cinnamon-desktop)
    (name "cinnamon-desktop-5.4")
    (version "5.4.2")
    (source
     (origin
       (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/linuxmint/cinnamon-desktop")
                     (commit version)))
       (sha256
        (base32 "03yfh1fhcs9g4qxczq1k2fi0c9354ryapvhg24210scgds4ljkjk"))))
    (build-system meson-build-system)
    (native-inputs (append (package-native-inputs cinnamon-desktop)
                           `(("gobject-introspection" ,gobject-introspection)
                             ("intltool" ,intltool))))))

(define-public xapp
  (package
   (name "xapp")
   (version "2.2.15")
   (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/linuxmint/xapp")
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1rjlrbaf4c02viwbp1vxhh4nsv9zbvlsmrvry3af9grz0rfv3xsz"))))
   (build-system meson-build-system)
   (arguments
    `(#:glib-or-gtk? #t
      #:configure-flags
      (list
       (string-append "-Dpy-overrides-dir="
                      (assoc-ref %build-inputs "python")
                      "/lib/python3.9/site-packages/gi/overrides"))
      #:phases
      (modify-phases %standard-phases
        (add-after 'patch-source-shebangs 'fix-build
          (lambda* (#:key inputs outputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out"))
                   (py (assoc-ref inputs "python"))
                   (pygobject (assoc-ref inputs "python-pygobject")))
              (setenv "PYTHON_PATH"
                      (string-append py "/lib/python3.9:"
                                     (string-append pygobject "/lib/python3.9:")))
              (substitute*
                  "./libxapp/meson.build"
                       (("gtk3_dep\\.get_pkgconfig_variable\\('libdir'\\)")
                        (string-append "'" out "/lib'")))
              ;;for some reason
              (substitute*
                  "./pygobject/meson.build"
                       (("install_dir: override_dir,")
                        (string-append "install_dir: '" out
                                       "/lib/python3.9/site-packages/gi/overrides',")))
              #t))))))
   (propagated-inputs (list libgnomekbd))
   (native-inputs (list pkg-config python gettext-minimal libxml2 vala
                        `(,glib "bin") gobject-introspection))
   (inputs (list glib gtk+ gnome-menus libdbusmenu python-pygobject
                 libgnomekbd python atk))
   (home-page "https://github.com/linuxmint/xapp")
   (synopsis "Cross-desktop libraries and common resources of the Mint project")
   (description "This project gathers the components which are common to multiple GTK
 desktop environments (Cinnamon, MATE and Xfce) and
 required to implement cross-DE solutions.")
   (license license:expat)))

(define %nemo-extension-patch
  (local-file (search-patch "nemo_extension_dir.patch")))

(define-public nemo
  (package
    (name "nemo")
    (version "5.4.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/nemo")
         (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1r3zjkpmisdi54kkq6fvaslagzpbbz4swjlali90lwsfbb9cwykz"))))
    (build-system meson-build-system)
    (arguments
     (list #:glib-or-gtk? #t
           #:tests? #f                  ;tests stall after first one
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'make-files-writable
                 (lambda* (#:key inputs #:allow-other-keys)
                   (let ((patch-file
                          #$(local-file 
                             (search-patch "nemo_extension_dir.patch"))))
                     (make-file-writable "libnemo-private/nemo-module.c")
                     (invoke "patch""-f" "-p1" patch-file)))) 
               (add-after 'patch-source-shebangs 'adjust-prefix
                 (lambda*
                     (#:key outputs #:allow-other-keys)
                   (let ((out (assoc-ref outputs "out")))
                     (substitute*
                         (find-files "meson.build")
                       (("'data_dir")
                        (string-append "'" out "/share"))))
                   #t)))))
    (native-inputs
     (list pkg-config `(,gtk+ "bin") gettext-minimal
           intltool `(,glib "bin")
           gobject-introspection %nemo-extension-patch))
    (inputs
     (list libnotify libxml2 gsettings-desktop-schemas libgsf libx11
           gtk+ cinnamon-desktop-5.4 xkeyboard-config
           libxkbfile
           xapp
           libexif
           exempi
           atk
           tracker))
    (native-search-paths
     (list
      (search-path-specification
       (variable "NEMO_EXTENSION_DIR")
       (files '("lib/nemo/extensions")))))
    (search-paths native-search-paths)
    (home-page "https://github.com/linuxmint/nemo")
    (synopsis "File browser for Cinnamon")
    (description "Nemo is the file manager for the Cinnamon desktop
 environment.")
    (license license:expat)))


;; TODO don't rely on python version in configure-flags and phases.
;; one approach maybe by running find files for /gi/overrides on python input dir,
;; then substract pythons store dir

 (define-public nemo-extensions-python
  (package
    (name "nemo-extensions-python")
    (version "5.0.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/nemo-extensions")
         (commit (string-append "nemo-python-" version))))
       (sha256
        (base32 "1wji2h1v73xnqpxw54i8mi7ma4zi6y2pdy867pcgii4ixpziq1gh"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'chdir
           (lambda _
             (delete-file-recursively (string-append (getcwd) "/build"))
             (chdir (string-append (getcwd) "/nemo-python"))))
         (add-after
               'chdir 'fix-install-dir
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (py (assoc-ref inputs "python"))
                      (pygobject (assoc-ref inputs "python-pygobject")))
                      ;;subprojects are in ./subprojects/projectname/meson.build
                 (substitute* "meson.build"
                   (("join_paths\\(get_option\\('libdir'\\), 'pkgconfig'\\),")
                    (string-append "'" out "/lib/nemo/extensions'")))
                 (substitute* (list "meson.build" "./src/meson.build")
                   (("nemo\\.get_pkgconfig_variable\\('extensiondir'\\)")
                    (string-append "'" out "/lib/nemo/extensions'"))) 
                 #t))))))
    (native-inputs
     (list pkg-config `(,gtk+ "bin") python `(,glib "bin")
           gobject-introspection intltool))
    (inputs (list nemo glib python
                  python-pygobject xapp gtk+))
    (native-search-paths
     (list
     (search-path-specification
      (variable "NEMO_EXTENSION_DIR")
      (files '("lib/nemo/extensions")))))
    (search-paths native-search-paths)
    (home-page "https://github.com/linuxmint/nemo")
    (synopsis "libnemo-extension Python bindings")
    (description "These are unstable bindings for the Nemo extension library.

For examples and documentation check the examples sub directory.

As of nemo-python 0.7.0, scripts are also loaded from $XDG_DATA_DIR/nemo-python/extensions,
which includes ~/.local/share and /usr/share (or whatever your $XDG_DATA_DIR is
set to).")
    (license license:expat)))

(define-public nemo-extensions-preview
  (package
    (name "nemo-extensions-preview")
    (version "5.2.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/nemo-extensions")
         (commit (string-append "nemo-preview-" version))))
       (sha256
        (base32 "17kj6zhxrpw67iss9pgw31rg4zpxjav9gwdf39h3bhpq6pbj4pk1"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'chdir
           (lambda _
             (delete-file-recursively (string-append (getcwd) "/build"))
             (chdir (string-append (getcwd) "/nemo-preview")))))))
    (native-search-paths
     (list
     (search-path-specification
      (variable "NEMO_EXTENSION_DIR")
      (files '("lib/nemo/extensions")))))
    (native-inputs
     (list pkg-config gobject-introspection))
    (inputs (list nemo glib
                  clutter clutter-gst clutter-gtk gstreamer gst-plugins-base xapp gtk+))
    (home-page "https://github.com/linuxmint/nemo")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public nemo-extensions-share
  (package
    (name "nemo-extensions-share")
    (version "5.2.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/nemo-extensions")
         (commit  version)))
       (sha256
        (base32 "09kpwylj8bnnm46i0b50bl5y7fqv3w093bsgrkwzdgjgm1j85lxw"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'chdir
           (lambda _
             (delete-file-recursively (string-append (getcwd) "/build"))
             (chdir (string-append (getcwd) "/nemo-share"))))
       (add-after
               'chdir 'fix-install-dir
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out")))
                 ;;subprojects are in ./subprojects/projectname/meson.build
                 (substitute* (find-files "." "meson.build")
                   (("libnemo\\.get_pkgconfig_variable\\('extensiondir'\\)")
                    (string-append "'" out "/lib/nemo'")))
                 #t))))))
    (native-search-paths
     (list
     (search-path-specification
      (variable "NEMO_EXTENSION_DIR")
      (files '("lib/nemo")))))
    (native-inputs
     (list pkg-config gobject-introspection))
    (inputs (list glib cinnamon-desktop-5.4 libxkbfile
                  clutter clutter-gst clutter-gtk xapp gtk+))
    
    (propagated-inputs (list nemo gstreamer
                  gst-plugins-base ))
    (home-page "https://github.com/linuxmint/nemo-extensions")
    (synopsis "")
    (description "")
    (license license:expat)))


(define-public nemo-extensions-terminal
  (package
    (name "nemo-extensions-terminal")
    (version "5.4.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/nemo-extensions")
         (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "01p4qhj0fcn7mfn8mwkzkyv2q4ncargrr0rjjlw440x31hs8sbsh"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'chdir
           (lambda _
             (delete-file-recursively (string-append (getcwd) "/build"))
             (chdir (string-append (getcwd) "/nemo-terminal"))))
         (add-after
             'chdir 'fix-install-dir
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               ;;subprojects are in ./subprojects/projectname/meson.build
               (substitute* "./setup.py"
                 (("/usr") out))
               #t))))))
    (native-search-paths
     (list
     (search-path-specification
      (variable "NEMO_EXTENSION_DIR")
      (files '("lib/nemo")))
     (search-path-specification
      (variable "PYTHON_EXTENSION_DIR")
      (files '("share/nemo-python")))))
    (native-inputs (list gobject-introspection pkg-config))
    (propagated-inputs (list python python-pygobject python-pyxdg xapp
                  nemo-extensions-python vte nemo))
    (home-page "https://github.com/linuxmint/nemo-extensions/nemo-terminal")
    (synopsis "")
    (description "")
    (license license:expat)))


(define-public xfce4-xapp-status-plugin
  (package
    (name "xfce4-xapp-status-plugin")
    (version "0.2.6")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linuxmint/xfce4-xapp-status-plugin")
         (commit version)))
       (sha256
        (base32
         "0c3hc40hn1sy5klhx8d70hqihpbxjfqi6h1caaa0q6yg40f8jvbg"))
       ))
    (build-system glib-or-gtk-build-system)
    (native-inputs (list pkg-config `(,glib "bin") gobject-introspection))
    (inputs
     (list xapp glib))
    (native-search-paths
     (list (search-path-specification
            (variable "X_XFCE4_LIB_DIRS")
            (files '("lib/xfce4")))))
    (home-page "https://www.xfce.org/")
    (synopsis "Xfce desktop panel")
    (description
     "Desktop panel for Xfce, which contains program launchers, window buttons,
applications menu, workspace switcher and more.")
    ;; Libraries are under LGPLv2.1+, and programs under GPLv2+.
    (license (list license:gpl2+ license:lgpl2.1+))))
