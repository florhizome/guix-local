(define-module (guix-local packages  enlightenment-theme-eminence)
  #:use-module (gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages enlightenment)

  #:use-module (gnu packages build-tools))


  (define-public enlightenment-theme-eminence
    (package
     (name "enlightenment-theme-eminence")
     (version "1.18")
     (source
     (origin
      (method url-fetch)
      (uri "https://github.com/Obsidian-StudiosInc/eminence/archive/refs/tags/v0.1.13.tar.gz")
      (sha256
       (base32 "07bapx4kdsmg7yks2655b0dxzy7jlg6x0dv53fzsmk6arr841j9r"))))
     (build-system gnu-build-system)
     (arguments `(#:tests? #f 
                  #:phases (modify-phases %standard-phases
                                          (delete 'configure)
                                          (delete 'build)
                                          (add-after 'patch-source-shebangs 'build-script
                                                     (lambda _
                                                     (chmod "eminence.sh"  #o555)
                                                     (system "./eminence.sh"))))))
     (native-inputs (list efl))
     (home-page "")
     (synopsis "")
     (description"")
     (license "")))

enlightenment-theme-eminence
  
