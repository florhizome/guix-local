(define-module (guix-local services clight)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services configuration)
  #:use-module (gnu services linux)
  #:use-module (gnu services dbus)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages linux)
  #:use-module (guixrus packages clight)
  #:export (clightd-configuration
            clightd-configuration?
            clightd-service-type))



(define-record-type* <clightd-configuration>
  clightd-configuration make-clightd-configuration
  clightd-configuration?
  (clightd clight-configuration-clightd ;file-like
           (default clightd)))

(define (clightd-shepherd-service-type config)
  (match-record config <clightd-configuration>
    (clightd)
    (list (shepherd-service
           (documentation "Run clightd daemon.")
           (provision '(clightd))
           (requirement '(elogind dbus-system)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append clightd "/libexec/clightd"))))
           (stop #~(make-kill-destructor))))))

(define clight-service-type
  (service-type
   (name 'clight)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list clight-configuration-clightd))
          (service-extension polkit-service-type
                             (compose list clight-configuration-clightd))
          (service-extension shepherd-root-service-type
                             clightd-shepherd-service-type)
          (service-extension profile-service-type
                             (compose list clight-configuration-clightd))))   
   (description "This will simply launch the clightd daemon. The
actual configuration and action has to be done from the user side.")
   (default-value (clightd-configuration))))
