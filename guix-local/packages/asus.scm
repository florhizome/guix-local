(define-module (guix-local packages asus)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system linux-module)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)  
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages glib))


(define-public rust-evdev-sys-0.2
  (package
    (name "rust-evdev-sys")
    (version "0.2.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "evdev-sys" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vgax74wjsm22nrx6ikh8m7bj3nggf83s961i5qd85bvahmx9shl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/ndesh26/evdev-rs")
    (synopsis "Raw bindings to libevdev

High level Rust bindings are available in the `evdev` crate
")
    (description "Raw bindings to libevdev

High level Rust bindings are available in the `evdev` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-evdev-rs-0.5
  (package
    (name "rust-evdev-rs")
    (version "0.5.0")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "evdev-rs" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1kv7appqdpwpkd66yskkc46wbmqlaix8vmlddrp5l85kysmm3nyp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-evdev-sys" ,rust-evdev-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/ndesh26/evdev-rs")
    (synopsis
     "Bindings to libevdev for interacting with evdev devices. It moves the
common tasks when dealing with evdev devices into a library and provides
a library interface to the callers, thus avoiding erroneous ioctls, etc.
")
    (description
     "Bindings to libevdev for interacting with evdev devices.  It moves the common
tasks when dealing with evdev devices into a library and provides a library
interface to the callers, thus avoiding erroneous ioctls, etc.")
    (license (list license:expat license:asl2.0))))

(define-public rust-i2cdev-0.5
  (package
    (name "rust-i2cdev")
    (version "0.5.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "i2cdev" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0n4kydva33jl1vxcgz25dqyfsgpn1nzk3lagwpnqircw3qs63zmq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       ;;#:cargo-build-flags '("--features" "debug_all")
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-nix" ,rust-nix-0.23))
       #:cargo-development-inputs (("rust-docopt" ,rust-docopt-1))))
    (home-page "https://github.com/rust-embedded/rust-i2cdev")
    (synopsis "Provides API for safe access to Linux i2c device interface.
")
    (description
     "This package provides API for safe access to Linux i2c device interface.")
    (license (list license:expat license:asl2.0))))

(define-public rust-asus-numpad
  (package
    (name "rust-asus-numpad")
    (version "0.4.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/iamkroot/asus-numpad")
         (commit "4f4a20c68b5d11046283a7de282c26efd3682072")))
       (file-name (git-file-name name version))
        (sha256
         (base32 "0prbn8x42xqjni6bz3jb9cdd0hgvfh59ih396s23syspind8sk8c"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (propagated-inputs (list libevdev))
    ;;FIXME this feels hacky 
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
                       (add-after 'unpack 'enable-unstable-features
                                  (lambda _
                                    (setenv "RUSTC_BOOTSTRAP" "1")
                                    #t)))
       #:cargo-inputs
       `(("rust-anyhow" ,rust-anyhow-1)
        ("rust-clap" ,rust-clap-2)
        ("rust-serde-derive" ,rust-serde-derive-1)
        ("rust-env-logger" ,rust-env-logger-0.9)
        ("rust-libc" ,rust-libc-0.2)
        ("rust-evdev-rs" ,rust-evdev-rs-0.5)
        ("rust-toml" ,rust-toml-0.5)
        ("rust-i2cdev" ,rust-i2cdev-0.5)
        ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/iamkroot/asus-numpad")
    (synopsis "Linux daemon for Asus laptops to activate numpad on touchpad")
    (description "Linux tool to allow using the numpad that is overlayed on
 various Asus Laptop touchpads.")
    (license (list license:expat))))

