(define-module (guix-local packages rclone)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system go)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages syncthing)
  #:use-module (guix-local packages golang)
  #:use-module (gnu packages golang))


(define-public go-github-com-stackexchange-wmi
  (package
    (name "go-github-com-stackexchange-wmi")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/StackExchange/wmi")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ijhmr8sl768vkxslvw7fprav6srw4ivp32rzg3ydj8nc1wh86nl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/StackExchange/wmi"))
    (propagated-inputs
      `(("go-github-com-go-ole-go-ole" ,go-github-com-go-ole-go-ole)))
    (home-page "https://github.com/StackExchange/wmi")
    (synopsis "wmi")
    (description "Package wmi provides a WQL interface for WMI on Windows.")
    (license license:expat)))


(define-public go-github-com-rclone-ftp
  (package
    (name "go-github-com-rclone-ftp")
    (version "1.0.0-210902h")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rclone/ftp")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "024wvsx55j4anykhl2hncqi8z8xh25k21hhssgym7yc7h5lh8kfq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rclone/ftp"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/rclone/ftp")
    (synopsis "goftp")
    (description
      "Package ftp implements a FTP client as described in
@url{https://rfc-editor.org/rfc/rfc959.html,RFC 959}.")
    (license license:isc)))



(define-public rclone
  (package
    (name "rclone")
    (version "1.58.0")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/rclone/rclone")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "15yjispkvwsfwz4xbnj5rrnpic09by2qmm1y360av2lyga5rf8nc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rclone/rclone/cmd"
                 #:unpack-path "github.com/rclone/rclone"
                 #:phases
                 (modify-phases %standard-phases
                   (replace 'build
                     (lambda* (#:key import-path build-flags #:allow-other-keys)
              (with-throw-handler #t 
                  (lambda _
                    (apply invoke "go" "install" "-ldflags=-s -w"
                           (cons import-path build-flags)))
                (lambda (key . args)
                  (display (string-append "Building '" import-path "' failed.\n\"
Here are the results of `go env`:\n"))
                  (invoke "go" "env"))))))))
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs
     `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
       ("go-golang-org-x-term" ,go-golang-org-x-term)
       ("go-github-com-rivo-uniseg" ,go-github-com-rivo-uniseg)
       ("go-github-com-prometheus-procfs" ,go-github-com-prometheus-procfs)
       ("go-github-com-prometheus-common" ,go-github-com-prometheus-common)
       ("go-github-com-prometheus-client-model"
        ,go-github-com-prometheus-client-model)        
       ("go-github-com-kr-fs" ,go-github-com-kr-fs)
       ("go-golang-org-x-time" ,go-golang-org-x-time)
       ("go-golang-org-x-text" ,go-golang-org-x-text)
       ("go-golang-org-x-sys" ,go-golang-org-x-sys)
       ("go-golang-org-x-sync" ,go-golang-org-x-sync)
       ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
       ("go-golang-org-x-net" ,go-golang-org-x-net)
       ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
       ("go-github-com-google-go-querystring"
        ,go-github-com-google-go-querystring)
       ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
       ("go-github-com-skratchdot-open-golang"
        ,go-github-com-skratchdot-open-golang)
       ("go-github-com-spf13-pflag" ,go-github-com-spf13-pflag)
       ("go-github-com-spf13-cobra" ,go-github-com-spf13-cobra)
       ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
       ("go-github-com-azure-go-ntlmssp" ,go-github-com-azure-go-ntlmssp)
       ("go-github-com-aws-aws-sdk-go" ,go-github-com-aws-aws-sdk-go)
       ("go-github-com-klauspost-compress" ,go-github-com-klauspost-compress)
       ("go-github-com-mattn-go-runewidth" ,go-github-com-mattn-go-runewidth)
       ("go-github-com-mattn-go-colorable" ,go-github-com-mattn-go-colorable)
       ("go-github-com-mitchellh-go-homedir"
        ,go-github-com-mitchellh-go-homedir)
       ("go-github-com-prometheus-client-golang"
         ,go-github-com-prometheus-client-golang)
       ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
       ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
       ("go-github-com-google-uuid" ,go-github-com-google-uuid)
       ("go-github-com-russross-blackfriday-v2"
         ,go-github-com-russross-blackfriday-v2)
       ("go-github-com-cpuguy83-go-md2man-v2"
         ,go-github-com-cpuguy83-go-md2man-v2)
       ("go-github-com-gogo-protobuf" ,go-github-com-gogo-protobuf)
        ("go-github-com-cespare-xxhash-v2" ,go-github-com-cespare-xxhash-v2)
        ;;("go-go-opencensus-io" ,go-go-opencensus-io)        
        ;;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ;;("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ;;("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
        ;;("go-storj-io-drpc" ,go-storj-io-drpc)
        ;;("go-storj-io-common" ,go-storj-io-common)        
        ;;("go-github-com-mattn-go-ieproxy" ,go-github-com-mattn-go-ieproxy)
        ("go-github-com-googleapis-gax-go-v2"
         ,go-github-com-googleapis-gax-go-v2)
        ;;("go-github-com-golang-jwt-jwt-v4" ,go-github-com-golang-jwt-jwt-v4)
        ;;("go-github-com-go-ole-go-ole" ,go-github-com-go-ole-go-ole)
        ;;("go-github-com-inconshreveable-mousetrap"
        ;; ,go-github-com-inconshreveable-mousetrap)
        ("go-github-com-azure-azure-storage-blob-go"
         ,go-github-com-azure-azure-storage-blob-go)
        ("go-github-com-azure-azure-pipeline-go"
         ,go-github-com-azure-azure-pipeline-go)
        ("go-cloud-google-com-go" ,go-cloud-google-com-go)
        ("go-google-golang-org-api" ,go-google-golang-org-api)
        
        ;;("go-github-com-tklauser-numcpus" ,go-github-com-tklauser-numcpus)
        ;;
        ;;("go-github-com-tklauser-go-sysconf"
        ;; ,go-github-com-tklauser-go-sysconf)
        ;;("go-github-com-spacemonkeygo-monkit-v3"
        ;; ,go-github-com-spacemonkeygo-monkit-v3)
        ;;("go-github-com-jmespath-go-jmespath"
        ;; ,go-github-com-jmespath-go-jmespath)
        ;;("go-github-com-hashicorp-go-uuid" ,go-github-com-hashicorp-go-uuid)
        ("go-github-com-golang-groupcache" ,go-github-com-golang-groupcache)
        ;;("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-azure-go-autorest" ,go-github-com-azure-go-autorest)
        ;;("go-github-com-stackexchange-wmi" ,go-github-com-stackexchange-wmi)
        ;;("go-github-com-zeebo-errs" ,go-github-com-zeebo-errs)        
        ;;("go-github-com-calebcase-tmpfile" ,go-github-com-calebcase-tmpfile)
        ;;("go-github-com-btcsuite-btcutil" ,go-github-com-btcsuite-btcutil)
        ("go-github-com-rclone-ftp" ,go-github-com-rclone-ftp)
        ("go-github-com-coreos-go-systemd" ,go-github-com-coreos-go-systemd)
        ("go-github-com-coreos-go-semver" ,go-github-com-coreos-go-semver)
        ("go-goftp-io-server" ,go-goftp-io-server)
        ("go-storj-io-uplink" ,go-storj-io-uplink)
        ("go-github-com-putdotio-go-putio"
         ,go-github-com-putdotio-go-putio)
        ("go-github-com-shirou-gopsutil-v3" ,go-github-com-shirou-gopsutil-v3)
        ("go-github-com-iguanesolutions-go-systemd-v5"
         ,go-github-com-iguanesolutions-go-systemd-v5)
        ("go-github-com-microsoft-go-winio" ,go-github-com-microsoft-go-winio)
        ("go-github-com-dropbox-dropbox-sdk-go-unofficial-v6"
         ,go-github-com-dropbox-dropbox-sdk-go-unofficial-v6)
        ;;
        ("go-github-com-lufia-plan9stats" ,go-github-com-lufia-plan9stats)
        ;;("go-github-com-jcmturner-rpc-v2" ,go-github-com-jcmturner-rpc-v2)
        ;;("go-github-com-jcmturner-goidentity-v6"
        ;; ,go-github-com-jcmturner-goidentity-v6)
        ;;("go-github-com-jcmturner-gofork" ,go-github-com-jcmturner-gofork)
        ;;("go-github-com-jcmturner-dnsutils-v2"
        ;; ,go-github-com-jcmturner-dnsutils-v2)
        ;;("go-github-com-jcmturner-aescts-v2"
        ;; ,go-github-com-jcmturner-aescts-v2)
        ("go-github-com-beorn7-perks" ,go-github-com-beorn7-perks)
        ("go-go-etcd-io-bbolt" ,go-go-etcd-io-bbolt)
        ("go-github-com-yunify-qingstor-sdk-go-v3"
         ,go-github-com-yunify-qingstor-sdk-go-v3)
        ;;("go-github-com-pengsrc-go-shared" ,go-github-com-pengsrc-go-shared)
        ("go-github-com-vivint-infectious" ,go-github-com-vivint-infectious)
        ("go-github-com-youmark-pkcs8" ,go-github-com-youmark-pkcs8)
        ("go-github-com-xanzy-ssh-agent" ,go-github-com-xanzy-ssh-agent)
        ("go-github-com-t3rm1n4l-go-mega" ,go-github-com-t3rm1n4l-go-mega)
        ("go-github-com-rfjakob-eme" ,go-github-com-rfjakob-eme)
        ("go-github-com-pkg-sftp" ,go-github-com-pkg-sftp)
        ("go-github-com-patrickmn-go-cache" ,go-github-com-patrickmn-go-cache)
        ("go-github-com-nsf-termbox-go" ,go-github-com-nsf-termbox-go)
        ("go-github-com-ncw-swift-v2" ,go-github-com-ncw-swift-v2)
        ("go-github-com-ncw-go-acd" ,go-github-com-ncw-go-acd)
        ("go-github-com-koofr-go-koofrclient"
         ,go-github-com-koofr-go-koofrclient)
        ("go-github-com-jzelinskie-whirlpool"
         ,go-github-com-jzelinskie-whirlpool)
        ;;("go-github-com-jcmturner-gokrb5-v8"
        ;; ,go-github-com-jcmturner-gokrb5-v8)
        ("go-github-com-hanwen-go-fuse-v2" ,go-github-com-hanwen-go-fuse-v2)
        ("go-github-com-go-chi-chi-v5" ,go-github-com-go-chi-chi-v5)
        ("go-github-com-gabriel-vasile-mimetype"
         ,go-github-com-gabriel-vasile-mimetype)
        ("go-github-com-dop251-scsu" ,go-github-com-dop251-scsu)
        ("go-github-com-colinmarc-hdfs-v2" ,go-github-com-colinmarc-hdfs-v2)
        
        ("go-github-com-billziss-gh-cgofuse"
         ,go-github-com-billziss-gh-cgofuse)
        ("go-github-com-atotto-clipboard" ,go-github-com-atotto-clipboard)
        ("go-github-com-artyom-mtab" ,go-github-com-artyom-mtab)
        ("go-github-com-anacrolix-dms" ,go-github-com-anacrolix-dms)
        ("go-github-com-abbot-go-http-auth" ,go-github-com-abbot-go-http-auth)
        ("go-github-com-aalpar-deheap" ,go-github-com-aalpar-deheap)
        ("go-github-com-a8m-tree" ,go-github-com-a8m-tree)
        ("go-github-com-unknwon-goconfig" ,go-github-com-unknwon-goconfig)
        ("go-github-com-max-sum-base32768" ,go-github-com-max-sum-base32768)
        ("go-bazil-org-fuse" ,go-bazil-org-fuse)
        ("go-github-com-buengese-sgzip" ,go-github-com-buengese-sgzip)
        ("go-github-com-matttproud-golang-protobuf-extensions-pbutil"
         ,go-github-com-matttproud-golang-protobuf-extensions-pbutil)))
    (synopsis "@code{rsync} for cloud storage")
    (description "@code{Rclone} is a command line program to sync files and
directories to and from different cloud storage providers.

Features include:
@itemize
@item MD5/SHA1 hashes checked at all times for file integrity
@item Timestamps preserved on files
@item Partial syncs supported on a whole file basis
@item Copy mode to just copy new/changed files
@item Sync (one way) mode to make a directory identical
@item Check mode to check for file hash equality
@item Can sync to and from network, e.g., two different cloud accounts
@item Optional encryption (Crypt)
@item Optional cache (Cache)
@item Optional FUSE mount (rclone mount)
@end itemize")
    (home-page "https://rclone.org/")
    (license license:expat)))
