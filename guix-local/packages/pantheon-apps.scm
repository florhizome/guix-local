;;; Copyright © 2021 florhizome <florhizome@posteo.net>
;;; Copyright © 2020 Ryan Prior <rprior@protonmail.com>
;;; Copyright © 2020 L  p R n  d n <guix@lprndn.info>
;;; Copyright © 2020 pkill9 <pkill9@runbox.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-local packages  pantheon-apps)
  #:use-module (gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages file)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pantheon)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages text-editors)
  #:use-module (gnu packages code)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages iso-codes)
  #:use-module (gnu packages language)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages sqlite)
#:use-module (guix-local packages pantheon))


(define-public elementary-camera
  (package
    (name "elementary-camera")
    (version "6.2.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/camera")
         (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0rclcbls0bvq6y6s6nkrx9xcc1bj9j6kama2r7v0na865n9qibpk"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("goject-introspection" ,gobject-introspection)
       ("desktop-file-utils" ,desktop-file-utils)
       ("gettext" ,gettext-minimal)
       ("glib:bin" ,glib "bin")
       ("vala" ,vala)))
    (inputs
     (list gstreamer
           gst-plugins-base
           gtk+
           granite
           libcanberra
           libhandy
           libgee
           libhandy))
    (home-page "https://github.com/elementary/camera")
    (synopsis "Camera app designed for pantheon")
    (description "The camera app designed for pantheon elementary OS.")
    (license license:gpl3)))

(define-public elementary-music
  (package
    (name "elementary-music")
    (version "7.0.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/music")
         (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1p3fgk2sf8p5p9i6vimbiz3s1bhcjkxdnn90g45dr9ixq80262vv"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("goject-introspection" ,gobject-introspection)
       ("desktop-file-utils" ,desktop-file-utils)
       ("gettext" ,gettext-minimal)
       ("glib:bin" ,glib "bin")
       ("vala" ,vala)))
    (inputs
     (list
      libadwaita
      gstreamer
      gst-plugins-base
      gtk
      granite-next
      libcanberra))
    (home-page "https://github.com/elementary/music")
    (synopsis "")
    (description "The camera app designed for elementary OS")
    (license license:gpl3)))

(define-public elementary-screenshot
  (package
    (name "elementary-screenshot")
    (version "6.0.2")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/screenshot")
         (commit version)))
       (sha256
        (base32
         "1k7rv42znwdxj8zcxcbqgx43d0ai8r9igxwkcz3qkrfn5zqg9qlz"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("goject-introspection" ,gobject-introspection)
       ("desktop-file-utils" ,desktop-file-utils)
       ("gettext" ,gettext-minimal)
       ("glib:bin" ,glib "bin")
       ("vala" ,vala)))
    (inputs (list libcanberra granite libgee gtk+ libhandy
                  gdk-pixbuf))
    (home-page "https://github.com/elementary/screenshot")
    (synopsis "Screenshot tool designed for elementary OS")
    (description "Desktop calendar app designed for elementary OS.")
    (license license:gpl3+)))


(define-public elementary-code
  (package
    (name "elementary-code")
    (version "6.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/code")
         (commit version)))
       (sha256
        (base32
         "0g268y8k09bqapl1p5amwwl1m73r7r44vxwv3gvzv1gnz1q8qy81"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (inputs
     (list glib
           libhandy
           gsettings-desktop-schemas
           gtkspell3
           vte
           gtk+
           granite
           gtksourceview-4
           json-glib
           libgee
           libgit2-glib
           libpeas
           libsoup-minimal-2
           libxml2))
    (native-inputs
     (list appstream-glib
           desktop-file-utils           
           editorconfig-core-c
           (list glib "bin")
           (list gtk+ "bin")
           gobject-introspection
           intltool
           pkg-config
           python 
           polkit
           universal-ctags
           vala))
    (home-page "")
    (synopsis "")
    (description
     "")
    (license license:gpl3+)))

(define-public elementary-mail
  (package
    (name "elementary-mail")
    (version "6.4.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/elementary/mail")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1rsq9fsk77c5wspx2w0464ls453yrw9p5nn72njs20hyr0s9b2m2"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (inputs (list
             granite
             folks
             glib
             gmime
                  `(,gnome-online-accounts "lib")

                  gsettings-desktop-schemas gspell gsound gtk+
                  iso-codes

                  json-glib libcanberra sqlite libgdata libgee libhandy
                  sqlite webkitgtk-with-libsoup2 evolution-data-server))
    (native-inputs (list appstream-glib cmake-minimal desktop-file-utils `(,glib "bin")
                         gmime gobject-introspection gsettings-desktop-schemas itstool
                         libarchive libxml2 pkg-config vala xorg-server-for-tests))
    (synopsis "")
    (description "")
    (home-page "https://github.com/elementary/code")
    (license (list
              license:lgpl2.1+
              ;; icons
              license:cc-by3.0
              license:cc-by-sa3.0
              license:public-domain
              ;; snowball
              license:bsd-2))))


(define-public elementary-calendar
  (package
    (name "elementary-calendar")
    (version "6.1.0")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/elementary/calendar")
        (commit version)))
      (sha256 (base32
               "0i5lhsibl5kj7syrb58yyn83nxq2z1rvij025r94glfw0bnlk99d"))))
    (build-system meson-build-system)
    (arguments
     `(#:tests? #f
       #:glib-or-gtk? #t))
    (native-inputs
     (list pkg-config gobject-introspection desktop-file-utils
           gettext-minimal vala))
    (inputs
     (list evolution-data-server clutter folks geoclue geocode-glib
           libhandy granite libchamplain libical libgee gtk+ libnotify))
    (home-page "https://github.com/elementary/calendar")
    (synopsis "Desktop calendar app designed for elementary OS")
    (description "Desktop calendar app designed for elementary OS.")
    (license license:gpl3)))

(define-public elementary-files
  (package
    (name "elementary-files")
    (version "6.1.2")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/elementary/files")
        (commit version)))
      (sha256
       (base32 "0lawgvc8fv7a6fhmmnhyl3rxfpw9jiqyjz8bw5jfg3p3jp03in43"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:tests? #f
       #:configure-flags
       (list "-Dsystemduserunitdir=no")))
    (native-inputs
     (list pkg-config gobject-introspection glib-networking desktop-file-utils
           gettext-minimal `(,glib "bin") `(,gtk+ "bin") vala))
    (inputs
     (list bamf
           granite
           gtk+
           libgee
           libcanberra
           libcloudproviders 
           libdbusmenu
           libgit2-glib
           libnotify
           libhandy
           pango
           plank
           sqlite
           zeitgeist))
    (home-page "https://github.com/elementary/files")
    (synopsis "File browser designed for elementary OS")
    (description "File browser app designed for elementary OS.")
    (license license:gpl3)))

(define-public elementary-tasks
  (package
    (name "elementary-tasks")
    (version "6.2.0")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/elementary/tasks")
        (commit version)))
      (sha256 (base32 "1kgwb9dzfqc0jmhgmbhvzd4dp2xh2rfbhyf4z534x4jbgdg9cxkq"))))
    (build-system meson-build-system)
    (arguments
     `(#:tests? #f
       #:glib-or-gtk? #t))
    (native-inputs
     (list gobject-introspection vala pkg-config))
    (inputs
     (list
      evolution-data-server
      clutter
      libgdata
      geoclue
      libsoup-minimal-2
      geocode-glib-with-libsoup2
      libhandy
      granite
      libchamplain
      libical
      libgee
      gtk+
      libnotify))
    (home-page "https://github.com/elementary/tasks")
    (synopsis "Desktop calendar app designed for elementary OS")
    (description "Desktop calendar app designed for elementary OS.")
    (license license:gpl3)))
