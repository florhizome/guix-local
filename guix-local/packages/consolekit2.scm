(define-module (guix-local packages  consolekit2)
  #:use-module (gnu)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages virtualization)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages popt)  
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages linux))

;;;TODO get lxc-cgmanager, libnih for cgroups support, inotify (which package)?
;; INFO lxcfs has ig surpassed lxc-cgmanager, seems not relevant for ck2
;; TODO build: config options? system dbus dir? generate docs, tests
(define-public consolekit2
  (package
   (name "consolekit2")
   (version "1.2.4")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/ConsoleKit2/ConsoleKit2/archive/refs/tags/"
       version ".tar.gz"))
     (sha256
      (base32 "1s21y7m77pqgdzbnhhi1nv57lrh2mrk5may48qs79cpcqxya0p3w"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f)) ;;doc-tests fail
   (native-inputs
    (list automake
     ;;autogen
          libtool xmlto gtk-doc gettext-minimal gobject-introspection
          `(,glib "bin") autoconf pkg-config))
   (inputs
    (list libxml2 zlib libdrm dbus libevdev
     eudev lxc-cgmanager libx11 libxslt linux-pam polkit pm-utils
     inotify-tools))
   (home-page "https://consolekit2.github.io/ConsoleKit2/")
   (synopsis "Framework for defining and tracking users, login
sessions, and seats.")
   (description "ConsoleKit2 is a framework for defining and tracking
users, login sessions, and seats. It allows multiple users to be
logged in at the same time and share hardware for their graphical
session. ConsoleKit2 will keep track of those resources and whichever
session is active will have use of the hardware at that time.")
   (license license:expat)))

(define-public pm-utils
  (package
   (name "pm-utils")
   (version "0.2")
   (source
       (origin
         (method url-fetch)
         (uri "http://pm-utils.freedesktop.org/releases/pm-utils-1.4.1.tar.gz")
         (sha256
          (base32 "02qc6zaf7ams6qcc470fwb6jvr4abv3lrlx16clqpn36501rkn4f"))))
      (build-system gnu-build-system)
      (native-inputs
       (list xmlto pkg-config))
      (inputs
       (list ethtool wireless-tools hdparm
        ;;vbetool
       ))
  (home-page "http://pm-utils.freedesktop.org")
  (synopsis "Power management scripts for suspend and hibernate")
  (description "pm-utils is a small collection of scripts that handle
suspend and resume on behalf of HAL.")
  (license license:expat)))

;;fails with "multiple definitions of "output_package""
(define-public libnih
  (package
   (name "libnih")
   (version "1.0.3")
   (source
       (origin
         (method url-fetch)
         (uri "https://launchpad.net/libnih/1.0/1.0.3/+download/libnih-1.0.3.tar.gz")
         (sha256
          (base32 "01glc6y7z1g726zwpvp2zm79pyb37ki729jkh45akh35fpgp4xc9"))))
   (build-system gnu-build-system)
   (arguments
    `(#:tests? #f
               #:phases
               (modify-phases %standard-phases
                      (add-before 'build 'setenv 
                        (lambda _
                          
                          (setenv "CFLAGS" "${CFLAGS} -fcommon")))
                      (add-after 'unpack 'fix-for-gcc-10 
                        (lambda _
                          (substitute* "nih-dbus-tool/output.h"
                            (("char \\*output_package") "extern char *output_package")))))))
   (native-inputs
    (list
     autoconf
     libtool
     automake
     gettext-minimal
     pkg-config))
   (inputs
    (list
     dbus
     expat))
  (home-page "https://github.com/keybuk/libnih")
  (synopsis "")
  (description "libnih is a light-weight \"standard library\" of C
functions to ease the development of other libraries and applications.")
  (license license:gpl2)))

;;; note: choosing the lxc prefix in analogy to upstreams naming
(define-public lxc-cgmanager
  (package
   (name "lxc-cgmanager")
   (version "0.42")
   (source
       (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/lxc/cgmanager")
         (commit (string-append "v" version))))
         (sha256
          (base32 "1l8xdm1j2byyj822803apk4vzlyf2rvnfb3rankhi6nqyq7id459"))))
      (build-system gnu-build-system)
      (native-inputs
       (list
        autoconf
        libtool
        automake
        pkg-config))
      (inputs
       (list
        libnih
        popt
        linux-pam
        dbus))
  (home-page "")
  (synopsis "")
  (description "")
  (license license:expat)))
