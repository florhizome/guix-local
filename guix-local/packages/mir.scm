(define-module (guix-local packages  mir)
  #:use-module (gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
#:use-module (gnu packages gcc)
  
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages nettle)
  #:use-module (gnu packages instrumentation)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python)
  #:use-module (gnu packages code)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages valgrind)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages ninja)

  #:use-module (gnu packages xml)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages check)  
  #:use-module (gnu packages maths))
 
(define-public mir
  (package
   (name "mir")
   (version "2.9.0")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/MirServer/mir")
        (commit (string-append "v" version)))) 
      (sha256
       (base32 "1fvnjrf4pmrz4blcnsya51jrszd3mb7jdcmjbcq14w6v8d96cgj1"))
      ))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags
      '("-DMIR_USE_PRECOMPILED_HEADERS=OFF"
        "-DMIR_PLATFORM=\"gbm-kms;x11;wayland;eglstream-kms\""
        ;;"-GNinja"
        )))
   (native-inputs
    (list pkg-config lcov googletest boost valgrind gobject-introspection
          wayland-protocols python-pillow python python-dbusmock
          umockdev wlcs glm font-gnu-freefont gcc-12 libxslt))
   (inputs
    (list boost capnproto protobuf mesa wayland egl-wayland
          glog glm liburcu nettle yaml-cpp
          gflags lttng-ust eudev libxcursor libxml++-2
          libxkbcommon libevdev libglvnd libinput glib
          libxcb xcb-util-errors xcb-util-renderutil
          libepoxy freetype wlcs))
   (home-page "https://mir-server.com")
   (synopsis "The Mir compositor")
   (description "Mir is set of libraries for building Wayland based shells.
 Mir simplifies the complexity that shell authors need to deal with:
 it provides a stable, well tested and performant platform with touch,
 mouse and tablet input, multi-display capability and secure
 client-server communications.")
   (license (list license:lgpl3+ license:gpl3+))))

(define-public wlcs
  (package
   (name "wlcs")
   (version "1.4.0")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/MirServer/wlcs")
        (commit (string-append "v" version)))) 
      (sha256
       (base32 "14bfrfhhlkvzi477fvj1xbb1r06in0kcjj8cs9wz10agmwfl37ks"))))
   (build-system cmake-build-system)
   (arguments `(#:tests? #f))
   (native-inputs (list pkg-config wayland-protocols))
   (inputs
    (list boost googletest wayland))
   (home-page "")
   (synopsis "Wayland Conformance Test Suite")
   (description "wlcs aspires to be a protocol-conformance-verifying
 test suite usable by Wayland compositor implementors.

It is growing out of porting the existing Weston test suite to be run
 in Mir's test suite, but it is designed to be usable by any compositor.")
   ;;both licenses are in the repo
   (license '(list license:gpl3
                   license:lgpl3))))


