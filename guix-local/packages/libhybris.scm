(define-module (guix-local packages  libhybris)
  #:use-module (gnu)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages android))

(define-public libhybris
  (let ((commit "811c64c39bd36d61c11166495d3b06d45a44a721")
        (version "0.1")
        (revision "0"))
    (package
     (name "libhybris")
      (version (git-version version revision commit))
      (source
       (origin (method git-fetch)
               (uri (git-reference (url "https://github/libhybris/libhybris.git")
                                   (commit commit)))
               (sha256
                (base32 "0v2i0q0kcpxrv8sfi2vyj57kv095rxm5bzvkj6qp5hm94wqdjadf"))))
      (build-system gnu-build-system)
      (arguments
       `(#:phases (modify-phases %standard-phases
                                 (add-after 'unpack 'chdir
                                            (lambda _
                                             (chdir "./hybris"))))))
      (native-inputs (list pkg-config automake libtool autoconf android-make-stub gettext-minimal))
      (home-page "https://github/libhybris")
      (synopsis "")
      (description "Hybris is a solution that commits hybris, by allowing us to use bionic-based HW adaptations in glibc systems")
      (license ""))))

libhybris
