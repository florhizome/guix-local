(define-module (guix-local packages  librespot)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages man)
  #:use-module (gnu packages web)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo))


(define-public rust-version-compare-0.0.10
  (package
    (name "rust-version-compare")
    (version "0.0.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "version-compare" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18ack6rx18rp700h1dncljmpzchs3p2dfh76a8ds6vmfbfi5cdfn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/timvisee/version-compare")
    (synopsis
      "A Rust library to easily compare version numbers, and test them against various comparison operators.")
    (description
      "This package provides a Rust library to easily compare version numbers, and test
them against various comparison operators.")
    (license license:expat)))

(define-public rust-vcpkg-0.2
  (package
    (name "rust-vcpkg")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vcpkg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/mcgoo/vcpkg-rs")
    (synopsis
      "A library to find native dependencies in a vcpkg tree at build
time in order to be used in Cargo build scripts.
")
    (description
      "This package provides a library to find native dependencies in a vcpkg tree at
build time in order to be used in Cargo build scripts.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unidiff-0.3
  (package
    (name "rust-unidiff")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unidiff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b13vhp2x7jlvmkm44h5niqcxklyrmz6afmppvykp4zimhcjg9nq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/messense/unidiff-rs")
    (synopsis "Unified diff parsing/metadata extraction library for Rust")
    (description "Unified diff parsing/metadata extraction library for Rust")
    (license license:expat)))

(define-public rust-sdl2-sys-0.34
  (package
    (name "rust-sdl2-sys")
    (version "0.34.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sdl2-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0x4g141ais0k16frypykc1bfxlg5smraavg2lr0mlnqp3yi9m8j1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.53)
         ("rust-cfg-if" ,rust-cfg-if-0.1)
         ("rust-cmake" ,rust-cmake-0.1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-unidiff" ,rust-unidiff-0.3)
         ("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-version-compare" ,rust-version-compare-0.0.10))))
    (home-page "https://github.com/rust-sdl2/rust-sdl2")
    (synopsis "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (description "Raw SDL2 bindings for Rust, used internally rust-sdl2")
    (license license:expat)))

(define-public rust-c-vec-2
  (package
    (name "rust-c-vec")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "c_vec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1s765fviy10q27b0wmkyk4q728z9v8v5pdlxv5k564y0mlks9mzx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/GuillaumeGomez/c_vec-rs.git")
    (synopsis "Structures to wrap C arrays")
    (description "Structures to wrap C arrays")
    (license (list license:asl2.0 license:expat))))

(define-public rust-sdl2-0.34
  (package
    (name "rust-sdl2")
    (version "0.34.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sdl2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jymfs8ibf1xli4vn562l05bl6zknmff0qz5l7swy2j6m4zvrv6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-c-vec" ,rust-c-vec-2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
         ("rust-sdl2-sys" ,rust-sdl2-sys-0.34))))
    (home-page "https://github.com/Rust-SDL2/rust-sdl2")
    (synopsis "SDL2 bindings for Rust")
    (description "SDL2 bindings for Rust")
    (license license:expat)))

(define-public rust-slice-deque-0.3
  (package
    (name "rust-slice-deque")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slice-deque" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "098gvqjw52qw4gac567c9hx3y6hw9al7hjqb5mnvmvydh3i6xvri"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/gnzlbg/slice_deque")
    (synopsis "A double-ended queue that Deref's into a slice.")
    (description
      "This package provides a double-ended queue that Deref's into a slice.")
    (license (list license:expat license:asl2.0))))

(define-public rust-minimp3-sys-0.3
  (package
    (name "rust-minimp3-sys")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimp3-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "144vmf3s89kad0smjprzigcp2c9r5dm95n4ydilrbp399irp6772"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-minimp3-0.5
  (package
    (name "rust-minimp3")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimp3" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wj3nzj1swnvwsk3a4a3hkfj1d21jsi7babi40wlrxzbbzvkhm4q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-minimp3-sys" ,rust-minimp3-sys-0.3)
         ("rust-slice-deque" ,rust-slice-deque-0.3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/germangb/minimp3-rs.git")
    (synopsis "Rust bindings for the minimp3 library.")
    (description "Rust bindings for the minimp3 library.")
    (license license:expat)))

(define-public rust-hound-3
  (package
    (name "rust-hound")
    (version "3.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hound" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jbm25p2nc8758dnfjan1yk7hz2i85y89nrbai14zzxfrsr4n5la"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/hound")
    (synopsis "A wav encoding and decoding library")
    (description "This package provides a wav encoding and decoding library")
    (license license:asl2.0)))

(define-public rust-claxon-0.4
  (package
    (name "rust-claxon")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "claxon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ruuda/claxon#readme")
    (synopsis "A FLAC decoding library")
    (description "This package provides a FLAC decoding library")
    (license license:asl2.0)))

(define-public rust-rodio-0.14
  (package
    (name "rust-rodio")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rodio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ybb0ip0wi7ckcq54a63bpbp03kzrp445h9bgq2ja5dnazjzb62d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-claxon" ,rust-claxon-0.4)
         ("rust-cpal" ,rust-cpal-0.13)
         ("rust-hound" ,rust-hound-3)
         ("rust-lewton" ,rust-lewton-0.10)
         ("rust-minimp3" ,rust-minimp3-0.5))))
    (home-page "https://github.com/RustAudio/rodio")
    (synopsis "Audio playback library")
    (description "Audio playback library")
    (license (list license:expat license:asl2.0))))

(define-public rust-portaudio-sys-0.1
  (package
    (name "rust-portaudio-sys")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "portaudio-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xdpywirpr1kqkbak7hnny62gmsc93qgc3ij3j2zskrvjpxa952i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "")
    (synopsis "Bindings to PortAudio")
    (description "Bindings to PortAudio")
    (license license:expat)))

(define-public rust-portaudio-rs-0.3
  (package
    (name "rust-portaudio-rs")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "portaudio-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qnmc7amk0fzbcs985ixv0k4955f0fmpkhrl9ps9pk3cz7pvbdnd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-portaudio-sys" ,rust-portaudio-sys-0.1))))
    (home-page "")
    (synopsis "PortAudio bindings for Rust")
    (description "PortAudio bindings for Rust")
    (license license:expat)))

(define-public rust-librespot-metadata-0.3
  (package
    (name "rust-librespot-metadata")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-metadata" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kb61dnr9zbygszs1arscy5y952damn9l14ayxg5cbmdnfdw4jk2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The metadata logic for librespot")
    (description "The metadata logic for librespot")
    (license license:expat)))

(define-public rust-libpulse-simple-sys-1
  (package
    (name "rust-libpulse-simple-sys")
    (version "1.19.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libpulse-simple-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hbws8gj45lanvd0xr8d02m60n1247jcyq275ilhjj53kipzjwvw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libpulse-sys" ,rust-libpulse-sys-1)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis
      "FFI bindings for the PulseAudio libpulse-simple system library.")
    (description
      "FFI bindings for the PulseAudio libpulse-simple system library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pkg-config-0.3
  (package
    (name "rust-pkg-config")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pkg-config" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1bh3vij79cshj884py4can1f8rvk52niaii1vwxya9q69gnc9y0x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-lang/pkg-config-rs")
    (synopsis
      "A library to run the pkg-config system tool at build time in order to be used in
Cargo build scripts.
")
    (description
      "This package provides a library to run the pkg-config system tool at build time
in order to be used in Cargo build scripts.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-sys-1
  (package
    (name "rust-libpulse-sys")
    (version "1.19.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libpulse-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c3wybzyyarzagz0fy2vwflc3r15d6cyfdp16ijnx8z2xz86n7lr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-num-derive" ,rust-num-derive-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis "FFI bindings for the PulseAudio libpulse system library.")
    (description "FFI bindings for the PulseAudio libpulse system library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-binding-2
  (package
    (name "rust-libpulse-binding")
    (version "2.26.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libpulse-binding" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qrnf088vwgxm26vs5fmz3ijry5nxjsdmgq37jcsxq0p00b45ghp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libpulse-sys" ,rust-libpulse-sys-1)
         ("rust-num-derive" ,rust-num-derive-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis "A Rust language binding for the PulseAudio libpulse library.")
    (description
      "This package provides a Rust language binding for the PulseAudio libpulse
library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-libpulse-simple-binding-2
  (package
    (name "rust-libpulse-simple-binding")
    (version "2.25.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libpulse-simple-binding" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mdws3gr5rvvb64hf77mkq17a6vzs6hryf8616v8r939zlfimgvw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libpulse-binding" ,rust-libpulse-binding-2)
         ("rust-libpulse-simple-sys" ,rust-libpulse-simple-sys-1)
         ("rust-libpulse-sys" ,rust-libpulse-sys-1))))
    (home-page "https://github.com/jnqnfe/pulse-binding-rust")
    (synopsis
      "A Rust language binding for the PulseAudio libpulse-simple library.")
    (description
      "This package provides a Rust language binding for the PulseAudio libpulse-simple
library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ogg-0.8
  (package
    (name "rust-ogg")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ogg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vjxmqcv9252aj8byy70iy2krqfjknfcxg11lcyikj11pzlb8lb9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-0.4)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/ogg")
    (synopsis "Ogg container decoder and encoder written in pure Rust")
    (description "Ogg container decoder and encoder written in pure Rust")
    (license license:bsd-3)))

(define-public rust-lewton-0.10
  (package
    (name "rust-lewton")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lewton" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c60fn004awg5c3cvx82d6na2pirf0qdz9w3b93mbcdakbglhyvp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-futures" ,rust-futures-0.1)
         ("rust-ogg" ,rust-ogg-0.8)
         ("rust-tinyvec" ,rust-tinyvec-1)
         ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://github.com/RustAudio/lewton")
    (synopsis "Pure Rust vorbis decoder")
    (description "Pure Rust vorbis decoder")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-0.7
  (package
    (name "rust-jack")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jack" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r7bgfpbph3fl9xyp4i9qffcc4h923dcs7d967mpir13lxg216yp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-jack-sys" ,rust-jack-sys-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK.")
    (description "Real time audio and midi with JACK.")
    (license license:expat)))

(define-public rust-gstreamer-base-0.16
  (package
    (name "rust-gstreamer-base")
    (version "0.16.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-base" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kxgwvv0qh1dgvd08nxfhhkjp36z9fxrf3x1nps11jsrdz2h3zds"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-glib" ,rust-glib-0.10)
         ("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gobject-sys" ,rust-gobject-sys-0.10)
         ("rust-gstreamer" ,rust-gstreamer-0.16)
         ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.9)
         ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer Base library")
    (description "Rust bindings for GStreamer Base library")
    (license (list license:expat license:asl2.0))))

(define-public rust-gstreamer-base-sys-0.9
  (package
    (name "rust-gstreamer-base-sys")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-base-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xgf5dl7507hn9mvz46ffjj3y2shpl1gc4l6w8d0l5kf5pfbddx4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gobject-sys" ,rust-gobject-sys-0.10)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstbase-1.0")
    (description "FFI bindings to libgstbase-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-sys-0.9
  (package
    (name "rust-gstreamer-app-sys")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-app-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gbyp1jsqqs3x2bfjkbdfsb5kfb4zafwzvxr52w36ywybhkn8gw1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gstreamer-base-sys" ,rust-gstreamer-base-sys-0.9)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstapp-1.0")
    (description "FFI bindings to libgstapp-1.0")
    (license license:expat)))

(define-public rust-gstreamer-app-0.16
  (package
    (name "rust-gstreamer-app")
    (version "0.16.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-app" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03mg4ywgba0q02zdqfamzxv7887bab2az32xhzg3x31kf618i06c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-glib" ,rust-glib-0.10)
         ("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gobject-sys" ,rust-gobject-sys-0.10)
         ("rust-gstreamer" ,rust-gstreamer-0.16)
         ("rust-gstreamer-app-sys" ,rust-gstreamer-app-sys-0.9)
         ("rust-gstreamer-base" ,rust-gstreamer-base-0.16)
         ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer App library")
    (description "Rust bindings for GStreamer App library")
    (license (list license:expat license:asl2.0))))

(define-public rust-muldiv-0.2
  (package
    (name "rust-muldiv")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "muldiv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "014jlry2l2ph56mp8knw65637hh49q7fmrraim2bx9vz0a638684"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/sdroege/rust-muldiv")
    (synopsis
      "Provides a trait for numeric types to perform combined multiplication and
division with overflow protection
")
    (description
      "This package provides a trait for numeric types to perform combined
multiplication and division with overflow protection")
    (license license:expat)))

(define-public rust-gstreamer-sys-0.9
  (package
    (name "rust-gstreamer-sys")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07b09f2acaiczjl3725dhraym935yns8x2jziiqza6nhh901a7zw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gobject-sys" ,rust-gobject-sys-0.10)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-system-deps" ,rust-system-deps-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "FFI bindings to libgstreamer-1.0")
    (description "FFI bindings to libgstreamer-1.0")
    (license license:expat)))

(define-public rust-gstreamer-rs-lgpl-docs-0.16
  (package
    (name "rust-gstreamer-rs-lgpl-docs")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer-rs-lgpl-docs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06k4mr6478463q7hhsl4a252nhzf0b2qjqla3xhlh20ma0hz8912"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustdoc-stripper" ,rust-rustdoc-stripper-0.1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "LGPL-licensed docs for gstreamer-rs crates")
    (description "LGPL-licensed docs for gstreamer-rs crates")
    (license license:lgpl2.0)))

(define-public rust-gstreamer-0.16
  (package
    (name "rust-gstreamer")
    (version "0.16.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gstreamer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0crghv0qh0lys26712j3dshdwnvq2znnsyxldrzf72ihzzvx1xcz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-glib" ,rust-glib-0.10)
         ("rust-glib-sys" ,rust-glib-sys-0.10)
         ("rust-gobject-sys" ,rust-gobject-sys-0.10)
         ("rust-gstreamer-rs-lgpl-docs" ,rust-gstreamer-rs-lgpl-docs-0.16)
         ("rust-gstreamer-sys" ,rust-gstreamer-sys-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-muldiv" ,rust-muldiv-0.2)
         ("rust-num-rational" ,rust-num-rational-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-paste" ,rust-paste-1)
         ("rust-pretty-hex" ,rust-pretty-hex-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-bytes" ,rust-serde-bytes-0.11)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://gstreamer.freedesktop.org")
    (synopsis "Rust bindings for GStreamer")
    (description "Rust bindings for GStreamer")
    (license (list license:expat license:asl2.0))))

(define-public rust-stdweb-0.1
  (package
    (name "rust-stdweb")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gjk7ch31a3kgdc39kj4zqinf10yqaf717wanh9kwwbbwg430m7g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clippy" ,rust-clippy-0.0)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/koute/stdweb")
    (synopsis "A standard library for the client-side Web")
    (description
      "This package provides a standard library for the client-side Web")
    (license (list license:expat license:asl2.0))))

(define-public rust-fetch-unroll-0.3
  (package
    (name "rust-fetch-unroll")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fetch_unroll" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l3cf8fhcrw354hdmjf03f5v4bxgn2wkjna8n0fn8bgplh8b3666"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libflate" ,rust-libflate-1)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-ureq" ,rust-ureq-2))))
    (home-page "https://github.com/katyo/fetch_unroll")
    (synopsis "Simple utilities for fetching and unrolling .tar.gz archives")
    (description
      "Simple utilities for fetching and unrolling .tar.gz archives")
    (license license:asl2.0)))

(define-public rust-oboe-sys-0.4
  (package
    (name "rust-oboe-sys")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oboe-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gcl494yy880h2gfgsbdd32g2h0s1n94v58j5hil9mrf6yvsnw1k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.59)
         ("rust-cc" ,rust-cc-1)
         ("rust-fetch-unroll" ,rust-fetch-unroll-0.3))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
      "Unsafe bindings for oboe an android library for low latency audio IO")
    (description
      "Unsafe bindings for oboe an android library for low latency audio IO")
    (license license:asl2.0)))

(define-public rust-oboe-0.4
  (package
    (name "rust-oboe")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oboe" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lh20l8b4lx5h9a7lpf9n66z47sh2508w7x2203hsklvw7rchqr4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jni" ,rust-jni-0.19)
         ("rust-ndk" ,rust-ndk-0.6)
         ("rust-ndk-glue" ,rust-ndk-glue-0.6)
         ("rust-num-derive" ,rust-num-derive-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-oboe-sys" ,rust-oboe-sys-0.4))))
    (home-page "https://github.com/katyo/oboe-rs")
    (synopsis
      "Safe interface for oboe an android library for low latency audio IO")
    (description
      "Safe interface for oboe an android library for low latency audio IO")
    (license license:asl2.0)))

(define-public rust-ndk-macro-0.3
  (package
    (name "rust-ndk-macro")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0v3sxc11kq3d5vdwfml62l7y5dr0flsf6kp5xid9sbv7qh0arxqd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling" ,rust-darling-0.13)
         ("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Helper macros for android ndk")
    (description "Helper macros for android ndk")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-context-0.1
  (package
    (name "rust-ndk-context")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-context" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "112q689zc4338xmj55a8nxdlkjmrw34s3xkpy3l1zqiphv35qg2f"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Handles for accessing Android APIs")
    (description "Handles for accessing Android APIs")
    (license (list license:expat license:asl2.0))))

(define-public rust-android-log-sys-0.2
  (package
    (name "rust-android-log-sys")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "android_log-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bhhs1cgzp9vzjvkn2q31ppc7w4am5s273hkvl5iac5475kmp5l5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/nercury/android_log-sys-rs")
    (synopsis "FFI bindings to Android log Library.
")
    (description "FFI bindings to Android log Library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-android-logger-0.10
  (package
    (name "rust-android-logger")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "android_logger" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rigzgkaik2y7pvsilpjdy19mdq1kkamw2rdf9fjkvb5hfqhkvfr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-log-sys" ,rust-android-log-sys-0.2)
         ("rust-env-logger" ,rust-env-logger-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/Nercury/android_logger-rs")
    (synopsis
      "A logging implementation for `log` which hooks to android log output.
")
    (description
      "This package provides a logging implementation for `log` which hooks to android
log output.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-glue-0.6
  (package
    (name "rust-ndk-glue")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-glue" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nkqw0lvq8qjl15645jcic0kp1cqr9vqf0j5sm4q795b7m2bgzyr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-logger" ,rust-android-logger-0.10)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-ndk" ,rust-ndk-0.6)
         ("rust-ndk-context" ,rust-ndk-context-0.1)
         ("rust-ndk-macro" ,rust-ndk-macro-0.3)
         ("rust-ndk-sys" ,rust-ndk-sys-0.3))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Startup code for android binaries")
    (description "Startup code for android binaries")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-enum-derive-0.5
  (package
    (name "rust-num-enum-derive")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kj6b8f2fx8prlcl6y1k97668s5aiia4f9gjlk0nmpak3rj9h11v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Internal implementation details for ::num_enum (Procedural macros to make inter-operation between primitives and enums easier)")
    (description
      "Internal implementation details for ::num_enum (Procedural macros to make
inter-operation between primitives and enums easier)")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-num-enum-0.5
  (package
    (name "rust-num-enum")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1j8rq7i4xnbzy72z82k41469xlj1bmn4ixagd9wlbvv2ark9alyg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-enum-derive" ,rust-num-enum-derive-0.5))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (description
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-ndk-sys-0.3
  (package
    (name "rust-ndk-sys")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15zsq4p6k5asf4mc0rknd8cz9wxrwvi50qdspgf87qcfgkknlnkf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-jni-sys" ,rust-jni-sys-0.3))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "FFI bindings for the Android NDK")
    (description "FFI bindings for the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-0.6
  (package
    (name "rust-ndk")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m1dfjw35qpys1hr4qib6mm3zacd01k439l7cx5f7phd0dzcfci0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-jni" ,rust-jni-0.18)
         ("rust-jni-glue" ,rust-jni-glue-0.0)
         ("rust-jni-sys" ,rust-jni-sys-0.3)
         ("rust-ndk-sys" ,rust-ndk-sys-0.3)
         ("rust-num-enum" ,rust-num-enum-0.5)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Safe Rust bindings to the Android NDK")
    (description "Safe Rust bindings to the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-jni-0.19
  (package
    (name "rust-jni")
    (version "0.19.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jni" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v0pn0i1wb8zp4wns4l8hz9689hqsplv7iba7hylaznvwg11ipy6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cesu8" ,rust-cesu8-1)
         ("rust-combine" ,rust-combine-4)
         ("rust-jni-sys" ,rust-jni-sys-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/jni-rs/jni-rs")
    (synopsis "Rust bindings to the JNI")
    (description "Rust bindings to the JNI")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-sys-0.2
  (package
    (name "rust-jack-sys")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jack-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h9c9za19nyr1prx77gkia18ia93f73lpyjdiyrvmhhbs79g54bv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libloading" ,rust-libloading-0.6)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/RustAudio/rust-jack/tree/main/jack-sys")
    (synopsis "Low-level binding to the JACK audio API.")
    (description "Low-level binding to the JACK audio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-jack-0.8
  (package
    (name "rust-jack")
    (version "0.8.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jack" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lz10s0n2gy128m65pf96is9ip00vfgvnkfja0y9ydmv24pw2ajx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-jack-sys" ,rust-jack-sys-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/RustAudio/rust-jack")
    (synopsis "Real time audio and midi with JACK.")
    (description "Real time audio and midi with JACK.")
    (license license:expat)))

(define-public rust-bindgen-0.56
  (package
    (name "rust-bindgen")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bindgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fajmgk2064ca1z9iq1jjkji63qwwz38z3d67kv6xdy0xgdpk8rd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-cexpr" ,rust-cexpr-0.4)
         ("rust-clang-sys" ,rust-clang-sys-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-env-logger" ,rust-env-logger-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lazycell" ,rust-lazycell-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-shlex" ,rust-shlex-0.1)
         ("rust-which" ,rust-which-3))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-coreaudio-sys-0.2
  (package
    (name "rust-coreaudio-sys")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coreaudio-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "12r4icmi931jp6dvaf22499r8fqnq7ldy4n0ckq1b35xknjpjina"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.56))))
    (home-page "https://github.com/RustAudio/coreaudio-sys")
    (synopsis
      "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (description
      "Bindings for Apple's CoreAudio frameworks generated via rust-bindgen")
    (license license:expat)))

(define-public rust-coreaudio-rs-0.10
  (package
    (name "rust-coreaudio-rs")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coreaudio-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "125d4zr3n363ybga4629p41ym7iqjfb2alnwrc1zj7zyxch4p28i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-coreaudio-sys" ,rust-coreaudio-sys-0.2))))
    (home-page "https://github.com/RustAudio/coreaudio-rs")
    (synopsis "A friendly rust interface for Apple's CoreAudio API.")
    (description
      "This package provides a friendly rust interface for Apple's CoreAudio API.")
    (license (list license:expat license:asl2.0))))

(define-public rust-asio-sys-0.2
  (package
    (name "rust-asio-sys")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "asio-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m8v2jsa4n57s7a7142vs23dkz63dhjxgcjxykd17kvq66v9qqj7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.54)
         ("rust-cc" ,rust-cc-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-num-derive" ,rust-num-derive-0.3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/RustAudio/cpal/")
    (synopsis
      "Low-level interface and binding generation for the steinberg ASIO SDK.")
    (description
      "Low-level interface and binding generation for the steinberg ASIO SDK.")
    (license license:asl2.0)))

(define-public rust-alsa-0.6
  (package
    (name "rust-alsa")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0szx8finhqbffh08fp3bgh4ywz0b572vcdyh4hwyhrfgw8pza5ar"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (inputs (list alsa-lib))    
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa-sys" ,rust-alsa-sys-0.3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cpal-0.13
  (package
    (name "rust-cpal")
    (version "0.13.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05j11vz8rw19gqqvpd48i7wvm6j77v8fwx5lwhlkckqjllv7h4bl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa" ,rust-alsa-0.6)
         ("rust-asio-sys" ,rust-asio-sys-0.2)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-coreaudio-rs" ,rust-coreaudio-rs-0.10)
         ("rust-jack" ,rust-jack-0.8)
         ("rust-jni" ,rust-jni-0.19)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-ndk" ,rust-ndk-0.6)
         ("rust-ndk-glue" ,rust-ndk-glue-0.6)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-oboe" ,rust-oboe-0.4)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-stdweb" ,rust-stdweb-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-web-sys" ,rust-web-sys-0.3)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/rustaudio/cpal")
    (synopsis "Low-level cross-platform audio I/O library in pure Rust.")
    (description "Low-level cross-platform audio I/O library in pure Rust.")
    (license license:asl2.0)))

(define-public rust-alsa-sys-0.3
  (package
    (name "rust-alsa-sys")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09qmmnpmlcj23zcgx2xsi4phcgm5i02g9xaf801y7i067mkfx3yv"))))
    (native-inputs (list pkg-config))
    (inputs (list alsa-lib))    
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/diwic/alsa-sys")
    (synopsis
      "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (description
      "FFI bindings for the ALSA project (Advanced Linux Sound Architecture)")
    (license license:expat)))

(define-public rust-alsa-0.5
  (package
    (name "rust-alsa")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "alsa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03nmld6vbpxqg22fy07p51x2rmwl7bzsc7rszhd03gyknd5ldaqb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa-sys" ,rust-alsa-sys-0.3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.21))))
    (home-page "https://github.com/diwic/alsa-rs")
    (synopsis "Thin but safe wrappers for ALSA (Linux sound API)")
    (description "Thin but safe wrappers for ALSA (Linux sound API)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-librespot-playback-0.3
  (package
    (name "rust-librespot-playback")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-playback" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1whf5qzpjj6yrcjzrpcvxchmq6nz251gz0d4hizkirqad4mjb2qq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-alsa" ,rust-alsa-0.5)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-cpal" ,rust-cpal-0.13)
         ("rust-futures-executor" ,rust-futures-executor-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-glib" ,rust-glib-0.10)
         ("rust-gstreamer" ,rust-gstreamer-0.16)
         ("rust-gstreamer-app" ,rust-gstreamer-app-0.16)
         ("rust-jack" ,rust-jack-0.7)
         ("rust-lewton" ,rust-lewton-0.10)
         ("rust-libpulse-binding" ,rust-libpulse-binding-2)
         ("rust-libpulse-simple-binding" ,rust-libpulse-simple-binding-2)
         ("rust-librespot-audio" ,rust-librespot-audio-0.3)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-librespot-metadata" ,rust-librespot-metadata-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-ogg" ,rust-ogg-0.8)
         ("rust-portaudio-rs" ,rust-portaudio-rs-0.3)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rand-distr" ,rust-rand-distr-0.4)
         ("rust-rodio" ,rust-rodio-0.14)
         ("rust-sdl2" ,rust-sdl2-0.34)
         ("rust-shell-words" ,rust-shell-words-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-zerocopy" ,rust-zerocopy-0.3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The audio playback logic for librespot")
    (description "The audio playback logic for librespot")
    (license license:expat)))

(define-public rust-if-addrs-sys-0.3
  (package
    (name "rust-if-addrs-sys")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if-addrs-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1skrzs79rafv185064p44r0k1va9ig4bfnpbwlvyhxh4g3fvjx6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1) ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "if_addrs sys crate")
    (description "if_addrs sys crate")
    (license (list license:expat license:bsd-3))))

(define-public rust-if-addrs-0.6
  (package
    (name "rust-if-addrs")
    (version "0.6.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if-addrs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pkkkwm9znn07xq9s6glf8lxzn2rdxvy8kwkw6czrw64ywhy8wr2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-if-addrs-sys" ,rust-if-addrs-sys-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/messense/if-addrs")
    (synopsis "Return interface IP addresses on Posix and windows systems")
    (description "Return interface IP addresses on Posix and windows systems")
    (license (list license:expat license:bsd-3))))

(define-public rust-libmdns-0.6
  (package
    (name "rust-libmdns")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libmdns" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0siaarjhds4dxrxn7qgz4gnfb2hzcmsdcndd3rnkr1rfs2j8bhgs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-hostname" ,rust-hostname-0.3)
         ("rust-if-addrs" ,rust-if-addrs-0.6)
         ("rust-log" ,rust-log-0.4)
         ("rust-multimap" ,rust-multimap-0.8)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/librespot-org/libmdns")
    (synopsis
      "mDNS Responder library for building discoverable LAN services in Rust")
    (description
      "mDNS Responder library for building discoverable LAN services in Rust")
    (license license:expat)))

(define-public rust-dns-sd-0.1
  (package
    (name "rust-dns-sd")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dns-sd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11r0jymjshfnn3sh2nqjhrikk4r5rr1g36sip9iqy8i0xafm0j6p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/plietar/rust-dns-sd")
    (synopsis "Rust binding for dns-sd")
    (description "Rust binding for dns-sd")
    (license license:expat)))

(define-public rust-librespot-discovery-0.3
  (package
    (name "rust-librespot-discovery")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-discovery" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qsvzwj75aa7rd46y48808xrls9jj8gk66b53wlil9hj8bryzbf4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-ctr" ,rust-aes-ctr-0.6)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-dns-sd" ,rust-dns-sd-0.1)
         ("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-libmdns" ,rust-libmdns-0.6)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The discovery logic for librespot")
    (description "The discovery logic for librespot")
    (license license:expat)))

(define-public rust-librespot-connect-0.3
  (package
    (name "rust-librespot-connect")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-connect" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xmsymwkjxikvm2b4ya9a11ywjr11p32jkr8vis0hnj7xvcbz78y"))))
    (build-system cargo-build-system)
    ;;(native-inputs (list pkg-config))
    ;;(inputs (list alsa-lib openssl pulseaudio))
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs
        (("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-librespot-discovery" ,rust-librespot-discovery-0.3)
         ("rust-librespot-playback" ,rust-librespot-playback-0.3)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The discovery and Spotify Connect logic for librespot")
    (description "The discovery and Spotify Connect logic for librespot")
    (license license:expat)))

(define-public rust-tokio-stream-0.1
  (package
    (name "rust-tokio-stream")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-stream" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qwq0y21xprsql4v9y1cm1ymhgw66rznjmnjrjsii27zxy25852h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities to work with `Stream` and `tokio`.
")
    (description "Utilities to work with `Stream` and `tokio`.")
    (license license:expat)))

(define-public rust-shannon-0.2
  (package
    (name "rust-shannon")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "shannon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qa52zs4y1i87ysr11g9p6shpdagl14bb340gfm6rd97jhfb99by"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "Shannon cipher implementation")
    (description "Shannon cipher implementation")
    (license license:expat)))

(define-public rust-priority-queue-1
  (package
    (name "rust-priority-queue")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "priority-queue" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w6a4wkxm7h7qhxqgivgxbixw51czmkd83x1vr0gqg4dq054ifh0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/garro95/priority-queue")
    (synopsis
      "A Priority Queue implemented as a heap with a function to efficiently change the priority of an item.")
    (description
      "This package provides a Priority Queue implemented as a heap with a function to
efficiently change the priority of an item.")
    (license (list license:lgpl3 license:mpl2.0))))

(define-public rust-password-hash-0.2
  (package
    (name "rust-password-hash")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "password-hash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rr4kd52ld978a2xhcvlc54p1d92yhxl9kvbajba7ia6rs5b5q3p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64ct" ,rust-base64ct-1)
         ("rust-rand-core" ,rust-rand-core-0.6)
         ("rust-subtle" ,rust-subtle-2))))
    (home-page
      "https://github.com/RustCrypto/traits/tree/master/password-hash")
    (synopsis
      "Traits which describe the functionality of password hashing algorithms,
as well as a `no_std`-friendly implementation of the PHC string format
(a well-defined subset of the Modular Crypt Format a.k.a. MCF)
")
    (description
      "Traits which describe the functionality of password hashing algorithms, as well
as a `no_std`-friendly implementation of the PHC string format (a well-defined
subset of the Modular Crypt Format a.k.a.  MCF)")
    (license (list license:expat license:asl2.0))))

(define-public rust-pbkdf2-0.8
  (package
    (name "rust-pbkdf2")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pbkdf2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ykgicvyjm41701mzqhrfmiz5sm5y0zwfg6csaapaqaf49a54pyr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64ct" ,rust-base64ct-1)
         ("rust-crypto-mac" ,rust-crypto-mac-0.11)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-password-hash" ,rust-password-hash-0.2)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-sha2" ,rust-sha2-0.9))))
    (home-page
      "https://github.com/RustCrypto/password-hashes/tree/master/pbkdf2")
    (synopsis "Generic implementation of PBKDF2")
    (description "Generic implementation of PBKDF2")
    (license (list license:expat license:asl2.0))))

(define-public rust-protobuf-codegen-2
  (package
    (name "rust-protobuf-codegen")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01ypnn1194br42c5ign40s4v2irw3zypv6j38c1n4blgghmn7hdf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e. g. from `build.rs`) and `protoc-gen-rust` binary.
")
    (description
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e.  g.  from `build.rs`) and
`protoc-gen-rust` binary.")
    (license license:expat)))

(define-public rust-protobuf-codegen-pure-2
  (package
    (name "rust-protobuf-codegen-pure")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen-pure" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1iicacpccn3bg65pm3ylvjndf35pplb8l23bg461jmcfn7yj50cz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen" ,rust-protobuf-codegen-2))))
    (home-page
      "https://github.com/stepancheg/rust-protobuf/tree/master/protobuf-codegen-pure/")
    (synopsis
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP
")
    (description
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15jgq6nkhysicmnb2a998aiqan8jr4rd46hdsc10kkcffcc6szng"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license license:expat)))

(define-public rust-librespot-protocol-0.3
  (package
    (name "rust-librespot-protocol")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-protocol" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r1yrhycxaqd5iismw8wr7y8v4nvmarzbdx5lp550k6b0ndnkcs1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-glob" ,rust-glob-0.3)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen-pure" ,rust-protobuf-codegen-pure-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The protobuf logic for communicating with Spotify servers")
    (description "The protobuf logic for communicating with Spotify servers")
    (license license:expat)))

(define-public rust-hyper-proxy-0.9
  (package
    (name "rust-hyper-proxy")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-proxy" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1k3mpq6d4rhz58dam1757sav14j32n39q8x37wjgpz943f4mm0fa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-hyper-rustls" ,rust-hyper-rustls-0.22)
         ("rust-hyper-tls" ,rust-hyper-tls-0.5)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-openssl" ,rust-openssl-0.10)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.5)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-openssl" ,rust-tokio-openssl-0.6)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-webpki" ,rust-webpki-0.21)
         ("rust-webpki-roots" ,rust-webpki-roots-0.21))))
    (home-page "https://github.com/tafia/hyper-proxy")
    (synopsis "A proxy connector for Hyper-based applications")
    (description
      "This package provides a proxy connector for Hyper-based applications")
    (license license:expat)))

(define-public rust-librespot-core-0.3
  (package
    (name "rust-librespot-core")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1yxamxy3pwq80xvf99rrgm2ciphf1fphn54x0yahh0lwf66qspi5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes" ,rust-aes-0.6)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-hyper-proxy" ,rust-hyper-proxy-0.9)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-pbkdf2" ,rust-pbkdf2-0.8)
         ("rust-priority-queue" ,rust-priority-queue-1)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-shannon" ,rust-shannon-0.2)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-url" ,rust-url-2)
         ("rust-uuid" ,rust-uuid-0.8)
         ("rust-vergen" ,rust-vergen-3))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis "The core functionality provided by librespot")
    (description "The core functionality provided by librespot")
    (license license:expat)))

(define-public rust-aes-ctr-0.6
  (package
    (name "rust-aes-ctr")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-ctr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qspjxzrclnb83501595y01dhc0km1ssrbjnwlxhcrsdwp6w6abp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-soft" ,rust-aes-soft-0.6)
         ("rust-aesni" ,rust-aesni-0.10)
         ("rust-cipher" ,rust-cipher-0.2)
         ("rust-ctr" ,rust-ctr-0.6))))
    (home-page "https://github.com/RustCrypto/block-ciphers/tree/master/aes")
    (synopsis "DEPRECATED: replaced by the `aes` crate")
    (description "DEPRECATED: replaced by the `aes` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-librespot-audio-0.3
  (package
    (name "rust-librespot-audio")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot-audio" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p52wi7ql9aqcvb9y2cd665fq1ryjf66c8z53ccrc98w9mzcrxz5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-ctr" ,rust-aes-ctr-0.6)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "")
    (synopsis "The audio fetching and processing logic for librespot")
    (description "The audio fetching and processing logic for librespot")
    (license license:expat)))

(define-public rust-librespot-0.3
  (package
    (name "rust-librespot")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "librespot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14alj0v8zyh8yzkqyd3sszrs4m8kdvaik4kfmq7r8zcpcv29pd2z"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (inputs (list alsa-lib openssl pulseaudio))
    
    (arguments
      `(#:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-env-logger" ,rust-env-logger-0.8)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-getopts" ,rust-getopts-0.2)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-librespot-audio" ,rust-librespot-audio-0.3)
         ("rust-librespot-connect" ,rust-librespot-connect-0.3)
         ("rust-librespot-core" ,rust-librespot-core-0.3)
         ("rust-librespot-discovery" ,rust-librespot-discovery-0.3)
         ("rust-librespot-metadata" ,rust-librespot-metadata-0.3)
         ("rust-librespot-playback" ,rust-librespot-playback-0.3)
         ("rust-librespot-protocol" ,rust-librespot-protocol-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-rpassword" ,rust-rpassword-5)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/librespot-org/librespot")
    (synopsis
      "An open source client library for Spotify, with support for Spotify Connect")
    (description
      "An open source client library for Spotify, with support for Spotify Connect")
    (license license:expat)))

