(define-module (guix-local services hardware)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages freedesktop) 
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services configuration)
  #:use-module (gnu services linux)
  #:use-module (gnu services dbus)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system pam)
  #:use-module (gnu packages linux)
  #:use-module (guix-local packages hardware)
  #:use-module (guix-local packages asus)  
  #:export (power-profiles-configuration
            power-profiles-configuration?
            power-profiles-service-type
	    iio-sensor-proxy-configuration
            iio-sensor-proxy-configuration?
            iio-sensor-proxy-service-type
            low-memory-monitor-service-type
            low-memory-monitor-configuration
            low-memory-monitor-configuration?
            asus-numpad-configuration
            asus-numpad-configuration?
            asus-numpad-service-type
            rust-asus-numpad-configuration
            rust-asus-numpad-configuration?
            rust-asus-numpad-service-type
            acpid-configuration?
            acpid-service-type
            acpid-configuration
            bolt-configuration
            bolt-configuration?
            bolt-service-type
            nbfc-linux-configuration
            nbfc-linux-configuration?
            nbfc-linux-service-type
            rtkit-configuration
            rtkit-configuration?
            rtkit-service-type))

(define-record-type* <power-profiles-configuration>
  power-profiles-configuration make-power-profiles-configuration
  power-profiles-configuration?
  (power-profiles-daemon-package power-profiles-configuration-package ;file-like
                                 (default power-profiles-daemon)))

(define %power-profiles-activation
  #~(begin
      (use-modules (guix build utils))
      (mkdir-p "/var/lib/power-profiles-daemon")))

(define (power-profiles-shepherd-service-type config)
  (match-record config <power-profiles-configuration>
    (power-profiles-daemon-package)
    (list (shepherd-service
           (documentation "Control power-profiles daemon.")
           (provision '(power-profiles-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append power-profiles-daemon-package
                                      "/libexec/power-profiles-daemon") "-v")))
           (stop #~(make-kill-destructor))))))

(define power-profiles-service-type
  (service-type
   (name 'power-profiles)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list power-profiles-configuration-package))
          (service-extension polkit-service-type
                             (compose list power-profiles-configuration-package))
          (service-extension shepherd-root-service-type
                             power-profiles-shepherd-service-type)
	  (service-extension activation-service-type
                                            (const %power-profiles-activation))
                         
          (service-extension profile-service-type
                             (compose list power-profiles-configuration-package))))   
   (description "service to manage power-profiles on linux")
   (default-value (power-profiles-configuration))))

;; iio-sensor-proxy

(define-record-type* <iio-sensor-proxy-configuration>
  iio-sensor-proxy-configuration make-iio-sensor-proxy-configuration
  iio-sensor-proxy-configuration?
  (iio-sensor-proxy-package iio-sensor-proxy-configuration-package ;file-like
           (default iio-sensor-proxy)))

;;TODO add configuration option for verbose logging
(define (iio-sensor-proxy-shepherd-service-type config)
  (match-record config <iio-sensor-proxy-configuration>
    (iio-sensor-proxy-package)
    (list (shepherd-service
           (documentation "Control iio-sensor-proxy daemon.")
           (provision '(iio-sensor-proxy-daemon))
           (requirement '(user-processes dbus-system)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append iio-sensor-proxy-package
                                      "/libexec/iio-sensor-proxy") "-v")))
           (stop #~(make-kill-destructor))))))

(define iio-sensor-proxy-service-type 
  (service-type
   (name 'iio-sensor-proxy)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list iio-sensor-proxy-configuration-package))
           (service-extension polkit-service-type
                             (compose list iio-sensor-proxy-configuration-package))
          (service-extension udev-service-type
                             (compose list iio-sensor-proxy-configuration-package))
          (service-extension shepherd-root-service-type
                             iio-sensor-proxy-shepherd-service-type)
          (service-extension profile-service-type
                             (compose list iio-sensor-proxy-configuration-package))))
   (description "")
   (default-value (iio-sensor-proxy-configuration))))

;; rtkit

(define-record-type* <rtkit-configuration>
  rtkit-configuration make-rtkit-configuration
  rtkit-configuration?
  (rtkit-package rtkit-configuration-package ;file-like
           (default rtkit)))

  
(define %rtkit-accounts
  (list (user-group (name "rtkit") (system? #t))
        (user-account
         (name "rtkit")
         (group "rtkit")
         (system? #t)
         (comment "rtkit daemon user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))

;;TODO add configuration option for verbose logging
(define (rtkit-shepherd-service-type config)
  (match-record config <rtkit-configuration>
    (rtkit-package)
    (list (shepherd-service
           (documentation "Control rtkit daemon.")
           (provision '(rtkit-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append rtkit-package "/libexec/rtkit-daemon"))))
           (stop #~(make-kill-destructor))))))

(define rtkit-service-type 
  (service-type
   (name 'rtkit)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list rtkit-configuration-package))
          (service-extension polkit-service-type
                             (compose list rtkit-configuration-package))
          (service-extension account-service-type
                             (const %rtkit-accounts))
          (service-extension profile-service-type
                             (compose list rtkit-configuration-package))))
   (description "")
   (default-value (rtkit-configuration))))


;; low-memory-monitor
(define (bool value)
  (if value "true\n" "false\n"))

(define-record-type* <low-memory-monitor-configuration>
  low-memory-monitor-configuration make-low-memory-monitor-configuration
  low-memory-monitor-configuration?
  (low-memory-monitor-package lmm-configuration-package ;file-like
                              (default low-memory-monitor))
  (trigger-kernel-oom? lmm-configuration-trigger-kernel-oom? (default #f)))

(define (lmm-etc-file config)
  `(("low_memory_monitor.conf"
     ,(plain-file
       "low_memory_monitor.conf"
       (string-append
        "[Configuration]\n"
        "TriggerKernelOom=" (bool (lmm-configuration-trigger-kernel-oom? config)))))))


;;TODO add configuration option for verbose logging
(define (low-memory-monitor-shepherd-service-type config)
  (match-record config <low-memory-monitor-configuration>
    (low-memory-monitor-package)
    (list (shepherd-service
           (documentation "Control low-memory-monitor daemon.")
           (provision '(low-memory-monitor-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append low-memory-monitor-package
                                      "/libexec/low-memory-monitor"))))
           (stop #~(make-kill-destructor))))))

(define low-memory-monitor-service-type 
  (service-type
   (name 'low-memory-monitor)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list lmm-configuration-package))
          (service-extension shepherd-root-service-type
                             low-memory-monitor-shepherd-service-type)
          (service-extension etc-service-type lmm-etc-file)
          (service-extension profile-service-type
                             (compose list lmm-configuration-package))))
   (description "Run the 'low-memory-monitor' service. This gives applications
access to information about memory pressure.")
   (default-value (low-memory-monitor-configuration))))

;; asus-numpad

(define-record-type* <asus-numpad-configuration>
  asus-numpad-configuration make-asus-numpad-configuration
  asus-numpad-configuration?
  (asus-numpad-package asus-numpad-configuration-package ;file-like
           (default asus-numpad)))

;;TODO add configuration option for verbose logging
(define (asus-numpad-shepherd-service-type config)
  (match-record config <asus-numpad-configuration>
    (asus-numpad-package)
    (list (shepherd-service
           (documentation "Control asus-numpad daemon.")
           (provision '(asus-numpad-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append asus-numpad-package "/bin/asus_numpad") "/dev/input/event15")))
           (stop #~(make-kill-destructor))))))

(define asus-numpad-service-type 
  (service-type
   (name 'asus-numpad)
   (extensions
    (list (service-extension shepherd-root-service-type
                             asus-numpad-shepherd-service-type)
          (service-extension profile-service-type
                             (compose list asus-numpad-configuration-package))
          (service-extension udev-service-type
                             (compose list asus-numpad-configuration-package))))
   (description "")
   (default-value (asus-numpad-configuration))))

;; acpid

(define-record-type* <acpid-configuration>
  acpid-configuration make-acpid-configuration
  acpid-configuration?
  (acpid-package acpid-configuration-package ;file-like
           (default acpid)))

;;TODO add configuration option for verbose logging
(define (acpid-shepherd-service-type config)
  (match-record config <acpid-configuration>
    (acpid-package)
    (list (shepherd-service
           (documentation "Control acpid daemon.")
           (provision '(acpid-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append acpid-package "/sbin/acpid") "-l")))
           (stop #~(make-kill-destructor))))))

(define %acpid-activation
  #~(begin
      (use-modules (guix build utils))
      (mkdir-p "/etc/acpi/events")))

(define acpid-service-type 
  (service-type
   (name 'acpid)
   (extensions
    (list (service-extension shepherd-root-service-type
                             acpid-shepherd-service-type)
          ;;add acpi_listen to profile
          (service-extension activation-service-type
                             (const %acpid-activation))
          (service-extension profile-service-type
                             (compose list acpid-configuration-package))))
   (description "")
   (default-value (acpid-configuration))))

;; linux-nbfc

(define-record-type* <nbfc-linux-configuration>
  nbfc-linux-configuration make-nbfc-linux-configuration
  nbfc-linux-configuration?
  (nbfc-linux-package nbfc-linux-configuration-package ;file-like
                      (default nbfc-linux)))

(define %nbfc-linux-service-config 
  (plain-file "nbfc-config.json"
                 "{
    \"SelectedConfigId\": \"HP Spectre x360 Convertible 13-ae0xx\",
    \"EmbeddedControllerType\": \"ec_linux\",
    \"ReadOnly\": false,
}"))

;;TODO add configuration option for verbose logging
(define (nbfc-linux-shepherd-service-type config)
  (match-record config <nbfc-linux-configuration>
    (nbfc-linux-package)
    (list (shepherd-service
           (documentation "Control notebook-fancontrol daemon for linux.")
           (provision '(nbfc-linux-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append nbfc-linux-package "/bin/nbfc_service")
                       "-f" "-d" "-r" "-c" #$%nbfc-linux-service-config)))
           (stop #~(make-kill-destructor))))))


(define nbfc-linux-service-type 
  (service-type
   (name 'nbfc)
   (extensions
    (list (service-extension shepherd-root-service-type
                             nbfc-linux-shepherd-service-type)
          (service-extension profile-service-type
                             (compose list nbfc-linux-configuration-package))))
   (description "")
   (default-value (nbfc-linux-configuration))))

;; rust-asus-numpad

(define-record-type* <rust-asus-numpad-configuration>
  rust-asus-numpad-configuration make-rust-asus-numpad-configuration
  rust-asus-numpad-configuration?
  (rust-asus-numpad-package rust-asus-numpad-configuration-package ;file-like
           (default rust-asus-numpad)))

(define (rust-asus-numpad-shepherd-service-type config)
  (match-record config <rust-asus-numpad-configuration>
    (rust-asus-numpad-package)
    (list (shepherd-service
           (documentation "Control rust-asus-numpad daemon.")
           (provision '(rust-asus-numpad-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append rust-asus-numpad-package "/bin/asus-numpad"))))
           (stop #~(make-kill-destructor))))))

(define (rust-asus-numpad-etc-file config) 
   `(("xdg/asus_numpad.toml"
      ,(plain-file "asus_numpad.toml" "layout = \"M433IA\"
disable_numlock_on_start = true"))))


(define rust-asus-numpad-service-type 
  (service-type
   (name 'rust-asus-numpad)
   (extensions
    (list (service-extension shepherd-root-service-type
                             rust-asus-numpad-shepherd-service-type)
          (service-extension profile-service-type
                             (compose list rust-asus-numpad-configuration-package))
          (service-extension etc-service-type
                             rust-asus-numpad-etc-file)))
   (description "")
   (default-value (rust-asus-numpad-configuration))))


;; boltd

(define-record-type* <bolt-configuration>
  bolt-configuration make-bolt-configuration
  bolt-configuration?
  (bolt-package bolt-configuration-package ;file-like
           (default bolt)))

(define (bolt-shepherd-service-type config)
  (match-record config <bolt-configuration>
    (bolt-package)
    (list (shepherd-service
           (documentation "Control thunderbolt daemon.")
           (provision '(boltd-daemon))
           (requirement '(user-processes)) 
           (start #~(make-forkexec-constructor
                     '(#$(file-append bolt-package "/libexec/boltd") "-v")))
           (stop #~(make-kill-destructor))))))

(define %boltd-activation
  #~(begin
      (use-modules (guix build utils))
      (mkdir-p "/var/lib/boltd")))


(define bolt-service-type 
  (service-type
   (name 'bolt)
   (extensions
    (list (service-extension dbus-root-service-type
                             (compose list bolt-configuration-package))
          (service-extension udev-service-type
                             (compose list bolt-configuration-package))
	  (service-extension activation-service-type
                                            (const %boltd-activation))                         
          (service-extension shepherd-root-service-type
                             bolt-shepherd-service-type)
	  (service-extension polkit-service-type
                             (compose list bolt-configuration-package))
          (service-extension profile-service-type
                             (compose list bolt-configuration-package))))
   (description "")
   (default-value (bolt-configuration))))

  
