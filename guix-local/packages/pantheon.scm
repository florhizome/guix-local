;;; Copyright © 2021 florhizome <florhizome@posteo.net>
;;; Copyright © 2020 Ryan Prior <rprior@protonmail.com>
;;; Copyright © 2020 L  p R n  d n <guix@lprndn.info>
;;; Copyright © 2020 pkill9 <pkill9@runbox.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-local packages pantheon)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python) 
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages display-managers) 
  #:use-module (gnu packages file)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages hunspell)
  #:use-module (gnu packages ibus)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libreoffice)
  #:use-module (gnu packages xdisorg)
    #:use-module (gnu packages ncurses)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pantheon)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages web)  
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages text-editors)
  #:use-module (gnu packages code)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages iso-codes)
  #:use-module (gnu packages language)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages backup)
  #:use-module (ice-9 optargs)
  #:use-module (gnu packages sqlite)
  ;;#:use-module (guix-local packages pantheon-switchboard-plugs)
  )

;;;TODO
;;;apps: iconbrowser friends keyboard
;; plugs
;; notifications 
;; naming-scheme ?

;;needed for videos
(define-public granite-next
  (package
    (inherit granite)
    (name "granite")
    (version "7.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/granite")
         (commit  version)))
       (sha256
        (base32 "0a7arbiajkjkh29v6m2z6kb7n2qr5d3jswdlhlp6y1dyha455mmm"))))
   (arguments
    (list #:configure-flags #~(list "-Dintrospection=true")
          #:phases
          #~(modify-phases %standard-phases
              (add-after 'unpack 'disable-icon-cache
                (lambda _
                  (setenv "DESTDIR" "/"))))))   
   (native-inputs
    (list vala pkg-config gettext-minimal gobject-introspection `(,glib "bin")
          python))
   (inputs (list glib gtk pango-next libgee))))

(define-public onboard
  (package
   (name "onboard")
   (version "1.4.1")
   (source
    (origin
       (method url-fetch)
             (uri
              (string-append "https://launchpad.net/onboard/1.4/"
                             version "/+download/onboard-" version ".tar.gz"))
      (sha256
       (base32 "0r9q38ikmr4in4dwqd8m9gh9xjbgxnfxglnjbfcapw8ybfnf3jh1"))
      (patches
       ;;search switchboard plugs via env var (from nixos)
       (parameterize
           ((%patch-path
             (map
              (lambda (directory)
                (string-append directory "/guix-local/packages/patches"))
              %load-path)))
         (search-patches "fix-paths.patch"
                         "hunspell-use-xdg-datadirs.patch")))))
   (build-system python-build-system)
   (arguments
    (list #:tests? #f ;FIXME display not found
          ;;nix does much more, needs testing
          #:phases
          #~(modify-phases
             %standard-phases
             (add-before
              'build 'patch-paths
              (lambda* (#:key inputs #:allow-other-keys)
                  
                  (substitute* "./setup.py"
                    (("/bin/bash")                      
                     (search-input-file inputs "bin/bash"))
                    (("/etc") (string-append #$output "/etc"))
                    
                    (("killall")
                     (string-append
                      (search-input-file inputs "bin/pkill") " -x")))
                  (substitute* "./Onboard/LanguageSupport.py"
                               (("/usr/share/xml/iso-codes")
                      (search-input-directory inputs "share/xml/iso-codes")))
                  (substitute* "./Onboard/Indicator.py"
                               (("/usr/bin/yelp")
                      (search-input-file inputs "bin/yelp")))
                  (substitute* "./gnome/Onboard_Indicator@onboard.org/extension.js"
                               (("/usr/bin/yelp")
                                 (search-input-file inputs "bin/yelp")))
                  (substitute* "./Onboard/SpellChecker.py"
                               (("/usr/lib")
                                (string-append #$output "/lib")))
                  (substitute* "./Onboard/utils.py" (("/usr/share")
                                                     (string-append #$output "/share")))
                  (substitute* "./onboard-defaults.conf.example" (("/usr/share")
                                                                  (string-append #$output "/share")))
                  (substitute* "./Onboard/Config.py" (("/usr/share/onboard")
                                                      (string-append #$output "/share/onboard")))
                  (substitute* "./Onboard/WordSuggestions.py" (("/usr/bin")
                                                            (string-append #$output "/bin")))
                  )
                  )
              (replace 'install
                (lambda* _
                  (invoke "python" "setup.py" "install"
                          (string-append "--prefix=" #$output))
                  (copy-recursively "onboard-default-settings.gschema.override.example"
                                    (string-append #$output "/share/glib-2.0/schemas/10_onboard-default-settings.gschema.override.example"))
                  ))
              (add-after 'install 'wrap-launcher
                         (lambda* (#:key outputs #:allow-other-keys)
                           (let* ((out (string-append (assoc-ref outputs "out")))
                                  (prog (string-append out "/bin/onboard"))
                                  (sharedir (string-append out "/share"))
                                  (python-path
                                  (getenv "GUIX_PYTHONPATH")))
                             
                             (wrap-program prog
                                           `("GUIX_PYTHONPATH" =  (,python-path))
                                           `("GI_TYPELIB_PATH" = (,(getenv "GI_TYPELIB_PATH")))
                                           ;;`("XDG_DATA_DIRS" = (,out ,(getenv "GI_TYPELIB_PATH")))
                 )))))))
   (native-inputs
    (list gobject-introspection intltool pkg-config python-nose bash-minimal))
   (inputs (list
            at-spi2-core
            bash-minimal
            dconf
            eudev
            glib
            gtk+
            hunspell
            ;;iso-codes
            libcanberra
            libappindicator
            libgee
            libhandy
            libxkbcommon
            libxtst
            libxkbfile
            procps
            
            yelp))
   (propagated-inputs (list
                       iso-codes
                       ncurses
                       python-dbus
                       python-distutils-extra
                       python-pyatspi
                       python-pycairo
                       python-pygobject))
   (home-page "https://launchpad.net/onboard")
   (synopsis "")
   (description "")
   (license license:lgpl2.1)))


(define-public bamf
  (package
    (name "bamf")
    (version "0.5.5")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://git.launchpad.net/bamf")
        (commit version)))
      (sha256
       (base32 "1qz2q3m339xg63pyi4igs7d6587xc18l54z90dngx7zz8lhryffn"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       (list "--enable-gtk-doc"
             "--enable-export-action-menus"
             ;; Taken from NixOS
             "--enable-headless-tests"
             "CFLAGS=-DGLIB_DISABLE_DEPRECATION_WARNINGS")
       #:make-flags
       (list (string-append "INTROSPECTION_GIRDIR="
                            (assoc-ref %outputs "out")
                            "/share/gir-1.0/")
             (string-append "INTROSPECTION_TYPELIBDIR="
                            (assoc-ref %outputs "out")
                            "/lib/girepository-1.0"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'fix-hardcoded-paths
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (substitute*
                   "data/Makefile"
                 (("/usr/lib/systemd/user")
                  (string-append out "/lib/systemd/user")))
               #t)))
         (add-after 'install 'link-to-bamfdaemon
           ;; symlink bamfdaemon in $PATH for plank
           ;; and other applications to find it
           ;; maybe use search path for that?
           (lambda _
             (mkdir-p (string-append
                       (assoc-ref %outputs "out") "/bin"))
             (symlink "../libexec/bamf/bamfdaemon"
                      (string-append
                       (assoc-ref %outputs "out")
                       "/bin/bamfdaemon")))))))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("gobject-introspection" ,gobject-introspection)
       ("vala" ,vala)
       ("glib:bin" ,glib "bin")
       ("python-wrapper" ,python-wrapper)
       ("python-lxml" ,python-lxml)
       ("which" ,which)
       ("gnome-common" ,gnome-common)
       ("automake" ,automake)
       ("xorg-server" ,xorg-server) ; needed for headless tests
       ("python" ,python) ;for tests
       ("autoconf" ,autoconf)
       ("libtool" ,libtool)
       ("gtk-doc" ,gtk-doc)))
    (inputs
     `(("libwnck" ,libwnck)
       ("dbus" ,dbus)
       ("dbus-glib" ,dbus-glib)
       ("startup-notification" ,startup-notification)))
    (propagated-inputs (list libgtop))
    (home-page "https://launchpad.net/bamf")
    (synopsis "Application matching framework")
    (description "Removes the headache of applications matching
 into a simple DBus daemon and c wrapper library.")
    (license (list license:gpl3
                   license:lgpl3
                   license:lgpl2.1))))

;;config: hidpi enabled? (no answer displayed), enable gmock tests - how?
;;will fail with git-fetch ("makefile.in.in macros use gettext version 0.19")
(define-public plank
  (package
    (name "plank")
    (version "0.11.89")
    (source
     (origin (method url-fetch)
             (uri
              (string-append "https://launchpad.net/plank/1.0/"
                             version "/+download/plank-" version ".tar.xz"))
             (sha256
              (base32 "17cxlmy7n13jp1v8i4abxyx9hylzb39andhz3mk41ggzmrpa8qm6"))))
    (build-system glib-or-gtk-build-system)
    (arguments
     `(#:configure-flags (list "--enable-headless-tests")
       #:make-flags
       (list
        (string-append "INTROSPECTION_GIRDIR="
                       (assoc-ref %outputs "out")
                       "/share/gir-1.0/")
        (string-append "INTROSPECTION_TYPELIBDIR="
                       (assoc-ref %outputs "out")
                       "/lib/girepository-1.0"))))
    (native-inputs
     (list autoconf
           automake
           `(,glib "bin")
           gnome-common
           gettext-minimal
           gobject-introspection
           intltool
           libtool
           libxml2
           vala
           pkg-config 
           xvfb-run
           xorg-server-for-tests))
    (inputs
     `(("bamf" ,bamf)
       ("cairo" ,cairo)
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("pango" ,pango)
       ("dconf" ,dconf)
       ("atk" ,atk)
       ("file" ,file)
       ("gnome-menus" ,gnome-menus)
       ("libdbusmenu" ,libdbusmenu)
       ("gtk+" ,gtk+)
       ("dbus" ,dbus)
       ("libx11" ,libx11)
       ("libxfixes" ,libxfixes)
       ("libxi" ,libxi)
       ("libgee" ,libgee)
       ("libwnck" ,libwnck)))
    (home-page "https://launchpad.net/plank")
    (synopsis "Simple dock application")
    (description
     "Plank is a dock application with an extendable library
to create other dock programs.")
    (license (list license:gpl3+
                   license:lgpl2.1+))))

(define-public elementary-dock
  (package
    (inherit plank)
    (name "elementary-dock")
    (version "0.11.89")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/elementary/dock")
             (commit "5e4b5ba2eec3b522e107ad834a59c0f1271d4699")))
       (sha256
        (base32 "16kvrwf1r3yk2zbig2g1gm9hm9gkk23wgpq6jmk1h5smvzb7cjgx"))))
    (build-system meson-build-system)
    (inputs
     (append
      (package-inputs plank)
      `(("granite" ,granite)
        ("xorg-server-xwayland" ,xorg-server-xwayland))))
    (arguments
     '(#:configure-flags (list "-Denable-headless-tests=true")
       #:tests? #f ;;tests fail for e-dock but work for plank ?! :/
       #:glib-or-gtk? #t))))

(define-public gala
  (package
    (name "gala")
    (version "6.3.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/gala")
         (commit version)))
       (sha256
        (base32 "0y9j4mmhb9nr4nw6h4q9a7qiwgzg0ihhvrlg3ph90bbjng5lf7w7"))))
    (native-inputs
     (list
      desktop-file-utils
      gettext-minimal
      (list glib "bin")
      gobject-introspection
      libxml2
      (list gtk+ "bin")
      python
      pkg-config
      vala))
    (inputs
     (list
      atk
      clutter
      mutter
      glib
      gdk-pixbuf
      pango
      graphene
      gsettings-desktop-schemas
      gexiv2
      gnome-desktop
      gnome-settings-daemon 
      granite
      gtk+
      libgee
      mesa
      libcanberra))
    (build-system  meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:configure-flags '("-Dsystemd=false")))
    (home-page "https://www.github.com/elementary/gala")
    (synopsis "Window Manager for elementary OS and Pantheon")
    (description "A window & compositing manager based on libmutter
and designed by elementary for use with Pantheon.")
    (license license:lgpl3+)))


(define-public pantheon-shortcut-overlay
  (package
    (name "pantheon-shortcut-overlay")
    (version "1.2.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/shortcut-overlay")
         (commit version)))
       (sha256
        (base32 "1csny14h1h70i2cm4pms2gl8cb08p4wwwafdrcx56w9p4ccb6sma"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config gettext-minimal vala `(,glib "bin") desktop-file-utils gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public pantheon-notifications
  (package
    (name "pantheon-notifications")
    (version "6.0.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/notifications")
         (commit version)))
       (sha256
        (base32 "0kvmvgg8p7v79kjdpalsxv9nqirq9bh28ia9v8xnyy11vva2hp07"))))
    (build-system meson-build-system)
    (native-inputs
     (list desktop-file-utils gettext-minimal
           `(,glib "bin")
           gobject-introspection
           pkg-config
           python
           vala))
    (inputs
     (list gtk+
           glib
           granite
           libcanberra
           libhandy
           libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))


(define-public pantheon-portals
  (package
    (name "pantheon-portals")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/portals")
         (commit version)))
       (sha256
        (base32 "1xymbafm0fk47hh8hh9gyqlpbmd0mdndrllmxpha37c6y27srk5k"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config desktop-file-utils gettext-minimal desktop-file-utils vala
           `(,glib "bin") gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy vte libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public pantheon-agent-polkit
  (package
    (name "pantheon-agent-polkit")
    (version "1.0.4")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/pantheon-agent-polkit")
         (commit version)))
       (sha256
        (base32 "1acqjjarl225yk0f68wkldsamcrzrj0ibpcxma04wq9w7jlmz60c"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config desktop-file-utils gettext-minimal desktop-file-utils vala
           `(,glib "bin") gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy polkit libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public pantheon-agent-geoclue2
  (package
    (name "pantheon-agent-geoclue2")
    (version "1.0.5")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/pantheon-agent-geoclue2")
         (commit version)))
       (sha256
        (base32 "0hx3sky0vd2vshkscy3w5x3s18gd45cfqh510xhbmvc0sa32q9gd"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config desktop-file-utils gettext-minimal desktop-file-utils vala
           `(,glib "bin") gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy geoclue libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public pantheon-portals
  (package
    (name "pantheon-portals")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/portals")
         (commit version)))
       (sha256
        (base32 "1xymbafm0fk47hh8hh9gyqlpbmd0mdndrllmxpha37c6y27srk5k"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config desktop-file-utils gettext-minimal desktop-file-utils vala
           `(,glib "bin") gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy vte libgee))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define-public wingpanel
  (package
    (name "wingpanel")
    (version "")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/wingpanel")
         (commit version)))
       (sha256
        (base32 "10n1ypb42q682q7zdi6xwaza6kwfwxwlac25wkgiayzkgm1fx5za"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (native-inputs
     (list
      desktop-file-utils
      (list glib "bin")
      (list gtk "bin")
      vala
      pkg-config))
    (inputs
     (list
      accountsservice
      dbus
      gtk+
      glib
      gala
      granite
      geoclue
      libgee
      libhandy
      mutter))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define make-wingpanel-indicator
  (lambda* (name version hash #:key (more-inputs '()) synopsis description)
    (package
      (name name)
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url (string-append "https://github.com/elementary/" name))
           (commit version)))
         (sha256
          (base32 hash))))
      (build-system meson-build-system)
      (arguments
       `(#:glib-or-gtk? #t))
      (native-inputs 
       (list pkg-config vala `(,glib "bin") gettext-minimal
             gobject-introspection python-minimal))
      (inputs (cons* libgee
                     gtk+
                     granite
                     wingpanel
                     bamf
                     more-inputs))
      (home-page "https://elementary.io")
      (synopsis "")
      (description "")
      (license license:gpl2+))))

(define-public wingpanel-indicator-a11y
  (make-wingpanel-indicator
   "wingpanel-indicator-a11y"
   "1.0.0"
   "1adx1sx9qh02hjgv5h0gwyn116shjl3paxmyaiv4cgh6vq3ndp3c"))

(define-public wingpanel-indicator-sound
  (make-wingpanel-indicator
   "wingpanel-indicator-sound"
   "6.0.2"
   "0d6g3m6x88k2lq0dpqlcdkqmbiisfn37ccmrsfyzh526nscc67hz"))

(define-public wingpanel-indicator-datetime
  (make-wingpanel-indicator
   "wingpanel-indicator-datetime"
   "2.4.1"
   "0zpx713pz0i3bx1bzvxd3r61hbmxzff9j50m7s24fv3ngm638676"
   #:more-inputs (list evolution-data-server libical libhandy libsoup-minimal-2)))

(define-public wingpanel-indicator-network
  (make-wingpanel-indicator
   "wingpanel-indicator-network"
   "2.3.4"
   "0n73ig3zqikwvhll64khwcfzgiw38m8mghdq3z5d14k278li59cg"
   #:more-inputs (list network-manager libnma)))

(define-public wingpanel-indicator-power
    (make-wingpanel-indicator
  "wingpanel-indicator-power"
   "6.2.0"
   "0qr61m8qcs5i7b72zmy73kl0zll3jvs8jmi79sjwidi2pj8yq6jg"
   #:more-inputs (list libgudev libnotify )))

(define-public wingpanel-indicator-bluetooth
  (make-wingpanel-indicator
   "wingpanel-indicator-bluetooth"
   "2.1.8"
   "12rasf8wy3cqnfjlm9s2qnx4drzx0w0yviagkng3kspdzm3vzsqy"
   #:more-inputs (list libnotify)))

(define-public wingpanel-indicator-nightlight
  (make-wingpanel-indicator
   "wingpanel-indicator-nightlight"
   "2.1.1"
   "0jmdhzjypv23jq9a8bcxph2npvc6by51m0x7h1ydm6v5wfyhz5d1"))

(define-public wingpanel-indicator-session
  (make-wingpanel-indicator
   "wingpanel-indicator-session"
   "2.3.1"
   "1n98gwnib39hcg76ch3iw7zcvj28flaij8jkbff3idawjxxhq0fq"
   #:more-inputs (list libhandy accountsservice)))

(define-public wingpanel-indicator-notifications
  (make-wingpanel-indicator
   "wingpanel-indicator-notifications"
   "6.0.7"
   "0xxr9317yxbp7swz7j49fi2dr2rwyqzrl5648332539qc9ab52rh"
   #:more-inputs (list libhandy)))

(define-public wingpanel-indicator-keyboard
  (make-wingpanel-indicator
   "wingpanel-indicator-keyboard"
   "2.4.1"
   "0d4mry1mmb28wq0ig2h8fpm2fvgsjbzvi9q9a88kdnvynfbw0r02"
   #:more-inputs (list ibus xkeyboard-config libxml2)))

(define-public wingpanel-indicator-privacy
  (make-wingpanel-indicator
   "wingpanel-indicator-privacy"
   "6dea369e25a421c6cce513a8bab4cac162162238"
   "0y5nlgbyq6ww32hbhvimsf5fz0i8hvgzxwg3il5sjqfznclxfc1q"))


;;;FIXME
(define-public pantheon-session-settings
  (package
    (name "pantheon-session-settings")
    (version "6.0.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/session-settings")
         (commit version)))
       (sha256
        (base32 "1faglpa7q3a4335gnd074a3lnsdspyjdnskgy4bfnf6xmwjx7kjx"))))
    (build-system meson-build-system)
    (arguments
     (list #:configure-flags
           #~(list "-Dmimeapps-list=false"
                   "-Dfallback-session=gnome"
                   "-Ddetect-program-prefixes=true"
                   (string-append "--sysconfdir=" #$output "/etc"))))
    (native-inputs
     (list pkg-config desktop-file-utils))
    (inputs
     (list desktop-file-utils
           gnome-settings-daemon
           gnome-keyring
           onboard
           orca))
    (home-page "https://github.com/elementary/session-settings")
    (synopsis "Session settings for elementary OS")
    (description "")
    (license license:gpl2)))



(define-public pantheon-default-settings
  (package
    (name "pantheon-default-settings")
    (version "7.0.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/default-settings")
         (commit version)))
       (sha256
        (base32 "0fi6qqwvjvyq0sffa1d0b69ihm561bv283d45x9g47z9x9jxgls0"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config python `(,glib "bin") `(,gtk "bin") desktop-file-utils))
    (inputs
     (list polkit dbus accountsservice))
    (arguments `(#:configure-flags '("-Dappcenter-flatpak=false")))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))


(define-public contractor
  (package
    (name "contractor")
    (version "0.3.5")
    (source
     (origin
       (method git-fetch)
       (file-name (git-file-name name version))
       (uri
        (git-reference
         (url "https://github.com/elementary/contractor.git")
         (commit version)))
       (sha256
        (base32
         "1sqww7zlzl086pjww3d21ah1g78lfrc9aagrqhmsnnbji9gwb8ab"))))
    (arguments
     `(#:glib-or-gtk? #t))
      (build-system meson-build-system)
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ("vala" ,vala)
         ("gobject-introspection" ,gobject-introspection)
         ("dbus" ,dbus)))
      (inputs
       `(("libgee" ,libgee)
         ("glib" ,glib)))
      (home-page "https://github.com/elementary/contractor")
      (synopsis "Desktop-wide extension service used by elementary OS")
      (description "A desktop-wide extension service used by elementary OS.")
      (license license:gpl3+)))

(define-public capnet-assist
  (package
    (name "capnet-assist")
    (version "2.4.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/capnet-assist")
         (commit version)))
        (sha256
         (base32 "1a8408a2zw5bm8gz0rwhc8yxl6529w7lgnpkar29rk81n7gnj67j"))))
    (build-system meson-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; https://github.com/elementary/capnet-assist/issues/3
         (add-after 'unpack 'dump-test
           (lambda _
             (substitute* "meson.build"
               (("get_option\\('sysconfdir'\\)")
                "'tmp'"))
             #t)))))
    (native-inputs
     (list pkg-config
           gettext-minimal
           vala `(,glib "bin")))       ;glib-compile-schemas
    (inputs (list granite libhandy libgee gtk+ gcr webkitgtk-with-libsoup2))
    (home-page "https://github.com/elementary/capnet-assist")
    (synopsis "Captive Portal Assistant")
    (description "Log into captive portals—like Wi-Fi networks at coffee shops,
airports, and trains.")
    (license license:gpl2+)))

;;FIXME only works up until libmutter-11/mutter-41
(define-public pantheon-greeter
  (package
    (name "pantheon-greeter")
    (version "6.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/elementary/greeter/")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32  "0083h4dw4wq6alym35hnm3q74qwc98xmk5d5qbckhfa9gwwxkf7a"))))
    (build-system meson-build-system)
    (arguments
     (list #:glib-or-gtk? #t
       #:configure-flags
       #~(list
          (string-append "-Dgsd-dir="
                           #$(this-package-input "gnome-settings-daemon")
                           "/libexec/"))))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("vala" ,vala)
       ("gettext" ,gettext-minimal)
       ("desktop-file-utils" ,desktop-file-utils)
       ("gobject-introspection" ,gobject-introspection)
        ("libxml2" ,libxml2)
       ("glib:bin" ,glib "bin")
       ("gtk+:bin" ,gtk+ "bin")))
    (inputs
     (list accountsservice
           lightdm
           granite
           gdk-pixbuf
           gnome-settings-daemon
           gtk+
           libgee
           libhandy
           mutter
           xorg-server))
    (home-page "https://github.com/elementary/gala")
    (synopsis "")
    (description "")
    (license license:gpl3)))

;;no description at all there..
(define-public pantheon-settings-daemon
  (package
    (name "pantheon-settings-daemon")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/settings-daemon")
         (commit version)))
       (sha256
        (base32 "0lnickmlr7ydzckc1j3c1phmp2vi6hyjlq8jjscy25n3kjw7aynm"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:configure-flags '("-Dsystemduserunitdir=false")))
    (native-inputs
     (list pkg-config desktop-file-utils `(,glib "bin") `(,gtk "bin") vala))
    (inputs
     (list gtk+ glib granite libhandy
           geoclue accountsservice dbus libgee))
    (home-page "https://github.com/elementary/settings-daemon")
    (synopsis "")
    (description "")
    (license license:gpl3)))

(define-public pantheon-applications-menu
  (package
    (name "pantheon-applications-menu")
    (version "2.10.2")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/applications-menu")
         (commit version)))
       (sha256
        (base32 "0afg10wvhcnvjjdv7p8nwcg4v3q0777msbywpj9fffh568kqq6y4"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
       
    (native-inputs
     (list pkg-config appstream desktop-file-utils gettext-minimal desktop-file-utils vala
           `(,glib "bin") gobject-introspection))
    (inputs
     (list gtk+ glib granite libhandy polkit
           libgee plank zeitgeist json-glib gnome-menus libsoup-minimal-2
           ;;switchboard
           wingpanel bamf))
    (home-page "https://github.com/elementary/applications-menu")
    (synopsis "The Pantheon applications menu")
    (description "The Pantheon applications menu.")
    (license license:gpl3+)))


(define-public pantheon-stylesheet
  (package
    (name "pantheon-stylesheet")
    (version "6.1.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/stylesheet")
         (commit version)))
       (sha256
        (base32 "1vrz1vg0bk18byb77q6n1qs9nq6qmm0fd2qkfnz2pr0hjfgq3j41"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config sassc))
    (home-page "https://elementary.io")
    (synopsis "GTK stylesheet for elementary OS")
    (description "This is the GTK Stylesheet for elementary OS.")
    (license license:gpl3)))

(define-public elementary-sound-theme
  (package
    (name "elementary-sound-theme")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (file-name (git-file-name name version))
       (uri
        (git-reference
         (url "https://github.com/elementary/sound-theme.git")
         (commit version)))
       (sha256
        (base32
         "0kyqh4y6fk0mhlxc5ary17f1fhg2n6f6gl2v8wval9vxmjsa07kx"))))
    (build-system meson-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://github.com/elementary/sound-theme")
    (synopsis "Set of system sounds for elementary OS")
    (description "A set of system sounds for elementary OS.
Designed to be light, natural/physical, and pleasant.")
    (license license:unlicense)))

(define-public elementary-wallpapers
  (package
    (name "elementary-wallpapers")
    (version "6.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/wallpapers")
         (commit version)))
       (sha256
        (base32 "0c2lf7ixp8n0fpffhyvxlpg6w4hzmgxvmvj8sksdydn6mz2i9xqk"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config libexif))
    (home-page "https://elementary.io")
    (synopsis "Collection of wallpapers for elementary OS")
    (description "The collection of wallpapers for elementary OS shipped by default.")
    (license (list
              ;;might not be all
              license:cc0
              license:cc-by-sa4.0))))

(define-public elementary-icon-theme
  (package
    (name "elementary-icon-theme")
    (version "6.1.0")
    (source
     (origin
       (method git-fetch)
       (uri
       (git-reference
        (url "https://github.com/elementary/icons")
        (commit version)))
       (sha256
        (base32 "0ziyl7fcw6hdwd205j16gy2hpvg0par5qir1g4b3san995bhf7jr"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       (list "-Dvolume_icons=false")))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("xcursorgen" ,xcursorgen)
       ("librsvg" ,librsvg)))
    (inputs
     `(("gtk+" ,gtk+)))
    (propagated-inputs
     `(("hicolor-icon-theme" ,hicolor-icon-theme)))
    (home-page "https://github.com/elementary/icons")
    (synopsis "Named, vector icons for elementary OS")
    (description "An original set of vector icons designed
 specifically for elementary OS and its desktop environment: Pantheon.")
    (license license:gpl3)))
