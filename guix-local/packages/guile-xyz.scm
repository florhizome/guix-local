(define-module (guix-local packages  guile-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cargo)
  #:use-module ((guix licenses)   #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages guile-xyz))

(define-public tekuti
  (package
    (name "tekuti")
    (version "0.0.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/wingo/tekuti")
          (commit "master")))
       (sha256
        (base32
         "1l66kg4nh3dvwvcdc93q3xsbihwny31887hyf7qrzan2km3xlv8k"))))
    (build-system gnu-build-system)
    (native-inputs
     (list guile-next pkg-config automake autoconf libtool))
    (inputs
     (list guile-next))
    (propagated-inputs (list git))
    (arguments
     `(#:make-flags '("GUILE_AUTO_COMPILE=0")
     #:phases
       (modify-phases %standard-phases
         (add-before 'bootstrap 'no-autocomp
           (lambda _
             (setenv "GUILE_AUTO_COMPILE" "0"))))))
    (synopsis "Server-side blog engine written in Scheme")
    (description "Tekuti is a simple functional blogging engine based on git and sxml used by guile's maintainer Andy Wingo.")
    (home-page "https://github.com/wingo/tekuti")
    (license license:gpl3+)))

(define-public guilescript
  (package
    (name "guilescript")
    (version "0.2.0")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/aconchillo/guilescript")
          (commit (string-append "v" version))))
       (sha256
        (base32
         "15bvgklv77kvkl8dizriqblfir6rid5nm79ymi3m2fvpd7wf77qy"))))
    (build-system gnu-build-system)
    (native-inputs
     (list guile-3.0 pkg-config automake autoconf libtool))
    (inputs
     (list guile-3.0-latest))
    (arguments
     `(#:make-flags '("GUILE_AUTO_COMPILE=0")))
    (synopsis "Compile Guile to Javascript, similar to Clojurescript")
    (description "")
    (home-page "https://github.com/wingo/tekuti")
    (license license:gpl3+)))

(define-public rust-guile-0.1.6
  (package
    (name "rust-guile")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri
        (crate-uri "rust-guile" version))
       (sha256
        (base32 "1rp6ajkhbxkig8z6w8qn6k11hh11h40zkxh4dqqpkajkh7irxmyh"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (inputs (list guile-next))
    ;;FIXME this feels hacky 
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'chdir
           (lambda _
             (chdir "./guix-vendor/download"))))
       #:cargo-inputs
       (("rust-pkg-config-0.3" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/slondr/rust-guile")
    (synopsis "Provides a scheme runtime in your rust library")
    (description "This package provides a way to create access to
rust functions from guile.")
    (license (list license:agpl3))))
