(define-module (guix-local packages  kvantum)
  #:use-module (guix download)
  #:use-module (guix build-system qt)
  #:use-module ((guix licenses)  #:prefix license:)
  #:use-module (guix utils) ;; for substitute-keyword-arguments
  #:use-module (guix download)
  #:use-module (gnu packages base)
  #:use-module (guix build utils)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages kde-frameworks))

(define-public kvantum
  (package
    (name "kvantum")
    (version "1.0.5")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://github.com/tsujan/Kvantum/releases/download/V"
                       version "/Kvantum-" version ".tar.xz"))
       (sha256
        (base32 "1j58xighh9zd2shc1hjya6n8kixii8g5qkj2wlcsh82hf63kbdvx"))))
    (build-system qt-build-system)
    (native-inputs
     (list qttools-5 pkg-config))
    (inputs
     (list qtbase-5 libx11 libxext qtx11extras qtsvg-5 kwindowsystem))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'correct-directory
           (lambda _
             (display (find-files "."))
             (copy-recursively "./Kvantum" ".")
             (delete-file-recursively "./Kvantum")))
         (add-after 'patch-source-shebangs 'alter-target-dirs
           ;; maybe could use less code here
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (substitute* (find-files "style/CMakeLists.txt")
                 (("\\$\\{_Qt5_PLUGIN_INSTALL_DIR\\}")
                  (string-append out "/lib/qt5/plugins")))
               (substitute* (find-files "style/CMakelists.txt")
                 (("\\$\\{QT_PLUGINS_DIR\\}")
                  (string-append out "/lib/qt5/plugins")))
               #t))))))
       (native-search-paths
        (list (search-path-specification
               (variable "QT_PLUGIN_PATH")
               (files '("/lib/qt5/plugins")))))       
       (home-page "https://www.github.com/tsujan/kvantum")
       (synopsis "An SVG-based theme engine for Qt, tuned to KDE and LXQt, with an emphasis on elegance, usability and practicality")
   (description "Kvantum has a default dark theme, which is inspired by the default theme of Enlightenment. Creation of realistic themes like that for KDE was my first reason to make Kvantum but it goes far beyond its default theme: you could make themes with very different looks and feels for it, whether they be photorealistic or cartoonish, 3D or flat, embellished or minimalistic, or something in between, and Kvantum will let you control almost every aspect of Qt widgets.")
   (license license:gpl3+))) 

(define-public qt5ct-fix
  (package
   (inherit qt5ct)
   (version "1.2")
   (source
     (origin
       (method url-fetch)
       (uri
        (string-append "mirror://sourceforge/qt5ct/qt5ct-" version ".tar.bz2"))
       (sha256
        (base32 "0bl7dc03b7vm435khkr932ybslsbq1nfajd936zlc1sxdmpg1qqx"))))

   (native-search-paths
    (list (search-path-specification
           (variable "QT_PLUGIN_PATH")
           (files '("/lib/qt5/plugins")))
          ))))


