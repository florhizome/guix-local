(define-module (guix-local packages scheme-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system node)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system guile)
  #:use-module ((guix licenses)   #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages ncurses)
  
  #:use-module (gnu packages guile)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages guile-xyz)
   )



(define-public lips
  (package
    (name "lips")
    (version "1.0.0-beta.16")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/jcubic/lips")
          (commit version)))
       (sha256
        (base32
         "0j7f9qhcr8rrcnpxwkxh7fsframz5kdsh5cpsfl48h9s7s551bk8"))))
    (build-system node-build-system)
    (synopsis "Scheme based lisp interpreter written in JS")
    (description "")
    (home-page "https://lips.js.org")
    (license license:gpl3+)))


(define-public scheme-scss
  (package
    (name "scheme-scss")
    (version "0.4.2")
    (source
     (origin
       (method url-fetch)
       (uri "http://savannah.nongnu.org/download/scss/scss-0.4.2.tar.gz")
       (sha256
        (base32
         "08br175464m5a1gr0bfjh1c33zjyiwgipal72pa8ghbpdl2jn5lg"))))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
           (add-before 'build 'copy-to-module-dir
             (lambda _
               (mkdir-p "scss/interface")
               (let ((scheme-files (find-files "." "\\.scm$")))
                 (for-each
                  (lambda (file)
                    (copy-recursively file (string-append "./scss" "/" file)))
                  scheme-files)))))))
    (native-inputs (list guile-3.0 scheme-sdom))
    (inputs (list scheme-sdom))
    (build-system guile-build-system)
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public scheme-sdom
  (package
    (name "scheme-sdom")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri "http://savannah.nongnu.org/download/sdom/sdom-0.5.1.tar.gz")
       (sha256
        (base32
         "06fwfisia0jnfrkhc6h444sxvy6imsz1icr1kxyh4ypgdhr2jp1m"))))
    (build-system guile-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
           (add-before 'build 'copy-to-module-dir
             (lambda _
               (mkdir-p "sdom")
               (let ((scheme-files (find-files "." "\\.scm$")))
                 (for-each
                  (lambda (file)
                    (copy-file file (string-append "./sdom" "/" file)))
                  scheme-files)))))))
    (native-inputs (list guile-3.0))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public libruin
  (package
    (name "libruin")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri "http://savannah.nongnu.org/download/libruin/libruin-0.2.0.tar.gz")
     (sha256
      (base32
       "1vwhc3xnxybxkp15r0p3fvs1g4fyhgk7yqw2x7dm0h9b2p8laaxf"))))
    (build-system gnu-build-system)
    (native-inputs (list pkg-config ncurses guile-2.0))  
    (inputs (list glib))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

