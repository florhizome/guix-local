(define-module (guix-local packages wayfire-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses)   #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages zig) 
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages video)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages wm)
  #:use-module (guix-local packages wayland-updates))


(define-public wf-shell-patch
  (package
   (name "wf-shell-patch")
   (version "0.8")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://www.github.com/florhizome/wf-shell")
       (commit "73b0d1503a56174728f336a4e4984ee22012ed75")))
     (sha256
      (base32 "1f9angn3mll64mldyww0y4051xxm8ns39gw7vrxps0qbf30ibqz6"))))
   (build-system meson-build-system)
   (arguments
    `(#:configure-flags '("-Dwayland-logout=false")))
   (native-inputs
    (list
     gobject-introspection
     pkg-config))
   (inputs
    (list alsa-lib
     glib
     gtk-layer-shell
     gtkmm-3
     libdrm
     wayfire
     wlroots-for-wayfire
     wayland
     wf-config))
   (native-search-paths
    (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build a
fully functional DE based around wayfire.  Currently it has only a GTK-based
panel and background client.")
   (license license:expat)))


(define-public wf-dbus-interface
  (package
   (name "wf-dbus-interface")
   (version "0.2")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/florhizome/wayfire-plugin_dbus_interface")
       (commit "f3790fd3042a1dca96c980108286c0e78b18b8f2")))
     (sha256
      (base32 "09lckn87bvmfckdfxm3x9w0w509cgkbwgw57zim4kj563b69x32j"))))
   (build-system meson-build-system)
   (arguments
    `(#:glib-or-gtk? #t
      #:meson ,meson-0.59
      #:phases                     
      (modify-phases %standard-phases
        (add-after
            'unpack 'alter-target-dirs
          (lambda* (#:key outputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out")))
              (substitute* "meson.build"
                (("wayfire\\.get_variable\\(pkgconfig: 'metadatadir'\\)")
                 (string-append "'" out "/share/wayfire/metadata'")))                              
              (substitute* "meson.build"
                (("wayfire\\.get_variable\\(pkgconfig: 'plugindir'\\)")
                 (string-append "'" out "/lib/wayfire'")))
              #t))))))
   (native-inputs
    (list
     gobject-introspection
     `(,glib "bin")
     `(,gtk+ "bin")
     pkg-config))
   (inputs
    (list gtk+ gtkmm-3 glib gtk-layer-shell wayfire
     wayfire-plugins-extra wlroots-for-wayfire wayland dbus-glib
     wf-config))
   (native-search-paths (package-native-search-paths wayfire))
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build a
fully functional DE based around wayfire.  Currently it has only a GTK-based
panel and background client.")
   (license license:expat)))

(define-public wf-windecor
  (package
   (name "wf-windecor")
   (version "0.2")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/wayfireplugins/windecor")
       (commit "9ff6f7d6fc46b075a7d55db642b9b67a81bfddd1")))       
     (sha256
      (base32 "0rm7a8qhzjdzxbcn0ksnxs6jyql1qqd37ryc7g40f0zm34hvsr10"))))
   (build-system meson-build-system)
   (arguments
    `(#:glib-or-gtk? #t
      #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'alter-target-dirs
          (lambda* (#:key outputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out")))
              ;;subprojects are in ./subprojects/projectname/meson.build
              (substitute* "./meson.build"
                (("wayfire\\.get_variable\\( pkgconfig: 'metadatadir' \\)")
                 (string-append "'" out "/share/wayfire/metadata'")))                 
                 #t))))))
   (native-inputs
    (list pkg-config))
   (inputs
    (list
     wayland wf-config pixman wayfire wayfire-plugins-extra
     wlroots-for-wayfire librsvg))
   (native-search-paths (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "Third Party plugin for Wayfire to customize window
 decorations")
   (description "Windecor is a featureful plugin for customizing the
titlebars in Wayfire. It supports placing the titlebars on every site
of the window.")
   (license license:expat)))

(define-public wf-firedecor
  (package
   (name "wf-firededor")
   (version "0.2")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/AhoyISki/Firedecor")
       (commit "0170c4df209f0226295a9e2aa3cae782d9861908")))       
     (sha256
      (base32 "18b7aj628k2mi0h1fr6k10bknf8d8c7kxqbfgbp745wqi2lm14bk"))))
   (build-system meson-build-system)
   (arguments
    `(#:glib-or-gtk? #t
      ))
   (native-inputs
    (list pkg-config))
   (inputs
    (list
     wayland-1.20 wf-config boost wayfire
     wlroots-0.15 librsvg))
   (native-search-paths (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "Third Party plugin for Wayfire to customize window
 decorations")
   (description "Windecor is a featureful plugin for customizing the
titlebars in Wayfire. It supports placing the titlebars on every site
of the window.")
   (license license:expat)))


(define-public wf-lua
  (package
    (name "wf-lua")
    (version "0.1")
    (source
     (origin (method git-fetch)
             (uri
              (git-reference
               (url "https://github.com/Javyre/wf-lua")
               (commit "889e7cc090365be63cd6bf604c58a2b962a6cb5f")))
             (sha256
              (base32 "1h182p7rwbw41m4hg0k7pqs1zizixyg0095g7r3ka7dl4jzd318a"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
                    (let* ((out (assoc-ref outputs "out")))
                      (setenv "HOME" (getcwd))
                      (setenv "CC" "gcc")
                      (invoke "zig" "build" "install" "--prefix"
                              out "-Drelease-fast=true" "--verbose") #t)))
         (delete 'install))))
    (native-inputs
     (list pkg-config zig clang))
    (inputs
     (list wf-config wayfire luajit wayland wlroots))
    (native-search-paths (package-native-search-paths wayfire))
    (search-paths  native-search-paths)       
    (synopsis "Lua as an advanced configuration language for wayfire")
    (description "An experiment to use Lua as an advanced configuration language for @code{wayfire}.
@code{wf-lua} is meant for use-cases where writing an actual wayfire plugin shouldn't be necessary, such as user configuration, window management automation scripts, eventually an ipc.")
    (home-page "https://gitlab.com/javyre/wf-lua")
    (license license:gpl3)))

(define-public wf-scud
  (package
   (name "wf-scud")
   (version "0.1")
   (source
    (origin (method git-fetch)
            (uri
             (git-reference
              (url "https://gitlab.com/magus/wf-scud.git") 
              (commit "6447c269821c7222bf732224809612e48fc0e73d")))
            (sha256 (base32 "01s1wa3ywidhz8aj002wka955rbcc3dhklvxh3yf3xishp6a4qnn"))))
   (build-system gnu-build-system)
   (native-inputs
    `(("pkg-config" ,pkg-config)
       ("ninja" ,ninja)
       ("meson" ,meson)))
    (inputs 
     `(("wayfire" ,wayfire)
       ("wf-config" ,wf-config)
       ("wlroots" ,wlroots)
       ("wayland" ,wayland)
       ("wayland-protocols" ,wayland-protocols)))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
                      (replace 'configure 
                              (lambda _
                                (system "make setup"))) ;problem for future: needs to return #t                                
                      (replace 'install
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (let ((out (assoc-ref outputs "out")))
                                   (install-file
                                    "./_build/src/libscud.so" (string-append out "/lib/wayfire"))
                                    (install-file "./metadata/scud.xml"
                                                  (string-append out "/share/wayfire/metadata"))
                                    #t))))))
    (native-search-paths (package-native-search-paths wayfire))
    (search-paths  native-search-paths)       
    (synopsis "Wayfire plugin to manage windows in a tiling and sliding way")
    (description "A wayfire plugin inspired by PaperWM.")
    (home-page "https://gitlab.com/magus/wf-scud")
    (license "")))

(define-public wf-gsettings
  (let ((commit "2f872daa8731d7da8e49e93010e282e1ae30214d")
        (revision "0")
        (version "0.09"))
  (package
    (name "wf-gsettings")
    (version "0.09")
    (source
     (origin (method git-fetch)
             (uri
              (git-reference
               (url "https://github.com/DankBSD/wf-gsettings.git") 
               (commit commit)
               (recursive? #t)))
             (sha256 (base32 "03ivnip0blrmylqcn4a7nz1j7pssvky3vabnz92qgka8089dzpr1"))))
    (build-system meson-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("gobject-introspection" ,gobject-introspection)))
    (inputs (append (package-inputs wlroots)
     `(("glib" ,glib)
       ("libxslt" ,libxslt)
       ("wlroots" ,wlroots)
       ("wf-config" ,wf-config)
       ("wayfire" ,wayfire))))       
    (native-search-paths (package-native-search-paths wayfire))
    (search-paths  native-search-paths)       
    (description "A gsettings plugin for wayfire")
    (synopsis "GSettings plugin for Wayfire")
    (home-page "https://github.com/DankBSD/wf-gsettings")
    (license license:unlicense))))

(define-public wf-wallpaper 
  (let ((commit "3543946c6163822d4572ebf93f25ce332d70c000")
        (revision "0")
        (version "0.09"))
  (package
    (name "wf-wallpaper")
    (version "0.09")
    (source
     (origin (method git-fetch)
             (uri
              (git-reference
               (url "https://github.com/DankBSD/wf-wallpaper") 
               (commit commit)
               (recursive? #t)))
             (sha256
              (base32 "1s2xfxddmzjs3xnp4z1gwmg81004z2w8rj3f4galnsz77m4z19ma"))))
    (build-system meson-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (inputs
     (append
      (package-inputs wlroots)
      `(("gdk-pixbuf" ,gdk-pixbuf)
        ("wlroots" ,wlroots)
        ("wf-config" ,wf-config)
        ("wayfire" ,wayfire))))       
    (native-search-paths (package-native-search-paths wayfire))
    (search-paths  native-search-paths)       
    (description "An advanced wallpaper plugin for Wayfire that supports targetting specific workspaces, and shaders.")
    (synopsis "Advanced wallpaper plugin for Wayfire" )
    (home-page "")
    (license license:unlicense))))

(define-public wstroke
  (let
      ((version "0.01")
       (revision "0")
       (commit "da946a3a4e5944b1b90b021a678df0fd3f8a8ec2"))
  (package
    (name "wstroke")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/dkondor/wstroke.git") 
         (commit commit)))
       (sha256
        (base32 "098wha9zp1lxibjzcgfagc5a8grmjfj7d9v3vg51gll6l4isn19r"))))
    (build-system meson-build-system)
    (arguments `(#:phases 
                 (modify-phases %standard-phases
                   (add-after 'unpack 'alter-target-dirs
                     (lambda* (#:key outputs #:allow-other-keys)
                       (let*
                           ((out (assoc-ref outputs "out")))
                         (substitute*
                             "./meson.build"
                           (("wayfire\\.get_variable\\(pkgconfig: 'metadatadir'\\)")
                            (string-append "'" out "/share/wayfire/metadata'")))
                         (substitute*
                             "./meson.build"
                         (("wayfire\\.get_variable\\(pkgconfig: 'plugindir'\\)")
                          (string-append "'" out "/lib/wayfire'")))
                         ;; 'datadir' either not provided correctly or taken from wayfire
                         (substitute*
                             "./icons/meson.build" (("wayfire\\.get_variable\\(pkgconfig: 'icondir'\\)")
                          (string-append "'" out "/share/wayfire/icons'"))))
                       #t)))))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("glib:bin" ,glib "bin")       
       ("vala" ,vala)
       ("gobject-introspection" ,gobject-introspection)
       ("libxml2" ,libxml2)))
    (inputs
     `(("wlroots" ,wlroots)
       ("wf-config" ,wf-config)
       ("wayfire" ,wayfire)
       ("gtkmm-3" ,gtkmm-3)
       ("glibmm" ,glibmm)
       ("gtk+" ,gtk+)
       ("gtk-layer-shell" ,gtk-layer-shell)
       ("glib" ,glib)
       ("boost" ,boost)
       ("libinput" ,libinput)))
    (native-search-paths (package-native-search-paths wayfire))
    (search-paths  native-search-paths)       
    (description "Port of Easystroke mouse gestures as a plugin for Wayfire. Mouse gestures are shapes drawn on the screen while holding down one of the buttons (typically the right or middle button). This plugin allows associating such gestures with various actions.")
    (synopsis "Port of easystroke mouse gestures as a plugin for Wayfire")
    (home-page "https://github.com/dkondor/wstroke")
    (license license:isc))))

(define-public wayfire-plugins-ammen99 
  (package
    (name "wayfire-plugins-ammen99")
    (version "0.01")
    (source
     (origin (method git-fetch)
             (uri
              (git-reference
               (url "https://github.com/ammen99/wayfire-plugins.git") 
               (commit "763109bb431c7cefa445dc17bdc1f5fdd7a769ad")))
             (sha256 (base32 "1znvcsw916i9pbbm8wzligmi9i112c688p44q1w6c4idfipydkd6"))))
    (build-system meson-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)
        ))
    (inputs
     `(("wlroots" ,wlroots)
       ("wf-config" ,wf-config)
       ("wayfire" ,wayfire)))      
  (arguments `(#:phases
               (modify-phases %standard-phases
                              (add-after 'unpack 'alter-target-dirs
                                         (lambda* (#:key outputs #:allow-other-keys)
                                           (let* ((out (assoc-ref outputs "out")))
                                             (substitute*
                                              "./metadata/meson.build"
                                              (("wayfire\\.get_variable\\(pkgconfig: 'metadatadir'\\)")
                                               (string-append "'" out "/share/wayfire/metadata'")))                                 
                                             (substitute* "./src/meson.build"
                                                          (("wayfire\\.get_variable\\(pkgconfig: 'plugindir'\\)")
                                                           (string-append "'" out "/lib/wayfire'"))) #t))))))
  (native-search-paths (package-native-search-paths wayfire))
  (search-paths  native-search-paths)       
  (description "")
  (synopsis "")
  (home-page "")
  (license license:expat)))
