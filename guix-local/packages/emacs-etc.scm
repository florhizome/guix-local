(define-module (guix-local packages  emacs-etc)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix build utils)
  #:use-module (guix build gremlin)
  #:use-module (guix build emacs-utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages python)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-webkit
  (let ((commit "96a4850676b74ffa55b52ff8e9824f7537df6a47")
        (revision "0")
        (version "0.09"))
  (package
   (name "emacs-webkit")
   (version (git-version version revision commit))
   (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/akirakyle/emacs-webkit")
            (commit commit)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "0ifdngan6jhbz6p72igwvmz7lhmz7hl8ak5n7zjkvxmq05kxkc5a"))
;;; The snippet before (search-patches ...) is copied from nonguix:
;;; what didn't work: just putting it in /guixrus/patches
;;; NOTE: there are also channels where patches just lie in the rootdir
;;; TODO: How to find patches in channels/locally without this?
      ;;an unmerged commit; package doesn't build without
      (patches
       (parameterize
           ((%patch-path
             (map
              (lambda (directory)
                (string-append directory "/guix-local/packages/patches"))
              %load-path)))
         (search-patches "emacs-webkit-require-org-link.patch")))))
   (build-system emacs-build-system)
   (native-inputs (list libtool
                        gsettings-desktop-schemas
                        pkg-config))
   (inputs (list emacs-evil-collection
                 gstreamer
                 glib
                 glib-networking
                 webkitgtk-with-libsoup2))
   (arguments
    `(;;#:emacs ,emacs-next
      ;; TODO most of these might actually be unneeded
      #:phases
       (modify-phases %standard-phases
         (add-before 'install 'make-webkit-module
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (setenv "CC" ,(cc-for-target))
               (setenv "PREFIX" out)
               (setenv "HOME" (getcwd))
               (invoke "make" "all"))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((install (assoc-ref %standard-phases 'install)))
               (install
                #:outputs outputs
                #:include (cons* "\\.so$" "\\.css$" "\\.js$"
                                 %default-include))))))))
;;; some other packages that install dynamic modules also install them in /lib
;;; TODO: include this, change to symlink?
         ;;   (add-after 'build 'install-dynamic-module
         ;;     (lambda*
         ;;         (#:key outputs #:allow-other-keys)
         ;;       ;; Move the file into /lib.
         ;;       (install-file
         ;;        "webkit-module.so"
         ;;        (string-append (assoc-ref outputs "out") "/lib")) #t)))
   (synopsis "Use the webkit browser engine from emacs")
   (description "@{code}Emacs-webkit comes with a dynamic module for
webkitgtk in emacs, making it possible to be more powerful/flexible
then the traditional xwidget variant")
   (home-page "https://github.com/akirakyle/emacs-webkit")
   (license license:expat))))

(define-public emacs-dashboard
  (let ((version "1.7.0")
        (commit "6cdefdfd96c740d0e744b0c346360e28afad3acf")
        (revision "0"))
  (package
    (name "emacs-dashboard")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rakanalh/emacs-dashboard")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1iwp3rzi76hfwysi5a39zj9lv9140mzvqbccl69g3d5jnzd90h28"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-page-break-lines))
    (arguments
     '(#:include '("\\.el$" "\\.txt$" "\\.png$")
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-dashboard-widgets
           ;; This phase fixes compilation error.
           (lambda _
             (chmod "dashboard-widgets.el" #o666)
             (emacs-substitute-variables "dashboard-widgets.el"
               ("dashboard-init-info"
                '(format "Loaded in %s" (emacs-init-time))))
             #t)))))
    (home-page "https://github.com/rakanalh/emacs-dashboard")
    (synopsis "Startup screen extracted from Spacemacs")
    (description "This package provides an extensible Emacs dashboard, with
sections for bookmarks, projectil projects, org-agenda and more.")
    (license license:gpl3+))))

(define-public emacs-eshell-vterm
  (let ((commit "4e8589fcaf6243011a76b4816e7689d913927aab")
        (version "0.1")
        (revision "1"))
    (package
      (name "emacs-eshell-vterm")
      (version (git-version version revision commit))
      (source
        (origin
          (method git-fetch)
          (uri
            (git-reference
              (url "https://github.com/iostapyshyn/eshell-vterm")
              (commit commit)))
          (sha256
            (base32 "0hsby6ardi9g37agh181sgvsdvjmvi1n6jsqp34mwslwx7xxjvbv"))))
      (propagated-inputs
        (list emacs-vterm))
      (build-system emacs-build-system)
      (home-page "https://github.com/iostapyshyn/eshell-vterm")
      (synopsis "Use vterm in e-shell")
      (description
"An Emacs global minor mode allowing eshell to use vterm for visual
commands.")
      (license license:expat))))

(define-public emacs-list-utils
  (package
    (name "emacs-list-utils")
    (version "0.4.6")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rolandwalker/list-utils")
             (commit "ca9654cd1418e874c876c6b3b7d4cd8339bfde77")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0pkkfjjpak8xxbzr2xl5ah3isrn0bm8d7zfhrmhivpzpkhgdzvfx"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/rolandwalker/list-utils")
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-require-cl
           (lambda _
             (substitute* "list-utils.el"
               (("\\(require 'cl\\)") "(require 'cl-lib)"))
             #t)))))
    
    (synopsis "List-manipulation utility functions")
    (description "This package provides a list manipulation library for Emacs.")
    (license license:gpl3+)))


(define-public emacs-calfw
  (let ((version "1.6")
        (commit "03abce97620a4a7f7ec5f911e669da9031ab9088")
        (revision "0"))
  (package
    (name "emacs-calfw")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/kiwanami/emacs-calfw")
         (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0wiggihw9ackjdssqgp2cqccd3sil13n3pfn33d3r320fmxfjbch"))
       (patches
        (parameterize
            ((%patch-path
              (map
               (lambda (directory)
                 (string-append directory "/guix-local/packages/patches"))
               %load-path)))
          (search-patches "128.patch")))))
    (arguments
     `(#:emacs ,emacs-next
       #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'replace-deprecated-depends
             (lambda _
               (substitute* "calfw-cal.el"
                 
               (("\\(require 'calfw\\)") "\(require 'calfw)\n\(require 'cl-lib)"))  
               (substitute* (list "calfw.el" "calfw-cal.el")
                 (("rest") "cl-rest")))))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-howm))
    (home-page "https://github.com/kiwanami/emacs-calfw/")
    (synopsis "Calendar framework for Emacs")
    (description
     "This package displays a calendar view with various schedule data in the
Emacs buffer.")
    (license license:gpl3+))))
 

(define-public emacs-gitsum
  (let ((commit "e2f112f220db2b511471c02095e3a28f40d419fd")
        (version "0.01")
        (revision "0"))
    (package
      (name "emacs-gitsum")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/leahneukirchen/gitsum")
               (commit commit)))
         (sha256
          (base32 "0xr96js1637c8y5qja8hknqcnh77cdldalp6cfih4slhryvshjwz"))))   
      (build-system emacs-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'replace-deprecated-depends
               (lambda _
                 (substitute* "gitsum.el"
                   (("require 'cl") "require 'cl-lib")))))))
      (description "")
      (synopsis "")
      (license license:expat)
      (home-page "https://github.com/leahneukirchen/gitsum"))))

(define-public emacs-git-emacs
  (let ((commit "cef196abf398e2dd11f775d1e6cd8690567408aa")
        (version "0.01")
        (revision "0"))
    (package
      (name "emacs-git-emacs")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/tsgates/git-emacs")
               (commit commit)))
         (sha256
          (base32 "1n6x69z1s3hk6m6w8gpmqyrb2cxfzhi9w7q94d46c3z6r75v18vz"))))   
      (arguments
       `(#:emacs ,emacs
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'replace-deprecated-depends
             (lambda _
               (substitute* (list "git-emacs.el" "git-blamed.el")
                 (("require 'cl") "require 'cl-lib"))
               (substitute* (list "git-emacs.el" "git-blamed.el")
                 (("defstruct") "cl-defstruct")))))))
      (build-system emacs-build-system)
      (description "")
      (synopsis "")
      (license license:expat)
      (home-page "https://github.com/tsgates/git-emacs"))))

;;(define-public emacs-git-email
;;  (let ((release "0.2.0")
;;        (revision "2")
;;        (commit "b5ebade3a48dc0ce0c85699f25800808233c73be"))
;;    (package
;;      (name "emacs-git-email")
;;      (version (git-version release revision commit))
;;      (home-page "https://git.sr.ht/~yoctocell/git-email")
;;      (source
;;       (origin
;;         (method git-fetch)
;;         (uri (git-reference
;;               (url home-page)
;;               (commit commit)))
;;         (file-name (git-file-name name version))
;;         (sha256
;;          (base32 "1lk1yds7idgawnair8l3s72rgjmh80qmy4kl5wrnqvpmjrmdgvnx"))))
;;      (build-system emacs-build-system)
;;      (arguments
;;       `(#:emacs ,emacs-next
;;         #:phases
;;         (modify-phases %standard-phases
;;          (add-after 'unpack 'delete-files
;;             ;; Delete unnecessary files.
;;             (lambda _
;;               (delete-file "git-email-mu4e.el")))
;;          (add-before 'install 'makeinfo
;;            (lambda _
;;              (invoke "makeinfo" "doc/git-email.texi"))))))
;;      (native-inputs
;;       `(("texinfo" ,texinfo)))
;;      (inputs 
;;       (list emacs-piem
;;             emacs-magit 
;;             emacs-notmuch))
;;      (synopsis "Integrates git and email with Emacs")
;;      (description "@code{git-email} provides functions for formatting
;; and sending Git patches via email, without leaving Emacs.")
;;      (license license:gpl3+))))

(define-public emacs-prism
  (let ((commit "b0cbdaf4916c1cf348a8f0e4f6158e040a627562")
        (version "0.23")
        (revision "0"))
    (package
      (name "emacs-prism")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/alphapapa/prism.el")
               (commit commit)))
         (sha256
          (base32 "1h5fca47kinwr5qy58nh0bqn259vcj540013wb7kwki6ab33l809"))))   
      (build-system emacs-build-system)
      (propagated-inputs (list emacs-dash))
      (description "Disperse Lisp forms (and other languages) into a
 spectrum of colors by depth")
      (synopsis "@{code}prism disperses lisp forms (and other languages)
into a spectrum of color by depth. It’s similar to
@{code}rainbow-blocks, but it respects existing non-color face
properties, and allows flexible configuration of faces and colors.
It also optionally colorizes strings and/or comments by code depth in
 a similar, customizable way.")
      (license license:expat)
      (home-page "https://github.com/alphapapa/prism.el"))))


 (define-public emacs-mini-modeline
  (let ((commit "434b98b22c69c8b3b08e9c267c935591c49a8301")
        (version "20200408.729")
        (revision "0"))
  (package
   (name "emacs-mini-modeline")
   (version (git-version version revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/kiennq/emacs-mini-modeline")
           (commit commit)))
     (sha256
      (base32 "063bpi3gxzi6kkc3mb9h4m8lvbsvfw47z559960h912h2l3z6vhq"))))
   (propagated-inputs (list emacs-dash))   
   (build-system emacs-build-system)
   (home-page "https://github.com/kiennq/emacs-mini-modeline")
   (synopsis "Display emacs mode line in the echo area")
   (description "Despite the title, this package enables you to
display an emacs modeline consisting of a @{code}modeline-format
list in the echo-area")
   (license license:gpl3))))

(define-public emacs-mini-frame
  (let ((commit "57a049b9e1ea4a9b65e82fc10c8c7e70a042f636")
        ;;there is no versioning
        (version "0.0.1")
        (revision "0"))
    (package
     (name "emacs-mini-frame")
     (version (git-version version revision commit))
     (source
      (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/muffinmad/emacs-mini-frame")
         (commit commit)))
       (sha256
        (base32 "0bhsicy4van5xml8s9if2q7b36fbnzqg7kbg41zknf7nyhzl8pxi"))))
     (build-system emacs-build-system)
     (home-page "https://github.com/muffinmad/emacs-mini-frame")
     (synopsis "Show minibuffer in child frame with read-from-minibuffer")
     (description "Mini-Frame places the minibuffer at the top of
     the current frame with the help of @{code}read-from-minibuffer.")
     (license license:gpl3))))


(define-public emacs-hydra-posframe
  (let ((commit "343a269b52d6fb6e5ae6c09d91833ff4620490ec")
        (version "0.0.1")
        (revision "0"))
    (package
     (name "emacs-hydra-posframe")
     (version (git-version version revision commit))
     (source
      (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/Ladicle/hydra-posframe")
         (commit commit)))
       (sha256
        (base32 "03f9r8glyjvbnwwy5dmy42643r21dr4vii0js8lzlds7h7qnd9jm"))))
     (propagated-inputs (list emacs-hydra emacs-posframe))   
     (build-system emacs-build-system)
     (home-page "")
     (synopsis "")
     (description "")
     (license license:gpl3))))
  
  (define-public emacs-which-key-posframe
    (let ((commit "90e85d74899fc23d95798048cc0bbdb4bab9c1b7")
          (version "0.0.1")
          (revision "0"))
      (package
       (name "emacs-which-key-posframe")
       (version version)
       (source
        (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/yanghaoxie/which-key-posframe")
           (commit commit)))
         (sha256
          (base32 "173a04s00rydqpkrwdd9khwijbslkwmzqa557x6r1vpp0pdgaz0l"))))
        (propagated-inputs (list emacs-which-key emacs-posframe))   
        (build-system emacs-build-system)
        (home-page "https://github.comyanghaoxie/which-key-posframe")
        (synopsis "")
        (description "")
        (license license:gpl3)))) 
