(define-module (guix-local packages telephant)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system go)
  #:use-module (guix build-system qt)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages syncthing)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages golang))


(define-public go-github-com-chimeracoder-anaconda
  (package
    (name "go-github-com-chimeracoder-anaconda")
    (version "2.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ChimeraCoder/anaconda")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "04qhpk2p35b950kz53gsqd2vpmpy4c9wm2c5cp60yfyz43szkhq3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ChimeraCoder/anaconda"))
    (home-page "https://github.com/ChimeraCoder/anaconda")
    (synopsis #f)
    (description
      "Package anaconda provides structs and functions for accessing version 1.1 of the
Twitter API.")
    (license license:expat)))

(define-public go-github-com-chimeracoder-tokenbucket
  (package
    (name "go-github-com-chimeracoder-tokenbucket")
    (version "0.0.0-20131201223612-c5a927568de7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ChimeraCoder/tokenbucket")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1cyzlvk1mgdvdfmqsdsy5y2rflfz5q54a9rz9jylc2mg40c1d6dq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ChimeraCoder/tokenbucket"))
    (home-page "https://github.com/ChimeraCoder/tokenbucket")
    (synopsis #f)
    (description #f)
    (license license:lgpl3)))

(define-public go-github-com-azr-backoff
  (package
    (name "go-github-com-azr-backoff")
    (version "0.0.0-20160115115103-53511d3c7330")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/azr/backoff")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vd8svhhsgrgyaqj94vxwfp835qj4j1bzz51mq8r210ddwhpc4av"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/azr/backoff"))
    (home-page "https://github.com/azr/backoff")
    (synopsis "backoff")
    (description "Package backoff helps you at backing off !")
    (license license:expat)))

(define-public go-github-com-brianvoe-gofakeit
  (package
    (name "go-github-com-brianvoe-gofakeit")
    (version "2.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/brianvoe/gofakeit")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0haaynbrlpimd5ls2vdgfa074f3wkb3pv48agx797bzgacg1qhl3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/brianvoe/gofakeit"
                 #:tests? #f))
    (home-page "https://github.com/brianvoe/gofakeit")
    (synopsis "gofakeit")
    (description "Package gofakeit is a random data generator written in go")
    (license license:expat)))

(define-public go-github-com-dustin-gojson
  (package
    (name "go-github-com-dustin-gojson")
    (version "0.0.0-20160307161227-2e71ec9dd5ad")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dustin/gojson")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vrmmyn7l568l1k71mxd54iqf3d54pn86cf278i374j86jn0bdxf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dustin/gojson"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/dustin/gojson")
    (synopsis #f)
    (description
      "Package json implements encoding and decoding of JSON objects as defined in
@url{https://rfc-editor.org/rfc/rfc4627.html,RFC 4627}.  The mapping between
JSON objects and Go values is described in the documentation for the Marshal and
Unmarshal functions.")
    (license license:bsd-3)))

(define-public go-github-com-garyburd-go-oauth
  (package
    (name "go-github-com-garyburd-go-oauth")
    (version "0.0.0-20180319155456-bca2e7f09a17")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/garyburd/go-oauth")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0s1fgr17ss7krh6r3s6lp18w02kl3ds4r096ym3qrd29h63s8s8y"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/garyburd/go-oauth"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/garyburd/go-oauth")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-go-toast-toast
  (package
    (name "go-github-com-go-toast-toast")
    (version "0.0.0-20190211030409-01e6764cf0a4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-toast/toast")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1z16b01j6fym4y98srpwl77qvqkvsph5rq7hpbrxrr51fjvl65ng"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-toast/toast"))
    (propagated-inputs (list go-github-com-nu7hatch-gouuid))
    (home-page "https://github.com/go-toast/toast")
    (synopsis "Toast")
    (description
      "This package provides a go package for Windows 10 toast notifications.")
    (license license:expat)))


(define-public go-github-com-tadvi-systray
  (package
    (name "go-github-com-tadvi-systray")
    (version "0.0.0-20190226123456-11a2b8fa57af")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tadvi/systray")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0br5lds2wnlrpfigm1d2nrsn7jp5dawmnh1j18zr15vwnyg7wim3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tadvi/systray"))
    (home-page "https://github.com/tadvi/systray")
    (synopsis "systray")
    (description
      "Go package for Windows Systray icon, menu and notifications.")
    (license license:expat)))

(define-public go-github-com-gen2brain-beeep
  (package
    (name "go-github-com-gen2brain-beeep")
    (version "0.0.0-20220402123239-6a3042f4b71a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gen2brain/beeep")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0bpbs9mlby7m4bv6k7rqsbjkf39pa7bymnnczdldn0isgac53gzp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/gen2brain/beeep"
                               ;;#:tests? #f
                               ))
    (native-inputs (list libnotify dbus))
    (propagated-inputs
      `(("go-github-com-nu7hatch-gouuid" ,go-github-com-nu7hatch-gouuid)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-tadvi-systray" ,go-github-com-tadvi-systray)
        ("go-github-com-godbus-dbus-v5" ,go-github-com-godbus-dbus-v5)
        ;;("go-github-com-go-toast-toast" ,go-github-com-go-toast-toast)
        ))
    (home-page "https://github.com/gen2brain/beeep")
    (synopsis "beeep")
    (description
      "Package beeep provides a cross-platform library for sending desktop
notifications and beeps.")
    (license license:bsd-2)))


(define-public go-github-com-tomnomnom-linkheader
  (package
    (name "go-github-com-tomnomnom-linkheader")
    (version "0.0.0-20180905144013-02ca5825eb80")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tomnomnom/linkheader")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ghrv28vrvvrpyr4d4q817yby8g1j04mid8ql00sds1pvfv67d32"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tomnomnom/linkheader"))
    (home-page "https://github.com/tomnomnom/linkheader")
    (synopsis #f)
    (description
      "Package linkheader provides functions for parsing HTTP Link headers")
    (license license:expat)))

(define-public go-github-com-mattn-go-mastodon
  (package
    (name "go-github-com-mattn-go-mastodon")
    (version "0.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mattn/go-mastodon")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wx19rwnqm9h0335va7rqfin8qn6cb7pydkjdmhfh9r3nmx52lqj"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mattn/go-mastodon"
                 #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-urfave-cli" ,go-github-com-urfave-cli)
        ("go-github-com-tomnomnom-linkheader"
         ,go-github-com-tomnomnom-linkheader)
        ("go-github-com-mattn-go-tty" ,go-github-com-mattn-go-tty)
        ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
        ("go-github-com-mattn-go-colorable" ,go-github-com-mattn-go-colorable)
        ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)
        ("go-github-com-fatih-color" ,go-github-com-fatih-color)
        ("go-github-com-puerkitobio-goquery"
         ,go-github-com-puerkitobio-goquery)))
    (home-page "https://github.com/mattn/go-mastodon")
    (synopsis "go-mastodon")
    (description
      "Package mastodon provides functions and structs for accessing the mastodon API.")
    (license license:expat)))

(define-public go-github-com-muesli-go-app-paths
  (package
    (name "go-github-com-muesli-go-app-paths")
    (version "0.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/muesli/go-app-paths")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19g7fzyqg8d0nkb50h1rjk3q87v0yd1g9mh55ywgw85m5cji3w69"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/muesli/go-app-paths"
                               #:tests? #f))
    (propagated-inputs
      `(("go-github-com-mitchellh-go-homedir"
         ,go-github-com-mitchellh-go-homedir)))
    (home-page "https://github.com/muesli/go-app-paths")
    (synopsis "go-app-paths")
    (description
      "Lets you retrieve platform-specific paths (like directories for app-data, cache,
config, and logs).  It is fully compliant with the
@url{https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html,XDG
Base Directory Specification} on Unix, but also provides implementations for
macOS and Windows systems.")
    (license license:expat)))

(define-public go-github-com-tachiniererin-bananasplit
  (package
    (name "go-github-com-tachiniererin-bananasplit")
    (version "0.0.0-20200419143847-12495f5ee341")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tachiniererin/bananasplit")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qz8w00ahz1kh9qjxnsahq1dhnsgf9pxs1655802hz207ilk3xvi"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tachiniererin/bananasplit"))
    (propagated-inputs
      `(("go-github-com-rivo-uniseg" ,go-github-com-rivo-uniseg)))
    (home-page "https://github.com/tachiniererin/bananasplit")
    (synopsis #f)
    (description #f)
    (license #f)))


(define-public go-github-com-golang-crypto
  (package
    (name "go-github-com-golang-crypto")
    (version "0.0.0-20220411220226-7b82a4e95df4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/crypto")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qigy2f444a7myj4wr9qw86qc4ivc9sm30gja8v9dm1wzw438nhp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/crypto"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-term" ,go-golang-org-x-term)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/golang/crypto")
    (synopsis "Go Cryptography")
    (description
      "This repository holds supplementary Go cryptography libraries.")
    (license license:bsd-3)))

(define-public go-github-com-golang-net
  (package
    (name "go-github-com-golang-net")
    (version "0.0.0-20220425223048-2871e0cb64e4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/net")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0m8r3bb534yinay53a9yanyg27mvyxb2jxb6vyrfxvbxz5g0gsb7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/net"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-term" ,go-golang-org-x-term)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/golang/net")
    (synopsis "Go Networking")
    (description
      "This repository holds supplementary Go networking libraries.")
    (license license:bsd-3)))

(define-public go-github-com-golang-sys
  (package
    (name "go-github-com-golang-sys")
    (version "0.0.0-20220422013727-9388b58f7150")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "08i3r3l6aadkcij83h86k3qmbb47135fjk7bwx9xa4zmaa6wm9a2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/sys"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/golang/sys")
    (synopsis "sys")
    (description
      "This repository holds supplemental Go packages for low-level interactions with
the operating system.")
    (license license:bsd-3)))

(define-public go-github-com-golang-tools
  (package
    (name "go-github-com-golang-tools")
    (version "0.1.10")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/tools")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1aayyzraia15vmq3df3vnqsrihpzdkwrgk7byasjasv1ypzpxgdg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/tools"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)
        ("go-github-com-yuin-goldmark" ,go-github-com-yuin-goldmark)))
    (home-page "https://github.com/golang/tools")
    (synopsis "Go Tools")
    (description
      "This subrepository holds the source for various packages and tools that support
the Go programming language.")
    (license license:bsd-3)))

(define-public go-github-com-golang-text
  (package
    (name "go-github-com-golang-text")
    (version "0.3.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/text")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0xkw0qvfjyifdqd25y7nxdqkdh92inymw3q7841nricc9s01p4jy"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/text"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs `(("go-golang-org-x-tools" ,go-golang-org-x-tools)))
    (home-page "https://github.com/golang/text")
    (synopsis "Go Text")
    (description
      "text is a repository of text-related packages related to internationalization
(i18n) and localization (l10n), such as character encodings, text
transformations, and locale-specific text handling.")
    (license license:bsd-3)))

(define-public go-github-com-therecipe-qt-qtdeploy
  (package
    (name "go-github-com-therecipe-qt-qtdeploy")
    (version "0.0.0-20200904063919-c0c124a5770d")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/therecipe/qt")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "197wdh2v0g5g2dpb1gcd5gp0g4wqzip34cawisvy6z7mygmsc8rd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/therecipe/qt/cmd/qtdeploy"
                 #:unpack-path "github.com/therecipe/qt"
                 #:phases
                 (modify-phases %standard-phases
                   (replace 'install
                     (lambda arguments
                       (for-each
                        (lambda (directory)
                          (apply (assoc-ref %standard-phases 'install)
                                 `(,@arguments #:import-path ,directory)))
                  (list "github.com/therecipe/qt")))))))      
    (propagated-inputs
      `(("go-github-com-golang-text" ,go-github-com-golang-text)
        ("go-github-com-golang-tools" ,go-github-com-golang-tools)
        ("go-github-com-golang-sys" ,go-github-com-golang-sys)
        ("go-github-com-golang-net" ,go-github-com-golang-net)
        ("go-github-com-golang-crypto" ,go-github-com-golang-crypto)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-stretchr-objx" ,go-github-com-stretchr-objx)
        ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
        ("go-github-com-konsorten-go-windows-terminal-sequences"
         ,go-github-com-konsorten-go-windows-terminal-sequences)
        ("go-github-com-gopherjs-gopherjs" ,go-github-com-gopherjs-gopherjs)))
    (home-page "https://github.com/therecipe/qt")
    (synopsis "Introduction")
    (description
      "@url{https://en.wikipedia.org/wiki/Qt_(software),Qt} is a free and open-source
widget toolkit for creating graphical user interfaces as well as cross-platform
applications that run on various software and hardware platforms with little or
no change in the underlying codebase.")
    (license license:lgpl3)))

(define-public go-github-com-therecipe-qt
  (package
    (name "go-github-com-therecipe-qt")
    (version "0.0.0-20200904063919-c0c124a5770d")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/therecipe/qt")
               (commit (go-version->git-ref version))))
        (sha256
          (base32 "197wdh2v0g5g2dpb1gcd5gp0g4wqzip34cawisvy6z7mygmsc8rd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/therecipe/qt"))
    (inputs (list qtbase-5))
    (propagated-inputs
      `(("go-github-com-golang-text" ,go-github-com-golang-text)
        ("go-github-com-golang-tools" ,go-github-com-golang-tools)
        ("go-github-com-golang-sys" ,go-github-com-golang-sys)
        ("go-github-com-golang-net" ,go-github-com-golang-net)
        ("go-github-com-golang-crypto" ,go-github-com-golang-crypto)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-stretchr-objx" ,go-github-com-stretchr-objx)
        ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
        ("go-github-com-konsorten-go-windows-terminal-sequences"
         ,go-github-com-konsorten-go-windows-terminal-sequences)
        ("go-github-com-gopherjs-gopherjs" ,go-github-com-gopherjs-gopherjs)))
    (home-page "https://github.com/therecipe/qt")
    (synopsis "Introduction")
    (description
      "@url{https://en.wikipedia.org/wiki/Qt_(software),Qt} is a free and open-source
widget toolkit for creating graphical user interfaces as well as cross-platform
applications that run on various software and hardware platforms with little or
no change in the underlying codebase.")
    (license license:lgpl3)))

(define-public go-github-com-dustin-go-jsonpointer
  (package
    (name "go-github-com-dustin-go-jsonpointer")
    (version "0.0.0-20160814072949-ba0abeacc3dc")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dustin/go-jsonpointer")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "109v4pw2aycxwyfs7g4yld36phxd6zibls9d6kmdkm6v4x4kdw9d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dustin/go-jsonpointer"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/dustin/go-jsonpointer")
    (synopsis "JSON Pointer for go")
    (description "Package jsonpointer implements RFC6901 JSON Pointers")
    (license license:expat)))

(define-public go-github-com-mattn-go-tty
  (package
    (name "go-github-com-mattn-go-tty")
    (version "0.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mattn/go-tty")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0p7akvh0n2l64bdqa0m7ifhminfy7jyi315jysrdnqfc8msc2fvs"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mattn/go-tty"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-mattn-go-runewidth" ,go-github-com-mattn-go-runewidth)
        ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
        ("go-github-com-mattn-go-colorable"
         ,go-github-com-mattn-go-colorable)))
    (home-page "https://github.com/mattn/go-tty")
    (synopsis "go-tty")
    (description "Simple tty utility")
    (license license:expat)))



(define-public telephant
  (package
    (name "telephant")
    (version "0.0.0-20210130202124-da437b70539f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/muesli/telephant")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1pjrpy85jlrq0lvmd8nhy8kkc5hw8xpajixxqs7f75yc1a96p49a"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/muesli/telephant"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((qtdeploy (assoc-ref inputs "go-github-com-therecipe-qt-qtdeploy")))
               (chdir "src/github.com/muesli/telephant")
               (setenv "GOROOT" (dirname (getcwd)))
               (setenv "GOPATH" (string-append (getcwd)"/gopath"))
               (setenv "QT_PKG_CONFIG" "true")
               (setenv "QT_VERSION" "5.15.3")
               (invoke "go" "mod" "vendor")
               (invoke (string-append qtdeploy "/bin/qtdeploy")
                       "build" "desktop" ".")))))))
    (native-inputs (list  pkg-config qttools qtbase-5))
    (inputs (list mesa qtbase-5 qtquickcontrols qtquickcontrols2
                  qtdeclarative qtmultimedia qtsvg qtgraphicaleffects pulseaudio
                  glib))
    
    (propagated-inputs
     `(("go-github-com-therecipe-qt-qtdeploy" ,go-github-com-therecipe-qt-qtdeploy)
        
        ("go-github-com-tachiniererin-bananasplit"
         ,go-github-com-tachiniererin-bananasplit)
        ("go-github-com-muesli-go-app-paths"
         ,go-github-com-muesli-go-app-paths)
        ("go-github-com-mattn-go-mastodon" ,go-github-com-mattn-go-mastodon)
        ("go-github-com-gen2brain-beeep" ,go-github-com-gen2brain-beeep)
        ("go-github-com-garyburd-go-oauth" ,go-github-com-garyburd-go-oauth)
        ("go-github-com-dustin-gojson" ,go-github-com-dustin-gojson)
        ("go-github-com-dustin-go-jsonpointer"
         ,go-github-com-dustin-go-jsonpointer)
        ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-brianvoe-gofakeit" ,go-github-com-brianvoe-gofakeit)
        ("go-github-com-azr-backoff" ,go-github-com-azr-backoff)
        ("go-github-com-chimeracoder-tokenbucket"
         ,go-github-com-chimeracoder-tokenbucket)
        ("go-github-com-chimeracoder-anaconda"
         ,go-github-com-chimeracoder-anaconda)
        ("go-github-com-burntsushi-toml" ,go-github-com-burntsushi-toml)))
    (home-page "https://github.com/muesli/telephant")
    (synopsis "Telephant")
    (description
      "This package provides a lightweight but modern Mastodon client for the desktop,
written in Go & QML.")
    (license license:expat)))

