(define-module (guix-local packages  clight)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages base)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages shells))

(define-public libmodule
  (package
    (name "libmodule")
    (version "5.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/FedeDP/libmodule")
             (commit version)))
       (sha256
        (base32 "1j48gkj6zlifrmx9qh37ijmqjvdvgmpvxah7j2qlrhdxcc6n4i62"))))
    (build-system cmake-build-system)
    (native-inputs (list pkg-config))
    (arguments '(#:tests? #f)) ;tests not found
    (home-page "https://github.com/FedeDP/libmodule")
    (synopsis "Simple and elegant implementation of an actor library in C")
    (description "Libmodule offers a small and simple C implementation of an
 actor library that aims to let developers easily create modular C projects")
    (license license:expat)))

(define-public clight
  (package
    (name "clight")
    (version "4.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/FedeDP/clight")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "09hf80kb01xwzr8p87d5r8rw1dq9jqdibyc7k2xc4z5v5j7hlnlv"))))
    (build-system cmake-build-system)
    (native-inputs (list pkg-config bash-completion fish zsh))
    (inputs (list clightd
                  dbus
                  ddcutil
                  elogind
                  gsl
                  geoclue
                  libconfig
                  libmodule
                  popt
                  upower))
    (arguments
     (list
      #:tests? #f  ;no rule to make target test
      #:configure-flags
      #~(list (string-append "-DBASH_COMPLETIONS_DIR="
                             #$output "/share/bash-completion/completions")
              (string-append "-DFISH_COMPLETIONS_DIR="
                             #$output "/share/fish/completions"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after
              'patch-source-shebangs 'patch-target-dirs
            (lambda _
              (substitute*
                  "./CMakeLists.txt"
                (("DESTINATION \\$\\{SESSION_BUS_DIR\\}")
                 (string-append "DESTINATION " #$output "/share/dbus-1/services"))
                (("DESTINATION /etc/xdg/autostart")
                 (string-append "DESTINATION " #$output "/etc/xdg/autostart"))
                (("DESTINATION /usr/share/*")
                 (string-append "DESTINATION " #$output "/share/*")))
              #t)))))
    (home-page "https://github.com/FedeDP/Clight")
    (synopsis "User daemon utility to fully manage screens")
    (description "Clight is a tiny C utility that can turn your webcam
into a light sensor; moreover it supports a redshift-like gamma
control, a screen dimmer and dpms settings.
It is the userspace interface for clightd.")
    (license license:gpl3)))

(define-public clightd
  (package
    (name "clightd")
    (version "5.7")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/FedeDP/clightd")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1b6hc98vk0nm06plld4rgk70f825g8kakcxrv2prsxxvkvlqrmp1"))))
    (build-system cmake-build-system)
    (native-inputs (list pkg-config libmodule))
    (inputs (list elogind
                  eudev
                  dbus
                  ddcutil
                  libdrm
                  libjpeg-turbo
                  libmodule
                  libxrandr
                  libdrm
                  libusb
                  libx11
                  libxext
                  polkit-duktape
                  wayland
                  wayland-protocols))
    (arguments
     (list
      #:tests? #f  ;;;tests not found
      #:configure-flags #~(list "-DENABLE_GAMMA=1"
                                "-DENABLE_DPMS=1"
                                "-DENABLE_DDC=1"
                                "-DENABLE_SCREEN=1"
                                "-DENABLE_YOCTOLIGHT=1"
                                (string-append "-DMODULE_LOAD_DIR=" #$output
                                               "/etc/modprobe.d")
                                (string-append "-DDBUS_CONFIG_DIR=" #$output
                                               "/share/dbus-1/system.d"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after
              'patch-source-shebangs 'patch-target-dirs
            (lambda _
              (substitute* "./CMakeLists.txt"
                (("DESTINATION \\$\\{SYSTEM_BUS_DIR\\}")
                 (string-append "DESTINATION "
                                #$output "/share/dbus-1/system-services"))
                (("DESTINATION \\$\\{POLKIT_ACTION_DIR\\}")
                 (string-append "DESTINATION "
                                #$output "/share/polkit-1/actions"))))))))
    (home-page "https://github.com/FedeDP/Clightd")
    (description "Bus interface for many display capabilities")
    (synopsis "Clightd is a bus interface that lets you easily set/get screen
 brightness, gamma temperature and display dpms state. Moreover, it enables
 getting ambient brightness through webcam frames capture or ALS devices.

 It works on X, Wayland and tty.")
    (license license:gpl3)))

(define-public clight-gui
  (let ((commit "f60189909f5eb4ce2e2bcfea591ffa5e598d6668")
        (version "0.0")
        (revision "0"))
    (package
     (name "clight-gui")
     (version (git-version version revision commit))
     (source
      (origin
        (method git-fetch)
        (uri
         (git-reference  (url "https://github.com/nullobsi/clight-gui")
                         (commit commit)))
        (sha256
         (base32 "18yxbszlsfwrz4916djn523ymsi356dliwxh2j577zh9l2wgkraj"))))
     (arguments `(#:tests? #f)) ;no tests found
     (build-system qt-build-system)
     (native-inputs `(("pkg-config" ,pkg-config)
                      ("libmodule" ,libmodule)))
     (inputs `(("qtbase" ,qtbase)
               ("libxrandr" ,libxrandr)
              ("wayland-protocols" ,wayland-protocols)
              ("libdrm" ,libdrm)
              ("libjpeg-turbo" ,libjpeg-turbo)
              ("wayland" ,wayland)
              ("libx11" ,libx11)
              ("libxext" ,libxext)
              ("qtcharts" ,qtcharts)
              ("libjpeg-turbo" ,libjpeg-turbo)
              ("eudev" ,eudev)
              ("polkit" ,polkit)
              ("dbus" ,dbus)
              ("elogind" ,elogind)
              ("qtscxml" ,qtscxml)
              ("clight" ,clight)
              ("clightd" ,clightd)))
    (home-page "https://github.com/nullobsi/clight-gui")
    (description "Daemon dbus interface to clight")
    (synopsis "C user daemon for lighting things")
    (license license:expat))))
