(define-module (guix-local packages writefreely)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system go)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages syncthing)
  #:use-module (guix-local packages golang))

(define-public go-github-com-go-test-deep
  (package
    (name "go-github-com-go-test-deep")
    (version "1.0.8")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-test/deep")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1mmw2w3by7y24jjpjwmf2gfl08c65jihn3si9m0sswmagmdsk8q0"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-test/deep"))
    (home-page "https://github.com/go-test/deep")
    (synopsis "Deep Variable Equality for Humans")
    (description
     "Package deep provides function deep.Equal which is like reflect.DeepEqual but
returns a list of differences.  This is helpful when comparing complex types
like structures and maps.")
    (license license:expat)))

(define-public go-github-com-gorilla-feeds
  (package
    (name "go-github-com-gorilla-feeds")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gorilla/feeds")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1lwqibra4hyzx0jhaz12rfhfnw73bmdf8cn9r51nqidk8k7zf7sg"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gorilla/feeds"
       ;;FIXME
       #:tests? #f))
    (home-page "https://github.com/gorilla/feeds")
    (synopsis "gorilla/feeds")
    (description "Syndication (feed) generator library for golang.")
    (license license:bsd-2)))

(define-public go-github-com-gorilla-schema
  (package
    (name "go-github-com-gorilla-schema")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gorilla/schema")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0lbpncv6p7xqf1rb52b6rlxsib6l795bzsqy4hh6012c7dhl6hvw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gorilla/schema"))
    (home-page "https://github.com/gorilla/schema")
    (synopsis "schema")
    (description "Package gorilla/schema fills a struct with form values.")
    (license license:bsd-3)))

(define-public go-github-com-gorilla-sessions
  (package
    (name "go-github-com-gorilla-sessions")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gorilla/sessions")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1zjw2s37yggk9231db0vmgs67z8m3am8i8l4gpgz6fvlbv52baxp"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gorilla/sessions"))
    (propagated-inputs `(("go-github-com-gorilla-securecookie" ,go-github-com-gorilla-securecookie)))
    (home-page "https://github.com/gorilla/sessions")
    (synopsis "sessions")
    (description
     "Package sessions provides cookie and filesystem sessions and infrastructure for
custom session backends.")
    (license license:bsd-3)))

(define-public go-github-com-guregu-null
  (package
    (name "go-github-com-guregu-null")
    (version "4.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/guregu/null")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wm5q6n5xfr595786vysqcz9ln6r8zc7x7mir2krkgym60b4f4an"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/guregu/null"))
    (home-page "https://github.com/guregu/null")
    (synopsis "null")
    (description
     "Package null contains SQL types that consider zero input and null input as
separate values, with convenient support for JSON and text marshaling.  Types in
this package will always encode to their null value if null.  Use the zero
subpackage if you want zero values and null to be treated the same.")
    (license license:bsd-2)))

(define-public go-github-com-beevik-etree
  (package
    (name "go-github-com-beevik-etree")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/beevik/etree")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12dqgh8swrnk8c1bwqmq4mgd65rj4waxgb02filkm3f52vyxryxn"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/beevik/etree"))
    (home-page "https://github.com/beevik/etree")
    (synopsis "etree")
    (description
     "Package etree provides XML services through an Element Tree abstraction.")
    (license license:bsd-2)))

(define-public go-github-com-clbanning-mxj
  (package
    (name "go-github-com-clbanning-mxj")
    (version "1.8.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/clbanning/mxj")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13qlrycdp63q1v8sdpv6n720b6h6jpg58r38ldg4a70iv1wg7s9g"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/clbanning/mxj"
       #:tests? #f))
    (home-page "https://github.com/clbanning/mxj")
    (synopsis #f)
    (description
     "Marshal/Unmarshal XML to/from map[string]interface{} values (and JSON);
extract/modify values from maps by key or key-path, including wildcards.")
    (license license:expat)))

(define-public go-github-com-fatih-structs
  (package
    (name "go-github-com-fatih-structs")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/fatih/structs")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wrhb8wp8zpzggl61lapb627lw8yv281abvr6vqakmf569nswa9q"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/fatih/structs"))
    (home-page "https://github.com/fatih/structs")
    (synopsis "Structs")
    (description
     "Package structs contains various utilities functions to work with structs.")
    (license license:expat)))

(define-public go-github-com-ikeikeikeike-go-sitemap-generator-v2
  (package
    (name "go-github-com-ikeikeikeike-go-sitemap-generator-v2")
    (version "2.0.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     "https://github.com/ikeikeikeike/go-sitemap-generator")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hj3d0nyvnd35xgkkcw9ngll3ag6nklbczdws3z5kxmxj5fs2cip"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ikeikeikeike/go-sitemap-generator/v2/stm"
       #:unpack-path "github.com/ikeikeikeike/go-sitemap-generator/v2"
       ;;FIXME
       ;;#:tests? #f
       ))
    (propagated-inputs `(("go-github-com-fatih-structs" ,go-github-com-fatih-structs)
                         ("go-github-com-clbanning-mxj" ,go-github-com-clbanning-mxj)
                         ("go-github-com-beevik-etree" ,go-github-com-beevik-etree)))
    (home-page "https://github.com/ikeikeikeike/go-sitemap-generator")
    (synopsis
     "go-sitemap-generator is the easiest way to generate Sitemaps in Go.")
    (description "Current Features or To-Do")
    (license license:expat)))

(define-public go-github-com-chzyer-readline
  (package
    (name "go-github-com-chzyer-readline")
    (version "1.5.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/chzyer/readline")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1msh9qcm7l1idpmfj4nradyprsr86yhk9ch42yxz7xsrybmrs0pb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/chzyer/readline"))
    (native-inputs (list go-github-com-chzyer-test))
    (propagated-inputs `(("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/chzyer/readline")
    (synopsis "Guide")
    (description
     "Readline is a pure go implementation for GNU-Readline kind library.")
    (license license:expat)))

(define-public go-github-com-chzyer-logex
  (package
    (name "go-github-com-chzyer-logex")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/chzyer/logex")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0c9yr3r7dl3lcs22cvmh9iknihi9568wzmdywmc2irkjdrn8bpxw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/chzyer/logex"
       #:tests? #f))
    (home-page "https://github.com/chzyer/logex")
    (synopsis "Logex")
    (description
     "An golang log lib, supports tracing and level, wrap by standard log lib")
    (license license:expat)))

(define-public go-github-com-chzyer-test
  (package
    (name "go-github-com-chzyer-test")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/chzyer/test")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1axdlcnx2qjsn5wsr2pr1m0w0a8k4nk5kkrngh742fgh81vzzy8s"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/chzyer/test"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)))
    (home-page "https://github.com/chzyer/test")
    (synopsis "test")
    (description #f)
    (license license:expat)))

(define-public go-github-com-manifoldco-promptui
  (package
    (name "go-github-com-manifoldco-promptui")
    (version "0.9.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/manifoldco/promptui")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1nnlj1ahwq4ar5gbvxg8dqjl1wl5r8mhcm0bixg1c4wiihz8xv8m"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/manifoldco/promptui"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-chzyer-test" ,go-github-com-chzyer-test)
                         ("go-github-com-chzyer-readline" ,go-github-com-chzyer-readline)
                         ("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)))
    (home-page "https://github.com/manifoldco/promptui")
    (synopsis "promptui")
    (description
     "Package promptui is a library providing a simple interface to create
command-line prompts for go.  It can be easily integrated into spf13/cobra,
urfave/cli or any cli go application.")
    (license license:bsd-3)))

(define-public go-github-com-nu7hatch-gouuid
  (package
    (name "go-github-com-nu7hatch-gouuid")
    (version "0.0.0-20131221200532-179d4d0c4d8d")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/nu7hatch/gouuid")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1isyfix5w1wm26y3a15ha3nnpsxqaxz5ngq06hnh6c6y0inl2fwj"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/nu7hatch/gouuid"
       ;;FIXME
       #:tests? #f))
    (home-page "https://github.com/nu7hatch/gouuid")
    (synopsis "Pure Go UUID implementation")
    (description
     "This package provides immutable UUID structs and the functions NewV3, NewV4,
NewV5 and Parse() for generating versions 3, 4 and 5 UUIDs as specified in
@@url{https://rfc-editor.org/rfc/rfc4122.html,RFC 4122}.")
    (license license:expat)))

(define-public go-git-mills-io-prologic-go-gopher
  (package
    (name "go-git-mills-io-prologic-go-gopher")
    (version "0.0.0-20220331140345-72e36e5710a1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.mills.io/prologic/go-gopher.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "08wq50xg3kad04gf6dxi3a4mkrxm023zj7j90hf4r2icvfrds9r5"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.mills.io/prologic/go-gopher"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-sasha-s-go-deadlock" ,go-github-com-sasha-s-go-deadlock)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://git.mills.io/prologic/go-gopher")
    (synopsis "Gopher protocol library for Golang")
    (description
     "Package gopher provides an implementation of the Gopher protocol
(@@url{https://rfc-editor.org/rfc/rfc1436.html,RFC 1436})")
    (license license:expat)))

(define-public go-github-com-prologic-go-gopher
  (package
    (name "go-github-com-prologic-go-gopher")
    (version "0.0.0-20220331140345-72e36e5710a1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.mills.io/prologic/go-gopher.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "08wq50xg3kad04gf6dxi3a4mkrxm023zj7j90hf4r2icvfrds9r5"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/prologic/go-gopher"
       #:unpack-path "github.com/prologic/go-gopher"))
    (native-inputs `(("go-git-mills-io-prologic-go-gopher" ,go-git-mills-io-prologic-go-gopher)
                     ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-sasha-s-go-deadlock" ,go-github-com-sasha-s-go-deadlock)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/prologic/go-gopher")
    (synopsis "Gopher protocol library for Golang")
    (description
     "Package gopher provides an implementation of the Gopher protocol
(@@url{https://rfc-editor.org/rfc/rfc1436.html,RFC 1436})")
    (license license:expat)))

(define-public go-github-com-rainycape-unidecode
  (package
    (name "go-github-com-rainycape-unidecode")
    (version "0.0.0-20150907023854-cb7f23ec59be")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/rainycape/unidecode")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wvzdijd640blwkgmw6h09frkfa04kcpdq87n2zh2ymj1dzla5v5"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/rainycape/unidecode"))
    (home-page "https://github.com/rainycape/unidecode")
    (synopsis "unidecode")
    (description
     "Package unidecode implements a unicode transliterator which replaces non-ASCII
characters with their ASCII approximations.")
    (license license:asl2.0)))


(define-public go-github-com-neelance-astrewrite
  (package
    (name "go-github-com-neelance-astrewrite")
    (version "0.0.0-20160511093645-99348263ae86")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/neelance/astrewrite")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "07527807p8q6h05iq4qy0xrlcmwyzj76gpk0yqf71yaj447mz24v"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/neelance/astrewrite"))
    (home-page "https://github.com/neelance/astrewrite")
    (synopsis #f)
    (description #f)
    (license license:bsd-2)))

(define-public go-github-com-neelance-sourcemap
  (package
    (name "go-github-com-neelance-sourcemap")
    (version "0.0.0-20200213170602-2833bce08e4c")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/neelance/sourcemap")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "05ymjg1z9phf0wp4w058kvf13bmn4skv67chb3r04z69in8y8jih"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/neelance/sourcemap"))
    (home-page "https://github.com/neelance/sourcemap")
    (synopsis #f)
    (description #f)
    (license license:bsd-2)))

(define-public go-github-com-inconshreveable-mousetrap
  (package
    (name "go-github-com-inconshreveable-mousetrap")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/inconshreveable/mousetrap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a6xj24s53wh78937rri9nvwc8aqn3xf0h5fc33hn01hp2jgscv5"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/inconshreveable/mousetrap"))
    (home-page "https://github.com/inconshreveable/mousetrap")
    (synopsis "mousetrap")
    (description "mousetrap is a tiny library that answers a single question.")
    (license license:asl2.0)))


(define-public go-github-com-jtolds-gls
  (package
    (name "go-github-com-jtolds-gls")
    (version "4.20.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jtolio/gls")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1k7xd2q2ysv2xsh373qs801v6f359240kx0vrl0ydh7731lngvk6"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jtolds/gls"))
    (home-page "https://github.com/jtolds/gls")
    (synopsis "gls")
    (description "Package gls implements goroutine-local storage.")
    (license license:expat)))

(define-public go-github-com-smartystreets-assertions
  (package
    (name "go-github-com-smartystreets-assertions")
    (version "1.13.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/smartystreets/assertions")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0flf3fb6fsw3bk1viva0fzrzw87djaj1mqvrx2gzg1ssn7xzfrzr"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/smartystreets/assertions"))
    (home-page "https://github.com/smartystreets/assertions")
    (synopsis #f)
    (description
     "Package assertions contains the implementations for all assertions which are
referenced in goconvey's `convey` package
(github.com/smartystreets/goconvey/convey) and gunit
(github.com/smartystreets/gunit) for use with the So(...) method.  They can also
be used in traditional Go test functions and even in applications.")
    (license license:expat)))


(define-public go-github-com-writeas-activity
  (package
    (name "go-github-com-writeas-activity")
    (version "0.1.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/activity")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0bzq290bgjsknqc6pfjq5pmbxha94a3bxdxs9c5xvr70zv4bj6gj"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/activity"
       
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'build)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/writeas/activity/vocab"
               "github.com/writeas/activity/streams"
               "github.com/writeas/activity/pub"
               ))))
         (replace 'install
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'install)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/writeas/activity/vocab"
               "github.com/writeas/activity/streams"
               "github.com/writeas/activity/pub"
               ))))
          (replace 'check
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'check)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/writeas/activity/vocab"
               "github.com/writeas/activity/streams"
               "github.com/writeas/activity/pub"
         )))))))
    (propagated-inputs `(("go-github-com-go-fed-httpsig" ,go-github-com-go-fed-httpsig-0)))
    (native-inputs `(("go-github-com-go-test-deep" ,go-github-com-go-test-deep)))
    (home-page "https://github.com/writeas/activity")
    (synopsis "activity")
    (description
     "This repository supports @@code{vgo} and is remotely verifiable.")
    (license license:bsd-3)))

(define-public go-github-com-captncraig-cors
  (package
    (name "go-github-com-captncraig-cors")
    (version "0.0.0-20190703115713-e80254a89df1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/captncraig/cors")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1mgj6qn8mqj6510h1wnhivvxn3zw4lnscivqi91pb7lqwcqls69q"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/captncraig/cors"))
    (home-page "https://github.com/captncraig/cors")
    (synopsis "Syntax")
    (description
     "cors gives you easy control over Cross Origin Resource Sharing for your site.")
    (license license:expat)))

(define-public go-github-com-dchest-uniuri
  (package
    (name "go-github-com-dchest-uniuri")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/dchest/uniuri")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "10h57z323ha1mdjcc55mjbn2cd045b2ivi52ak8jay0lzhnl9s5j"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dchest/uniuri"))
    (home-page "https://github.com/dchest/uniuri")
    (synopsis "Package uniuri")
    (description
     "Package uniuri generates random strings good for use in URIs to identify unique
objects.")
    (license license:cc0)))

(define-public go-github-com-go-fed-httpsig
  (package
    (name "go-github-com-go-fed-httpsig")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-fed/httpsig")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1h2yk2ih8vrma8zrs1z8bd4r48hbqdwhgbqykrs4siyj9c80ykd2"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-fed/httpsig"
       ;;FIXME
       #:tests? #f))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)))
    (home-page "https://github.com/go-fed/httpsig")
    (synopsis "httpsig")
    (description
     "This package implements HTTP request and response signing and verification.
Supports the major MAC and asymmetric key signature algorithms.  It has several
safety restrictions: One, none of the widely known non-cryptographically safe
algorithms are permitted; Two, the RSA SHA256 algorithms must be available in
the binary (and it should, barring export restrictions); Finally, the library
assumes either the Authorizationn or Signature headers are to be set (but not
both).")
    (license license:bsd-3)))

(define-public go-github-com-go-fed-httpsig-0
  (package/inherit go-github-com-go-fed-httpsig
    (name "go-github-com-go-fed-httpsig-0")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-fed/httpsig")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0hw7r2br523a1bij3i0hfrilncs8jrc84flxdhf82qsb6yszfpm5"))))))

(define-public go-github-com-go-fed-httpsig-0.1.1
  (package/inherit go-github-com-go-fed-httpsig
    (name "go-github-com-go-fed-httpsig-0.1.1")
    (version "0.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-fed/httpsig")
                    (commit "0ef28562fabec1bf187d823018e94057097f6c1b")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0q460r9lr9sl4rd88rn7fphr5rzi0g1wwg8535gdclz2jq34dyp5"))))))

(define-public go-github-com-writeas-activityserve
  (package
    (name "go-github-com-writeas-activityserve")
    (version "0.0.0-20200409150223-d7ab3eaa4481")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/activityserve")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vlpk1dl3l9acfwg508q68pgiyx6yzgy37fm74wi201k4sdyqqx4"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/activityserve"))
    (propagated-inputs `(("go-gopkg-in-ini-v1" ,go-gopkg-in-ini-v1)
                         ("go-github-com-writefreely-go-nodeinfo" ,go-github-com-writefreely-go-nodeinfo)
                         ("go-github-com-writeas-go-webfinger" ,go-github-com-writeas-go-webfinger)
                         ("go-github-com-gorilla-mux" ,go-github-com-gorilla-mux)
                         ("go-github-com-gologme-log" ,go-github-com-gologme-log)
                         ("go-github-com-go-fed-httpsig" ,go-github-com-go-fed-httpsig-0.1.1)
                         ("go-github-com-dchest-uniuri" ,go-github-com-dchest-uniuri)
                         ("go-github-com-captncraig-cors" ,go-github-com-captncraig-cors)))
    (home-page "https://github.com/writeas/activityserve")
    (synopsis "ActivityServe")
    (description
     "This library was built to support the very little functions that
@@url{https://github.com/writeas/pherephone,pherephone} requires.  It might
never be feature-complete but it's a very good point to start your activityPub
journey.  Take a look at
@@url{https://github.com/writeas/activityserve-example,activityserve-example}
for a simple main file that uses @@strong{activityserve} to post a \"Hello,
world\" message.")
    (license license:expat)))

(define-public go-github-com-writeas-go-strip-markdown-v2
  (package
    (name "go-github-com-writeas-go-strip-markdown-v2")
    (version "2.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/go-strip-markdown")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "051s6wvp6xwp3x5nl0286mcxa9rsysv28ag4jj53kadqv6wh84wv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/go-strip-markdown/v2"))
    (home-page "https://github.com/writeas/go-strip-markdown")
    (synopsis "go-strip-markdown")
    (description "Package stripmd strips Markdown from text")
    (license license:expat)))

(define-public go-github-com-writeas-go-webfinger
  (package
    (name "go-github-com-writeas-go-webfinger")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/go-webfinger")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "01w6m1lwsjgmq3h198l07nisp6jplm8cx71yvys5jn2wcz5jn3s9"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/go-webfinger"
       ;;FIXME
       #:tests? #f))
    (native-inputs `(("go-github-com-pkg-errors" ,go-github-com-pkg-errors)))

    (propagated-inputs `(("go-github-com-captncraig-cors" ,go-github-com-captncraig-cors)))
    
    
    (home-page "https://github.com/writeas/go-webfinger")
    (synopsis "go-webfinger")
    (description
     "Package webfinger is a server implementation of the webfinger specification.
This is a general-case package which provides the HTTP handlers and interfaces
for adding webfinger support for your system and resources.")
    (license license:expat)))

(define-public go-github-com-writeas-httpsig
  (package
    (name "go-github-com-writeas-httpsig")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/httpsig")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0705f8pvp3m4l7yzx11307hdzk52n600j0sk5ldhfvm5rlxc4n14"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/httpsig"))
    (home-page "https://github.com/writeas/httpsig")
    (synopsis "HTTPSIG for Go")
    (description
     "This library implements HTTP request signature generation and verification based
on the RFC draft specification
@@url{https://tools.ietf.org/html/draft-cavage-http-signatures-06,https://tools.ietf.org/html/draft-cavage-http-signatures-06}.")
    (license license:asl2.0)))


(define-public go-github-com-writeas-go-writeas
  (package
    (name "go-github-com-writeas-go-writeas")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/go-writeas")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1rsr871gf9maig7g73xbvkpk60cdlasrw1sl6yv5dbwmfwrd031x"))))
    (propagated-inputs `(("go-github-com-writeas-impart" ,go-github-com-writeas-impart)
                         ("go-code-as-core-socks" ,go-code-as-core-socks)))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/go-writeas"
       #:tests? #f))
    (home-page "https://github.com/writeas/go-writeas")
    (synopsis "go-writeas")
    (description "Package writeas provides the binding for the Write.as API")
    (license license:expat)))

(define-public go-code-as-core-socks
  (package
    (name "go-code-as-core-socks")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://code.as/core/socks.git")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1j8j5h6vxv3p8vb9cdazwq6wh3x52r9k9f4j98xwwjwmgq0f0mmi"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "code.as/core/socks"))
    (home-page "https://code.as/core/socks")
    (synopsis "SOCKS")
    (description
     "Package socks implements a SOCKS (SOCKS4, SOCKS4A and SOCKS5) proxy client.")
    (license license:bsd-2)))

(define-public go-github-com-writeas-go-writeas-v2
  (package
    (name "go-github-com-writeas-go-writeas-v2")
    (version "2.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/go-writeas")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1f2a4np2sj7kiq754bv7cxsf80xyq1w3rf9hbh4nwlk68k1q7drv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/go-writeas/v2"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-writeas-impart" ,go-github-com-writeas-impart)
                         ("go-code-as-core-socks" ,go-code-as-core-socks)))
    (home-page "https://github.com/writeas/go-writeas")
    (synopsis "go-writeas")
    (description "Package writeas provides the binding for the Write.as API")
    (license license:expat)))

(define-public go-github-com-writeas-import
  (package
    (name "go-github-com-writeas-import")
    (version "0.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/import")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1dk66kwa325bdvidgnqyds2zs4fpr79km48867ss742pmjyywg6x"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/import"))
    (propagated-inputs `(("go-github-com-writeas-go-writeas-v2" ,go-github-com-writeas-go-writeas-v2)
                         ("go-github-com-writeas-go-writeas" ,go-github-com-writeas-go-writeas)
                         ("go-github-com-hashicorp-go-multierror" ,go-github-com-hashicorp-go-multierror)))
    (home-page "https://github.com/writeas/import")
    (synopsis "import")
    (description
     "Package wfimport provides library functions for parsing and importing
writefreely posts from various files.
@@url{https://github.com/writeas/writefreely,https://github.com/writeas/writefreely}")
    (license license:mpl2.0)))

(define-public go-github-com-writeas-slug
  (package
    (name "go-github-com-writeas-slug")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/slug")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1njikf777ap3j8idlvywsifw5alxsi1m1nnfhdvv2hi3m7z1ms5h"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/slug"))
    (native-inputs `(("go-github-com-rainycape-unidecode" ,go-github-com-rainycape-unidecode)
                         ))

    (home-page "https://github.com/writeas/slug")
    (synopsis "slug")
    (description
     "Package slug generate slug from unicode string, URL-friendly slugify with
multiple languages support.")
    (license license:mpl2.0)))

(define-public go-github-com-gofrs-uuid
  (package
    (name "go-github-com-gofrs-uuid")
    (version "4.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gofrs/uuid")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1szkrxs6s3b651mxmnr4a4qf75788lb3b65i0sv31yvqblvjs9y6"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gofrs/uuid"))
    (home-page "https://github.com/gofrs/uuid")
    (synopsis "UUID")
    (description
     "Package uuid provides implementations of the Universally Unique Identifier
(UUID), as specified in RFC-4122 and the Peabody RFC Draft (revision 03).")
    (license license:expat)))

(define-public go-github-com-kylemcc-twitter-text-go
  (package
    (name "go-github-com-kylemcc-twitter-text-go")
    (version "0.0.0-20180726194232-7f582f6736ec")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kylemcc/twitter-text-go")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ri8b51in68rmhw7qz2zkimbv7gyx6ldgmz9925zs17s502l2q36"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/kylemcc/twitter-text-go"
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'build)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/kylemcc/twitter-text-go/validate"
               "github.com/kylemcc/twitter-text-go/extract"))))
         (replace 'install
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'install)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/kylemcc/twitter-text-go/validate"
               "github.com/kylemcc/twitter-text-go/extract"))))
         (replace 'check
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'check)
                       `(,@arguments #:import-path ,directory)))
              (list
               "github.com/kylemcc/twitter-text-go/validate"
               "github.com/kylemcc/twitter-text-go/extract")))))))
    (native-inputs `(("go-gopkg-in-yaml-v1" ,go-gopkg-in-yaml-v1)
                     ;;("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
                     ))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)))
    (home-page "https://github.com/kylemcc/twitter-text-go")
    
    
    (synopsis "twitter-text-go")
    (description
     "Twitter-text-go is a @@url{http://golang.org/,Go} port of the various
twitter-text handling libraries.")
    (license license:bsd-3)))

(define-public go-github-com-writeas-go-strip-markdown
  (package
    (name "go-github-com-writeas-go-strip-markdown")
    (version "2.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/go-strip-markdown")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c9xlrdpnl1wkm4z5vwqplw7sqf8n7gdah6xm58z2kh0ry5hm325"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/go-strip-markdown"))
    (home-page "https://github.com/writeas/go-strip-markdown")
    (synopsis "go-strip-markdown")
    (description "Package stripmd strips Markdown from text")
    (license license:expat)))

(define-public go-github-com-writeas-impart
  (package
    (name "go-github-com-writeas-impart")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/impart")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1yy4hb2gsw0bffzapy9mih1l42vrv3brdi47xvvxpmkxxqqbf7xf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/impart"))
    (home-page "https://github.com/writeas/impart")
    (synopsis "impart")
    (description
     "Package impart provides a simple interface for a JSON-based API. It is designed
for passing errors around a web application, sending back a status code and
error message if needed, or a status code and some data on success.")
    (license license:expat)))

(define-public go-github-com-writeas-openssl-go
  (package
    (name "go-github-com-writeas-openssl-go")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/openssl-go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "10bncxgjg2a6x3qgg8gs1jg2pqdaca40v47zvshh8rh5zr2cmvyk"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/openssl-go"))
    (home-page "https://github.com/writeas/openssl-go")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-writeas-saturday
  (package
    (name "go-github-com-writeas-saturday")
    (version "1.7.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/saturday")
                    (commit "392b95a033203c3118dfa69a1077d62b5c7a53c6")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hx2j53p37mwapzjzvmzzr4k6snb6r39kwx3y2q7frnh0bfxsg2s"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/saturday"
       ;;FIXME
       #:tests? #f))
    (propagated-inputs
     `(("go-github-com-shurcool-sanitized-anchor-name" ,go-github-com-shurcool-sanitized-anchor-name)))
    (home-page "https://github.com/writeas/saturday")
    (synopsis "Blackfriday")
    (description "Package blackfriday is a markdown processor.")
    (license license:bsd-2)))

(define-public go-gopkg-in-yaml-v1
  (package
    (name "go-gopkg-in-yaml-v1")
    (version "1.0.0-20140924161607-9f9df34309c0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gopkg.in/yaml.v1")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1r8d346szqa9x8q03wiycik5qy3d6w8qq4hs99z1p64q5lm0g7gm"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gopkg.in/yaml.v1"
       #:unpack-path "gopkg.in/yaml.v1"
       ;;FIXME
       #:tests? #f))
    (native-inputs `(("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)))
    (home-page "https://gopkg.in/yaml.v1")
    (synopsis "YAML support for the Go language")
    (description "Package yaml implements YAML support for the Go language.")
    (license license:lgpl3)))

(define-public go-github-com-writeas-web-core
  (package
    (name "go-github-com-writeas-web-core")
    (version "1.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/web-core")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0vww0r4a7bc7gvah2cwppx1hy0fczb5032b3q65wpb88da64vmvl"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/web-core"
       #:tests? #f
        #:phases
        (modify-phases %standard-phases
          ;; Source-only package
          (delete 'build))))
    (propagated-inputs `(("go-gopkg-in-yaml-v1" ,go-gopkg-in-yaml-v1)
                         ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-writeas-saturday" ,go-github-com-writeas-saturday)
                         ("go-github-com-writeas-openssl-go" ,go-github-com-writeas-openssl-go)
                         ("go-github-com-writeas-impart" ,go-github-com-writeas-impart)
                         ("go-github-com-writeas-go-strip-markdown" ,go-github-com-writeas-go-strip-markdown)
                         ("go-github-com-shurcool-sanitized-anchor-name" ,go-github-com-shurcool-sanitized-anchor-name)
                         ("go-github-com-microcosm-cc-bluemonday" ,go-github-com-microcosm-cc-bluemonday)
                         ("go-github-com-kylemcc-twitter-text-go" ,go-github-com-kylemcc-twitter-text-go)
                         ("go-github-com-gofrs-uuid" ,go-github-com-gofrs-uuid)))
    (home-page "https://github.com/writeas/web-core")
    (synopsis "WriteFreely web core")
    (description
     "web-core holds components of the @@url{https://writefreely.org,WriteFreely} web
application, and is shared with other @@url{https://write.as,Write.as} products,
like @@url{https://snap.as,Snap.as}.")
    (license license:mpl2.0)))

(define-public go-github-com-writefreely-go-nodeinfo
  (package
    (name "go-github-com-writefreely-go-nodeinfo")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writefreely/go-nodeinfo")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0m94p49n4snjkkyaygdyfl8a2gi8g60fkqqr07jr1j940152ldbv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writefreely/go-nodeinfo"))
    (propagated-inputs
     `(("go-github-com-writeas-go-webfinger" ,go-github-com-writeas-go-webfinger)))
    
    (home-page "https://github.com/writefreely/go-nodeinfo")
    (synopsis "go-nodeinfo")
    (description
     "go-nodeinfo is an implementation of
@@url{https://github.com/jhass/nodeinfo,NodeInfo}, a standard metadata format
for federated social networks, in Go (golang).")
    (license license:expat)))


(define-public go-gopkg-in-fatih-set-v0
  (package
    (name "go-gopkg-in-fatih-set-v0")
    (version "0.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gopkg.in/fatih/set.v0")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vif67ijhsm3p2613jl51fshlrn4d5pncrly73jvfyc8nsss8i9x"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gopkg.in/fatih/set.v0"
       #:unpack-path "gopkg.in/fatih/set.v0"))
    (home-page "https://gopkg.in/fatih/set.v0")
    (synopsis "Archived project. No maintenance.")
    (description
     "Package set provides both threadsafe and non-threadsafe implementations of a
generic set data structure.  In the threadsafe set, safety encompasses all
operations on one set.  Operations on multiple sets are consistent in that the
elements of each set used was valid at exactly one point in time between the
start and the end of the operation.")
    (license license:expat)))

(define-public go-github-com-writeas-monday
  (package
    (name "go-github-com-writeas-monday")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writeas/monday")
                    (commit "54a7dd5792199cfeac78b93a55f99cebb6303164")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "18bcx3majshsq4idvr5iazh8s8ldglnciw8v8xhylzmilqz9wz20"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/writeas/monday"))
    (home-page "https://github.com/writeas/monday")
    (synopsis #f)
    (description #f)
    ;;not true FIXME
    (license license:expat)))

(define-public go-github-com-jteeuwen-go-bindata
  (package
    (name "go-github-com-jteeuwen-go-bindata")
    (version "3.0.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jteeuwen/go-bindata")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1v8xwwlv6my5ixvis31m3vgz4sdc0cq82855j8gxmjp1scinv432"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jteeuwen/go-bindata/go-bindata"
       #:unpack-path "github.com/jteeuwen/go-bindata"))
    (home-page "https://github.com/jteeuwen/go-bindata")
    (synopsis "bindata")
    (description
     "bindata converts any file into managable Go source code.  Useful for embedding
binary data into a go program.  The file data is optionally gzip compressed
before being converted to a raw byte slice.")
    (license license:cc0)))

(define-public go-github-com-go-bindata-go-bindata
  (package
    (name "go-github-com-go-bindata-go-bindata")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-bindata/go-bindata")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vi5qq28k4wmpmps2xybiai825zzy07ajqm3wcwvw4q1yyk3cr8d"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-bindata/go-bindata"))
    (home-page "https://github.com/go-bindata/go-bindata")
    (synopsis "bindata")
    (description
     "bindata converts any file into managable Go source code.  Useful for embedding
binary data into a go program.  The file data is optionally gzip compressed
before being converted to a raw byte slice.")
    (license license:cc0)))

(define-public writefreely
  (package
    (name "writefreely")
    (version "0.13.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/writefreely/writefreely")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0w51gvw153rp9r1b595rzvs869avaq589vmypsyw3fxz6251x1x9"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/writefreely/writefreely/cmd/writefreely"
       #:unpack-path "github.com/writefreely/writefreely"
       #:go ,go-1.19
       #:build-flags '("-tags='sqlite'")
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'gen-assets
           (lambda _
             (chdir "src/github.com/writefreely/writefreely")
             (chmod "bindata-lib.go" #o755)
             (invoke "go-bindata" "-pkg" "writefreely" "-ignore=\\.gitignore" "-o" "bindata-lib.go" "-tags='!wflib'" "schema.sql"))))))
    (native-inputs (list go-github-com-jteeuwen-go-bindata))
    (propagated-inputs `(("go-github-com-jteeuwen-go-bindata" ,go-github-com-jteeuwen-go-bindata)
                         ("go-gopkg-in-ini-v1" ,go-gopkg-in-ini-v1)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-writefreely-go-nodeinfo" ,go-github-com-writefreely-go-nodeinfo)
                         ("go-github-com-writeas-web-core" ,go-github-com-writeas-web-core)
                         ("go-github-com-writeas-slug" ,go-github-com-writeas-slug)
                         ("go-github-com-writeas-saturday" ,go-github-com-writeas-saturday)
                         ("go-github-com-writeas-monday" ,go-github-com-writeas-monday)
                         ("go-github-com-writeas-import" ,go-github-com-writeas-import)
                         ("go-github-com-writeas-impart" ,go-github-com-writeas-impart)
                         ("go-github-com-writeas-httpsig" ,go-github-com-writeas-httpsig)
                         ("go-github-com-writeas-go-webfinger" ,go-github-com-writeas-go-webfinger)
                         ("go-github-com-writeas-go-strip-markdown-v2" ,go-github-com-writeas-go-strip-markdown-v2)
                         ("go-github-com-writeas-activityserve" ,go-github-com-writeas-activityserve)
                         ("go-github-com-writeas-activity" ,go-github-com-writeas-activity)
                         ("go-github-com-urfave-cli-v2" ,go-github-com-urfave-cli-v2)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-smartystreets-goconvey" ,go-github-com-smartystreets-goconvey)
                         ("go-github-com-smartystreets-assertions" ,go-github-com-smartystreets-assertions)
                         ("go-github-com-rainycape-unidecode" ,go-github-com-rainycape-unidecode)
                         ("go-github-com-prologic-go-gopher" ,go-github-com-prologic-go-gopher)
                         ("go-github-com-nu7hatch-gouuid" ,go-github-com-nu7hatch-gouuid)
                         ("go-github-com-mitchellh-go-wordwrap" ,go-github-com-mitchellh-go-wordwrap)
                         ("go-github-com-microcosm-cc-bluemonday" ,go-github-com-microcosm-cc-bluemonday)
                         ("go-github-com-mattn-go-sqlite3" ,go-github-com-mattn-go-sqlite3)
                         ("go-github-com-manifoldco-promptui" ,go-github-com-manifoldco-promptui)
                         ("go-github-com-lunixbochs-vtclean" ,go-github-com-lunixbochs-vtclean)
                         ("go-github-com-kylemcc-twitter-text-go" ,go-github-com-kylemcc-twitter-text-go)
                         ("go-github-com-jtolds-gls" ,go-github-com-jtolds-gls)
                         ("go-github-com-ikeikeikeike-go-sitemap-generator-v2" ,go-github-com-ikeikeikeike-go-sitemap-generator-v2)
                         ("go-github-com-hashicorp-go-multierror" ,go-github-com-hashicorp-go-multierror)
                         ("go-github-com-guregu-null" ,go-github-com-guregu-null)
                         ("go-github-com-gorilla-sessions" ,go-github-com-gorilla-sessions)
                         ("go-github-com-gorilla-schema" ,go-github-com-gorilla-schema)
                         ("go-github-com-gorilla-mux" ,go-github-com-gorilla-mux)
                         ("go-github-com-gorilla-feeds" ,go-github-com-gorilla-feeds)
                         ("go-github-com-gorilla-csrf" ,go-github-com-gorilla-csrf)
                         ("go-github-com-gopherjs-gopherjs" ,go-github-com-gopherjs-gopherjs)
                         ("go-github-com-go-test-deep" ,go-github-com-go-test-deep)
                         ("go-github-com-go-sql-driver-mysql" ,go-github-com-go-sql-driver-mysql)
                         ("go-github-com-fatih-color" ,go-github-com-fatih-color)
                         ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
                         ("go-github-com-clbanning-mxj" ,go-github-com-clbanning-mxj)))
    (home-page "https://github.com/writefreely/writefreely")
    (synopsis "Features")
    (description
     "Package writefreely copied from
@@url{https://github.com/golang/tools/blob/master/internal/semver/semver.go,https://github.com/golang/tools/blob/master/internal/semver/semver.go}
slight modifications made")
    (license license:agpl3)))

writefreely
