# This is not a guix channel (yet) -- but maybe you find interesting things and can use them!

This is basically my local amalgamation of packages and services that could become a proper channel (the filesystem structure should be mostly ok) but it isn't because:
- I am kind of chaotic and I cannot guarantee things won't break in here
- I kind of don't want to make this an "institution" needing long time commitment
 - for these reasons, if i put work into a channel its mostly at [the guixrus community channel](https://git.sr.ht/~whereiseveryone/guixrus)
 
 **So, what is it then?**
 
 First of all, I now have a new modern laptop (an asus expertbook) next to/superceding my old thinkpad and I need to synchronize my guix modules between them. 
 Second of all in this hodgepodge, there might be a few interesting projects for others. Let's see, without saying this is complete or all correct, or maybe it landed already in guixrus, which you should subscribe to before using this, anyways 
- spotify
  there is an effort to backwards-engineer the spotify-connect and other apis [in rust](https://github.com/librespot). So we can have spotify clients that are not only frontends to the web api, i guess.
   - spotifyd, spotify-qt, eventually maybe the gtk4-rs app spot
   - since i am currently using gnome on the afroementioned new laptop, the gtk client, spot is also kind of a target but it looks very complicated

- hardware 
   these are some packages and or services to enable/work with hardware functionalities in my devices
   - iio-proxy-sensor (-service), enabling automatic screen rotation in gnome and other DEs
   - power-profiles-daemon, enabling a simple dbus api for choosing power profiles (like you know from android, windows, etc, which f.e. gnome recognizes
   - bolt, boltd passing thunderbolt rules to userspace
 - low-memory-monitor passing oom capabilities to userspace as far as i understand. 
   - thinkfan service to tune thinkpad fans 
     - TODO needs config serialization
   - clight, clightd quite extensive tool managing display settings like brightness, redshift automagically by using the ambient light sensor, location. works with multiple monitors (over ddcutil) and on xorg as well as wayland/wlroots. clightd runs as a system service, clight is the user 
   - TODO/FIXME something to help manage the fan of my asus device, enable the special numberpad
   - I packaged rtkit which i am not sure what it is for?
   - [nbfc-linux](https://github.com/nbfc-linux/nbfc-linux) for fan control on my asus device, working on the service 
   
- wayland stuff
   I stumbled upon the wayfire compositor before I started with guix and I still use it on my thinkpad. Since i still like getting my good chunk of eye candy and reconfiguring my graphical environment from time to time, i hope the more modular approach to wayland that is taking place in wlroots will continue and actually become part of smaller DEs. I package stuff to use it with wayfire and other compositors to try them out from time to time.
  - wayfire and plugins
     - FIXME which plugins work with the (new) stable release, which with -edge, and which don't anymore? the core could be upstreamed by now
  - other compositors: newm, labwc, hyperwm, sway/sway-forks, WIP gamescope, WIP mir 
  - utilities: wayland versions of rofi and cairo-dock, wapanel, nwg- ... stuff, wluma 
- eye candy:
  - the ubuntu yaru gtk theme
  - fonts: roboto-mono, roboto-slab, nunito, iosevka-mayukai, inter (signals font)
  - icons and cursors: fluent-, zafiro-, suru-, tela-, .... icons; capitaine- and some other cursors
  - Kvantum, the qt style engine
- DEs 
  unfortunately there is no ready wlroots DE. so long i'm trying different stuff, meaning it has to be built first. In general I would like to see guix become more approachable for more "normie" users (guix users atm are, even amongst other foss users pretty unusual) 
  - pantheon: probably the most extensive collection of pantheon packages atm. maybe we can soon upstream it? who and me <3?
  - budgie: far less work (only a handful core packages) but also kinda less cool. 
  - cinnamon: not sure if i will poke at the whole DE, for now it's nemo and having a go at extensions for it and some of their apps (warpinator f.e.)
  - right now i use gnome on my convertible asus notebook since it has good gesture support and i packaged/fixed some extensions
    - if someone could package typescript3.8  (a 2.x version is in guixrus) we could have the pop shell extension!
- emulators
  - citra
  - yuzu - also on a quite old version because guix' sdl2 is old, among things

- cli-tools
 - i believe i have package definitions for the full dependencies of both hugo and a recent rclone (i want the bidirectional sync capability to my nextcloud :>) but still problems building them. thanks to unmatched-paren for providing a way to unclutter the messages during build but to me what's left is still cryptic. well, trial and error...
 - evtest
- alternative system service tools 
 - the consolekit2 module contains consolekit2 and some dependencies to enable most capabilities (cg-manager, pm-utils). this stuff is pretty old but maybe someone else believes getting less dependant of systemd programs is maybe good. at least for the hurd, i would think so.
- some emacs stuff, probably
- pipewire home service

## How to use this repo?

Right now, `git clone` and then either subscribe to it as a local channel or put it in your load path with `guix command -L ./path-to/guix-local`

## What's to come?

Technically I should commit to upstreaming a good bunch of the stuff here and @guixrus. It's getting too much to remain in oversight. especially for the larger projects (everything that had me use an importer, stuff with more dependencies, stuff that cannot go on master), help would be welcome. 

## TODO list for contributing/upstreaming

1. icon packs, fonts and yaru theme, gnome-extensions (warm-up)
2. wayland related utilities
3. system utilities and service packages
4. other packages
   - kvantum
   - emulators
   - nemo
   - DEs
4. services
5. wayland compositors
   - waybox
   - hyprland
   - newm
   - wayfire
