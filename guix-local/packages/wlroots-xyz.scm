;; all the cool wlroots based stuff that doesn't fit into guix main repos or isn't really ready (mainly bc no official releases (forks), )
(define-module  (guix-local packages wlroots-xyz)
  #:use-module  (guix download)
  #:use-module  (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module  (guix utils)
  #:use-module  (guix build utils)
  ;;#:use-module ((guix build utils) #:select (alist-replace))
  #:use-module  (guix gexp)
  #:use-module  (guix packages)
  #:use-module  (gnu packages)
  #:use-module  (gnu packages base)
;;needed build-systems
  #:use-module  (guix build-system gnu)
  #:use-module  (guix build-system go)
  #:use-module  (guix build-system meson)
  #:use-module  (guix build-system cmake)
  #:use-module  (guix build-system cargo)
  #:use-module (gnu packages qt)
  #:use-module  (guix build-system python)
;;package-dependencies
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages lxde)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages man)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages web)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages freedesktop)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (guix-local packages wayland-updates))

(define-public lxappearance-gtk3-wayland
  (package
    (inherit lxappearance)
    (name "lxappearance-gtk3-wayland")
    (version "0.6.3")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "mirror://sourceforge/lxde/"
                       "LXAppearance/lxappearance-" version ".tar.xz"))
       (sha256
        (base32 "0f4bjaamfxxdr9civvy55pa6vv9dx1hjs522gjbbgx7yp1cdh8kj"))
       (patches
        (parameterize
            ((%patch-path
              (map
               (lambda (directory)
                 (string-append directory "/guix-local/packages/patches"))
               %load-path)))
        (search-patches
         ;;patches are created from github issues
         "lxappearence-gtk3-01-only-do-x11-on-x11.patch"
         "lxappearence-gtk3-02-set-some-settings-gsettings.patch")))))
    (build-system gnu-build-system)
    (inputs (list gtk+ dbus dbus-glib))
    (arguments '(#:configure-flags '("--enable-gtk3=yes" "--enable-dbus=yes" )))
    (synopsis "LXDE GTK+ theme switcher")
    (description "LXAppearance is a desktop-independent GTK+ theme switcher
able to change themes, icons, and fonts used by GTK+ applications.
This version is compiled for supporting GTK+3 and dbus, and applies
patches from LXDE's github repos Merge Requests to work on wayland as well.")
    (home-page "https://lxde.github.io")
    (license license:gpl2+)))

(define-public nwg-launchers
  (package
    (name "nwg-launchers")
    (version "0.7.1.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/nwg-piotr/nwg-launchers")
         (commit (string-append "v" version))))
       (sha256
        (base32 "0hq2qiqxvrw3g515ywcb676ljc8mdw3pyslgxr3vahizfljah1pv"))))
    (build-system meson-build-system)
    (native-inputs
     (list git-minimal pkg-config))
    (inputs
     (list gtk-layer-shell gtkmm-3 json-modern-cxx librsvg))
    (synopsis "GTK-based launchers")
    (description "This package provides application grid, button bar and dmenu
 applications for sway and other window managers.")
    (home-page "https://github.com/nwg-piotr/nwg-launchers")
    (license license:expat)))

;;; wluma
(define-public wluma
  (package
   (name "wluma")
   (version "4.1.2")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/maximbaz/wluma")
       (commit version)))
     (sha256
      (base32 "161hmrpyba2nwc3m4fw30mgyqfz7hjm6mf94a0s6yjz045x5fby9"))))
   (build-system cargo-build-system)
   (native-inputs (list pkg-config))
   ;; maybe just place those as reg inputs, too?
   ;;eudev is needed for rust-udev-sys, clang for rust-v4l-sys,
   ;; custom rust-toml needs to be in inputs to be found
   (inputs (list ;;wayland
            rust-toml-dotted-table-0.5 v4l2loopback-linux-module
            eudev clang))
   ;;(propagated-inputs (list vulkan-loader))
   (arguments
    `(#:cargo-inputs
       (("rust-dirs" ,rust-dirs-3)
        ("rust-itertools" ,rust-itertools-0.10)
        ("rust-toml-dotted-table" ,rust-toml-dotted-table-0.5)
        ("rust-chrono" ,rust-chrono-0.4)
        ("rust-serde-derive" ,rust-serde-derive-1)
        ("rust-serde-yaml" ,rust-serde-yaml-0.8)
        ("rust-wayland-client" ,rust-wayland-client-0.29)
        ("rust-wayland-sys" ,rust-wayland-sys-0.29)
        ("rust-wayland-scanner" ,rust-wayland-scanner-0.29)
        ("rust-wayland-commons" ,rust-wayland-commons-0.29)
        ("rust-wayland-protocols" ,rust-wayland-protocols-0.29)
        ("rust-ash" ,rust-ash-0.35)
        ("rust-env-logger" ,rust-env-logger-0.9)
        ("rust-lazy-static" ,rust-lazy-static-1)
        ("rust-log" ,rust-log-0.4)
        ("rust-v4l" ,rust-v4l-0.12)
        ("rust-inotify" ,rust-inotify-0.10)
        ("rust-ddc-hi" ,rust-ddc-hi-0.4))
       #:cargo-development-inputs
       (("rust-mockall" ,rust-mockall-0.10))
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'patch-cargo-toml
           ;; guix or guix-cargo cannot serialize the patched toml
           ;; that wluma wants
           (lambda _
             (substitute* "Cargo.toml"
               (("^toml.+$")
                "toml = \"0.5.8\"\n"))))
         (add-before 'install 'install-more
           (lambda* (#:key outputs #:allow-other-keys)
             (let*
                 ((out (assoc-ref outputs "out"))
                  (xdgdir (string-append out "/etc/xdg"))
                  (rulesdir (string-append out "/lib/udev/rules.d")))
               (mkdir-p rulesdir)
               (install-file
                (car
                 (find-files "." "90-wluma-backlight.rules")) rulesdir)
               (install-file
                (car
                 (find-files "." "config.toml")) xdgdir)))))))
   (home-page "https://github.com/maximbaz/wluma")
   (synopsis "Automatic brightness adjustment based on screen contents and ALS")
   (description
    "A tool for wlroots-based compositors that automatically adjusts screen
 brightness based on the screen contents and amount of ambient light around
 you.
The app will automatically brighten the screen when you are looking at a
 dark window (such as a fullscreen terminal) and darken the screen when
 you are looking at a bright window (such as web browser). The algorithm
 takes into consideration the amount of ambient light around you, so the
 same window can be brighter during the day than during the night.")
   (license license:gpl3+)))

;;;wluma rust dependencies (imported)

(define-public rust-takeable-option-0.5
  (package
    (name "rust-takeable-option")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "takeable-option" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "182axkm8pq7cynsfn65ar817mmdhayrjmbl371yqp8zyzhr8kbin"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "A small wrapper around option.")
    (description "This package provides a small wrapper around option.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-smithay-client-toolkit-0.6
  (package
    (name "rust-smithay-client-toolkit")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smithay-client-toolkit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "078gj7pw3x09y202yfz423iapky5q968n3qni1dj1jzmmk3qs722"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-andrew" ,rust-andrew-0.2)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-dlib" ,rust-dlib-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-memmap" ,rust-memmap-0.7)
         ("rust-nix" ,rust-nix-0.14)
         ("rust-wayland-client" ,rust-wayland-client-0.23)
         ("rust-wayland-protocols" ,rust-wayland-protocols-0.23))))
    (home-page "https://github.com/smithay/client-toolkit")
    (synopsis "Toolkit for making client wayland applications.")
    (description "Toolkit for making client wayland applications.")
    (license license:expat)))

(define-public rust-ndk-macro-0.1
  (package
    (name "rust-ndk-macro")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b2fg6cy43kh1zxvj97prl3ximvj38clpfjch7fymjrna3vqib11"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-proc-macro-crate" ,rust-proc-macro-crate-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Helper macros for android ndk")
    (description "Helper macros for android ndk")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-glue-0.1
  (package
    (name "rust-ndk-glue")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-glue" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pf8n60kggcswm5ld9dp8a23gks6vgvx56z11sl3fpsar42ghafb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-log-sys" ,rust-android-log-sys-0.1)
         ("rust-android-logger" ,rust-android-logger-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-ndk" ,rust-ndk-0.1)
         ("rust-ndk-macro" ,rust-ndk-macro-0.1)
         ("rust-ndk-sys" ,rust-ndk-sys-0.1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Startup code for android binaries")
    (description "Startup code for android binaries")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-sys-0.1
  (package
    (name "rust-ndk-sys")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02xafh4z7kzs1bz0bivzsvavyy90gygxsax42983a4ljgmj4ya1x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "FFI bindings for the Android NDK")
    (description "FFI bindings for the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-ndk-0.1
  (package
    (name "rust-ndk")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndk" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qksz81s5ymkpmlyg0jnab4h7fk024w6zh1z5jbkrzygb9yyf03q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jni" ,rust-jni-0.14)
         ("rust-jni-glue" ,rust-jni-glue-0.0)
         ("rust-jni-sys" ,rust-jni-sys-0.3)
         ("rust-ndk-sys" ,rust-ndk-sys-0.1)
         ("rust-num-enum" ,rust-num-enum-0.4)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/rust-windowing/android-ndk-rs")
    (synopsis "Safe Rust bindings to the Android NDK")
    (description "Safe Rust bindings to the Android NDK")
    (license (list license:expat license:asl2.0))))

(define-public rust-winit-0.22
  (package
    (name "rust-winit")
    (version "0.22.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qkqiv7h4ai6hy2ghs9lpd6vzihgx36wl5nfx8l7hqmnvpvwnk0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-cocoa" ,rust-cocoa-0.20)
         ("rust-core-foundation" ,rust-core-foundation-0.7)
         ("rust-core-graphics" ,rust-core-graphics-0.19)
         ("rust-core-video-sys" ,rust-core-video-sys-0.1)
         ("rust-dispatch" ,rust-dispatch-0.2)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-mio" ,rust-mio-0.6)
         ("rust-mio-extras" ,rust-mio-extras-2)
         ("rust-ndk" ,rust-ndk-0.1)
         ("rust-ndk-glue" ,rust-ndk-glue-0.1)
         ("rust-ndk-sys" ,rust-ndk-sys-0.1)
         ("rust-objc" ,rust-objc-0.2)
         ("rust-objc" ,rust-objc-0.2)
         ("rust-parking-lot" ,rust-parking-lot-0.10)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-raw-window-handle" ,rust-raw-window-handle-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-smithay-client-toolkit" ,rust-smithay-client-toolkit-0.6)
         ("rust-stdweb" ,rust-stdweb-0.4)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-wayland-client" ,rust-wayland-client-0.23)
         ("rust-web-sys" ,rust-web-sys-0.3)
         ("rust-winapi" ,rust-winapi-0.3)
         ("rust-x11-dl" ,rust-x11-dl-2))))
    (home-page "https://github.com/rust-windowing/winit")
    (synopsis "Cross-platform window creation library.")
    (description "Cross-platform window creation library.")
    (license license:asl2.0)))

(define-public rust-cocoa-0.20
  (package
    (name "rust-cocoa")
    (version "0.20.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cocoa" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1y0wd1lyiz8cgbsf0fwyw06gb1akg6rvg5jr3wah8mvdqdpyhj8c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-block" ,rust-block-0.1)
         ("rust-core-foundation" ,rust-core-foundation-0.7)
         ("rust-core-graphics" ,rust-core-graphics-0.19)
         ("rust-foreign-types" ,rust-foreign-types-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-objc" ,rust-objc-0.2))))
    (home-page "https://github.com/servo/core-foundation-rs")
    (synopsis "Bindings to Cocoa for macOS")
    (description "Bindings to Cocoa for macOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-glutin-0.24
  (package
    (name "rust-glutin")
    (version "0.24.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "glutin" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03z2q2717r2lnfrmnlhd24fss6p16n64dqjrcn7h1zcszp46d5ls"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-android-glue" ,rust-android-glue-0.2)
         ("rust-cgl" ,rust-cgl-0.3)
         ("rust-cocoa" ,rust-cocoa-0.20)
         ("rust-core-foundation" ,rust-core-foundation-0.7)
         ("rust-core-graphics" ,rust-core-graphics-0.19)
         ("rust-glutin-egl-sys" ,rust-glutin-egl-sys-0.1)
         ("rust-glutin-emscripten-sys" ,rust-glutin-emscripten-sys-0.1)
         ("rust-glutin-gles2-sys" ,rust-glutin-gles2-sys-0.1)
         ("rust-glutin-glx-sys" ,rust-glutin-glx-sys-0.1)
         ("rust-glutin-wgl-sys" ,rust-glutin-wgl-sys-0.1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libloading" ,rust-libloading-0.5)
         ("rust-log" ,rust-log-0.4)
         ("rust-objc" ,rust-objc-0.2)
         ("rust-osmesa-sys" ,rust-osmesa-sys-0.1)
         ("rust-parking-lot" ,rust-parking-lot-0.10)
         ("rust-wayland-client" ,rust-wayland-client-0.23)
         ("rust-winapi" ,rust-winapi-0.3)
         ("rust-winit" ,rust-winit-0.22))))
    (home-page "https://github.com/rust-windowing/glutin")
    (synopsis "Cross-platform OpenGL context provider.")
    (description "Cross-platform OpenGL context provider.")
    (license license:asl2.0)))

(define-public rust-glium-0.27
  (package
    (name "rust-glium")
    (version "0.27.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "glium" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ygbxqkra964vgry6mhvz64dyj4kx0qy3i82n24ybizs28xb42q3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-gl-generator" ,rust-gl-generator-0.14)
         ("rust-glutin" ,rust-glutin-0.24)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-memoffset" ,rust-memoffset-0.5)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-takeable-option" ,rust-takeable-option-0.5))))
    (home-page "https://github.com/glium/glium")
    (synopsis
      "Elegant and safe OpenGL wrapper.

Glium is an intermediate layer between OpenGL and your application. You still need to manually handle
the graphics pipeline, but without having to use OpenGL's old and error-prone API.

Its objectives:

 - Be safe to use. Many aspects of OpenGL that can trigger a crash if misused are automatically handled by glium.
 - Provide an API that enforces good pratices such as RAII or stateless function calls.
 - Be compatible with all OpenGL versions that support shaders, providing unified API when things diverge.
 - Avoid all OpenGL errors beforehand.
 - Produce optimized OpenGL function calls, and allow the user to easily use modern OpenGL techniques.
")
    (description
      "Elegant and safe OpenGL wrapper.

Glium is an intermediate layer between OpenGL and your application.  You still
need to manually handle the graphics pipeline, but without having to use
OpenGL's old and error-prone API.

Its objectives:

 - Be safe to use.  Many aspects of OpenGL that can trigger a crash if misused
are automatically handled by glium.   - Provide an API that enforces good
pratices such as RAII or stateless function calls.   - Be compatible with all
OpenGL versions that support shaders, providing unified API when things diverge.
- Avoid all OpenGL errors beforehand.   - Produce optimized OpenGL function
calls, and allow the user to easily use modern OpenGL techniques.")
    (license license:asl2.0)))

(define-public rust-v4l2-sys-mit-0.2
  (package
    (name "rust-v4l2-sys-mit")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "v4l2-sys-mit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ki535aipfnwvbzk7zwn8mw96yd8y8a44q4z4bxivbzldp035jg0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.56))))
    (home-page "")
    (synopsis "Raw v4l2 bindings (MIT licensed)")
    (description "Raw v4l2 bindings (MIT licensed)")
    (license license:expat)))

(define-public rust-bindgen-0.56
  (package
    (name "rust-bindgen")
    (version "0.56.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bindgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fajmgk2064ca1z9iq1jjkji63qwwz38z3d67kv6xdy0xgdpk8rd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-cexpr" ,rust-cexpr-0.4)
         ("rust-clang-sys" ,rust-clang-sys-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-env-logger" ,rust-env-logger-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lazycell" ,rust-lazycell-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-peeking-take-while" ,rust-peeking-take-while-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-shlex" ,rust-shlex-0.1)
         ("rust-which" ,rust-which-3))))
    (home-page "https://rust-lang.github.io/rust-bindgen/")
    (synopsis
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (description
      "Automatically generates Rust FFI bindings to C and C++ libraries.")
    (license license:bsd-3)))

(define-public rust-v4l-sys-0.2
  (package
    (name "rust-v4l-sys")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "v4l-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zj78arn0pani67qhqgbz4p8r9j3spr998bvmyha3838jrzw87gr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bindgen" ,rust-bindgen-0.56))))
    (home-page "")
    (synopsis "Raw video4linux (v4l) bindings")
    (description "Raw video4linux (v4l) bindings")
    (license license:expat)))

(define-public rust-v4l-0.12
  (package
    (name "rust-v4l")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "v4l" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sd8sn3lcpji4zqxgi79swyyi4agd4l8iqam7cslnsx8picclvjg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-v4l-sys" ,rust-v4l-sys-0.2)
         ("rust-v4l2-sys-mit" ,rust-v4l2-sys-mit-0.2))
        #:cargo-development-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-glium" ,rust-glium-0.27))))
    (home-page "https://github.com/raymanfx/libv4l-rs")
    (synopsis "Safe video4linux (v4l) bindings")
    (description "Safe video4linux (v4l) bindings")
    (license license:expat)))

(define-public rust-inotify-0.10
  (package
    (name "rust-inotify")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1yfkp6k5yn1lyy2qbsnikaix22zikygdqj69nabh2aawazwqiy5b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-inotify-sys" ,rust-inotify-sys-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-tokio" ,rust-tokio-1))
        #:cargo-development-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/hannobraun/inotify")
    (synopsis "Idiomatic wrapper for inotify")
    (description "Idiomatic wrapper for inotify")
    (license license:isc)))

(define-public rust-nvapi-sys-0.1
  (package
    (name "rust-nvapi-sys")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nvapi-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19vy3vy7hhik8gymi1gmsq6jqbgk3vnpcpygpxbfi7n6jf9rm7mj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/arcnmx/nvapi-rs")
    (synopsis "NVIDIA NVAPI FFI bindings")
    (description "NVIDIA NVAPI FFI bindings")
    (license license:expat)))

(define-public rust-nvapi-0.1
  (package
    (name "rust-nvapi")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nvapi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g40ira6m7wa08hlplc1akmkv30ag4r0lwv827mdlawbnhy2bhq2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-i2c" ,rust-i2c-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-nvapi-sys" ,rust-nvapi-sys-0.1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-void" ,rust-void-1))))
    (home-page "https://github.com/arcnmx/nvapi-rs")
    (synopsis "NVIDIA NVAPI bindings")
    (description "NVIDIA NVAPI bindings")
    (license license:expat)))

(define-public rust-serde-yaml-0.7
  (package
    (name "rust-serde-yaml")
    (version "0.7.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_yaml" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0l9hqmzfwswqvx5gci0hji6497gim73r10bjl6ckq9r8vz9rk07g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dtoa" ,rust-dtoa-0.4)
         ("rust-linked-hash-map" ,rust-linked-hash-map-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-yaml-rust" ,rust-yaml-rust-0.4))))
    (home-page "https://github.com/dtolnay/serde-yaml")
    (synopsis "YAML support for Serde")
    (description "YAML support for Serde")
    (license (list license:expat license:asl2.0))))

(define-public rust-mccs-db-0.1
  (package
    (name "rust-mccs-db")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mccs-db" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l16h39ww750d3mlzk1q6fqmqshhr6vgms31qh42j7p1w6xnywlr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-mccs" ,rust-mccs-0.1)
         ("rust-nom" ,rust-nom-3)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.7))))
    (home-page "https://github.com/arcnmx/mccs-rs")
    (synopsis "MCCS specification VCP database")
    (description "MCCS specification VCP database")
    (license license:expat)))

(define-public rust-mccs-caps-0.1
  (package
    (name "rust-mccs-caps")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mccs-caps" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kp3rcngwwl39lhk03hshx9qqyinywqpwyc12wzkzidf8l9nnxnr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-mccs" ,rust-mccs-0.1) ("rust-nom" ,rust-nom-3))))
    (home-page "https://github.com/arcnmx/mccs-rs")
    (synopsis "MCCS capability string parser")
    (description "MCCS capability string parser")
    (license license:expat)))

(define-public rust-edid-0.3
  (package
    (name "rust-edid")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "edid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zi4md5sy60nlcy45d3w9r48ki6ywzh7rdivzvf39n4k119pbki4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t #:cargo-inputs (("rust-nom" ,rust-nom-3))))
    (home-page "https://github.com/emersion/rust-edid")
    (synopsis "Parse EDID data structures")
    (description "Parse EDID data structures")
    (license license:expat)))

(define-public rust-widestring-0.3
  (package
    (name "rust-widestring")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "widestring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "108phf854bzmqg04cxgxy68gh7r8qr7slqrnz1251gwgllp944m2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/starkat99/widestring-rs")
    (synopsis
      "A wide string Rust library for converting to and from wide strings, such as those often used in Windows API or other FFI libaries. Both `u16` and `u32` string types are provided, including support for UTF-16 and UTF-32, malformed encoding, C-style strings, etc.")
    (description
      "This package provides a wide string Rust library for converting to and from wide
strings, such as those often used in Windows API or other FFI libaries.  Both
`u16` and `u32` string types are provided, including support for UTF-16 and
UTF-32, malformed encoding, C-style strings, etc.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ddc-winapi-0.2
  (package
    (name "rust-ddc-winapi")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ddc-winapi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17pwswh6k3yjjlww9vpal9jclbr0i6k4d5ajvqvf4w68cldyff1j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ddc" ,rust-ddc-0.2)
         ("rust-widestring" ,rust-widestring-0.3)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/arcnmx/ddc-winapi-rs")
    (synopsis "DDC/CI monitor control on Windows")
    (description "DDC/CI monitor control on Windows")
    (license license:expat)))

(define-public rust-mach-0.3
  (package
    (name "rust-mach")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mach" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1yksa8lwzqh150gr4417rls1wk20asy9vhp8kq5g9n7z58xyh8xq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1))))
    (home-page "https://github.com/fitzgen/mach")
    (synopsis
      "A Rust interface to the user-space API of the Mach 3.0 kernel that underlies OSX.")
    (description
      "This package provides a Rust interface to the user-space API of the Mach 3.0
kernel that underlies OSX.")
    (license #f)))

(define-public rust-io-kit-sys-0.1
  (package
    (name "rust-io-kit-sys")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "io-kit-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "186h7gm6kf1d00cb3w5mpyf9arcdkxw7jzhl1c4wvm2xk5scq7gj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-core-foundation-sys" ,rust-core-foundation-sys-0.6)
         ("rust-mach" ,rust-mach-0.2))))
    (home-page "https://github.com/jtakakura/io-kit-rs")
    (synopsis "Bindings to IOKit for macOS")
    (description "Bindings to IOKit for macOS")
    (license (list license:expat license:asl2.0))))

(define-public rust-ddc-macos-0.2
  (package
    (name "rust-ddc-macos")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ddc-macos" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1slx57l8yd9fak8n51fpzmrqn5mlvw282v45m06w7kqkq4bg7fjw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-core-foundation" ,rust-core-foundation-0.9)
         ("rust-core-foundation-sys" ,rust-core-foundation-sys-0.8)
         ("rust-core-graphics" ,rust-core-graphics-0.22)
         ("rust-ddc" ,rust-ddc-0.2)
         ("rust-io-kit-sys" ,rust-io-kit-sys-0.1)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/haimgel/ddc-macos-rs")
    (synopsis "DDC/CI monitor control on MacOS")
    (description "DDC/CI monitor control on MacOS")
    (license license:expat)))

(define-public rust-libudev-sys-0.1
  (package
    (name "rust-libudev-sys")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libudev-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09236fdzlx9l0dlrsc6xx21v5x8flpfm3d5rjq9jr5ivlas6k11w"))))
    (build-system cargo-build-system)
    (native-inputs (list pkg-config))
    (inputs (list eudev))
    (arguments
      `(#:skip-build? #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/dcuddeback/libudev-sys")
    (synopsis "FFI bindings to libudev")
    (description "FFI bindings to libudev")
    (license license:expat)))

(define-public rust-udev-0.2
  (package
    (name "rust-udev")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "udev" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00gkkicb8vfhf32c4h5lp7z07kw31agivpbx7q9ipsmj94d4sl27"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-libudev-sys" ,rust-libudev-sys-0.1))))
    (home-page "https://github.com/Smithay/udev-rs")
    (synopsis "libudev bindings for Rust")
    (description "libudev bindings for Rust")
    (license license:expat)))

(define-public rust-uninitialized-0.0.2
  (package
    (name "rust-uninitialized")
    (version "0.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uninitialized" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10by0nyjl44a4y7y2lgv1h03yycbbwghnvs0932pd0n3252smhbl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/arcnmx/uninitialized-rs")
    (synopsis "Opt-in unsafe uninitialized memory")
    (description "Opt-in unsafe uninitialized memory")
    (license license:expat)))

(define-public rust-smallvec-0.1
  (package
    (name "rust-smallvec")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "042lhr2f1lxh0lgz0p4i59rvkbpp4sdig8m7wjawzkma2a9d3j7w"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/servo/rust-smallvec")
    (synopsis
      "'Small vector' optimization: store up to a small number of items on the stack")
    (description
      "'Small vector' optimization: store up to a small number of items on the stack")
    (license license:mpl2.0)))

(define-public rust-resize-slice-0.1
  (package
    (name "rust-resize-slice")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "resize-slice" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jlc614745fvf7ykxfz379n4cnx2d79crq5rb1lyg4cq9bvv4g4a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-smallvec" ,rust-smallvec-0.1)
         ("rust-uninitialized" ,rust-uninitialized-0.0.2))))
    (home-page "https://github.com/arcnmx/resize-slice-rs")
    (synopsis "Shrink slice references")
    (description "Shrink slice references")
    (license license:expat)))

(define-public rust-i2c-linux-sys-0.2
  (package
    (name "rust-i2c-linux-sys")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "i2c-linux-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1y5w7g6sm4fkydcqwfhz1plls22q04xs5lsfvb9j2rh1s070dkam"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/arcnmx/i2c-linux-sys-rs")
    (synopsis "Linux i2c-dev ioctls")
    (description "Linux i2c-dev ioctls")
    (license license:expat)))

(define-public rust-i2c-linux-0.1
  (package
    (name "rust-i2c-linux")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "i2c-linux" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "196jgnw9hpmgiy6cd1qdkgji0iznvjz5x1y2sqhi41xa3a3ql9n0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-i2c" ,rust-i2c-0.1)
         ("rust-i2c-linux-sys" ,rust-i2c-linux-sys-0.2)
         ("rust-resize-slice" ,rust-resize-slice-0.1)
         ("rust-udev" ,rust-udev-0.2))))
    (home-page "https://github.com/arcnmx/i2c-linux-rs")
    (synopsis "Linux I2C device interface")
    (description "Linux I2C device interface")
    (license license:expat)))

(define-public rust-i2c-0.1
  (package
    (name "rust-i2c")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "i2c" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16nfl82c2759w5fbabzjnzlgsivsivcxg84lrzyqbadksyyvgiv0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "https://github.com/arcnmx/i2c-rs")
    (synopsis "Generic I2C traits")
    (description "Generic I2C traits")
    (license license:expat)))

(define-public rust-ddc-i2c-0.2
  (package
    (name "rust-ddc-i2c")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ddc-i2c" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hn8nscv9g3f6123iz7wf54mszn55qxy7sxk6asj3z21pmbk0l36"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ddc" ,rust-ddc-0.2)
         ("rust-i2c" ,rust-i2c-0.1)
         ("rust-i2c-linux" ,rust-i2c-linux-0.1)
         ("rust-resize-slice" ,rust-resize-slice-0.1))))
    (home-page "https://github.com/arcnmx/ddc-i2c-rs")
    (synopsis "DDC/CI monitor control over I2C")
    (description "DDC/CI monitor control over I2C")
    (license license:expat)))

(define-public rust-mccs-0.1
  (package
    (name "rust-mccs")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mccs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08l41y7bsj2h1mln21d6xml7lrkyk9wlc6jmskh4348plinnqdkl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t #:cargo-inputs (("rust-void" ,rust-void-1))))
    (home-page "https://github.com/arcnmx/mccs-rs")
    (synopsis "VESA Monitor Control Command Set")
    (description "VESA Monitor Control Command Set")
    (license license:expat)))

(define-public rust-ddc-0.2
  (package
    (name "rust-ddc")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ddc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1if6nf8mkv49fls7z47931ps7l84pyxh5jqpmnmw83rj7v2z4sds"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-mccs" ,rust-mccs-0.1))))
    (home-page "https://github.com/arcnmx/ddc-rs")
    (synopsis "DDC/CI monitor control")
    (description "DDC/CI monitor control")
    (license license:expat)))

(define-public rust-ddc-hi-0.4
  (package
    (name "rust-ddc-hi")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ddc-hi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pllvvsmra839jdgdqphpaky69n7dhd0zcrr8yijlsljgjqlfrww"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-ddc" ,rust-ddc-0.2)
         ("rust-ddc-i2c" ,rust-ddc-i2c-0.2)
         ("rust-ddc-macos" ,rust-ddc-macos-0.2)
         ("rust-ddc-winapi" ,rust-ddc-winapi-0.2)
         ("rust-edid" ,rust-edid-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-mccs" ,rust-mccs-0.1)
         ("rust-mccs-caps" ,rust-mccs-caps-0.1)
         ("rust-mccs-db" ,rust-mccs-db-0.1)
         ("rust-nvapi" ,rust-nvapi-0.1))))
    (home-page "https://github.com/arcnmx/ddc-hi-rs")
    (synopsis "High level DDC/CI monitor control")
    (description "High level DDC/CI monitor control")
    (license license:expat)))

(define-public rust-ash-0.35
  (package
    (name "rust-ash")
    (version "0.35.2+1.2.203")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ph72v2d1nh1x5b7233i53bcbqfm14znxfy2b8sknmnlr8dljslg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libloading" ,rust-libloading-0.7))))
    (home-page "https://github.com/MaikKlein/ash")
    (synopsis "Vulkan bindings for Rust")
    (description "Vulkan bindings for Rust")
    (license license:expat)))

(define-public rust-wayland-server-0.29
  (package
    (name "rust-wayland-server")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri
         (crate-uri "wayland-server" version))
        (file-name
         (string-append name "-" version ".tar.gz"))
        (sha256
         (base32
          "1wj8gf28dbjwb824i29wf3wr5r6wp6ssknjm9b5dnb1fah47mk66"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-downcast-rs" ,rust-downcast-rs-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.22)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-wayland-commons" ,rust-wayland-commons-0.29)
         ("rust-wayland-scanner" ,rust-wayland-scanner-0.29)
         ("rust-wayland-sys" ,rust-wayland-sys-0.29))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
      "Bindings to the standard C implementation of the wayland protocol, server side.")
    (description
      "Bindings to the standard C implementation of the wayland protocol, server side.")
    (license license:expat)))

(define-public rust-wayland-protocols-0.29
  (package
    (name "rust-wayland-protocols")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wayland-protocols" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hap8vky2fwsq05c98c8xs00gb9m5kxp8kq3zr0jwh036gi7l530"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-wayland-client" ,rust-wayland-client-0.29)
         ("rust-wayland-commons" ,rust-wayland-commons-0.29)
         ("rust-wayland-scanner" ,rust-wayland-scanner-0.29)
         ("rust-wayland-server" ,rust-wayland-server-0.29))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis "Generated API for the officials wayland protocol extensions")
    (description "Generated API for the officials wayland protocol extensions")
    (license license:expat)))

(define-public rust-wayland-scanner-0.29
  (package
    (name "rust-wayland-scanner")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wayland-scanner" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q7r764z8k922xf51fj56b1xm29ffi9ap8jnf4c478gp8cqyv89r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-xml-rs" ,rust-xml-rs-0.8))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
     "Wayland Scanner for generating rust APIs from XML wayland
 protocol files. Intended for use with wayland-sys.
 You should only need this crate if you are working on custom
 wayland protocol extensions. Look at the crate wayland-client
 for usable bindings.")
    (description
      "Wayland Scanner for generating rust APIs from XML wayland protocol files.
Intended for use with wayland-sys.  You should only need this crate if you are
working on custom wayland protocol extensions.  Look at the crate wayland-client
for usable bindings.")
    (license license:expat)))

(define-public rust-dlib-0.5
  (package
    (name "rust-dlib")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dlib" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1547hy7nrhkrb2i09va244c0h8mr845ccbs2d2mc414c68bpa6xc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libloading" ,rust-libloading-0.7))))
    (home-page "https://github.com/vberger/dlib")
    (synopsis
      "Helper macros for handling manually loading optional system libraries.")
    (description
      "Helper macros for handling manually loading optional system libraries.")
    (license license:expat)))

(define-public rust-wayland-sys-0.29
  (package
    (name "rust-wayland-sys")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wayland-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m2jwk5q36jidwbdmdicmi27r9dzi4wanzg3i28nfxc9kbvisd6r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dlib" ,rust-dlib-0.5)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memoffset" ,rust-memoffset-0.6)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
     "FFI bindings to the various libwayland-*.so libraries.
 You should only need this crate if you are working on custom wayland protocol extensions.
 Look at the crate wayland-client for usable bindings.")
    (description
      "FFI bindings to the various libwayland-*.so libraries.  You should only need
this crate if you are working on custom wayland protocol extensions.  Look at
the crate wayland-client for usable bindings.")
    (license license:expat)))

(define-public rust-wayland-commons-0.29
  (package
    (name "rust-wayland-commons")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wayland-commons" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gnk4a771i3g1k4fbzx54xnganpc9j68jrx8xj839hfp83iybxll"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nix" ,rust-nix-0.22)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-wayland-sys" ,rust-wayland-sys-0.29))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
      "Common types and structures used by wayland-client and wayland-server.")
    (description
      "Common types and structures used by wayland-client and wayland-server.")
    (license license:expat)))

(define-public rust-wayland-client-0.29
  (package
    (name "rust-wayland-client")
    (version "0.29.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wayland-client" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13s5sj9344izk2g48yizk81kcg8jg4940gg2v6bzcmrjwxh388li"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-downcast-rs" ,rust-downcast-rs-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.22)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-wayland-commons" ,rust-wayland-commons-0.29)
         ("rust-wayland-scanner" ,rust-wayland-scanner-0.29)
         ("rust-wayland-sys" ,rust-wayland-sys-0.29))
        #:cargo-development-inputs
        (("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/smithay/wayland-rs")
    (synopsis
      "Bindings to the standard C implementation of the wayland protocol, client side.")
    (description
      "Bindings to the standard C implementation of the wayland protocol, client side.")
    (license license:expat)))


(define-public rust-toml-dotted-table-0.5
  (package
   (inherit rust-toml-0.5)
   (name "rust-toml-dotted-table")
   (version "0.5.8")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/vgao1996/toml-rs")
       (commit (string-append "dotted-table-" version ))))
     (file-name
      (git-file-name name version))
     (sha256
      (base32 "162xj5scdcvk61hzcw2iajacmkpp2b0cyyjpjfypsgqp2q8vksk8"))))
   (arguments
    `(#:cargo-inputs
      (("rust-indexmap" ,rust-indexmap-1)
       ("rust-serde" ,rust-serde-1)
       ("rust-bencher" ,rust-bencher-0.1))
      #:cargo-development-inputs
      (("rust-serde-derive" ,rust-serde-derive-1)
       ("rust-serde-json" ,rust-serde-json-1))))))

(define-public wdisplays
  (package
   (name "wdisplays")
   (version "1.1")
   (source
    (origin
     (method url-fetch)
     (uri "https://github.com/artizirk/wdisplays/archive/refs/tags/1.1.tar.gz")
            (sha256
             (base32 "103gpg74fjqaaakx033axichaxdnnkvdfa1grbkqz4hdrvg3k9ss"))))
   (build-system meson-build-system)
   (arguments
    (list #:glib-or-gtk? #t
          #:phases
          ;;fixes issue with finding libGLESv2.so....
          #~(modify-phases %standard-phases
              (add-after 'install 'mesa-wrap
                (lambda* (#:key outputs inputs #:allow-other-keys)
                  (let* ((out (assoc-ref outputs "out"))
                         (mesa (assoc-ref inputs "mesa")))
                    (wrap-program (string-append out "/bin/wdisplays")
                      `("LD_LIBRARY_PATH" ":" prefix
                        (,(string-append mesa "/lib"))))) #t)))))
   (native-inputs (list `(,glib "bin") pkg-config))
   (inputs (list gtk+
                 libepoxy
                 mesa
                 wayland
                 wayland-protocols
                 wlroots
                 python-scour))
   (home-page "https://github.com/artizirk/wdisplays")
   (synopsis "GTK+ display configuration GUI for Wayland")
   (description "@code{wdisplays} is a graphical application for configuring
 displays in Wayland compositors.
 It should work in any compositor that implements the wlr-output-
-management-unstable-v1 protocol.")
   (license license:gpl3+)))

(define-public swaynotificationcenter
  (package
    (name "swaynotificationcenter")
    (version "0.7.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/ErikReider/SwayNotificationCenter")
         (commit (string-append "v" version))))
       (sha256
        (base32 "1xvr5m5sqznr3dd512i5pk0d56v7n0ywdcy6rnz85vbf2k7b6kj5"))))
    (build-system meson-build-system)
    (arguments (list #:configure-flags #~(list "-Dsystemd-service=false")))
    (native-inputs
     (list `(,glib "bin")
           gobject-introspection
           pkg-config
           python-minimal
           scdoc
           vala-next))
    (inputs
     (list json-glib
           glib
           gtk+
           gtk-layer-shell
           libhandy
           wayland-protocols))
    (synopsis "A simple notification daemon with a GTK gui built for
 Notifications and the control center")
    (description "Features
@itemize
@item Keyboard shortcuts
@item Notification body markup with image support
@item A panel to view previous notifications
@item Show album art for notifications like Spotify
@item Do not disturb
@item Click notification to execute default action
@item Show alternative notification actions
@item Customization through a CSS file
@item Trackpad/mouse gesture to close notification
@item The same features as any other basic notification daemon
@item Basic configuration through a JSON config file
@item Hot-reload config through swaync-client
@end itemize")
    (home-page "https://github.com/ErikReider/SwayNotificationCenter")
    (license license:expat)))

;;;nwg-shell packages and dependencies

(define-public nwg-wrapper
  (package
   (name "nwg-wrapper")
   (version "0.1.3")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://github.com/nwg-piotr/nwg-wrapper")
        (commit (string-append "v" version))))
      (sha256
       (base32 "1svyfrfvpj9bzi202gwki4r0zsj3lhxjhis112fsgvds6dvc180q"))))
   (build-system python-build-system)
   (native-inputs  (list pkg-config gobject-introspection python-pygobject))
   (inputs
    (list gobject-introspection glib python-pygobject python gtkmm-3 glibmm
          gtk+ wlr-randr gtk-layer-shell))
   (arguments
    `(#:tests? #f
      #:phases
      (modify-phases %standard-phases
        (delete 'sanity-check)
        (add-after
            'install 'wrap-program
          (lambda* (#:key outputs #:allow-other-keys)
            (let*
                ((out (string-append (assoc-ref outputs "out")))
                 (prog (string-append out "/bin/nwg-wrapper"))
                 (sharedir (string-append out "/share"))
                 (pylib
                  (string-append out
                                 "/lib/python"
                                 ,(version-major+minor
                                   (package-version python))
                                 "/site-packages")))
              (wrap-program prog
                `("GUIX_PYTHONPATH" =
                  (,(getenv "GUIX_PYTHONPATH") ,pylib))
                `("GI_TYPELIB_PATH" = (,(getenv "GI_TYPELIB_PATH")))
                `("XDG_DATA_DIRS" =
                  (,out ,(getenv "GI_TYPELIB_PATH"))))))))))
   (synopsis "")
   (description "This program is a GTK3-based wrapper to display a script
 output, or a text file content on the desktop in sway or other wlroots-based
 compositors. It uses the gtk-layer-shell library to place the window on the
 bottom layer.")
   (home-page "https://github.com/nwg-piotr/nwg-wrapper")
   (license license:expat)))

(define-public python-i3ipc
  (package
    (name "python-i3ipc")
    (version "2.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "i3ipc" version))
        (sha256
          (base32 "1s6crkdn7q8wmzl5d0pb6rdkhhbvp444yxilrgaylnbr2kbxg078"))))
    (build-system python-build-system)
    (propagated-inputs (list python-xlib))
    (native-inputs (list python-pytest))
    (arguments `(#:tests? #f))
    (home-page "https://github.com/altdesktop/i3ipc-python")
    (synopsis "An improved Python library to control i3wm and sway")
    (description "An improved Python library to control i3wm and sway")
    (license license:bsd-3)))

(define-public python-dasbus
  (package
    (name "python-dasbus")
    (version "1.6")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "dasbus" version))
        (sha256
          (base32 "0kg9aqpckwj2dlff4iwym2cli0xjg9a9n5ahmchq6a9xikydi6hl"))))
    (build-system python-build-system)
    (home-page "https://github.com/rhinstaller/dasbus")
    ;;(arguments
     ;; can't execute dbus-daemon
    ;; `(#:tests? #f))
    (inputs (list dbus-glib glib))
    (native-inputs (list python-pygobject))
    (synopsis "DBus library in Python 3")
    (description "DBus library in Python 3")
    (license #f)))

(define-public nwg-panel
  (package
    (name "nwg-panel")
    (version "0.7.8")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/nwg-piotr/nwg-panel")
         (commit (string-append "v" version))))
       (sha256
        (base32 "0h54c6413grfv2bw94x3p1g06wcylbwnfkqj9naidgkim7lhq5km"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f
       ;;tests fail with 'unable to init server' and
       ;;'no permissions for /homeless-shelter'
       #:imported-modules ((guix build glib-or-gtk-build-system)
                           ,@%python-build-system-modules)
       #:modules ((guix build python-build-system)
                  ((guix build glib-or-gtk-build-system) #:prefix glib-or-gtk:)
                  (guix build utils))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'rm-pycache
           ;; created python cache __pycache__ are non deterministic
           (lambda* (#:key outputs #:allow-other-keys)

             (let* ((out (string-append (assoc-ref outputs "out")))
                    (pylib (string-append out "/lib/python"
                                          ,(version-major+minor
                                            (package-version python))
                                          "/site-packages"))
                    (pycaches (find-files
                               (string-append pylib "/nwg_panel")
                               "__pycache__" #:directories? #t)))
               (for-each delete-file-recursively pycaches)
               #t)))
         (add-after 'install 'wrap-launcher
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (string-append (assoc-ref outputs "out")))
                    (prog (string-append out "/bin/nwg-panel"))
                    (sharedir (string-append out "/share"))
                    (pylib (string-append out
                                          "/lib/python"
                                          ,(version-major+minor
                                            (package-version python))
                                          "/site-packages")))
               (wrap-program prog
                 `("GUIX_PYTHONPATH" = (,(getenv "GUIX_PYTHONPATH") ,pylib))
                 `("GI_TYPELIB_PATH" = (,(getenv "GI_TYPELIB_PATH")))
                 `("XDG_DATA_DIRS" = (,out ,(getenv "GI_TYPELIB_PATH")))))))
         (add-after 'install 'install-icons-and-desktop-files
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (string-append (assoc-ref outputs "out")))
                    (pylib (string-append out "/lib/python"
                                          ,(version-major+minor
                                            (package-version python))
                                          "/site-packages"))
                    (icons_dark (string-append pylib "/nwg-panel/icons_dark"))
                    (icons_light (string-append pylib "/nwg-panel/icons_light"))
                    (hicolor-icons (string-append out "/share/icons/hicolor/apps/symbolic")))
               (mkdir-p hicolor-icons)
               (mkdir-p (string-append out "/share/applications"))
               (install-file "nwg-shell.svg" hicolor-icons)
               (install-file "nwg-panel.svg" hicolor-icons)
               (install-file "nwg-panel-config.desktop"
                             (string-append out "/share/applications")))))
         (add-after 'wrap-program 'glib-or-gtk-wrap
           (assoc-ref glib-or-gtk:%standard-phases 'glib-or-gtk-wrap))
         (delete 'sanity-check))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list atk
           gtk+
           gobject-introspection
           gdk-pixbuf
           gtk-layer-shell
           pango
           wlr-randr))
    (propagated-inputs
     (list python-dasbus
           python-netifaces
           python-i3ipc
           python-pygobject
           python-psutil))
    (synopsis "")
    (description "")
    (home-page "https://github.com/nwg-piotr/nwg-panel")
    (license license:expat)))

(define-public go-github-com-gotk3-gotk3
    (package
      (name "go-github-com-gotk3-gotk3")
      (version "0.6.1")
      (source
       (origin
         (method url-fetch)
         (uri
          (string-append "https://github.com/gotk3/gotk3/archive/refs/tags/v"
                         version ".tar.gz"))
         (file-name (git-file-name name version))
         (sha256
          (base32 "19aa0b91f64vwynxhlp3w9i67q5w4y8qzxnbs49k2hc4s79g27wp"))))
      (build-system go-build-system)
      (native-inputs
       (list gobject-introspection pkg-config))
      (inputs
       (list gtk+ glib ))
      (arguments
       `(#:unpack-path "github.com/gotk3/gotk3"
         #:tests? #f
         #:phases
         (modify-phases %standard-phases
           ;; Source-only package
           (delete 'build)
           (replace 'install
             (lambda arguments
               (for-each
                (lambda (directory)
                  (apply (assoc-ref %standard-phases 'install)
                         `(,@arguments #:import-path ,directory)))
                (list
                 "github.com/gotk3/gotk3/internal/slab"
                 "github.com/gotk3/gotk3/internal/callback"
                 "github.com/gotk3/gotk3/internal/closure"
                 "github.com/gotk3/gotk3/glib"
                 "github.com/gotk3/gotk3/cairo"
                 "github.com/gotk3/gotk3/gdk"
                 "github.com/gotk3/gotk3/pango"
                 "github.com/gotk3/gotk3/gtk"
                 "github.com/gotk3/gotk3/gio")))))))
      (synopsis "")
      (description "")
      (home-page "https://github.com/gotk3/gotk3")
      (license license:expat)))


(define-public go-github-com-allan-simon-go-singleinstance
  (package
    (name "go-github-com-allan-simon-go-singleinstance")
    (version "0.0.0-20210120080615-d0997106ab37")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/allan-simon/go-singleinstance")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0rmkin2vjkl4qpbdcll1f4gy22xj5v83ixvbsyn78632wv0x2mqv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/allan-simon/go-singleinstance"))
    (home-page "https://github.com/allan-simon/go-singleinstance")
    (synopsis "go-singleinstance")
    (description
     "Cross plateform library to have only one instance of a software
 (based on python's
@url{https://github.com/pycontribs/tendo/raw/master/tendo/singleton.py,tendo}).")
    (license license:expat)))


(define-public go-github-com-dlasky-gotk3-layershell
  (package
    (name "go-github-com-dlasky-gotk3-layershell")
    (version "0.0.0-20210827021656-e6ecab2731f7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dlasky/gotk3-layershell")
               (commit (go-version->git-ref version))))
        (file-name
         (git-file-name name version))
        (sha256
          (base32 "1jyyg7fk20bz4ilxllvr1q5mqx72iyn2nf5wrpalsnacpdflxq10"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/dlasky/gotk3-layershell"))
    (native-inputs
     (list pkg-config gobject-introspection))
    (inputs
     (list go-github-com-gotk3-gotk3 glib gtk+ cairo gtk-layer-shell))
    (propagated-inputs
     (list go-github-com-gotk3-gotk3))
    (home-page "https://github.com/dlasky/gotk3-layershell")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public nwg-drawer
  (package
   (name "nwg-drawer")
   (version "0.3.2")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/nwg-piotr/nwg-drawer/archive/refs/tags/v"
         version ".tar.gz"))
     ;;(file-name (git-file-name name version))
     (sha256
      (base32 "1ijclc82rsxcav6zdk9s2x9gkpf203di25xhwqnbmmz5y3h5wxb3"))))
   (build-system go-build-system)
   (arguments
    '(#:import-path "github.com/nwg-piotr/nwg-drawer"
      ;;SWAYSOCK is empty
      #:tests? #f))
   (native-inputs
    (list pkg-config gobject-introspection))
   (inputs (list gtk+ glib gtk-layer-shell))
   (propagated-inputs
    (list
     go-github-com-joshuarubin-go-sway go-github-com-gotk3-gotk3
     go-github-com-dlasky-gotk3-layershell
     go-github-com-fsnotify-fsnotify
     go-github-com-sirupsen-logrus
     go-github-com-allan-simon-go-singleinstance))
   (synopsis "")
   (description "")
   (home-page "https://github.com/nwg-drawer/nwg-drawer")
   (license license:expat)))

(define-public nwg-menu
  (package
    (name "nwg-menu")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nwg-piotr/nwg-menu")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vx9xzymddgl56cj6gli38dnpagcx19pyrjlnhagrz1xcm23rpik"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nwg-piotr/nwg-menu"))
    (native-inputs (list pkg-config gobject-introspection))
    (inputs (list glib gtk+ gtk-layer-shell))
    (propagated-inputs
      `(("go-github-com-joshuarubin-go-sway"
         ,go-github-com-joshuarubin-go-sway)
        ("go-github-com-gotk3-gotk3" ,go-github-com-gotk3-gotk3)
        ("go-github-com-dlasky-gotk3-layershell"
         ,go-github-com-dlasky-gotk3-layershell)
        ("go-github-com-allan-simon-go-singleinstance"
         ,go-github-com-allan-simon-go-singleinstance)))
    (home-page "https://github.com/nwg-piotr/nwg-menu")
    (synopsis "nwg-menu")
    (description
      "This code provides the
@url{https://github.com/nwg-piotr/nwg-panel/wiki/plugins:-MenuStart,MenuStart plugin}
to @url{https://github.com/nwg-piotr/nwg-panel,nwg-panel}.
It also may be used standalone, however, with a little help from
 command line arguments.")
    (license license:expat)))


(define-public nwg-dock
  (package
    (name "nwg-dock")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/nwg-piotr/nwg-dock")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1h0dhkm1im7npklq1sxc7my54x8fzb2z0mzgzpwrvmqlcmyqpn8h"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nwg-piotr/nwg-dock"))
    (native-inputs (list pkg-config gobject-introspection))
    (inputs (list glib gtk+ gtk-layer-shell))
    (propagated-inputs
      `(("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
        ("go-github-com-joshuarubin-go-sway"
         ,go-github-com-joshuarubin-go-sway)
        ("go-github-com-gotk3-gotk3" ,go-github-com-gotk3-gotk3)
        ("go-github-com-dlasky-gotk3-layershell"
         ,go-github-com-dlasky-gotk3-layershell)
        ("go-github-com-allan-simon-go-singleinstance"
         ,go-github-com-allan-simon-go-singleinstance)))
    (home-page "https://github.com/nwg-piotr/nwg-dock")
    (synopsis "nwg-dock")
    (description
      "This program is a part of the
@url{https://github.com/nwg-piotr/nwg-shell,nwg-shell} project.")
    (license license:expat)))

(define-public go-github-com-stackexchange-wmi
  (package
    (name "go-github-com-stackexchange-wmi")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/StackExchange/wmi")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ijhmr8sl768vkxslvw7fprav6srw4ivp32rzg3ydj8nc1wh86nl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/StackExchange/wmi"))
    (propagated-inputs
      `(("go-github-com-go-ole-go-ole" ,go-github-com-go-ole-go-ole)))
    (home-page "https://github.com/StackExchange/wmi")
    (synopsis "wmi")
    (description "Package wmi provides a WQL interface for WMI on Windows.")
    (license license:expat)))

(define-public go-github-com-go-ole-go-ole
  (package
    (name "go-github-com-go-ole-go-ole")
    (version "1.2.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-ole/go-ole")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0aswlz7dr6v0if6bdwj3ivawj8cql2hgp84yymsq3ic9nys6537s"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-ole/go-ole"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/go-ole/go-ole")
    (synopsis "Go OLE")
    (description
      "Go bindings for Windows COM using shared libraries instead of cgo.")
    (license license:expat)))

(define-public gopsuinfo
  (package
    (name "gopsuinfo")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nwg-piotr/gopsuinfo")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1h146n10is3ww7jqiphif8zc65048mqyxjr67zr0r0xp00i25vmk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nwg-piotr/gopsuinfo"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-shirou-gopsutil" ,go-github-com-shirou-gopsutil)
        ("go-github-com-go-ole-go-ole" ,go-github-com-go-ole-go-ole)
        ("go-github-com-stackexchange-wmi" ,go-github-com-stackexchange-wmi)))
    (home-page "https://github.com/nwg-piotr/gopsuinfo")
    (synopsis "psinfo in go")
    (description
      "This package provides a gopsutil-based command to display customizable system
usage info in a single line")
    (license license:bsd-2)))

(define-public go-github-com-joshuarubin-lifecycle
  (package
    (name "go-github-com-joshuarubin-lifecycle")
    (version "1.1.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/joshuarubin/lifecycle")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0czjc72abgg760gmmbhd9l4r9p622d0yqmqg3hjs687dvj429wg0"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/joshuarubin/lifecycle"))
    (propagated-inputs
     `(("go-golang-org-x-sync" ,go-golang-org-x-sync)))
    (home-page "https://github.com/joshuarubin/lifecycle")
    (synopsis "lifecycle")
    (description
      "@code{lifecycle} helps manage goroutines at the application level.
@code{context.Context} has been great for propagating cancellation signals, but
not for getting any feedback about  goroutines actually finish.")
    (license license:expat)))

(define-public go-github-com-joshuarubin-go-sway
  (package
    (name "go-github-com-joshuarubin-go-sway")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/joshuarubin/go-sway")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1a4h34m5l2fjxp4s5ps66rjwpkvccvbqk4hr86cd2ajh127icvyv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/joshuarubin/go-sway"
       #:tests? #f))                    ; error: SWAYSOCK is empty
    (propagated-inputs
     `(("go-go-uber-org-multierr" ,go-go-uber-org-multierr)
       ("go-go-uber-org-atomic" ,go-go-uber-org-atomic)
       ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
       ("go-github-com-joshuarubin-lifecycle"
        ,go-github-com-joshuarubin-lifecycle)))
    (home-page "https://github.com/joshuarubin/go-sway")
    (synopsis "")
    (description "This package simplifies working with the
@url{https://swaywm.org/,sway} IPC from Go.  It was highly influenced
 by the @url{https://github.com/i3/go-i3,i3 package}.")
    (license license:expat)))
