(define-module (guix-local packages emulators)
  #:use-module (ice-9 match)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix svn-download)
  #:use-module (guix hg-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages cdrom)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cross-base)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libedit)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages music)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages upnp)
  #:use-module (gnu packages video)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages wxwidgets)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages web)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages ibus)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages cpp))

;; could disable Web services for both citra and yuzu (telemetry and subscription, opt-in)
;; devendoring?!
;; unstable versions (nix has them)

(define boost-citra-src
  (let ((commit "66937ea62d126a92b5057e3fd9ceac7c44daf4f5"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/citra-emu/ext-boost")
            (commit commit)))
      (sha256
       (base32 "0wyvfzx65wma9rqv1gm8sjb8wgvs0l49acjck2kg3x927hfh928r"))
      (file-name (git-file-name "boost" commit)))))

(define catch2-src
  (let ((commit "dc001fa935d71b4b77f263fce405c9dbdfcbfe28"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/catchorg/Catch2")
            (commit commit)))
      (sha256
       (base32 "0fi87ya1r9vxzqp5bslgmlwnjd7cx8fnb7h8d8pyj9apfwd0z34x"))
      (file-name (git-file-name "catch2" commit)))))

(define cubeb-src
  (let ((commit "dc511c6b3597b6384d28949285b9289e009830ea"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/mozilla/cubeb")
            (commit commit)))
      (sha256
       (base32 "02lhggval2b1xsd3mcvh3p3n8frv6xjbmymgr8j6kaai7sf88id6"))
      (file-name (git-file-name "cubeb" commit)))))

(define cpp-jwt-src
  (let ((commit "e12ef06218596b52d9b5d6e1639484866a8e7067"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/arun11299/cpp-jwt")
            (commit commit)))
      (sha256
       (base32 "0bxs269svimxn2fqi2k12cpbz6iy33n5g8hny5421gy6fx091r93"))
      (file-name (git-file-name "cpp-jwt" commit)))))

(define fmt-src
  (let ((commit "a33701196adfad74917046096bf5a2aa0ab0bb50"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/fmtlib/fmt")
            (commit commit)))
      (sha256
       (base32 "13b0cm1vsbb7nx713799k6j2zr0ww6f3xw25qp5bkv2w4jdv5zmc"))
      (file-name (git-file-name "fmt" commit)))))

(define soundtouch-citra-src
  (let ((commit "060181eaf273180d3a7e87349895bd0cb6ccbf4a"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/citra-emu/soundtouch-ext")
            (commit commit)))
      (sha256
       (base32 "0x92a5wb2hcaqzckpdqsggbf863a9kicpa4xz4g4gdyid19n3ci9"))
      (file-name (git-file-name "soundtouch" commit)))))

(define enet-src
  (let ((commit "4f8e9bdc4ce6d1f61a6274b0e557065a38190952"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/lsalzman/enet")
            (commit commit)))
      (sha256
       (base32 "1rym7946piflbj74by29zbq0zasaj173dackw257561754n84lqd"))
      (file-name (git-file-name "enet" commit)))))

(define dynarmic-src-citra
  (let ((commit "7a926d689bcc1cc39dd26d5bba379dffcc6815a3"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/citra-emu/dynarmic")
            (commit commit)))
      (sha256
       (base32 "13l080aq4m217mahhvl35blm96ark90xjnnld9lxj57b78cds2mj"))
      (file-name (git-file-name "dynarmic" commit)))))


(define nihstro-src
  (let ((commit "fd69de1a1b960ec296cc67d32257b0f9e2d89ac6"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/neobrain/nihstro")
            (commit commit)))
      (sha256
       (base32 "1lfx92lpszmz1xsvb80a349r3i7g5msxixkb29v1qvi4a8v3ak58"))
      (file-name (git-file-name "nihstro" commit)))))

(define teakra-src
  (let ((commit "01db7cdd00aabcce559a8dddce8798dabb71949b"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/meryhime/teakra")
            (commit commit)))
      (sha256
       (base32 "0q2bqscf76djs1fadgvh6gbl4cqgdyh8ykz0f0cg98wdmfz5kqp3"))
      (file-name (git-file-name "teakra" commit)))))

(define xbyak-src
  (let ((commit "48457bfa0ded67bb4ae2d4c141c36b35469257ee"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/meryhime/xbyak")
            (commit commit)))
      (sha256
       (base32 "1f99x76qdw2x7gf336s8jacchpgm0yrzzv4sk9qbh6cl65wgp6py"))
      (file-name (git-file-name "xbyak" commit)))))

(define (version-with-underscores version)
  (string-map (lambda (x) (if (eq? x #\.) #\_ x)) version))

(define-public boost-for-citra
  (package
    (inherit boost)
    (name "boost")
    (version "1.71.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://boostorg.jfrog.io/artifactory/main/release/"
                                  version "/source/boost_"
                                  (version-with-underscores version) ".tar.bz2"))
              (sha256
               (base32
                "1vi40mcair6xgm9k8rsavyhcia3ia28q8k0blknwgy4b3sh8sfnp"))))))

(define-public catch2-next
  (package/inherit catch2
    (name "catch2-next")
    (version "3.2.1")
    (home-page "https://github.com/catchorg/Catch2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/catchorg/Catch2")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0jn5zk5fxddxhfv710c89rbfpjyc98szrqlhankiw20q94mvg53v"))))
    (arguments
       (list
        #:tests? #f))))

(define-public soundtouch-next
  (package/inherit soundtouch
    (name "soundtouch-next")
    (version "2.3.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/soundtouch/soundtouch.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "10znckb8mrnmvwj7vq12732al873qhqw27fpb5f8r0bkjdpcj3vr"))))
    (build-system cmake-build-system)
    (arguments
       (list
        #:tests? #f))
    ;;(native-inputs (list file))
    ))

(define-public citra
  (let (
        (revision "1819")
        (commit "1chckzi18mn78d2v4a5z6zcdhjswknavgrlpqp72kcpj9q1z7jv5"))
    (package
      (name "citra")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/citra-emu/citra-nightly")
               (commit (string-append "nightly-" revision))))
         (sha256
          (base32
           "136p1hixcgblbk1ky3x98vl3fbassdf92pbm93lkq87rccx86mh2"))
         (file-name (git-file-name name version))
         (modules '((guix build utils)))
         (snippet
          '(begin
             ;;(substitute* "CMakeLists.txt"
             ;; Drop unnecessary includes and targets.
             ;;(("libzstd_static") "zstd")
             ;;(("Boost 1.70.0") "Boost 1.77.0"))
             (substitute* "externals/CMakeLists.txt"
               ;; Drop unnecessary includes and targets.
               ;;(("libzstd_static") "zstd")
               ;;(("$add_subdirectory\\(zstd.*") "")
               ;;(("^.*add_library\\(boost_serialization.*$")
               ;;"add_library(Boost::serialization ALIAS boost_serialization)\n")
               (("add_subdirectory\\(cryptopp.*") "")
               ;;(("add_subdirectory\\(fmt.*") "find_package(fmt REQUIRED)")
               ;;(("add_subdirectory\\(catch2.*") "")
               ;;(("add_subdirectory\\(sdl2.*") "")
               ;;(("add_subdirectory\\(enet.*") "")
               ;;(("add_subdirectory\\(inih.*") "")
               ;;(("add_subdirectory\\(glad.*") "find_package(glad REQUIRED)")
               (("add_subdirectory\\(lodepng.*") ""))            
             ;; Remove as much external stuff as we can
             ;; f.e. some files in boost are still needed
             (for-each (lambda (dir)
                         (delete-file-recursively
                          (string-append "externals/" dir)))
                       ;;missing: boost
                       '("android-ifaddrs"
                         "boost"
                         "catch2"
                         "cubeb"
                         "cryptopp" ;;/cryptopp"
                         "cpp-jwt"
                         "dynarmic"
                         "discord-rpc"
                         "enet"
                         "fmt"
                         "getopt"
                         "httplib"
                         "inih/inih"
                         "libyuv"
                         "libressl"
                         "libusb"
                         "lodepng";/lodepng"
                         "nihstro"
                         "sdl2"
                         "soundtouch"
                         "teakra"
                         "xbyak"
                         "zstd"))
             ;; Clean up source.
             (for-each delete-file
                       (find-files "." ".*\\.(bin|dsy|exe|jar|rar)$"))
             #t))))
      (build-system qt-build-system)
      (arguments
       (list
        #:configure-flags
        #~(list
           "-DUSE_SYSTEM_BOOST=ON"
           "-DUSE_SYSTEM_SDL2=ON"
           "-DCMAKE_BUILD_TYPE=Release"
           "-DCITRA_ENABLE_COMPATIBILITY_REPORTING=OFF"
           "-DCITRA_USE_BUNDLED_FFMPEG=OFF"
           "-DENABLE_COMPATIBILITY_LIST_DOWNLOAD=OFF"
           "-DENABLE_QT_TRANSLATION=ON"
           "-DENABLE_WEB_SERVICE=OFF"
           "-DENABLE_FFMPEG_AUDIO_DECODER=ON"
           "-DUSE_DISCORD_PRESENCE=OFF"
           "-DUSE_SANITIZERS=OFF")
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'add-external-sources
              (lambda* (#:key inputs #:allow-other-keys)
                ;; (copy-recursively #$(package-source crypto++) "externals/cryptopp/cryptopp")
                (copy-recursively ;;#$catch2-src
                                  #$(package-source catch2-next) "externals/catch2")
                (copy-recursively #$(package-source libinih) "externals/inih/inih")
                ;;(copy-recursively #$enet-src "externals/enet")
                (with-directory-excursion "externals"
                  (invoke "tar" "xvf" #$(package-source boost))
                  (copy-recursively "boost_1_77_0" "boost")
                  (invoke "tar" "xvf" #$(package-source enet))
                  (copy-recursively "enet-1.3.17" "enet")
                  (invoke "unzip" #$(package-source fmt))
                  (copy-recursively "fmt-9.1.0" "fmt")
                  (invoke "tar" "xvf" #$(package-source zstd))
                  (copy-recursively "zstd-1.5.0" "zstd"))
                ;;(copy-recursively #$boost-citra-src "externals/boost")
                (copy-recursively ;;#$(package-source soundtouch-next)
                                  #$soundtouch-citra-src
                                  "externals/soundtouch")
                (copy-recursively #$cubeb-src "externals/cubeb")
                (copy-recursively #$dynarmic-src-citra "externals/dynarmic")
                (copy-recursively #$nihstro-src "externals/nihstro")
                (copy-recursively #$teakra-src "externals/teakra")             
                (copy-recursively #$xbyak-src "externals/xbyak")
		)
	      )         
            (add-before 'configure 'delete-submodules-check
              (lambda _
                (substitute* "CMakeLists.txt"
                  (("check_submodules_present\\(\\)")""))))
            (add-after 'qt-wrap 'wrap-gst-plugins
              (lambda _
                (let ((citra (string-append #$output "/bin/citra"))
                      (citra-qt (string-append #$output "/bin/citra-qt")))
                  (map
                   (lambda (prog)
                     (wrap-program prog
                       `("GST_PLUGIN_SYSTEM_PATH" ":" prefix
                         (,(getenv "GST_PLUGIN_SYSTEM_PATH")))))
                   (list citra citra-qt))))))))
      (native-inputs
       (list pkg-config doxygen robin-map unzip catch2-3.1 libunwind))
      (inputs
       (list boost
             crypto++
	     dynarmic
	     enet
	     fmt
             gst-plugins-bad-minimal
             libfdk
             libusb
             lodepng
             libxkbcommon
             openssl
             ffmpeg
             pulseaudio
             qtbase-5
             qtmultimedia-5
             qttools-5
             qtwayland-5
             sdl2
             speexdsp
             (list zstd "lib")
	     xbyak))
      ;;gstreamer-plugins-bad-minimal needed for camera support
      (propagated-inputs (list xdg-utils shared-mime-info))
      (home-page "https://citra-emu.org")
      (synopsis "Nintendo 3DS Emulator")
      (description "Citra is an experimental open-source Nintendo 3DS
 emulator/debugger written in C++. It is written with portability in
 mind, with builds actively maintained for Windows, Linux and macOS.")
      (license license:gpl2+))))

(define-public libdecor
  (package
    (name "libdecor")
    (version "0.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.gnome.org/jadahl/libdecor")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "015x6nf6hndc4m16p42ki956vg936ajh27h0cghkc34jlc5amgpi"))))
    (build-system meson-build-system)
    (arguments (list #:configure-flags #~(list "-Ddemo=false")))
    (native-inputs (list pkg-config egl-wayland ))
    (inputs (list cairo
                  dbus
                  mesa
                  pango
                  wayland
                  wayland-protocols))
    (home-page "https://gitlab.gnome.com/jadahl/libdecor")
    (synopsis "client-side decorations library for Wayland client")
    (description
     "libdecor is a library that can help Wayland clients draw window
decorations for them. It aims to provide multiple backends that implements the
decoration drawing.")
    (license license:expat)))

(define-public xbyak
  (package
    (name "xbyak")
    (version "6.68")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/herumi/xbyak")
             (commit (string-append "v"version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0xl3k3s26am572xs2g2992pfl1r42c8d7b42nyl01wqna5i644cm"))))
    (build-system meson-build-system)
    ;;(arguments (list #:configure-flags #~(list "-Ddemo=false")))
    (native-inputs (list pkg-config cmake))
    ;;(inputs (list robin-map mcl))
    (home-page "https://github.com/herumi/xbyak")
    (synopsis "A JIT assembler for x86")
    (description
     "")
    (license license:isc)))

(define-public merryhime-mcl
  (package
    (name "merryhime-mcl")
    (version "0.1.12")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/merryhime/mcl")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "16nn3b0h1ngmdikhxvrd4bc5mriw4bs3dm7aikp97416crrlnvp0"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f)) 
    (native-inputs (list pkg-config cmake))
    (inputs (list fmt))
    (home-page "https://github.com/merryhime/mcl")
    (synopsis "merryhime's utility's")
    (description
     "")
    (license license:expat)))


(define-public sirit
  (package
    (name "sirit")
    (version "0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ReinUsesLisp/sirit")
             (commit "d7ad93a88864bda94e282e95028f90b5784e4d20")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0ql20blsagjwjn1w8nl5rwlk340yq8rv05d2ay55lyqqqsyn4djq"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Remove as much external stuff as we can
           (for-each (lambda (dir)
                       (delete-file-recursively
                        (string-append "externals/" dir)))
                     '("SPIRV-Headers"))
           #t))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
	   #~(list "-DSIRIT_USE_SYSTEM_SPIRV_HEADERS=ON"
                   "-DSIRIT_TESTS=ON")
           #:imported-modules
           `((guix build copy-build-system)
             ,@%cmake-build-system-modules)
           #:modules
           '(((guix build copy-build-system)
              #:prefix copy:)
             (guix build cmake-build-system)
             (guix build utils))
           #:phases
           #~(modify-phases %standard-phases
               (replace 'install
                 (lambda args
                   (apply
		    (assoc-ref copy:%standard-phases 'install)
                    #:install-plan
                    '(("./src" "lib"
		       #:include-regexp ("\\.a$"))
                      ("../source/include/sirit" "include/sirit"
		       #:include ("sirit.h")))
                    args))))))
    (native-inputs (list pkg-config))
    (inputs (list spirv-headers-next))
    (home-page "https://github.com/merryhime/dynarmic")
    (synopsis "A runtime SPIR-V assembler")
    (description
     "This runtime SPRIR-V assembler aims to do SPIR-V code generation without
 calling external applications.")
    (license license:bsd-3)))

(define-public cubeb
  (let ((version "0.2")
	(revision "0")
	(commit "dc511c6b3597b6384d28949285b9289e009830ea"))		
  (package
    (name "cubeb")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mozilla/cubeb")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "02lhggval2b1xsd3mcvh3p3n8frv6xjbmymgr8j6kaai7sf88id6"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f
	   #:configure-flags
	   #~(list "-DBUILD_SHARED_LIBS=ON"
		   "-DBUNDLE_SPEEX=OFF"
		   "-DUSE_SANITIZERS=OFF")))
    (native-inputs (list googletest pkg-config))
    (inputs (list jack-2 pulseaudio speexdsp))
    (home-page "https://github.com/mozilla/cubeb")
    (synopsis "Cross platform audio library")
    (description
     "")
    (license license:isc))))

(define-public dynarmic
    (let ((commit "e01a1959a39acc478d0a5d8769e47222643e2de0"))
  (package
    (name "dynarmic")
    (version (git-version "6.4.4" "0" commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/merryhime/dynarmic")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "05kqsyd2mkwjb95fzw07lm23ixaj8a61l5mpwd105hycl6n2w8yl"))
       (modules '((guix build utils)))
       (snippet
        '(begin
	    ;; Remove as much external stuff as we can
           (for-each (lambda (dir)
                       (delete-file-recursively
                        (string-append "externals/" dir)))
                     '("zydis"
                       "zycore"
                       "robin-map"
                       "oaknut"
                       "fmt"
                       "catch"
                       "mcl"
                       "xbyak"))
           #t))))
    (build-system cmake-build-system)
    (arguments
     (list #:configure-flags
	   #~(list "-DDYNARMIC_USE_BUNDLED_EXTERNALS=OFF")))
    (native-inputs (list pkg-config catch2-3.1))
    (inputs (list boost
		  fmt
		  merryhime-mcl
		  robin-map
		  zydis
		  xbyak))
    (home-page "https://github.com/merryhime/dynarmic")
    (synopsis "An ARM dynamic recompiler")
    (description
     "")
    (license license:bsd-0))))

(define-public zycore-c
  (package
    (name "zycore-c")
    (version "1.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/zyantific/zycore-c")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0w7ckj6md7bakbnqy0kzqfslvqqz792v8xizqy32jwqyns1596cj"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f))
    (home-page "https://github.com/zyantific/zycore-c")
    (synopsis "An ARM dynamic recompiler")
    (description
     "")
    (license license:expat)))

(define-public zydis
  (package
    (name "zydis")
    (version "4.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/zyantific/zydis")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0hmlgawb38mzmbin4sm5l69ikk7x72m324lkhmqbkr2sagq3yypy"))))
    (build-system cmake-build-system)
    (arguments
     (list #:tests? #f
           #:configure-flags
           #~(list "-DZYAN_SYSTEM_ZYCORE=true"
		         "-DZYDIS_BUILD_SHARED_LIB=ON")))
    (native-inputs (list doxygen pkg-config))
    (propagated-inputs (list zycore-c))
    (home-page "https://github.com/zyantific/zydis")
    (synopsis "x86/x86-64 disassembler and code generation library")
    (description
     "Features

    Supports all x86 and x86-64 (AMD64) instructions and extensions
    Optimized for high performance
    No dynamic memory allocation (\"malloc\")
    Thread-safe by design
    Small file-size overhead
    Complete doxygen documentation
    No third party dependencies — not even libc")
    (license license:isc)))

(define-public libdecor
  (package
    (name "libdecor")
    (version "0.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.gnome.org/jadahl/libdecor")
             (commit version)))
       (sha256
        (base32
         "015x6nf6hndc4m16p42ki956vg936ajh27h0cghkc34jlc5amgpi"))))
    (build-system meson-build-system)
    (arguments (list #:configure-flags #~(list "-Ddemo=false")))
    (native-inputs (list pkg-config egl-wayland ))
    (inputs (list cairo
                  dbus
                  mesa
                  pango
                  wayland
                  wayland-protocols))
    (home-page "https://gitlab.gnome.com/jadahl/libdecor")
    (synopsis "Client-side decorations library for Wayland client")
    (description
     "libdecor is a library that can help Wayland clients draw window
decorations for them. It aims to provide multiple backends that implements the
decoration drawing.")
    (license license:expat)))

;;3....
(define mbedtls-src
  (let ((commit "8c88150ca139e06aa2aae8349df8292a88148ea1"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/yuzu-emu/mbedtls")
            (commit commit)))
      (sha256
       (base32 "0f3kz2min42p0qwdxslip0w24l6j3gm4rfayax7pdldxl754zi8y"))
      (file-name (git-file-name "mbedtls" commit)))))

(define-public vulkan-headers-next
  (let ((commit "33d4dd987fc8fc6475ff9ca2b4f0c3cc6e793337"))  
  (package/inherit vulkan-headers
    (name "vulkan-headers-next")
    (version "1.3.239")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/Vulkan-Headers")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0bkcyjxwn2nszcbgc7158xa5dm8f4gzwkcniyr0jh78rrvm56g4v")))))))

(define vulkan-headers-src
  (let ((commit "33d4dd987fc8fc6475ff9ca2b4f0c3cc6e793337"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/KhronosGroup/Vulkan-Headers")
            (commit commit)))
      (sha256
       (base32 "07vymk406j8hm7xx0zrdgwfbl0fyj4vyzmcbygc8nl72gbfzaxwq"))
      (file-name (git-file-name "Vulkan-Headers" commit)))))

(define-public spirv-headers-next
  (let ((commit "c214f6f2d1a7253bb0e9f195c2dc5b0659dc99ef"))
    (package/inherit spirv-headers
     (name "spirv-headers-next")     
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/SPIRV-headers")
             (commit commit)))
       (sha256
        (base32 "1c4sx4r021jbrhrcq6rv5gc57w0gzjmzr3qjhp1rrscd58x07lgz"))
       (file-name (git-file-name "spirv-headers" commit)))))))

(define sirit-src
  (let ((commit "d7ad93a88864bda94e282e95028f90b5784e4d20"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/ReinUsesLisp/sirit")
            (commit commit)))
      (sha256
       (base32 "0ql20blsagjwjn1w8nl5rwlk340yq8rv05d2ay55lyqqqsyn4djq"))
      (file-name (git-file-name "sirit" commit)))))

(define dynarmic-src
  (let ((commit "07c614f91b0af5335e1f9c0653c2d75e7b5f53bd"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/MerryMage/dynarmic")
            (commit commit)))
      (sha256
       (base32 "0m4nak0jfgyf8d4nbdxaihf6njhi0l2khjzhmkgh2f0ni3nmzqpq"))
      (file-name (git-file-name "dynarmic-yuzu" commit)))))


(define-public vulkan-tools
  (package
    (name "vulkan-tools")
    (version "1.2.162")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/Vulkan-Tools")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "129wzk7xj3vn3c8b4p7fzkd0npl58118s2i1d88gsfnlix54nagq"))))
    (build-system cmake-build-system)
    (inputs
     (list glslang libxrandr mesa vulkan-loader wayland))
    (native-inputs
     (list pkg-config python vulkan-headers))
    (arguments
     (list #:tests? #f                      ;no tests
           #:configure-flags
           #~(list (string-append "-DGLSLANG_INSTALL_DIR="
                                  (assoc-ref %build-inputs "glslang")))
                 #:phases
                 #~(modify-phases
                       %standard-phases
                     (add-after 'install 'wrap-vulkaninfo
                       (lambda _
                         (let ((vulkaninfo (string-append #$output "/bin/vulkaninfo"))
                               (mesa (string-append #$(this-package-input "mesa") "/lib/")))
                           (wrap-program vulkaninfo
                             `("LD_LIBRARY_PATH" ":" prefix (,mesa)))))))))
    (home-page
     "https://github.com/KhronosGroup/Vulkan-Tools")
    (synopsis "Tools and utilities for Vulkan")
    (description
     "Vulkan-Tools provides tools and utilities that can assist development by
enabling developers to verify their applications correct use of the Vulkan
API.")
    (license (list license:asl2.0))))


(define-public yuzu
  (package
    (name "yuzu")
    (version "0-1313")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/yuzu-emu/yuzu-mainline")
             (commit (string-append "mainline-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0pjwp0w64zp8dslzipvnswlkf368xdm64w85jlc8wk0wzi07zd4q"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Remove as much external stuff as we can
           ;; f.e. some files in boost are still needed
           (for-each (lambda (dir)
                       (delete-file-recursively
                        (string-append "externals/" dir)))
                     '("cubeb"
                       "cpp-httplib"
                       "cpp-jwt"
                       "dynarmic"
                       "discord-rpc"
                       "enet"
                       "getopt"
                       "inih/inih"
                       "libressl"
                       "libusb"
                       "mbedtls"
                       "opus"
                       "sirit"
                       "SDL"
                       "Vulkan-Headers"
                       "vcpkg"
                       "xbyak"))
           ;; Clean up source.
           (for-each delete-file
                     (find-files "." ".*\\.(bin|dsy|exe|jar|rar)$"))
           #t))))
    (build-system qt-build-system)
    (arguments
     (list
      #:configure-flags
      #~(list
         "-DYUZU_USE_BUNDLED_QT=OFF"
         "-DYUZU_USE_BUNDLED_FFMPEG=OFF"
	 "-DYUZU_USE_EXTERNAL_VULKAN_HEADERS=OFF"
         "-DCMAKE_BUILD_TYPE=Release"
         "-DENABLE_CUBEB=ON"
          "-DENABLE_QT_TRANSLATION=ON"
         "-DENABLE_QT6=ON"
         "-DYUZU_USE_EXTERNAL_SDL2=OFF"
         "-DYUZU_USE_QT_MULTIMEDIA=ON"
         "-DENABLE_WEB_SERVICE=OFF"
         "-DYUZU_ENABLE_COMPATIBILITY_REPORTING=OFF"
         "-DENABLE_COMPATIBILITY_LIST_DOWNLOAD=OFF"
         "-DYUZU_CHECK_SUBMODULES=OFF"
	 ;;for dynarmic
	 ;;"-DDYNARMIC_USE_BUNDLED_EXTERNALS=OFF"
	 ;;for sirit
	 "-DSIRIT_USE_SYSTEM_SPIRV_HEADERS=ON"
         #$(string-append "-DTITLE_BAR_FORMAT_IDLE='yuzu | " version " | {} '")
         #$(string-append "-DTITLE_BAR_FORMAT_RUNNING='yuzu | " version " | {3} '"))
      #:cmake cmake
      #:phases
      #~(modify-phases
            %standard-phases
          (add-before 'configure 'delete-submodules-check
            (lambda _
              (substitute* "CMakeLists.txt"
                (("check_submodules_present\\(\\)")""))))
          (add-after 'unpack 'add-external-sources
            (lambda* (#:key inputs #:allow-other-keys)
              (copy-recursively #$(package-source dynarmic) "externals/dynarmic")
              (copy-recursively
               #$mbedtls-src "externals/mbedtls")
              (copy-recursively
              #$(package-source dynarmic) "externals/sirit")))
          (add-after 'install 'wrap-vulkan-loader
            (lambda* (#:key inputs #:allow-other-keys)
              (let ((yuzu (string-append #$output "/bin/yuzu"))
                    (yuzu-cmd (string-append #$output "/bin/yuzu-cmd"))
                    (vk (string-append #$(this-package-input "vulkan-loader")
				       "/lib/")))
                (map
                 (lambda (prog)
                   (wrap-program prog
                     `("LD_LIBRARY_PATH" ":" prefix (,vk))))
                 (list yuzu yuzu-cmd))))))))
    (native-inputs
     (list catch2
	   doxygen
	   gcc-12
	   robin-map
	   pkg-config))
    (inputs (list boost
		  cubeb
                  egl-wayland
                  enet
                  dynarmic
		  merryhime-mcl
                  zydis
		  glslang
                  fmt
                  ffmpeg
                  json-modern-cxx
                  libinih
                  libdecor
                  libusb
                  lz4
                  libxkbcommon
                  ;;mbedtls-apache
                  qtbase
                  qtmultimedia
                  qttools
                  qtwayland
                  qtwebengine
                  wayland
                  wayland-protocols                     
                  openssl
                  libva
                  libfdk speexdsp
                  pulseaudio                   
                  opus
                  sdl2
                  shaderc
                  sirit
		  spirv-headers-next
                  vulkan-loader
                  vulkan-headers-next
                  xbyak
                  zlib
                  (list zstd "lib")))
    (propagated-inputs (list desktop-file-utils shared-mime-info))
    (home-page "https://yuzu-emu.org")
    (synopsis "Nintendo Switch Emulator")
    (description "Yuzu is an open-source Nintendo Switch
emulator.")
    ;;  more licenses? see https://github.com/yuzu-emu/yuzu-mainline/commit/bc992b9bdc256f39fe3ddf5d067a17a6dab3877f  
    (license license:gpl3+)))
