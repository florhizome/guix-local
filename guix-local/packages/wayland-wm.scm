(define-module (guix-local packages wayland-wm)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages man)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages web)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages admin)  
  #:use-module (gnu packages freedesktop)
  #:use-module (guix-local packages wayland-updates))


(define-public pywm
  (let ((commit "63f32828a1ac0d7e961727ceb84e68d6ff642d4a")
        (version "0.3alpha")
        (revision "0"))
    (package
      (name "pywm")
      (version "0.3alpha")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/jbuchermn/pywm.git")
           (commit (string-append "v" version))
           (recursive? #t)
           ))
         (sha256
          (base32 "066z5ks39zszqhizvfc7a0rw3s5hkvw93l3y0i4c0ppjwrpn29l8"))))
      (build-system python-build-system)
      (native-inputs
       (list meson ninja pkg-config))
      (inputs (list wlroots wayland-protocols-next
                    xcb-util-renderutil wayland seatd libinput libdrm-next
                    python-pycairo python-imageio python-evdev python-numpy
                    python-matplotlib))
      (arguments
       `(#:phases (modify-phases %standard-phases
                    (add-before 'check 'set-HOME
                      (lambda _ (setenv "HOME" (getcwd)))))))
      (synopsis "Wayland compositor core employing wlroots")
      (description "This is a very tiny compositor built on
 top of wlroots, making all the assumptions that wlroots does not. On
 the Python side pywm exposes Wayland clients (XDG and XWayland) as
 so-called views and passes along all input.")
      (home-page "https://github.com/jbuchermn/pywm")
      (license license:expat))))


(define-public python-pam
  (package
    (name "python-pam")
    (version "1.8.4")
    (source
      (origin
        ;;Tests not distributed via pypi, so we fetch from ghub
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/FirefighterBlu3/python-pam")
               (commit  (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32
          "0gp7vzd332j7jwndcnz7kc9j283d6lyv32bndd1nqv9ghzv69sxp"))))
    (build-system python-build-system)
    (arguments
     '(;;Test suite isn't designed to be run inside a container.
       ;;#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'hardcode-pam.so
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((pam (assoc-ref inputs "linux-pam")))
               (substitute* "pam.py"
                 (("find_library\\(\"pam\")")
                  (string-append "'" pam "/lib/libpam.so'")))
               #t))))))
    (propagated-inputs
     `(("linux-pam" ,linux-pam)))
    (native-inputs
     `(("python-tox" ,python-tox)
       ("python-virtualenv" ,python-virtualenv)))
    (home-page "https://github.com/FirefighterBlu3/python-pam")
    (synopsis "PAM interface using ctypes")
    (description "This package provides a PAM interface using @code{ctypes}.")
    (license license:expat)))

(define-public python-thefuzz
  (package
    (name "python-thefuzz")
    (version "0.19.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "thefuzz" version))
              (sha256
               (base32
                "0gdafjxkbxi82xsj0pfpjz292hjzwi0agqq55chm92ic5zdjcwbg"))))
    (build-system python-build-system)
    (native-inputs (list python-pytest-pycodestyle))
    (propagated-inputs (list python-levenshtein))
    (home-page "https://github.com/seatgeek/thefuzz")
    (synopsis "Fuzzy string matching in python")
    (description "Fuzzy string matching in python")
    (license license:lgpl2.1+)))

(define-public python-dasbus
  (package
    (name "python-dasbus")
    (version "1.6")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "dasbus" version))
              (sha256
               (base32
                "0kg9aqpckwj2dlff4iwym2cli0xjg9a9n5ahmchq6a9xikydi6hl"))))
    (build-system python-build-system)
    (native-inputs (list python-pygobject dbus))
    (home-page "https://github.com/rhinstaller/dasbus")    
    (synopsis "DBus library in Python 3")
    (description "DBus library in Python 3")
    (license license:gpl2+)))

(define-public newm
  (package
    (name "newm")
    (version "0.3alpha")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference (url "https://github.com/jbuchermn/newm.git")
                       (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ii6v3h1wdsiwa523ryy0672m5f9800wzzyl1pdvw2jzzhz5fsp6"))))
    (native-inputs (list pkg-config python-dasbus))
    (inputs
     (list python-psutil python-pam python-pyfiglet
           python-thefuzz        
           python-imageio python-evdev
           python-matplotlib
           python-pycairo))
    (propagated-inputs (list python-wrapper pywm))
    (build-system python-build-system)
    (arguments (list #:tests? #f))
   (synopsis "Tiled Wayland Compositor specialized for laptop usage")
   (description "Newm is a wayland compositor in python based on pywm.
 It uses a tiling mechanism that projects all windows on a virtual 2d
 pane in the background and aligns them dynamically to fit the screen.")
   (home-page "https://github.com/jbuecherm/newm")
   (license license:expat)))
 
(define-public sway-1.7
  (package
    (inherit sway)
    (version "1.7")
    (name "sway-1.7")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/swaywm/sway")
         (commit version)))
       (sha256
        (base32 "0ss3l258blyf2d0lwd7pi7ga1fxfj8pxhag058k7cmjhs3y30y5l"))))
    (native-inputs
     (modify-inputs
      (package-native-inputs sway)
      (replace "wayland-protocols" wayland-protocols-1.24)))
    (inputs
     (modify-inputs
      (package-inputs sway)
      (replace "wayland" wayland-1.20)
      (replace "wlroots" wlroots-0.15)
      (append libdrm-next bash-completion xcb-util-renderutil)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'hardcode-paths
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Hardcode path to swaybg.
             (substitute* "sway/config.c"
               (("strdup..swaybg..")
                (string-append "strdup(\"" (assoc-ref inputs "swaybg")
                               "/bin/swaybg\")")))
             ;; Hardcode path to scdoc.
             (substitute* "meson.build"
               (("scdoc.get_pkgconfig_variable..scdoc..")
                (string-append "'" (assoc-ref inputs "scdoc")
                               "/bin/scdoc'"))) #t))
         (add-before 'patch-source-shebangs 'modify-targets
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (substitute* "meson.build"
                 (("install_dir: bash_install_dir")
                  (string-append "install_dir: '"
                                 out "/share/bash-completion'")))
               #t))))))))

(define-public sway-borders
  (let
      ((commit "9ecbfe366596f627e843886d94e47097e19df5d5")
       (version "1.6")
       (revision "rc2")) ;;see tags in sway main
    (package
      (inherit sway)
      (version (git-version version revision commit))
      (name "sway-borders")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/fluix-dev/sway-borders")
           (commit commit)
           (recursive? #t)))
         (sha256
          (base32 "1wx521hv4rrshv34flridjxwqbx3q9cknrmmvxg27msa90pw8gc8"))))
      (native-inputs
       (modify-inputs
        (package-native-inputs sway)
        (replace "wayland-protocols" wayland-protocols-1.24)))
      (inputs
       (modify-inputs
        (package-inputs sway)
        (replace "wayland" wayland-1.20)
        (replace "wlroots" wlroots-0.15)
        (append libdrm-next bash-completion xcb-util-renderutil)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'hardcode-paths
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Hardcode path to swaybg.
             (substitute* "sway/config.c"
               (("strdup..swaybg..")
                (string-append "strdup(\"" (assoc-ref inputs "swaybg")
                               "/bin/swaybg\")")))
             ;; Hardcode path to scdoc.
             (substitute* "meson.build"
               (("scdoc.get_pkgconfig_variable..scdoc..")
                (string-append "'" (assoc-ref inputs "scdoc")
                               "/bin/scdoc'"))) #t))
         (add-before 'patch-source-shebangs 'modify-targets
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (substitute* "meson.build"
                 (("install_dir: bash_install_dir")
                  (string-append "install_dir: '"
                                 out "/share/bash-completion'"))) #t)))
         (add-after 'configure 'modify-desktop-session-file
           (lambda _
             (substitute* (find-files "sway.desktop")
               (("Name=Sway") "Name=Sway-Borders"))))))))))

(define-public labwc
    (package
      (version "0.5.3")
      (name "labwc")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/labwc/labwc")
           (commit version)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1ih9b42blyz0g2cbdfgvdvxlriwz4k6iikhv54xcpfmv2qdrngb0"))))
      (build-system meson-build-system)
      (native-inputs
       (list pkg-config wayland git-minimal))
      (inputs
       (list cairo libevdev libinput libxkbcommon pango
             glib wayland wayland-protocols-next wlroots-0.15
             libdrm-next libxml2 libseat))
     (synopsis "A Wayland stacking compositor")
     (description "@code{Labwc} is a wlroots-based stacking compositor
aiming to be lightweight and independent, with a focus on simply
stacking windows well and rendering some window decorations. It relies
on clients for wallpaper, panels, screenshots, and so on to create a
full desktop environment.

Labwc tries to stay in keeping with wlroots and sway in terms of
general approach and coding style.

In order to avoid reinventing configuration and theme syntax, the
openbox-3.4 specification is used. This does not mean that labwc
is an openbox clone but rather that configuration files will look and
feel familiar.")
     (home-page "https://github.com/labwc/labwc")
     (license license:expat)))

(define-public libliftoff
  (package
    (version "0.3.0")
    (name "libliftoff")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://gitlab.freedesktop.org/emersion/libliftoff")
         (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0p8z4vsiy5ajfw3q2g0vpryqisinriyr73my84x0dxh0819c7d9i"))))
    (build-system meson-build-system)
    (native-inputs (list pkg-config))
    (inputs (list libdrm-next))
    (home-page "https://github.com/emersion/libliftoff")
    (synopsis "")
    (description "")
    (license license:expat)))


(define-public stb
  (let ((commit "af1a5bc352164740c1cc1354942b1c6b72eacb8a")
        (version "0")
        (revision "0"))
    (package
      (version (git-version version revision commit))
      (name "stb")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference (url "https://github.com/nothings/stb")
                         (commit commit)))
         (sha256
          (base32 "0qq35cd747lll4s7bmnxb3pqvyp2hgcr9kyf758fax9lx76iwjhr"))))
      (build-system copy-build-system)
      (native-inputs
       (list pkg-config))
      (arguments
       `(#:install-plan
         '(("." "/include/stb/" ))))       
      (synopsis "single-file public domain (or MIT licensed) libraries for C/C++")
     (description "")
     (home-page "https://github.com/nothings/stb")
     (license license:cc0))))

(define-public vulkan-headers-next
  (package
    (inherit vulkan-headers)
    (name "vulkan-headers")
    (version "1.3.235")
    (source (origin
              (method git-fetch)
              (uri
               (git-reference
                (url "https://github.com/KhronosGroup/Vulkan-Headers")
                (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0s85q42wscawalz8gv8iflrg2vmr40rpb32yfpp326gm9qdlglhi"))))))

(define-public vulkan-loader-next
  (package
    (inherit vulkan-loader)
    (name "vulkan-loader")
    (version "1.3.235")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/KhronosGroup/Vulkan-Loader")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1x5mp5vpss3d2jichgn848c269ikh3ghpyqrnnwlvah8jm89dh4a"))))
    (arguments
     (list #:tests? #f
           #:configure-flags
           #~(list
              (string-append "-DVULKAN_HEADERS_INSTALL_DIR="
                             #$(this-package-input "vulkan-headers"))
              (string-append "-DCMAKE_INSTALL_INCLUDEDIR="
                             #$(this-package-input "vulkan-headers")
                             "/include"))
     #:phases
     #~(modify-phases %standard-phases
         (add-after 'unpack 'unpack-googletest
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((gtest (assoc-ref inputs "googletest:source")))
               (when gtest
                 (copy-recursively gtest "external/googletest"))
               #t))))))
    (inputs (list vulkan-headers-next))))

(define-public gamescope
  (package
    (version "3.11.43")
    (name "gamescope")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/plagman/gamescope")
         (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0wwspx3w67vjb37z2y537yz7wplaxwrf4dw52yllsy2nphrra4sz"))
       (patches
        (parameterize
            ((%patch-path
              (map
               (lambda (directory)
                 (string-append directory "/guix-local/packages/patches"))
               %load-path)))
          (search-patches "use-pkgconfig.patch")))))
    (build-system meson-build-system)
    (arguments
     (list
      #:configure-flags #~(list ;;"-Dforce_fallback_for=stb"
                                "-Dpipewire=enabled")
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'substitute-stb
            (lambda _
              (copy-recursively #$(package-source stb) "./subprojects/stb")
              (copy-file "./subprojects/packagefiles/stb/meson.build"
                         "subprojects/stb/meson.build"))))))
    (native-inputs
     (list glslang  pkg-config))
    (inputs
     (list cairo
           eudev
           libevdev
           libinput 
           libxkbcommon
           pango
           wayland-protocols-next
           wayland
           wlroots-0.15
           libdrm-next
           libliftoff
           libseat
           libxml2
           libxcomposite
           libxtst
           libxres
           libx11
           glslang
           libcap
           pipewire-0.3
           pixman
           sdl2
           vulkan-headers-next
           vulkan-loader-next
           xcb-util-errors
           xcb-util
           xorg-server-xwayland))
    (synopsis "micro-compositor used for playing games")
    (description "In an embedded session usecase, gamescope does the same thing
 as steamcompmgr, but with less extra copies and latency:

It's getting game frames through Wayland by way of Xwayland, so there's no copy within X itself before it gets the frame.
It can use DRM/KMS to directly flip game frames to the screen, even when stretching or when notifications are up, removing another copy.
When it does need to composite with the GPU, it does so with async Vulkan compute, meaning you get to see your frame quick even if the game already has the GPU busy with the next frame.
It also runs on top of a regular desktop, the 'nested' usecase steamcompmgr didn't support.

Because the game is running in its own personal Xwayland sandbox desktop, it can't interfere with your desktop and your desktop can't interfere with it.
You can spoof a virtual screen with a desired resolution and refresh rate as the only thing the game sees, and control/resize the output as needed. This can be useful in exotic display configurations like ultrawide or multi-monitor setups that involve rotation.")
    (home-page "https://github.com/plagman/gamescope")
    (license license:bsd-2)))

(define-public hyprland
    (package
      (version "0.14.0beta")
      (name "hyprland")
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference (url "https://github.com/hyprwm/Hyprland")
                         (commit (string-append "v" version))
                         (recursive? #t)))
         (sha256
          (base32 "1llig0a60m89cag1yi53b78b02x98mk5vk4765dq7kl3fwrcgdzb"))))
      (build-system meson-build-system)
      (arguments '(#:configure-flags '(;;"-Dstd=c++03"
                                       "-Dxwayland=enabled")))
      (native-inputs
       (list pkg-config gcc-12 git-minimal wayland-next))
      (inputs
       (list cairo
             pango
             libevdev
             libinput
             mesa
             libxkbcommon
             pango
             wayland-protocols-next
             wayland-next
             libdrm-next libseat libxcb xcb-util-renderutil
             xorg-server-xwayland xcb-util-errors
             xcb-util-wm))
     (synopsis "Dynamic tiling Wayland compositor that doesn't sacrifice on its looks")
     (description "Features
@itemize
@item Easily expandable and readable codebase
@item Config reloaded instantly upon saving
@item Custom bezier curve based animations
@item wlr_ext workspaces protocol support
@item Dual Kawase blur
@item Drop shadows
@item Fully dynamic workspaces
@item Closely follows wlroots-git
@item Global keybinds passed to your apps of choice
@item Bundled wlroots
@item Window/layer fade in/out
@item Tiling/pseudotiling/floating/fullscreen windows
@item Switching workspaces between window modes on the fly
@item Special workspace (scratchpad)
@item Window/monitor rules
@item Socket-based IPC
@item Event system for bash scripts
@item Rounded corners
@item Full damage tracking
@item Docks support
@item Drawing tablet support
@item Native IME + Input panels support
@item and much more...
@end itemize")
     (home-page "https://github.com/hyprwm/hyprland")
     (license license:expat)))
