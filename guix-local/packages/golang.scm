(define-module (guix-local packages golang)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system go)
  #:use-module (guix build-system gnu)  
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages image)  
  #:use-module (gnu packages linux)  
  #:use-module (gnu packages syncthing)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages glib))




(define-public go-github-com-hashicorp-go-multierror
  (package
    (name "go-github-com-hashicorp-go-multierror")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hashicorp/go-multierror")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0l4s41skdpifndn9s8y6s9vzgghdzg4z8z0lld9qjr28888wzp00"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hashicorp/go-multierror"))
    (propagated-inputs `(("go-github-com-hashicorp-errwrap" ,go-github-com-hashicorp-errwrap)))
    (home-page "https://github.com/hashicorp/go-multierror")
    (synopsis "go-multierror")
    (description
     "@@code{go-multierror} is a package for Go that provides a mechanism for
representing a list of @@code{error} values as a single @@code{error}.")
    (license license:mpl2.0)))


(define-public go-github-com-hashicorp-errwrap
  (package
    (name "go-github-com-hashicorp-errwrap")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hashicorp/errwrap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0p5wdz8p7dmwphmb33gwhy3iwci5k9wkfqmmfa6ay1lz0cqjwp7a"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hashicorp/errwrap"))
    (home-page "https://github.com/hashicorp/errwrap")
    (synopsis "errwrap")
    (description
     "Package errwrap implements methods to formalize error wrapping in Go.")
    (license license:mpl2.0)))


(define-public go-github-com-gabriel-vasile-mimetype
  (package
    (name "go-github-com-gabriel-vasile-mimetype")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gabriel-vasile/mimetype")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ysb0pjhyr9b7q2y39b5hpwsaidgfl42gd07yj34mgmb9wfjb7ch"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/gabriel-vasile/mimetype"
       ;; tree_test.go:15: open supported_mimes.md: permission denied
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         ;; system-error "open-fdes" "~A" ("Permission denied") (13)
         (delete 'reset-gzip-timestamps))))
    (home-page "https://github.com/gabriel-vasile/mimetype")
    (synopsis "Golang library for media type and file extension")
    (description
"Golang library for media type and file extension detection, based on
magic numbers.")
    (license license:expat)))

(define-public go-github-com-cpuguy83-go-md2man-v2
  (package
    (name "go-github-com-cpuguy83-go-md2man-v2")
    (version "2.0.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/cpuguy83/go-md2man")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "19qri18cinpzxblkid6ngz2vcxslv73s1aid900q0gfzvc71mqqb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/cpuguy83/go-md2man/v2"))
    (propagated-inputs `(("go-github-com-russross-blackfriday-v2" ,go-github-com-russross-blackfriday-v2)))
    (home-page "https://github.com/cpuguy83/go-md2man")
    (synopsis "go-md2man")
    (description "Converts markdown into roff (man pages).")
    (license license:expat)))

(define-public go-github-com-russross-blackfriday-v2
  (package
    (name "go-github-com-russross-blackfriday-v2")
    (version "2.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/russross/blackfriday")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0d1rg1drrfmabilqjjayklsz5d0n3hkf979sr3wsrw92bfbkivs7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/russross/blackfriday/v2"))
    (home-page "https://github.com/russross/blackfriday")
    (synopsis "Blackfriday")
    (description "Package blackfriday is a markdown processor.")
    (license license:bsd-2)))

(define-public go-github-com-shurcool-vfsgen
  (package
    (name "go-github-com-shurcool-vfsgen")
    (version "0.0.0-20200824052919-0d455de96546")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shurcooL/vfsgen")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0md1vgaq95x1jmxpnsfv6s9xf3v8gqi7lcl7mkxpf6274rf1n2pk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shurcooL/vfsgen"
                 #:tests? #f))
    (propagated-inputs (list go-github-com-shurcool-httpfs))
    (home-page "https://github.com/shurcooL/vfsgen")
    (synopsis "vfsgen")
    (description
      "Package vfsgen takes an http.FileSystem (likely at `go generate` time) and
generates Go code that statically implements the provided http.FileSystem.")
    (license license:expat)))

(define-public go-github-com-shurcool-go
  (package
    (name "go-github-com-shurcool-go")
    (version "0.0.0-20200502201357-93f07166e636")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shurcooL/go")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wgwlhsgx1c2v650xvf099hgrd4av18gfb0kha09klmsh0p0hc5r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shurcooL/go"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/shurcooL/go")
    (synopsis "go")
    (description "Common Go code.")
    (license license:expat)))

(define-public go-github-com-shurcool-httpfs
  (package
    (name "go-github-com-shurcool-httpfs")
    (version "0.0.0-20190707220628-8d4bc4ba7749")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shurcooL/httpfs")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qjkbjnp86kjr7r0xjwp39blnk1ggkzy6zm3xphr5dpin4jkgfa1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shurcooL/httpfs"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/shurcooL/httpfs")
    (synopsis "httpfs")
    (description
      "Collection of Go packages for working with the
@url{https://godoc.org/net/http#FileSystem,(code http.FileSystem)} interface.")
    (license license:expat)))

(define-public go-github-com-neelance-astrewrite
  (package
    (name "go-github-com-neelance-astrewrite")
    (version "0.0.0-20160511093645-99348263ae86")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/neelance/astrewrite")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07527807p8q6h05iq4qy0xrlcmwyzj76gpk0yqf71yaj447mz24v"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/neelance/astrewrite"))
    (home-page "https://github.com/neelance/astrewrite")
    (synopsis #f)
    (description #f)
    (license license:bsd-2)))

(define-public go-github-com-neelance-sourcemap
  (package
    (name "go-github-com-neelance-sourcemap")
    (version "0.0.0-20200213170602-2833bce08e4c")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/neelance/sourcemap")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "05ymjg1z9phf0wp4w058kvf13bmn4skv67chb3r04z69in8y8jih"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/neelance/sourcemap"))
    (home-page "https://github.com/neelance/sourcemap")
    (synopsis #f)
    (description #f)
    (license license:bsd-2)))



(define-public go-github-com-gopherjs-gopherjs
  (package
    (name "go-github-com-gopherjs-gopherjs")
    (version "1.17.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gopherjs/gopherjs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1dg22b3pw0g3g0a84fmzl0fl76540yjvlcvscw2876gdy5db98n3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/gopherjs/gopherjs/js"
                 #:unpack-path "github.com/gopherjs/gopherjs"))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-term" ,go-golang-org-x-term)
        ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
        ("go-github-com-shurcool-vfsgen" ,go-github-com-shurcool-vfsgen)
        ("go-github-com-inconshreveable-mousetrap"
         ,go-github-com-inconshreveable-mousetrap)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-spf13-pflag" ,go-github-com-spf13-pflag)
        ("go-github-com-spf13-cobra" ,go-github-com-spf13-cobra)
        ("go-github-com-shurcool-httpfs" ,go-github-com-shurcool-httpfs)
        ("go-github-com-shurcool-go" ,go-github-com-shurcool-go)
        ("go-github-com-neelance-sourcemap" ,go-github-com-neelance-sourcemap)
        ("go-github-com-neelance-astrewrite"
         ,go-github-com-neelance-astrewrite)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)))
    (home-page "https://github.com/gopherjs/gopherjs")
    (synopsis "GopherJS - A compiler from Go to JavaScript")
    (description
      "GopherJS compiles Go code (@url{https://golang.org/,golang.org}) to pure
JavaScript code.  Its main purpose is to give you the opportunity to write
front-end code in Go which will still run in all browsers.")
    (license license:bsd-2)))



(define-public go-golang-org-x-tools
  (package
    (name "go-golang-org-x-tools")
    (version "0.1.10")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/tools")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1aayyzraia15vmq3df3vnqsrihpzdkwrgk7byasjasv1ypzpxgdg"))))
      (build-system go-build-system)
      (arguments
       `(#:import-path "golang.org/x/tools"
         ;; Source-only package
         #:tests? #f
         #:phases
         (modify-phases %standard-phases
           (delete 'build))))
      (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)
        ("go-github-com-yuin-goldmark" ,go-github-com-yuin-goldmark)))
      (synopsis "Tools that support the Go programming language")
      (description "This package provides miscellaneous tools that support the
Go programming language.")
      (home-page "https://go.googlesource.com/tools/")
      (license license:bsd-3)))


(define-public go-github-com-jcmturner-goidentity-v6
  (package
    (name "go-github-com-jcmturner-goidentity-v6")
    (version "6.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/goidentity")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "064ysvxvrvij843s7qj1nkzl5qc6j1qbrsb3s0zmwd1sa7vq8q1n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jcmturner/goidentity/v6"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-hashicorp-go-uuid" ,go-github-com-hashicorp-go-uuid)))
    (home-page "https://github.com/jcmturner/goidentity")
    (synopsis "goidentity")
    (description "Please import as below")
    (license license:asl2.0)))

(define-public go-github-com-jcmturner-rpc-v2
  (package
    (name "go-github-com-jcmturner-rpc-v2")
    (version "2.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/rpc")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nm4j2nwcszghldw39rwdx2hr56i1lybfpv33y4gd67w6qcqbpsi"))))
    (build-system go-build-system)
    (arguments '(#:unpack-path "github.com/jcmturner/rpc/v2"
                 #:import-path "github.com/jcmturner/rpc/v2/ndr"))
    (propagated-inputs
      `(("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jcmturner/rpc")
    (synopsis "RPC")
    (description
      "This project relates to @url{http://pubs.opengroup.org/onlinepubs/9629399/,CDE
1.1: Remote Procedure Call}")
    (license license:asl2.0)))

(define-public go-github-com-jcmturner-dnsutils-v2
  (package
    (name "go-github-com-jcmturner-dnsutils-v2")
    (version "2.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/dnsutils")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "116zbgvfj88vv93fnapmmgyd5g8kzy774cdyzsnnzyzng92j61c9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jcmturner/dnsutils/v2"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jcmturner/dnsutils")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-github-com-jcmturner-aescts-v2
  (package
    (name "go-github-com-jcmturner-aescts-v2")
    (version "2.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/aescts")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0yrdiisdhcqfs8jpicc30dfmbqzxhkmbayn902xrgwkndky8w7l1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jcmturner/aescts/v2"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jcmturner/aescts")
    (synopsis #f)
    (description
      "Package aescts provides AES CBC CipherText Stealing encryption and decryption
methods")
    (license license:asl2.0)))

(define-public go-github-com-jcmturner-gofork
  (package
    (name "go-github-com-jcmturner-gofork")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/gofork")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0xzsnjqv3d59w9pgqzf6550wdwaqnac7zcdgqfd25w65yhcffzhr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jcmturner/gofork"))
    (home-page "https://github.com/jcmturner/gofork")
    (synopsis "GoFork")
    (description
      "This repository contains modified Go standard library packages for use as work
arounds until issues are addressed in the official distribution.")
    (license license:bsd-3)))

(define-public go-github-com-beorn7-perks
  (package
    (name "go-github-com-beorn7-perks")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/beorn7/perks")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "17n4yygjxa6p499dj3yaqzfww2g7528165cl13haj97hlx94dgl7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/beorn7/perks/quantile"
                 #:unpack-path "github.com/beorn7/perks"))
    (home-page "https://github.com/beorn7/perks")
    (synopsis "Perks for Go (golang.org)")
    (description
      "Perks contains the Go package quantile that computes approximate quantiles over
an unbounded data stream within low memory and CPU bounds.")
    (license license:expat)))

(define-public go-github-com-pengsrc-go-shared
  (package
    (name "go-github-com-pengsrc-go-shared")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pengsrc/go-shared")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1d0nfnfilvxlxdzw18k744py5b9p852qisz37ikfyxyp24jbi6fr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pengsrc/go-shared"
                #:tests? #f               
                #:phases
                 (modify-phases %standard-phases
                   (delete 'build)
         )))
    (home-page "https://github.com/pengsrc/go-shared")
    (synopsis "go-shared")
    (description "Useful packages for the Go programming language.")
    (license license:asl2.0)))

(define-public go-github-com-yunify-qingstor-sdk-go-v3
  (package
    (name "go-github-com-yunify-qingstor-sdk-go-v3")
    (version "3.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/qingstor/qingstor-sdk-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1m7lnvkvn66r50gjhlfymhw3cslqa9q815y6vwad4gdwnj7bhfwl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/yunify/qingstor-sdk-go/v3"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-pengsrc-go-shared" ,go-github-com-pengsrc-go-shared)))
    (home-page "https://github.com/yunify/qingstor-sdk-go")
    (synopsis "QingStor SDK for Go")
    (description
      "Package sdk is the official QingStor SDK for the Go programming language.")
    (license license:asl2.0)))

(define-public go-github-com-koofr-go-koofrclient
  (package
    (name "go-github-com-koofr-go-koofrclient")
    (version "0.0.0-20190724113126-8e5366da203a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/koofr/go-koofrclient")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "02vilwn1dz2y3bxqb3114d3k898wpvb71igfpb1zwa8mrkyhdpci"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/koofr/go-koofrclient"
                 ;;for tests, onsi-gingko package is needed                
                 #:tests? #f))
    (propagated-inputs (list go-github-com-koofr-go-httpclient))
    (home-page "https://github.com/koofr/go-koofrclient")
    (synopsis "go-koofrclient")
    (description "Go Koofr client.")
    (license license:expat)))

(define-public go-github-com-youmark-pkcs8
  (package
    (name "go-github-com-youmark-pkcs8")
    (version "0.0.0-20201027041543-1326539a0a0a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/youmark/pkcs8")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1bk20x279iiafxh39v75hrmxncbkmw17603g8xw5b59cqzzpnrmv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/youmark/pkcs8"))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)))
    (home-page "https://github.com/youmark/pkcs8")
    (synopsis "pkcs8")
    (description
      "Package pkcs8 implements functions to parse and convert private keys in PKCS#8
format, as defined in RFC5208 and RFC5958")
    (license license:expat)))

(define-public go-github-com-t3rm1n4l-go-mega
  (package
    (name "go-github-com-t3rm1n4l-go-mega")
    (version "0.0.0-20200416171014-ffad7fcb44b8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/t3rm1n4l/go-mega")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1yd7py1kyb7awzaixkjp56z0dmcl92j39f3ab5b1iff76x82qi1b"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/t3rm1n4l/go-mega"))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)))
    (home-page "https://github.com/t3rm1n4l/go-mega")
    (synopsis "go-mega")
    (description
      "This package provides a client library in go for mega.co.nz storage service.")
    (license license:expat)))

(define-public go-github-com-rfjakob-eme
  (package
    (name "go-github-com-rfjakob-eme")
    (version "1.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rfjakob/eme")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1yrbhvy0337mf12fp8p4sy8ry8r3w2qfdf8val5hj07p2lri0cqk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rfjakob/eme"))
    (home-page "https://github.com/rfjakob/eme")
    (synopsis "EME for Go")
    (description
      "EME (ECB-Mix-ECB or, clearer, Encrypt-Mix-Encrypt) is a wide-block encryption
mode developed by Halevi and Rogaway.")
    (license license:expat)))

(define-public go-github-com-pkg-sftp
  (package
    (name "go-github-com-pkg-sftp")
    (version "1.13.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pkg/sftp")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "18vp6anhczlkw247wy427gkri47y1h4nqbrbvqsdb170h878d6c6"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pkg/sftp"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-kr-fs" ,go-github-com-kr-fs)))
    (home-page "https://github.com/pkg/sftp")
    (synopsis "sftp")
    (description
      "Package sftp implements the SSH File Transfer Protocol as described in
@url{https://tools.ietf.org/html/draft-ietf-secsh-filexfer-02,https://tools.ietf.org/html/draft-ietf-secsh-filexfer-02}")
    (license license:bsd-2)))

(define-public go-github-com-patrickmn-go-cache
  (package
    (name "go-github-com-patrickmn-go-cache")
    (version "2.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/patrickmn/go-cache")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10020inkzrm931r4bixf8wqr9n39wcrb78vfyxmbvjavvw4zybgs"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/patrickmn/go-cache"))
    (home-page "https://github.com/patrickmn/go-cache")
    (synopsis "go-cache")
    (description
      "go-cache is an in-memory key:value store/cache similar to memcached that is
suitable for applications running on a single machine.  Its major advantage is
that, being essentially a thread-safe @code{map[string]interface{}} with
expiration times, it doesn't need to serialize or transmit its contents over the
network.")
    (license license:expat)))

(define-public go-github-com-ncw-go-acd
  (package
    (name "go-github-com-ncw-go-acd")
    (version "0.0.0-20201019170801-fe55f33415b1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ncw/go-acd")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jibj2xzzcwpjvaw06nvn89q23pkv2g5wd5ypxjc3kkwhgma74bj"))))
    (build-system go-build-system)
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs (list go-github-com-google-go-querystring))
    (arguments '(#:import-path "github.com/ncw/go-acd"))
    (home-page "https://github.com/ncw/go-acd")
    (synopsis "go-acd")
    (description "Go library for accessing the Amazon Cloud Drive.")
    (license license:isc)))

(define-public go-github-com-ncw-swift-v2
  (package
    (name "go-github-com-ncw-swift-v2")
    (version "2.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ncw/swift")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nsk23gb02q45wmab7x5p9jcq4hy2pzzlbd3wvj7jb45dv78sf5k"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ncw/swift/v2"))
    (home-page "https://github.com/ncw/swift")
    (synopsis "Swift")
    (description
      "Package swift provides an easy to use interface to Swift / Openstack Object
Storage / Rackspace Cloud Files")
    (license license:expat)))

(define-public go-github-com-nsf-termbox-go
  (package
    (name "go-github-com-nsf-termbox-go")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nsf/termbox-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n5jwnx53nkjvq8rcqzv2scs532iq9w06pd83w6cipniccqp4m2i"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nsf/termbox-go"))
    (propagated-inputs
      `(("go-github-com-mattn-go-runewidth"
         ,go-github-com-mattn-go-runewidth)))
    (home-page "https://github.com/nsf/termbox-go")
    (synopsis "IMPORTANT")
    (description
      "termbox is a library for creating cross-platform text-based interfaces")
    (license license:expat)))

(define-public go-github-com-dvyukov-go-fuzz
  (package
    (name "go-github-com-dvyukov-go-fuzz")
    (version "0.0.0-20220220162807-a217d9bdbece")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dvyukov/go-fuzz")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1100qg1s4s3503wpx81959w5j6mlkx2jwiwaibmn6c30x69rjya7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dvyukov/go-fuzz/go-fuzz"
                 #:unpack-path "github.com/dvyukov/go-fuzz"))
    (propagated-inputs (list go-golang-org-x-tools
                             go-golang-org-x-sys))
    (home-page "https://github.com/dvyukov/go-fuzz")
    (synopsis "go-fuzz: randomized testing for Go")
    (description
      "Go-fuzz is a coverage-guided
@url{http://en.wikipedia.org/wiki/Fuzz_testing,fuzzing solution} for testing of
Go packages.  Fuzzing is mainly applicable to packages that parse complex inputs
(both text and binary), and is especially useful for hardening of systems that
parse inputs from potentially malicious users (e.g.  anything accepted over a
network).")
    (license license:asl2.0)))

(define-public go-github-com-elazarl-go-bindata-assetfs
  (package
    (name "go-github-com-elazarl-go-bindata-assetfs")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/elazarl/go-bindata-assetfs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "05j8gy417gcildmxa04m8ylriaakadr7zvwn2ggq56pdg7b63knc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/elazarl/go-bindata-assetfs"))
    (home-page "https://github.com/elazarl/go-bindata-assetfs")
    (synopsis "go-bindata-assetfs")
    (description
      "assetfs allows packages to serve static content embedded with the go-bindata
tool with the standard net/http package.")
    (license license:bsd-2)))

(define-public go-github-com-julusian-godocdown
  (package
    (name "go-github-com-julusian-godocdown")
    (version "0.0.0-20170816220326-6d19f8ff2df8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Julusian/godocdown")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qb8l77jlnrpx316s6chgygv62vgsalmf3rsrgxrissmngpg71an"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/robertkrimen/godocdown/godocdown"
                               #:unpack-path "github.com/robertkrimen/godocdown"
                               ;;unexpected directory layout
                 #:tests? #f))
    (home-page "https://github.com/Julusian/godocdown")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-robertkrimen-godocdown
  (package
    (name "go-github-com-robertkrimen-godocdown")
    (version "0.0.0-20130622164427-0bfa04905481")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/robertkrimen/godocdown")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jgxv2y2anca4xp47lv4lv5n5dcfnfg78bn36vaqvg4ks2gsw0g6"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "github.com/robertkrimen/godocdown"
       #:import-path "github.com/robertkrimen/godocdown/godocdown"
       #:tests? #f))
    (home-page "https://github.com/robertkrimen/godocdown")
    (synopsis #f)
    (description #f)
    (license #f)))



(define-public go-github-com-stephens2424-writerset
  (package
    (name "go-github-com-stephens2424-writerset")
    (version "1.0.2")
    (source
      (origin  
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stephens2424/writerset")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0609dxb6h9lncv09b7sr04kwwhgjv9kb476rzdww406mb7g0ffkh"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stephens2424/writerset"
                 #:tests? #f))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ;;("go-github-com-robertkrimen-godocdown" ,go-github-com-robertkrimen-godocdown)
        ("go-github-com-julusian-godocdown"
         ,go-github-com-julusian-godocdown)))
    (home-page "https://github.com/stephens2424/writerset")
    (synopsis "writerset")
    (description
      "Package writerset implements a mechanism to add and remove writers from a
construct similar to io.MultiWriter.")
    (license license:bsd-3)))

(define-public go-github-com-billziss-gh-cgofuse
  (package
    (name "go-github-com-billziss-gh-cgofuse")
    (version "1.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/winfsp/cgofuse")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wqxn3plbp6rabmyfyxamjl0ilm0n5pkiwhsvnbrx9pcpmlc1b0p"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/billziss-gh/cgofuse/fuse"
                               #:unpack-path "github.com/billziss-gh/cgofuse"
                               #:tests? #f))
      (propagated-inputs (list fuse))
    (home-page "https://github.com/billziss-gh/cgofuse")
    (synopsis "How to build")
    (description
      "Cgofuse is a cross-platform FUSE library for Go.  It is supported on multiple
platforms and can be ported to any platform that has a FUSE implementation.  It
has @url{https://golang.org/cmd/cgo/,cgo} and
@url{https://github.com/golang/go/wiki/WindowsDLLs,!cgo} (\"nocgo\") variants
depending on the platform.")
    (license license:expat)))

(define-public go-github-com-dop251-scsu
  (package
    (name "go-github-com-dop251-scsu")
    (version "0.0.0-20220106150536-84ac88021d00")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dop251/scsu")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vm7yfbdaz5z1m7yih5r2awahdr1lyrdfjr8qvx3z0gqi5q3klnh"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dop251/scsu"))
    (home-page "https://github.com/dop251/scsu")
    (synopsis "SCSU")
    (description
      "This package provides a Standard Compression Scheme for Unicode implementation
in Go.")
    (license license:expat)))

(define-public go-gopkg-in-jcmturner-rpc-v1-ndr
  (package
    (name "go-gopkg-in-jcmturner-rpc-v1-ndr")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/rpc.v1")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0hkmvf8qdcifnzym8kv1xhq7lq0wpr0i6gzff159lh9xn0wfg175"))))
    (native-inputs (list go-github-com-stretchr-testify))
    
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/rpc.v1/ndr"
        #:unpack-path
        "gopkg.in/jcmturner/rpc.v1"))
    (home-page "https://gopkg.in/jcmturner/rpc.v1")
    (synopsis "RPC")
    (description
      "This project relates to @url{http://pubs.opengroup.org/onlinepubs/9629399/,CDE
1.1: Remote Procedure Call}")
    (license license:asl2.0)))

(define-public go-gopkg-in-jcmturner-rpc-v1-mstypes
  (package
    (name "go-gopkg-in-jcmturner-rpc-v1-mstypes")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/rpc.v1")
               (commit (string-append "v" version))))
        
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hkmvf8qdcifnzym8kv1xhq7lq0wpr0i6gzff159lh9xn0wfg175"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/rpc.v1/mstypes"
        #:unpack-path
        "gopkg.in/jcmturner/rpc.v1"))
    (native-inputs (list go-github-com-stretchr-testify))
    (home-page "https://gopkg.in/jcmturner/rpc.v1")
    (synopsis "RPC")
    (description
      "This project relates to @url{http://pubs.opengroup.org/onlinepubs/9629399/,CDE
1.1: Remote Procedure Call}")
    (license license:asl2.0)))


(define-public go-gopkg-in-jcmturner-goidentity-v3
  (package
    (name "go-gopkg-in-jcmturner-goidentity-v3")
    (version "3.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/goidentity.v3")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ymvs25i3j8nv3pml86bkxa73syki6k73bhhdcs15chr7bfgx0im"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/goidentity.v3"
        #:unpack-path
        "gopkg.in/jcmturner/goidentity.v3"))
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs (list go-github-com-hashicorp-go-uuid))
    
    (home-page "https://gopkg.in/jcmturner/goidentity.v3")
    (synopsis "goidentity")
    (description
      "Standard interface to holding authenticated identities and their attributes.")
    (license license:asl2.0)))


(define-public go-gopkg-in-jcmturner-aescts-v1
  (package
    (name "go-gopkg-in-jcmturner-aescts-v1")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/aescts.v1")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0rbq4zf3db48xa2gqdp2swws7wizmbwagigqkr1zxzd1ramps6rv"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/aescts.v1"
        #:unpack-path
        "gopkg.in/jcmturner/aescts.v1"))
    (native-inputs (list go-github-com-stretchr-testify))
    
    (home-page "https://gopkg.in/jcmturner/aescts.v1")
    (synopsis "AES CBC Ciphertext Stealing")
    (description
      "Package aescts provides AES CBC CipherText Stealing encryption and decryption
methods")
    (license license:asl2.0)))

(define-public go-github-com-colinmarc-hdfs-v2
  (package
    (name "go-github-com-colinmarc-hdfs-v2")
    (version "2.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/colinmarc/hdfs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "08dnvbfcq06qq6rw6h5kkxmsj06ndsxk6qklm7qnfy54wf2h7332"))))

    (build-system go-build-system)
    (arguments `(#:import-path "github.com/colinmarc/hdfs/v2"
                 ;;#:unpack-path "github.com/colinmarc/hdfs/v2"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build)
                   (add-before 'build 'setenv
                     (lambda _
                       (setenv "LDFLAGS" "-X main.version=2.3.0")
                       )))))
                 
    (native-inputs (list go git go-github-com-stretchr-testify))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-jcmturner-rpc-v2" ,go-github-com-jcmturner-rpc-v2)
        ;;("go-github-com-jcmturner-goidentity-v6"
        ;; ,go-github-com-jcmturner-goidentity-v6)
        ("go-github-com-jcmturner-gofork" ,go-github-com-jcmturner-gofork)
        ("go-github-com-jcmturner-dnsutils-v2"
         ,go-github-com-jcmturner-dnsutils-v2)
        ("go-github-com-jcmturner-gokrb5-v8"
         ,go-github-com-jcmturner-gokrb5-v8)
        
        ("go-gopkg-in-jcmturner-dnsutils-v1" ,go-gopkg-in-jcmturner-dnsutils-v1)
        ("go-gopkg-in-jcmturner-aescts-v1"
         ,go-gopkg-in-jcmturner-aescts-v1)
        ("go-github-com-hashicorp-go-uuid" ,go-github-com-hashicorp-go-uuid)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-github-com-pborman-getopt" ,go-github-com-pborman-getopt)
        
        ;;("go-gopkg-in-jcmturner-gokrb5-v7" ,go-gopkg-in-jcmturner-gokrb5-v7)
        
        ))
    (home-page "https://github.com/colinmarc/hdfs")
    (synopsis "HDFS for Go")
    (description
      "Package hdfs provides a native, idiomatic interface to HDFS.  Where possible, it
mimics the functionality and signatures of the standard `os` package.")
    (license license:expat)))

(define-public go-github-com-atotto-clipboard
  (package
    (name "go-github-com-atotto-clipboard")
    (version "0.1.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/atotto/clipboard")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ycd8zkgsq9iil9svhlwvhcqwcd7vik73nf8rnyfnn10gpjx97k5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/atotto/clipboard"
                               #:tests? #f))
    (home-page "https://github.com/atotto/clipboard")
    (synopsis "Clipboard for Go")
    (description "Package clipboard read/write on clipboard")
    (license license:bsd-3)))

(define-public go-github-com-abbot-go-http-auth
  (package
    (name "go-github-com-abbot-go-http-auth")
    (version "0.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/abbot/go-http-auth")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0phsnkfq1vy9b7nqsqqf0llvm7kad0nkmcrnlbwarci3p5p083qf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/abbot/go-http-auth"
                 #:tests? #f))
    (propagated-inputs (list go-golang-org-x-crypto go-golang-org-x-net))
    (home-page "https://github.com/abbot/go-http-auth")
    (synopsis "HTTP Authentication implementation in Go")
    (description
      "Package auth is an implementation of HTTP Basic and HTTP Digest authentication.")
    (license license:asl2.0)))

(define-public go-github-com-aalpar-deheap
  (package
    (name "go-github-com-aalpar-deheap")
    (version "0.0.0-20210914013432-0cc84d79dec3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aalpar/deheap")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1709nykin03n1adhh1kqx9j3w03awvd0p7bsim7p1xaqgdzqisq6"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aalpar/deheap"))
    (home-page "https://github.com/aalpar/deheap")
    (synopsis "deheap")
    (description
      "Package deheap provides the implementation of a doubly ended heap.  Doubly ended
heaps are heaps with two sides, a min side and a max side.  Like normal
single-sided heaps, elements can be pushed onto and pulled off of a deheap.
deheaps have an additional Pop function, PopMax, that returns elements from the
opposite side of the ordering.")
    (license license:expat)))

(define-public go-github-com-a8m-tree
  (package
    (name "go-github-com-a8m-tree")
    (version "0.0.0-20210414114729-ce3525c5c2ef")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/a8m/tree")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1skifwfxgmz0sjcakgbxvbadir30y6as3bmif2n8s306mjjwrx7d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/a8m/tree"))
    (home-page "https://github.com/a8m/tree")
    (synopsis "tree")
    (description
      "You can take a look on
@url{https://github.com/a8m/tree/raw/master/cmd/tree/tree.go,(code cmd/tree)},
and @url{http://github.com/a8m/s3tree,s3tree} or see the example below.")
    (license license:expat)))

(define-public go-github-com-artyom-mtab
  (package
    (name "go-github-com-artyom-mtab")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/artyom/mtab")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0zrrccxj27i3lg3cmlbc0bxlc1pp0861j1jvfbsa7h438gxxp970"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/artyom/mtab"))
    (home-page "https://github.com/artyom/mtab")
    (synopsis #f)
    (description
      "Package mtab parses /proc/self/mounts entries on a Linux system.")
    (license license:expat)))

(define-public go-github-com-matttproud-golang-protobuf-extensions
  (package
    (name "go-github-com-matttproud-golang-protobuf-extensions")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/matttproud/golang_protobuf_extensions")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1d0c1isd2lk9pnfq2nk0aih356j30k3h1gi2w0ixsivi5csl7jya"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "github.com/matttproud/golang_protobuf_extensions"))
    (home-page "https://github.com/matttproud/golang_protobuf_extensions")
    (synopsis "Overview")
    (description
      "This repository provides various Protocol Buffer extensions for the Go language
(golang), namely support for record length-delimited message streaming.")
    (license license:asl2.0)))

(define-public go-github-com-klauspost-compress-next
  (package
    (inherit go-github-com-klauspost-compress)
    (name "go-github-com-klauspost-compress")
    (version "1.15.4")
    (source
     (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/klauspost/compress")
           (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
         (base32 "0n8l4656y0l1j7gz1bhmbrw0yka179rv9f8jzm8hlfgijrlk70j2"))))))

(define-public go-github-com-buengese-sgzip
  (package
    (name "go-github-com-buengese-sgzip")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/buengese/sgzip")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01gh3d9nnb5aljjyk3svhdbihhz9x448qh6xkl2fps8w1h2knw58"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/buengese/sgzip"
                 ))
    (propagated-inputs
     `(("go-github-com-klauspost-compress" ,go-github-com-klauspost-compress)))
    (home-page "https://github.com/buengese/sgzip")
    (synopsis #f)
    (description
      "Package sgzip implements a seekable version of gzip format compressed files,
compliant with @url{https://rfc-editor.org/rfc/rfc1952.html,RFC 1952}.")
    (license license:expat)))

(define-public go-github-com-max-sum-base32768
  (package
    (name "go-github-com-max-sum-base32768")
    (version "0.0.0-20191205131208-7937843c71d5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Max-Sum/base32768")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01k833mj7psvzbqaii3mcarb15iswgmxb534vp0qd647zxbfjsbx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Max-Sum/base32768"))
    (home-page "https://github.com/Max-Sum/base32768")
    (synopsis "base32768")
    (description "go implementation of base32768, optimized for UTF-16.")
    (license license:expat)))

(define-public go-github-com-anacrolix-dms
  (package
    (name "go-github-com-anacrolix-dms")
    (version "1.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/dms")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n2yfyypl4czmhhhjv5zkd50jrvlvw4cfc54galdyj1wvx7z9wxd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/dms"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-nfnt-resize" ,go-github-com-nfnt-resize)
        ("go-github-com-anacrolix-log" ,go-github-com-anacrolix-log)
        ("go-github-com-anacrolix-ffprobe" ,go-github-com-anacrolix-ffprobe)))
    (home-page "https://github.com/anacrolix/dms")
    (synopsis #f)
    (description #f)
    (license license:bsd-3)))

(define-public go-github-com-hashicorp-go-uuid
  (package
    (name "go-github-com-hashicorp-go-uuid")
    (version "1.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hashicorp/go-uuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wd4maaq20alxwcvfhr52rzfnwwpmc2a698ihyr0vfns2sl7gkzk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/hashicorp/go-uuid"))
    (home-page "https://github.com/hashicorp/go-uuid")
    (synopsis "uuid")
    (description
      "Generates UUID-format strings using high quality,  bytes.  It is @strong{not}
intended to be RFC compliant, merely to use a well-understood string
representation of a 128-bit value.  It can also parse UUID-format strings into
their component bytes.")
    (license license:mpl2.0)))

(define-public go-github-com-anacrolix-missinggo-v2
  (package
    (name "go-github-com-anacrolix-missinggo-v2")
    (version "2.7.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/missinggo")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1d5na25rw4pgd0qjqq4rgi72s0pvbzyz63p51rfykpwhmzddwl2n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/missinggo/v2"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-willf-bitset" ,go-github-com-willf-bitset)
        ("go-github-com-tinylib-msgp" ,go-github-com-tinylib-msgp)
        ("go-github-com-prometheus-procfs" ,go-github-com-prometheus-procfs)
        ("go-github-com-prometheus-common" ,go-github-com-prometheus-common)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
        ("go-github-com-philhofer-fwd" ,go-github-com-philhofer-fwd)
        ;;("go-github-com-mschoch-smat" ,go-github-com-mschoch-smat)
        ("go-github-com-matttproud-golang-protobuf-extensions"
         ,go-github-com-matttproud-golang-protobuf-extensions)
        ("go-github-com-golang-snappy" ,go-github-com-golang-snappy)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ("go-github-com-golang-groupcache" ,go-github-com-golang-groupcache)
        ;;("go-github-com-glycerine-go-unsnap-stream" ,go-github-com-glycerine-go-unsnap-stream)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-cespare-xxhash-v2" ,go-github-com-cespare-xxhash-v2)
        ("go-github-com-beorn7-perks" ,go-github-com-beorn7-perks)
        ("go-github-com-benbjohnson-immutable"
         ,go-github-com-benbjohnson-immutable)
        ("go-go-opencensus-io" ,go-go-opencensus-io)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-ryszard-goskiplist" ,go-github-com-ryszard-goskiplist)
        ("go-github-com-prometheus-client-model"
         ,go-github-com-prometheus-client-model)
        ("go-github-com-prometheus-client-golang"
         ,go-github-com-prometheus-client-golang)
        ("go-github-com-huandu-xstrings" ,go-github-com-huandu-xstrings)
        ("go-github-com-google-btree" ,go-github-com-google-btree)
        ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
        ;;("go-github-com-docopt-docopt-go" ,go-github-com-docopt-docopt-go)
        ;;("go-github-com-bradfitz-iter" ,go-github-com-bradfitz-iter)
        ;;("go-github-com-anacrolix-tagflag" ,go-github-com-anacrolix-tagflag)
        ("go-github-com-anacrolix-stm" ,go-github-com-anacrolix-stm)
        ;;("go-github-com-anacrolix-missinggo" ,go-github-com-anacrolix-missinggo)
        ("go-github-com-anacrolix-log" ,go-github-com-anacrolix-log)
        ("go-github-com-anacrolix-envpprof" ,go-github-com-anacrolix-envpprof)
        ("go-github-com-roaringbitmap-roaring"
         ,go-github-com-roaringbitmap-roaring)
        ("go-crawshaw-io-sqlite" ,go-crawshaw-io-sqlite)))
    (home-page "https://github.com/anacrolix/missinggo")
    (synopsis "missinggo")
    (description
      "Package missinggo contains miscellaneous helpers used in many of anacrolix'
projects.")
    (license license:expat)))

(define-public go-github-com-anacrolix-tagflag
  (package
    (name "go-github-com-anacrolix-tagflag")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/tagflag")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "14i8kpx20am8yz0wpads8m9mhaakza01mj0daj0ng30yg4vkiiyg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/tagflag"))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
        ("go-github-com-huandu-xstrings" ,go-github-com-huandu-xstrings)
        ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
        ("go-github-com-bradfitz-iter" ,go-github-com-bradfitz-iter)
        ("go-github-com-anacrolix-missinggo" ,go-github-com-anacrolix-missinggo)
        ))
    (home-page "https://github.com/anacrolix/tagflag")
    (synopsis "tagflag")
    (description
      "Package tagflag uses reflection to derive flags and positional arguments to a
program, and parses and sets them from a slice of arguments.")
    (license license:expat)))

(define-public go-github-com-docopt-docopt-go
  (package
    (name "go-github-com-docopt-docopt-go")
    (version "0.0.0-20180111231733-ee0de3bc6815")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/docopt/docopt.go")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hlra7rmi5pmd7d93rv56ahiy4qkgmq8a6mz0jpadvbi5qh8lq6j"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/docopt/docopt-go"))
    (home-page "https://github.com/docopt/docopt-go")
    (synopsis "docopt-go")
    (description
      "Package docopt parses command-line arguments based on a help message.")
    (license license:expat)))

(define-public go-github-com-google-btree
  (package
    (name "go-github-com-google-btree")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/btree")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0fv5577bmpf2gkzw8z271q8mn3x6fqyzqjrbzm580bqld0a1xwnl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/btree"))
    (home-page "https://github.com/google/btree")
    (synopsis "BTree implementation for Go")
    (description
      "Package btree implements in-memory B-Trees of arbitrary degree.")
    (license license:asl2.0)))

(define-public go-github-com-ryszard-goskiplist
  (package
    (name "go-github-com-ryszard-goskiplist")
    (version "0.0.0-20150312221310-2dfbae5fcf46")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ryszard/goskiplist")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1135gmvcwnmk36zryxq554fmikrmg5c6y5ml00arqpagn5xhnmnl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ryszard/goskiplist"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   ;; Source-only package
                   (delete 'build))
                 ))
    (home-page "https://github.com/ryszard/goskiplist")
    (synopsis "About")
    (description
      "This is a library implementing skip lists for the Go programming language
(@url{http://golang.org/),http://golang.org/)}.")
    (license license:asl2.0)))

(define-public go-github-com-anacrolix-missinggo
  (package
    (name "go-github-com-anacrolix-missinggo")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/missinggo")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "15h0745hm6sx6dg0wmpmbn3k9iacipnb3djbmn1wig65zyvz5z9p"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/missinggo"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-ryszard-goskiplist" ,go-github-com-ryszard-goskiplist)
        ("go-github-com-huandu-xstrings" ,go-github-com-huandu-xstrings)
        ("go-github-com-google-btree" ,go-github-com-google-btree)
        ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
        ("go-github-com-docopt-docopt-go" ,go-github-com-docopt-docopt-go)
        ("go-github-com-bradfitz-iter" ,go-github-com-bradfitz-iter)
        ;;("go-github-com-anacrolix-tagflag" ,go-github-com-anacrolix-tagflag)
        ;;("go-github-com-anacrolix-missinggo-v2" ,go-github-com-anacrolix-missinggo-v2)
        ("go-github-com-anacrolix-envpprof" ,go-github-com-anacrolix-envpprof)
        ("go-github-com-roaringbitmap-roaring"
         ,go-github-com-roaringbitmap-roaring)))
    (home-page "https://github.com/anacrolix/missinggo")
    (synopsis "missinggo")
    (description
      "Package missinggo contains miscellaneous helpers used in many of anacrolix'
projects.")
    (license license:expat)))

(define-public go-github-com-anacrolix-ffprobe
  (package
    (name "go-github-com-anacrolix-ffprobe")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/ffprobe")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ng61skdq8fs4ig02nx6aaksnryz1yhaghz8hji12n0a6r55caxc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/ffprobe"
                 #:tests? #f))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ;;("go-github-com-anacrolix-missinggo" ,go-github-com-anacrolix-missinggo)
        ("go-github-com-anacrolix-envpprof"
         ,go-github-com-anacrolix-envpprof)))
    (home-page "https://github.com/anacrolix/ffprobe")
    (synopsis #f)
    (description
      "Package ffprobe wraps and interprets ffmpeg's ffprobe for Go.")
    (license license:mpl2.0)))

(define-public go-github-com-anacrolix-log
  (package
    (name "go-github-com-anacrolix-log")
    (version "0.13.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/log")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "129hrqs3dp4rc5iwzgpi6iw0xmqkn9b1a4wgvgjc72x7aa9w1l0g"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/log"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-rogpeppe-go-internal"
         ,go-github-com-rogpeppe-go-internal)
        ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/anacrolix/log")
    (synopsis #f)
    (description
      "Package log implements a std log compatible logging system that draws some
inspiration from the Python standard library [logging
module](@url{https://docs.python.org/3/library/logging.html,https://docs.python.org/3/library/logging.html}).
 It supports multiple handlers, log levels, zero-allocation, scopes, custom
formatting, and environment and runtime configuration.")
    (license license:mpl2.0)))

(define-public go-github-com-mschoch-smat
  (package
    (name "go-github-com-mschoch-smat")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mschoch/smat")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qcb2jjg37krxmc915kqynghd6n26w2wxwgcafvxcwn8g0jx96qd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mschoch/smat"))
    (home-page "https://github.com/mschoch/smat")
    (synopsis "smat – State Machine Assisted Testing")
    (description
      "The concept is simple, describe valid uses of your library as states and
actions.  States describe which actions are possible, and with what probability
they should occur.  Actions mutate the context and transition to another state.")
    (license license:asl2.0)))

(define-public go-github-com-philhofer-fwd
  (package
    (name "go-github-com-philhofer-fwd")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/philhofer/fwd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1z88ry83lf01mv69kd2jwbhngh24qwhhknj3l5jn2kz5nycq6bkx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/philhofer/fwd"))
    (home-page "https://github.com/philhofer/fwd")
    (synopsis "fwd")
    (description
      "The `fwd` package provides a buffered reader and writer.  Each has methods that
help improve the encoding/decoding performance of some binary protocols.")
    (license license:expat)))

(define-public go-github-com-tinylib-msgp
  (package
    (name "go-github-com-tinylib-msgp")
    (version "1.1.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tinylib/msgp")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "04s5wkl0qiihl729d1sc10pxnqi0x4xdq6v2dbdgly4j910qsgdd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tinylib/msgp"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-philhofer-fwd" ,go-github-com-philhofer-fwd)))
    (home-page "https://github.com/tinylib/msgp")
    (synopsis "MessagePack Code Generator")
    (description
      "msgp is a code generation tool for creating methods to serialize and
de-serialize Go data structures to and from MessagePack.")
    (license license:expat)))

(define-public go-github-com-glycerine-go-unsnap-stream
  (package
    (name "go-github-com-glycerine-go-unsnap-stream")
    (version "0.0.0-20210130063903-47dfef350d96")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/glycerine/go-unsnap-stream")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1sqpjify17sjff92f8biwb9vnn443shk3zr9myzm8qscjpzkby3w"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/glycerine/go-unsnap-stream"))
    (home-page "https://github.com/glycerine/go-unsnap-stream")
    (synopsis "go-unsnap-stream")
    (description
      "This is a small golang library for decoding and encoding the snappy  format,
specified here:
@url{https://github.com/google/snappy/blob/master/framing_format.txt,https://github.com/google/snappy/blob/master/framing_format.txt}")
    (license license:expat)))

(define-public go-github-com-benbjohnson-immutable
  (package
    (name "go-github-com-benbjohnson-immutable")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/benbjohnson/immutable")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1wnwqbxichjpaiq1gbh66lkaqwswcfnmm7hj4bvp747n7xd8jwmc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/benbjohnson/immutable"
                               #:tests? #f))
    (home-page "https://github.com/benbjohnson/immutable")
    (synopsis "Immutable")
    (description "Package immutable provides immutable collection types.")
    (license license:expat)))


(define-public go-github-com-antihax-optional
  (package
    (name "go-github-com-antihax-optional")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/antihax/optional")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ix08vl49qxr58rc6201cl97g1yznhhkwvqldslawind99js4rj0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/antihax/optional"))
    (home-page "https://github.com/antihax/optional")
    (synopsis #f)
    (description #f)
    (license license:expat)))

(define-public go-github-com-rogpeppe-fastuuid
  (package
    (name "go-github-com-rogpeppe-fastuuid")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rogpeppe/fastuuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "028acdg63zkxpjz3l639nlhki2l0canr2v5jglrmwa1wpjqcfff8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rogpeppe/fastuuid"))
    (home-page "https://github.com/rogpeppe/fastuuid")
    (synopsis "fastuuid")
    (description
      "Package fastuuid provides fast UUID generation of 192 bit universally unique
identifiers.")
    (license license:bsd-3)))

(define-public go-github-com-anacrolix-stm
  (package
    (name "go-github-com-anacrolix-stm")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/stm")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vd05hcbxglj6b69b8rvk5daz63b454d9299ssm86vbq4z388as9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/stm"
                               ;;missinggo needed
                               #:tests? #f))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-benbjohnson-immutable"
         ,go-github-com-benbjohnson-immutable)
        ("go-github-com-anacrolix-envpprof"
         ,go-github-com-anacrolix-envpprof)))
    (home-page "https://github.com/anacrolix/stm")
    (synopsis "stm")
    (description
      "Package stm provides Software Transactional Memory operations for Go.  This is
an alternative to the standard way of writing concurrent code (channels and
mutexes).  STM makes it easy to perform arbitrarily complex operations in an
atomic fashion.  One of its primary advantages over traditional locking is that
STM transactions are composable, whereas locking functions are not -- the
composition will either deadlock or release the lock between functions (making
it non-atomic).")
    (license license:expat)))

(define-public go-crawshaw-io-iox
  (package
    (name "go-crawshaw-io-iox")
    (version "0.0.0-20181124134642-c51c3df30797")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/crawshaw/iox")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "180jcd7c34sscaxiydl0i99wry1xawslxss2hl02svxr8b0jwqv0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "crawshaw.io/iox"))
    (home-page "https://crawshaw.io/iox")
    (synopsis "iox: I/O tools for Go programs")
    (description "Package iox contains I/O utilities.")
    (license license:isc)))

(define-public go-crawshaw-io-sqlite
  (package
    (name "go-crawshaw-io-sqlite")
    (version "0.3.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/crawshaw/sqlite")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01km8y906sagdq42ik56iz5z230wpb7xfs0q6w7fwfky60078smj"))))
    (build-system go-build-system)
    (arguments '(#:import-path "crawshaw.io/sqlite"))
    (propagated-inputs `(("go-crawshaw-io-iox" ,go-crawshaw-io-iox)))
    (home-page "https://crawshaw.io/sqlite")
    (synopsis "Go interface to SQLite.")
    (description "Package sqlite provides a Go interface to SQLite 3.")
    (license license:isc)))

(define-public go-github-com-bits-and-blooms-bitset
  (package
    (name "go-github-com-bits-and-blooms-bitset")
    (version "1.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bits-and-blooms/bitset")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0p1g98klqwbdilqk0fkg8n7x8rjncqc52cva95rh7jl0k3q9d9x4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bits-and-blooms/bitset"))
    (home-page "https://github.com/bits-and-blooms/bitset")
    (synopsis "bitset")
    (description
      "Package bitset implements bitsets, a mapping between non-negative integers and
boolean values.  It should be more efficient than map[uint] bool.")
    (license license:bsd-3)))



(define-public go-github-com-iguanesolutions-go-systemd-v5
  (package
    (name "go-github-com-iguanesolutions-go-systemd-v5")
    (version "5.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/iguanesolutions/go-systemd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ylmhrscz0vrkms7fx00hlwdgkdgwk2asqp9ly4rx1b2ggfib16y"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/iguanesolutions/go-systemd/v5"))
    (propagated-inputs
      `(("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-miekg-dns" ,go-github-com-miekg-dns)
        ("go-github-com-godbus-dbus-v5" ,go-github-com-godbus-dbus-v5)))
    (home-page "https://github.com/iguanesolutions/go-systemd")
    (synopsis "go-systemd")
    (description
      "Easily communicate with systemd when run as daemon within a service unit.")
    (license license:expat)))

(define-public go-github-com-lufia-plan9stats
  (package
    (name "go-github-com-lufia-plan9stats")
    (version "0.0.0-20220517141722-cf486979b281")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/lufia/plan9stats")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "17l9r85halxhzc5d7jaf1jfqybaplwl2442rjlzi3y3hr7jni05d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/lufia/plan9stats"))
    (propagated-inputs
      `(("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)))
    (home-page "https://github.com/lufia/plan9stats")
    (synopsis "plan9stats")
    (description "Package stats provides statistic utilities for Plan 9.")
    (license license:bsd-3)))

(define-public go-github-com-microsoft-go-winio
  (package
    (name "go-github-com-microsoft-go-winio")
    (version "0.5.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/microsoft/go-winio")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19rzcwq07c2y7c06pkjjc8pbg68a24g1khwp7cdc5ypfzj509sc3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/microsoft/go-winio"
                 #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)))
    (home-page "https://github.com/microsoft/go-winio")
    (synopsis "go-winio")
    (description
      "This repository contains utilities for efficiently performing Win32 IO
operations in Go.  Currently, this is focused on accessing named pipes and other
file handles, and for using named pipes as a net transport.")
    (license license:expat)))

(define-public go-github-com-roaringbitmap-roaring
  (package
    (name "go-github-com-roaringbitmap-roaring")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/RoaringBitmap/roaring")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0l7mjj9qnrcxz88rfssl4i7plkn4gj985x9qdjy4yqay82vnzvl7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/RoaringBitmap/roaring"
                 #:tests? #f))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-mschoch-smat" ,go-github-com-mschoch-smat)
        ("go-github-com-bits-and-blooms-bitset"
         ,go-github-com-bits-and-blooms-bitset)))
    (home-page "https://github.com/RoaringBitmap/roaring")
    (synopsis "roaring")
    (description
      "Package roaring is an implementation of Roaring Bitmaps in Go.  They provide
fast compressed bitmap data structures (also called bitset).  They are ideally
suited to represent sets of integers over relatively small ranges.  See
@url{http://roaringbitmap.org,http://roaringbitmap.org} for details.")
    (license (list license:asl2.0))))

(define-public go-github-com-anacrolix-envpprof
  (package
    (name "go-github-com-anacrolix-envpprof")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/anacrolix/envpprof")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ldswafrflclf1kyiqj7fs0fqkslvsfhyizbhnmbbwcam1bbjp88"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/anacrolix/envpprof"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-anacrolix-log" ,go-github-com-anacrolix-log)))
    (home-page "https://github.com/anacrolix/envpprof")
    (synopsis "envpprof")
    (description
      "Allows run-time configuration of Go's pprof features and default HTTP mux using
the environment variable @code{GOPPROF}.  Import the package with @code{import _
\"github.com/anacrolix/envpprof\"}. @code{envpprof} has an @code{init} function
that will run at process initialization that checks the value of the
@code{GOPPROF} environment variable.  The variable can contain a comma-separated
list of values, for example @code{GOPPROF=http,block}.  The supported keys are:")
    (license license:expat)))


(define-public go-github-com-gorilla-sessions
  (package
    (name "go-github-com-gorilla-sessions")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gorilla/sessions")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1zjw2s37yggk9231db0vmgs67z8m3am8i8l4gpgz6fvlbv52baxp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/gorilla/sessions"))
    (propagated-inputs
      `(("go-github-com-gorilla-securecookie"
         ,go-github-com-gorilla-securecookie)))
    (home-page "https://github.com/gorilla/sessions")
    (synopsis "sessions")
    (description
      "Package sessions provides cookie and filesystem sessions and infrastructure for
custom session backends.")
    (license license:bsd-3)))


(define-public go-bazil-org-fuse
  (package
    (name "go-bazil-org-fuse")
    (version "0.0.0-20200524192727-fb710f7dfd05")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bazil/fuse")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "188rpxd5pa6n9k87ypwadamfvmxd13dvfp80mrj5ybwpl35rdy6k"))))
    (build-system go-build-system)
    (arguments '(#:import-path "bazil.org/fuse"
                 ;; fuse device not found
                 #:tests? #f))
    (native-inputs (list fuse))
    
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-tv42-httpunix" ,go-github-com-tv42-httpunix)
        ("go-github-com-stephens2424-writerset"
         ,go-github-com-stephens2424-writerset)
        ("go-github-com-elazarl-go-bindata-assetfs"
         ,go-github-com-elazarl-go-bindata-assetfs)
        ("go-github-com-dvyukov-go-fuzz" ,go-github-com-dvyukov-go-fuzz)))
    (home-page "https://bazil.org/fuse")
    (synopsis "bazil.org/fuse -- Filesystems in Go")
    (description
      "Package fuse enables writing FUSE file systems on Linux and FreeBSD.")
    (license #f)))

(define-public go-github-com-vivint-infectious
  (package
    (name "go-github-com-vivint-infectious")
    (version "0.0.0-20200605153912-25a574ae18a3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vivint/infectious")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0729lir8kszrqg52b2l3snjv9wck5kavwnz42gx1bhm0rr4mcjba"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/vivint/infectious"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/vivint/infectious")
    (synopsis "infectious")
    (description
      "Package infectious implements Reed-Solomon forward error correction [1].  It
uses the Berlekamp-Welch [2] error correction algorithm to achieve the ability
to actually correct errors.")
    (license #f)))


(define-public go-github-com-unknwon-goconfig
  (package
    (name "go-github-com-unknwon-goconfig")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/unknwon/goconfig")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10f0lhb3l971b2fw2xsvbsy96gx9isyckg2003av2f4rlzs27n7b"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/Unknwon/goconfig"
                     ;;permission to open file denied
                     #:tests? #f))
    (native-inputs (list go-github-com-smartystreets-goconvey))
    
    (home-page "https://github.com/unknwon/goconfig")
    (synopsis "goconfig")
    (description
      "Package goconfig is a fully functional and comments-support configuration
file(.ini) parser.")
    (license license:asl2.0)))

(define-public go-github-com-hanwen-go-fuse
  (package
    (name "go-github-com-hanwen-go-fuse")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hanwen/go-fuse")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "04xa8mh34639lv1b2p8dx13x742j5i493i3sk89hd3jfskzvval1"))))
    (build-system go-build-system)
    (arguments '(#:unpack-path "github.com/hanwen/go-fuse"
                 #:import-path "github.com/hanwen/go-fuse/fuse"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/hanwen/go-fuse")
    (synopsis "GO-FUSE")
    (description "native bindings for the FUSE kernel module.")
    (license license:bsd-3)))

(define-public go-github-com-hanwen-go-fuse-v2
  (package
    (name "go-github-com-hanwen-go-fuse-v2")
    (version "2.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hanwen/go-fuse")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "05ymw2pp58avf19wvi0cgdzqf3d88k1jdf6ldj4hmhbkm3waqf7l"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hanwen/go-fuse/v2"
       ;;#:unpack-path "github.com/hanwen/go-fuse/v2"
       ;;"setup failed"
       #:phases
       (modify-phases %standard-phases
         (delete 'build))
       #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-github-com-kylelemons-godebug" ,go-github-com-kylelemons-godebug)
        ;;("go-github-com-hanwen-go-fuse" ,go-github-com-hanwen-go-fuse)
        
        ))
    (home-page "https://github.com/hanwen/go-fuse")
    (synopsis "GO-FUSE")
    (description
      "This is a repository containing Go bindings for writing FUSE file systems.")
    (license license:bsd-3)))

(define-public go-github-com-jzelinskie-whirlpool
  (package
    (name "go-github-com-jzelinskie-whirlpool")
    (version "0.0.0-20201016144138-0675e54bb004")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jzelinskie/whirlpool")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0w74h9dz8pkwal3aqymymsq2zgl7d16dw1kxa7dfkad167g3s1mz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jzelinskie/whirlpool"))
    (home-page "https://github.com/jzelinskie/whirlpool")
    (synopsis "whirlpool.go")
    (description
      "Package whirlpool implements the ISO/IEC 10118-3:2004 whirlpool cryptographic
hash.  Whirlpool is defined in
@url{http://www.larc.usp.br/~pbarreto/WhirlpoolPage.html,http://www.larc.usp.br/~pbarreto/WhirlpoolPage.html}")
    (license license:bsd-3)))

(define-public go-gopkg-in-jcmturner-dnsutils-v1
  (package
    (name "go-gopkg-in-jcmturner-dnsutils-v1")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/dnsutils.v1")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0l543c64pyzbnrc00jspg21672l3a0kjjw9pbdxwna93w8d8m927"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/dnsutils.v1"
        #:unpack-path
        "gopkg.in/jcmturner/dnsutils.v1"))
    (native-inputs (list go-github-com-stretchr-testify))
    
    (home-page "https://gopkg.in/jcmturner/dnsutils.v1")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-gopkg-in-jcmturner-gokrb5-v7
  (package
    (name "go-gopkg-in-jcmturner-gokrb5-v7")
    (version "7.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/jcmturner/gokrb5.v7")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "181l30y5dwp0ackn04dd00a4wqawk99nlkpnx2pjh1sfwazghq3y"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/jcmturner/gokrb5.v7"
        #:unpack-path
        "gopkg.in/jcmturner/gokrb5.v7"
        #:tests? #f))
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs
     (list go-gopkg-in-jcmturner-dnsutils-v1
           go-gopkg-in-jcmturner-goidentity-v3
           go-gopkg-in-jcmturner-aescts-v1 
           go-gopkg-in-jcmturner-rpc-v1-mstypes
           go-gopkg-in-jcmturner-rpc-v1-ndr))
    (home-page "https://gopkg.in/jcmturner/gokrb5.v7")
    (synopsis "gokrb5")
    (description "Package gokrb5 provides a Kerberos 5 implementation for Go")
    (license license:asl2.0)))

(define-public go-github-com-jcmturner-gokrb5-v8
  (package
    (name "go-github-com-jcmturner-gokrb5-v8")
    (version "8.4.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jcmturner/gokrb5")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0w9d1pa3r6qmdblk25bghf78ncs03l15l1sxnh4n536c356rzq4b"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jcmturner/gokrb5/v8"
                 #:tests? #f))
    (native-inputs (list go-github-com-stretchr-testify go-github-com-gorilla-sessions))
    (propagated-inputs
     `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
       ;;("go-gopkg-in-jcmturner-gokrb5-v7" ,go-gopkg-in-jcmturner-gokrb5-v7)
        ;;("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-jcmturner-rpc-v2" ,go-github-com-jcmturner-rpc-v2)
        ("go-github-com-jcmturner-goidentity-v6"
         ,go-github-com-jcmturner-goidentity-v6)
        ("go-github-com-jcmturner-gofork" ,go-github-com-jcmturner-gofork)
        ("go-github-com-jcmturner-dnsutils-v2"
         ,go-github-com-jcmturner-dnsutils-v2)
        ("go-github-com-jcmturner-aescts-v2"
         ,go-github-com-jcmturner-aescts-v2)
        ("go-github-com-hashicorp-go-uuid" ,go-github-com-hashicorp-go-uuid)
        ))
    (home-page "https://github.com/jcmturner/gokrb5")
    (synopsis "gokrb5")
    (description "Package gokrb5 provides a Kerberos 5 implementation for Go.")
    (license license:asl2.0)))

(define-public go-github-com-koofr-go-httpclient
  (package
    (name "go-github-com-koofr-go-httpclient")
    (version "0.0.0-20200420163713-93aa7c75b348")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/koofr/go-httpclient")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0krwgyd9xhwbk707q4rnm2zn35h60w8bfrmm016mg1ji3hfx13m2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/koofr/go-httpclient"
                 #:tests? #f))
    (native-inputs
     
      `(;;("go-github-com-onsi-gomega" ,go-github-com-onsi-gomega)
        ;;("go-github-com-onsi-ginkgo" ,go-github-com-onsi-ginkgo)
        ))
    (home-page "https://github.com/koofr/go-httpclient")
    (synopsis "go-httpclient")
    (description "Go HTTP client.")
    (license license:expat)))

(define-public go-github-com-golang-jwt-jwt-v4
  (package
    (name "go-github-com-golang-jwt-jwt-v4")
    (version "4.4.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang-jwt/jwt")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0h1wnshxpxzccpf683r00g8fp7ylskknx5bh5lwlnrl3nv8hdbva"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang-jwt/jwt/v4"))
    (home-page "https://github.com/golang-jwt/jwt")
    (synopsis "jwt-go")
    (description
      "Package jwt is a Go implementation of JSON Web Tokens:
@url{http://self-issued.info/docs/draft-jones-json-web-token.html,http://self-issued.info/docs/draft-jones-json-web-token.html}")
    (license license:expat)))



(define-public go-github-com-zeebo-incenc
  (package
    (name "go-github-com-zeebo-incenc")
    (version "0.0.0-20180505221441-0d92902eec54")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/zeebo/incenc")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1yzyrcgw65lb8rmgbqid4czrarakm4mbzr89qws8dpywq93i2wd2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/zeebo/incenc"))
    (propagated-inputs (list go-github-com-zeebo-errs))
    (home-page "https://github.com/zeebo/incenc")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-storj-io-uplink
  (package
    (name "go-storj-io-uplink")
    (version "1.8.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/storj/uplink")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1icb1jjbhyl42i4c1cwl49w7ymqlfwh9m19hrkcm4gnag4i9331p"))))
    (build-system go-build-system)
    (arguments '(#:import-path "storj.io/uplink"))
    (propagated-inputs
      `(("go-storj-io-drpc" ,go-storj-io-drpc)
        ("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
        ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-zeebo-incenc" ,go-github-com-zeebo-incenc)
        ("go-github-com-zeebo-float16" ,go-github-com-zeebo-float16)
        ("go-github-com-zeebo-admission-v3" ,go-github-com-zeebo-admission-v3)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
        ("go-github-com-google-pprof" ,go-github-com-google-pprof)
        ("go-github-com-gogo-protobuf" ,go-github-com-gogo-protobuf)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-calebcase-tmpfile" ,go-github-com-calebcase-tmpfile)
        ("go-storj-io-common" ,go-storj-io-common)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-github-com-zeebo-errs" ,go-github-com-zeebo-errs)
        ("go-github-com-vivint-infectious" ,go-github-com-vivint-infectious)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-spacemonkeygo-monkit-v3"
         ,go-github-com-spacemonkeygo-monkit-v3)))
    (home-page "https://storj.io/uplink")
    (synopsis "Libuplink")
    (description
      "Package uplink is the main entrypoint to interacting with Storj Labs'
decentralized storage network.")
    (license license:expat)))

(define-public go-github-com-jtolds-gls
  (package
    (name "go-github-com-jtolds-gls")
    (version "4.20.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jtolio/gls")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k7xd2q2ysv2xsh373qs801v6f359240kx0vrl0ydh7731lngvk6"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jtolds/gls"))
    (home-page "https://github.com/jtolds/gls")
    (synopsis "gls")
    (description "Package gls implements goroutine-local storage.")
    (license license:expat)))

(define-public go-github-com-smartystreets-assertions
  (package
    (name "go-github-com-smartystreets-assertions")
    (version "1.13.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/smartystreets/assertions")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0flf3fb6fsw3bk1viva0fzrzw87djaj1mqvrx2gzg1ssn7xzfrzr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/smartystreets/assertions"))
    (home-page "https://github.com/smartystreets/assertions")
    (synopsis #f)
    (description
      "Package assertions contains the implementations for all assertions which are
referenced in goconvey's `convey` package
(github.com/smartystreets/goconvey/convey) and gunit
(github.com/smartystreets/gunit) for use with the So(...) method.  They can also
be used in traditional Go test functions and even in applications.")
    (license license:expat)))

(define-public go-github-com-smartystreets-goconvey
  (package
    (name "go-github-com-smartystreets-goconvey")
    (version "1.7.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/smartystreets/goconvey")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0gwr0d6gb0jzqam76xpan279r2dnifsnhr4px8l6a84bavslqgv1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/smartystreets/goconvey"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-smartystreets-assertions"
         ,go-github-com-smartystreets-assertions)
        ("go-github-com-jtolds-gls" ,go-github-com-jtolds-gls)
        ("go-github-com-gopherjs-gopherjs" ,go-github-com-gopherjs-gopherjs)))
    (home-page "https://github.com/smartystreets/goconvey")
    (synopsis "GoConvey is awesome Go testing")
    (description
      "This executable provides an HTTP server that watches for file system changes to
.go files within the working directory (and all nested go packages).  Navigating
to the configured host and port in a web browser will display the latest results
of running `go test` in each go package.")
    (license license:expat)))

(define-public go-github-com-minio-minio-go-v6
  (package
    (name "go-github-com-minio-minio-go-v6")
    (version "6.0.57")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/minio/minio-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "068yxi9zzcjlys676v5jl6bd131w2qqfhbzzn1c1mgms4c4rvlbr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/minio/minio-go/v6"
                 #:tests? #f))
    (propagated-inputs
      `(("go-gopkg-in-ini-v1" ,go-gopkg-in-ini-v1)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-smartystreets-goconvey"
         ,go-github-com-smartystreets-goconvey)
        ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
        ("go-github-com-mitchellh-go-homedir"
         ,go-github-com-mitchellh-go-homedir)
        ("go-github-com-minio-sha256-simd" ,go-github-com-minio-sha256-simd)
        ("go-github-com-minio-md5-simd" ,go-github-com-minio-md5-simd)
        ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
        ("go-github-com-dustin-go-humanize"
         ,go-github-com-dustin-go-humanize)))
    (home-page "https://github.com/minio/minio-go")
    (synopsis "MinIO Go Client SDK for Amazon S3 Compatible Cloud Storage")
    (description
      "The MinIO Go Client SDK provides simple APIs to access any Amazon S3 compatible
object storage.")
    (license license:asl2.0)))

(define-public go-github-com-klauspost-cpuid-v2
  (package
    (name "go-github-com-klauspost-cpuid-v2")
    (version "2.0.12")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/klauspost/cpuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vf716p0f3b9dll1zx7waq8liia85v31zwvsaxs55q2nnwkdr5y2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/klauspost/cpuid/v2"))
    (home-page "https://github.com/klauspost/cpuid")
    (synopsis "cpuid")
    (description
      "Package cpuid provides information about the CPU running the current program.")
    (license license:expat)))

(define-public go-github-com-modern-go-concurrent
  (package
    (name "go-github-com-modern-go-concurrent")
    (version "0.0.0-20180306012644-bacd9c7ef1dd")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/modern-go/concurrent")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0s0fxccsyb8icjmiym5k7prcqx36hvgdwl588y0491gi18k5i4zs"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/modern-go/concurrent"))
    (home-page "https://github.com/modern-go/concurrent")
    (synopsis "concurrent")
    (description
      "because sync.Map is only available in go 1.9, we can use concurrent.Map to make
code portable")
    (license license:asl2.0)))

(define-public go-github-com-modern-go-reflect2
  (package
    (name "go-github-com-modern-go-reflect2")
    (version "1.0.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/modern-go/reflect2")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "05a89f9j4nj8v1bchfkv2sy8piz746ikj831ilbp54g8dqhl8vzr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/modern-go/reflect2"))
    (home-page "https://github.com/modern-go/reflect2")
    (synopsis "reflect2")
    (description "reflect api that avoids runtime reflect.Value cost")
    (license license:asl2.0)))

(define-public go-github-com-json-iterator-go
  (package
    (name "go-github-com-json-iterator-go")
    (version "1.1.12")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/json-iterator/go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1c8f0hxm18wivx31bs615x3vxs2j3ba0v6vxchsjhldc8kl311bz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/json-iterator/go"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-modern-go-reflect2" ,go-github-com-modern-go-reflect2)
        ("go-github-com-modern-go-concurrent"
         ,go-github-com-modern-go-concurrent)
        ("go-github-com-google-gofuzz" ,go-github-com-google-gofuzz)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/json-iterator/go")
    (synopsis "Benchmark")
    (description
      "Package jsoniter implements encoding and decoding of JSON as defined in
@url{https://rfc-editor.org/rfc/rfc4627.html,RFC 4627} and provides interfaces
with identical syntax of standard lib encoding/json.  Converting from
encoding/json to jsoniter is no more than replacing the package with jsoniter
and variable type declarations (if any).  jsoniter interfaces gives 100%
compatibility with code using standard lib.")
    (license license:expat)))

(define-public go-github-com-minio-md5-simd
  (package
    (name "go-github-com-minio-md5-simd")
    (version "1.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/minio/md5-simd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qj8ipifbdg3ppilyqj8zy68f72rmqy8flli1vch3fibrbw8vpd0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/minio/md5-simd"))
    (propagated-inputs
      `(("go-github-com-klauspost-cpuid-v2"
         ,go-github-com-klauspost-cpuid-v2)))
    (home-page "https://github.com/minio/md5-simd")
    (synopsis "md5-simd")
    (description
      "This is a SIMD accelerated MD5 package, allowing up to either 8 (AVX2) or 16
(AVX512) independent MD5 sums to be calculated on a single CPU core.")
    (license license:asl2.0)))

(define-public go-goftp-io-server
  (package
    (name "go-goftp-io-server")
    (version "0.4.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitea.com/goftp/server")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ah97hmnkg5rydj1n0zjhq6pa9yv990n3s7sh9wfb76m6jmsnr7b"))))
    (build-system go-build-system)
    (arguments '(#:import-path "goftp.io/server"))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-minio-minio-go-v6" ,go-github-com-minio-minio-go-v6)
        ("go-github-com-jlaffaye-ftp" ,go-github-com-jlaffaye-ftp)))
    (home-page "https://goftp.io/server")
    (synopsis "server")
    (description
      "Package server is a backwards compatibility shim for this module")
    (license license:expat)))

(define-public go-github-com-jlaffaye-ftp
  (package
    (name "go-github-com-jlaffaye-ftp")
    (version "0.0.0-20220524001917-dfa1e758f3af")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jlaffaye/ftp")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "11nclysmwcz1qi005jvjhi0x9s0bbgfrixy5s1vc6rs8m9a3arh0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jlaffaye/ftp"))
    (native-inputs (list go-github-com-stretchr-testify))
    
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-hashicorp-errwrap" ,go-github-com-hashicorp-errwrap)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-hashicorp-go-multierror"
         ,go-github-com-hashicorp-go-multierror)))
    (home-page "https://github.com/jlaffaye/ftp")
    (synopsis "goftp")
    (description
      "Package ftp implements a FTP client as described in
@url{https://rfc-editor.org/rfc/rfc959.html,RFC 959}.")
    (license license:isc)))

(define-public go-github-com-miekg-dns
  (package
    (name "go-github-com-miekg-dns")
    (version "1.1.49")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/miekg/dns")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0w9lk6kkqgwywjb4lvbmvsyyg8j42znykl9mz76iw3ncb69ha7zj"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/miekg/dns"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/miekg/dns")
    (synopsis "Alternative (more granular) approach to a DNS library")
    (description
      "Package dns implements a full featured interface to the Domain Name System.
Both server- and client-side programming is supported.  The package allows
complete control over what is sent out to the DNS.  The API follows the
less-is-more principle, by presenting a small, clean interface.")
    (license license:bsd-3)))

(define-public go-github-com-godbus-dbus-v5
  (package
    (name "go-github-com-godbus-dbus-v5")
    (version "5.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/godbus/dbus")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1kayd4x7idrhi06ahh5kqkgwzgh9icvv71mjar2d0jl486dfs8r5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/godbus/dbus/v5"
                 ;;--- FAIL: TestSessionBus (0.02s)
                 ;;conn_test.go:18: dbus: couldn't determine address of session bus
                 #:tests? #f))
    (native-inputs (list dbus))
    (home-page "https://github.com/godbus/dbus")
    (synopsis "dbus")
    (description
      "Package dbus implements bindings to the D-Bus message bus system.")
    (license license:bsd-2)))


(define-public go-github-com-dropbox-dropbox-sdk-go-unofficial-v6
  (package
    (name "go-github-com-dropbox-dropbox-sdk-go-unofficial-v6")
    (version "6.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dropbox/dropbox-sdk-go-unofficial")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nw5bkm9za8ic1gmy6nywga8jil8jcpb6bxka75srf28208323xr"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "github.com/dropbox/dropbox-sdk-go-unofficial"
                     #:import-path "github.com/dropbox/dropbox-sdk-go-unofficial/v6/dropbox"))
    (propagated-inputs (list go-golang-org-x-oauth2))
    (home-page "https://github.com/dropbox/dropbox-sdk-go-unofficial")
    (synopsis "Dropbox SDK for Go [UNOFFICIAL]")
    (description
      "An @strong{UNOFFICIAL} Go SDK for integrating with the Dropbox API v2.  Tested
with Go 1.11+")
    (license license:expat)))

(define-public go-github-com-putdotio-go-putio
  (package
    (name "go-github-com-putdotio-go-putio")
    ;;last release that works for rclone
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/putdotio/go-putio")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "046kh644iv1l0rsynqxnwxldb3d1pbh2plak8hxrq5w3ah43zx9v"))))
    (build-system go-build-system)
    (arguments '(#:unpack-path "github.com/putdotio/go-putio"
                 #:import-path "github.com/putdotio/go-putio/putio"))
    (propagated-inputs `(("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)))
    (home-page "https://github.com/putdotio/go-putio")
    (synopsis "putio")
    (description "Package putio is the Put.io API v2 client for Go.")
    (license license:expat)))

(define-public go-go-etcd-io-bbolt
  (package
    (name "go-go-etcd-io-bbolt")
    (version "1.3.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/etcd-io/bbolt")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0pj5245d417za41j6p09fmkbv05797vykr1bi9a6rnwddh1dbs8d"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "go.etcd.io/bbolt"
       ;;tests are stalling for a long time                        
       #:tests? #f))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://go.etcd.io/bbolt")
    (synopsis "bbolt")
    (description
      "package bbolt implements a low-level key/value store in pure Go.  It supports
fully serializable transactions, ACID semantics, and lock-free MVCC with
multiple readers and a single writer.  Bolt can be used for projects that want a
simple data store without the need to add large dependencies such as Postgres or
MySQL.")
    (license license:expat)))

(define-public go-github-com-tklauser-numcpus
  (package
    (name "go-github-com-tklauser-numcpus")
    (version "0.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tklauser/numcpus")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jcz9dvgkdj93j2v4g83vsa97ahjffjfs58idqpaipcvfgd3rlcx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tklauser/numcpus"
                 ;;can't see /sys/.. fs              
                 #:tests? #f))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/tklauser/numcpus")
    (synopsis "numcpus")
    (description
      "Package numcpus provides information about the number of CPUs in the system.")
    (license license:asl2.0)))

(define-public go-github-com-tklauser-go-sysconf
  (package
    (name "go-github-com-tklauser-go-sysconf")
    (version "0.3.10")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tklauser/go-sysconf")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07nb0nkwhymx8fi9f5i2ymnm42qxp33hjvwgyy67gy1x0fr8vzb5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tklauser/go-sysconf"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-tklauser-numcpus" ,go-github-com-tklauser-numcpus)))
    (home-page "https://github.com/tklauser/go-sysconf")
    (synopsis "go-sysconf")
    (description
      "Package sysconf implements the sysconf(3) function and provides the associated
SC_* constants to query system configuration values.")
    (license license:bsd-3)))

(define-public go-github-com-go-ole-go-ole
  (package
    (name "go-github-com-go-ole-go-ole")
    (version "1.2.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-ole/go-ole")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0aswlz7dr6v0if6bdwj3ivawj8cql2hgp84yymsq3ic9nys6537s"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-ole/go-ole"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/go-ole/go-ole")
    (synopsis "Go OLE")
    (description
      "Go bindings for Windows COM using shared libraries instead of cgo.")
    (license license:expat)))


(define-public go-github-com-power-devops-perfstat
  (package
    (name "go-github-com-power-devops-perfstat")
    (version "0.0.0-20220216144756-c35f1ee13d7c")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/power-devops/perfstat")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0033vj6z7311h968wglj2dd80lz9kidgjzrrqhnf4pz88f8s7jn4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/power-devops/perfstat"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/power-devops/perfstat")
    (synopsis #f)
    (description
      "Copyright 2020 Power-Devops.com.  All rights reserved.  Use of this source code
is governed by the license that can be found in the LICENSE file.")
    (license license:expat)))

(define-public go-github-com-tklauser-go-sysconf
  (package
    (name "go-github-com-tklauser-go-sysconf")
    (version "0.3.10")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tklauser/go-sysconf")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07nb0nkwhymx8fi9f5i2ymnm42qxp33hjvwgyy67gy1x0fr8vzb5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tklauser/go-sysconf"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-tklauser-numcpus" ,go-github-com-tklauser-numcpus)))
    (home-page "https://github.com/tklauser/go-sysconf")
    (synopsis "go-sysconf")
    (description
      "Package sysconf implements the sysconf(3) function and provides the associated
SC_* constants to query system configuration values.")
    (license license:bsd-3)))

(define-public go-github-com-yusufpapurcu-wmi
  (package
    (name "go-github-com-yusufpapurcu-wmi")
    (version "1.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/yusufpapurcu/wmi")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wm1c3ia8wwrpid62pnnbgw3lxcp1ak9gl460qjyh8c9rlbklyj6"))))
    (build-system go-build-system)
    (arguments
     '(;;#:unpack-path "github.com/yusufpapurcu/wmi"
       #:import-path "github.com/yusufpapurcu/wmi"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
           (delete 'build)
         )))
    (propagated-inputs
      `(("go-github-com-go-ole-go-ole" ,go-github-com-go-ole-go-ole)))
    (home-page "https://github.com/yusufpapurcu/wmi")
    (synopsis "wmi")
    (description "Package wmi provides a WQL interface for WMI on Windows.")
    (license license:expat)))

(define-public go-github-com-shirou-gopsutil-v3
  (package
    (name "go-github-com-shirou-gopsutil-v3")
    (version "3.22.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shirou/gopsutil")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "16rksrsqbh0196hxb3gxh8r7s5w0bvsfscs7rhah59wwhqlrgns2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shirou/gopsutil/v3"))
    (native-inputs (list go-github-com-stretchr-testify))    
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-yusufpapurcu-wmi" ,go-github-com-yusufpapurcu-wmi)
        ("go-github-com-tklauser-go-sysconf"
         ,go-github-com-tklauser-go-sysconf)
        ;; ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ;;("go-github-com-power-devops-perfstat" ,go-github-com-power-devops-perfstat)
        ("go-github-com-lufia-plan9stats" ,go-github-com-lufia-plan9stats)
        ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)))
    (home-page "https://github.com/shirou/gopsutil")
    (synopsis "gopsutil: psutil for golang")
    (description
      "This is a port of psutil
(@url{https://github.com/giampaolo/psutil),https://github.com/giampaolo/psutil)}.
 The challenge is porting all psutil functions on some architectures.")
    (license license:bsd-3)))


(define-public go-github-com-coreos-go-systemd
  (package
    (name "go-github-com-coreos-go-systemd")
    (version "0.0.0-20191104093116-d3cd4ed1dbcf")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/coreos/go-systemd")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "193mgqn7n4gbb8jb5kyn6ml4lbvh4xs55qpjnisaz7j945ik3kd8"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "github.com/coreos/go-systemd"
       #:import-path "github.com/coreos/go-systemd/util"              
       #:tests? #f
       #:phases
        (modify-phases %standard-phases
        (replace 'install
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'install)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/coreos/go-systemd")))))
       ))
    (home-page "https://github.com/coreos/go-systemd")
    (synopsis "go-systemd")
    (description "Go bindings to systemd.  The project has several packages:")
    (license license:asl2.0)))


(define-public go-github-com-btcsuite-btcutil
  (package
    (name "go-github-com-btcsuite-btcutil")
    (version "1.0.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/btcutil")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wwykb4cbq8xj2mls2mxma5vaahdgdy3vqw1r2fi4wyj0yr4kyw9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/btcutil"))
    (propagated-inputs
      `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-kkdai-bstream" ,go-github-com-kkdai-bstream)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-btcsuite-btcd" ,go-github-com-btcsuite-btcd)
        ("go-github-com-aead-siphash" ,go-github-com-aead-siphash)))
    (home-page "https://github.com/btcsuite/btcutil")
    (synopsis "btcutil")
    (description
      "Package btcutil provides bitcoin-specific convenience functions and types.")
    (license license:isc)))

(define-public go-github-com-btcsuite-btcd
  (package
    (name "go-github-com-btcsuite-btcd")
    (version "0.22.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/btcd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1hznnrl1pi5nyh6xm05wmb8knq1sm44pz4w998q01lqfpyc56z86"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/btcd"))
    (propagated-inputs
      `(("go-github-com-kkdai-bstream" ,go-github-com-kkdai-bstream)
        ("go-github-com-btcsuite-snappy-go" ,go-github-com-btcsuite-snappy-go)
        ("go-github-com-aead-siphash" ,go-github-com-aead-siphash)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-jrick-logrotate" ,go-github-com-jrick-logrotate)
        ("go-github-com-jessevdk-go-flags" ,go-github-com-jessevdk-go-flags)
        ("go-github-com-decred-dcrd" ,go-github-com-decred-dcrd)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-btcsuite-winsvc" ,go-github-com-btcsuite-winsvc)
        ("go-github-com-btcsuite-websocket" ,go-github-com-btcsuite-websocket)
        ("go-github-com-btcsuite-goleveldb" ,go-github-com-btcsuite-goleveldb)
        ("go-github-com-btcsuite-go-socks" ,go-github-com-btcsuite-go-socks)
        ;;("go-github-com-btcsuite-btcutil" ,go-github-com-btcsuite-btcutil)
        ("go-github-com-btcsuite-btclog" ,go-github-com-btcsuite-btclog)
        ;;("go-github-com-btcsuite-btcd-chaincfg-chainhash" ,go-github-com-btcsuite-btcd-chaincfg-chainhash)
        ))
    (home-page "https://github.com/btcsuite/btcd")
    (synopsis "btcd")
    (description "btcd is a full-node bitcoin implementation written in Go.")
    (license license:isc)))

(define-public go-github-com-btcsuite-websocket
  (package
    (name "go-github-com-btcsuite-websocket")
    (version "0.0.0-20150119174127-31079b680792")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/websocket")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0xpkf257ml6fpfdgv7hxxc41n0d5yxxm3njm50qpzp7j71l9cjwa"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/websocket"))
    (home-page "https://github.com/btcsuite/websocket")
    (synopsis "Gorilla WebSocket")
    (description
      "Package websocket implements the WebSocket protocol defined in
@url{https://rfc-editor.org/rfc/rfc6455.html,RFC 6455}.")
    (license license:bsd-2)))

(define-public go-github-com-btcsuite-btclog
  (package
    (name "go-github-com-btcsuite-btclog")
    (version "0.0.0-20170628155309-84c8d2346e9f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/btclog")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "02dl46wcnfpg9sqvg0ipipkpnd7lrf4fnvb9zy56jqa7mfcwc7wk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/btclog"))
    (home-page "https://github.com/btcsuite/btclog")
    (synopsis "btclog")
    (description
      "Package btclog defines an interface and default implementation for subsystem
logging.")
    (license license:isc)))

(define-public go-github-com-kkdai-bstream
  (package
    (name "go-github-com-kkdai-bstream")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kkdai/bstream")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "02brc58hhkkfhgb4yxjv3wshq355w533q7d49qrlfkhk265qph7w"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/kkdai/bstream"))
    (home-page "https://github.com/kkdai/bstream")
    (synopsis "BStream: A Bit Stream helper in Golang")
    (description
      "It is one of my @url{https://github.com/kkdai/project52,project 52}.")
    (license license:expat)))

(define-public go-github-com-decred-dcrd
  (package
    (name "go-github-com-decred-dcrd")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/decred/dcrd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0pmm6ahk0s0qgswvchw60vi0qx9ji3dry8dyd85lhav7xlhpy2mx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/decred/dcrd"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-jrick-logrotate" ,go-github-com-jrick-logrotate)
        ("go-github-com-jrick-bitset" ,go-github-com-jrick-bitset)
        ("go-github-com-jessevdk-go-flags" ,go-github-com-jessevdk-go-flags)
        ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)
        ("go-github-com-decred-slog" ,go-github-com-decred-slog)
        ("go-github-com-decred-base58" ,go-github-com-decred-base58)
        ("go-github-com-dchest-siphash" ,go-github-com-dchest-siphash)
        ("go-github-com-dchest-blake256" ,go-github-com-dchest-blake256)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-btcsuite-winsvc" ,go-github-com-btcsuite-winsvc)
        ("go-github-com-btcsuite-snappy-go" ,go-github-com-btcsuite-snappy-go)
        ("go-github-com-btcsuite-goleveldb" ,go-github-com-btcsuite-goleveldb)
        ("go-github-com-btcsuite-go-socks" ,go-github-com-btcsuite-go-socks)
        ("go-github-com-agl-ed25519" ,go-github-com-agl-ed25519)))
    (home-page "https://github.com/decred/dcrd")
    (synopsis "dcrd")
    (description "dcrd is a full-node Decred implementation written in Go.")
    (license license:isc)))
(define-public go-github-com-agl-ed25519
  (package
    (name "go-github-com-agl-ed25519")
    (version "0.0.0-20200225211852-fd4d107ace12")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/agl/ed25519")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "13hq6sfbfnhp3734pnmka58hidfz17y18y9a40qqqb3j3f85g2dp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/agl/ed25519"))
    (home-page "https://github.com/agl/ed25519")
    (synopsis #f)
    (description
      "Package ed25519 implements the Ed25519 signature algorithm.  See
@url{http://ed25519.cr.yp.to/,http://ed25519.cr.yp.to/}.")
    (license license:bsd-3)))

(define-public go-github-com-btcsuite-go-socks
  (package
    (name "go-github-com-btcsuite-go-socks")
    (version "0.0.0-20170105172521-4720035b7bfd")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/go-socks")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "18cv2icj059lq4s99p6yh91hlas5f2gi3f1p4c10sjgwrs933d7b"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/go-socks"))
    (home-page "https://github.com/btcsuite/go-socks")
    (synopsis "SOCKS5 Proxy Package for Go")
    (description
      "Documentation:
@url{http://godoc.org/github.com/btcsuite/go-socks/socks,http://godoc.org/github.com/btcsuite/go-socks/socks}")
    (license license:bsd-3)))


(define-public go-github-com-btcsuite-goleveldb
  (package
    (name "go-github-com-btcsuite-goleveldb")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/goleveldb")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0kwllw5yflpn362xbsqphbm10qcy08v3zqs15zbyp4mkjjk0bl9z"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/goleveldb"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-onsi-gomega" ,go-github-com-onsi-gomega)
        ("go-github-com-onsi-ginkgo" ,go-github-com-onsi-ginkgo)
        ("go-github-com-btcsuite-snappy-go"
         ,go-github-com-btcsuite-snappy-go)))
    (home-page "https://github.com/btcsuite/goleveldb")
    (synopsis "Installation")
    (description
      "This is an implementation of the @url{http:code.google.com/p/leveldb,LevelDB
key/value database} in the @url{http:golang.org,Go programming language}.")
    (license license:bsd-2)))

(define-public go-github-com-btcsuite-snappy-go
  (package
    (name "go-github-com-btcsuite-snappy-go")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/snappy-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0b2536a58l6advb0ag9ywz7i5cdzclvmm3x796jydv1yrbj5x2vk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/snappy-go"))
    (home-page "https://github.com/btcsuite/snappy-go")
    (synopsis #f)
    (description
      "Package snappy implements the snappy block-based compression format.  It aims
for very high speeds and reasonable compression.")
    (license license:bsd-3)))

(define-public go-github-com-btcsuite-winsvc
  (package
    (name "go-github-com-btcsuite-winsvc")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/btcsuite/winsvc")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0nsw8y86a5hzr2a3j6ch9myrpccj5bnsgaxpgajhzfk5d31xlw1z"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/btcsuite/winsvc"))
    (home-page "https://github.com/btcsuite/winsvc")
    (synopsis "winsvc")
    (description
      "Package winsvc is simply a fork from
@url{http://code.google.com/p/winsvc,http://code.google.com/p/winsvc}.  As can
be guessed from the name, it only works on Windows.")
    (license license:bsd-3)))

(define-public go-github-com-dchest-blake256
  (package
    (name "go-github-com-dchest-blake256")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dchest/blake256")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "00244qhky34ym53dqxzhlmyqx80b3966w5shlnwd6v8vcs7iza4n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dchest/blake256"))
    (home-page "https://github.com/dchest/blake256")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-decred-dcrd-crypto-blake256
  (package
    (name "go-github-com-decred-dcrd-crypto-blake256")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/decred/dcrd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0m2mxrkbnr4dfan9ljxq9dd5hhqmpx5n9pd4hnwn5mlj4zgv018a"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "github.com/decred/dcrd/crypto/blake256"
        #:unpack-path
        "github.com/decred/dcrd"))
    (home-page "https://github.com/decred/dcrd")
    (synopsis "Package blake256")
    (description
      "Package blake256 implements BLAKE-256 and BLAKE-224 hash functions (SHA-3
candidate).")
    (license license:isc)))

(define-public go-github-com-decred-base58
  (package
    (name "go-github-com-decred-base58")
    (version "1.0.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/decred/base58")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19cr56g0k0wi9cppypyk47qzhvmwhj4ifbvgpwisgrqi9rq0zdc3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/decred/base58"))
    (propagated-inputs
      `(("go-github-com-decred-dcrd-crypto-blake256"
         ,go-github-com-decred-dcrd-crypto-blake256)))
    (home-page "https://github.com/decred/base58")
    (synopsis "base58")
    (description
      "Package base58 provides an API for working with modified base58 and Base58Check
encodings.")
    (license license:isc)))

(define-public go-github-com-decred-slog
  (package
    (name "go-github-com-decred-slog")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/decred/slog")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1pnrldkvpq04v6392ghyzsccaqd2vf5wxbw38jk23w77f6hqfxy2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/decred/slog"))
    (home-page "https://github.com/decred/slog")
    (synopsis "slog")
    (description
      "Package slog defines an interface and default implementation for subsystem
logging.")
    (license license:isc)))

(define-public go-github-com-jessevdk-go-flags
  (package
    (name "go-github-com-jessevdk-go-flags")
    (version "1.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jessevdk/go-flags")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "13ixw1yx4bvcj66lkc8zgwf9j7gkvj686g991gycdsafvdvca0lj"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jessevdk/go-flags"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/jessevdk/go-flags")
    (synopsis "go-flags: a go library for parsing command line arguments")
    (description
      "Package flags provides an extensive command line option parser.  The flags
package is similar in functionality to the go built-in flag package but provides
more options and uses reflection to provide a convenient and succinct way of
specifying command line options.")
    (license license:bsd-3)))

(define-public go-github-com-jrick-bitset
  (package
    (name "go-github-com-jrick-bitset")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jrick/bitset")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "140mqhxmi00hqmyxpbh5k1i1bacyrbgn18n05yiqnkgzhsnf11sd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jrick/bitset"))
    (home-page "https://github.com/jrick/bitset")
    (synopsis "bitset")
    (description
      "Package bitset provides bitset implementations for bit packing binary values
into pointers and bytes.")
    (license license:isc)))

(define-public go-github-com-jrick-logrotate
  (package
    (name "go-github-com-jrick-logrotate")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jrick/logrotate")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0srl6figwjqpi3nbp7br8sxpmvh4v8lzbny1b4lar4ny0156p5nl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jrick/logrotate"))
    (home-page "https://github.com/jrick/logrotate")
    (synopsis "logrotate: slightly better than")
    (description "Command logrotate writes and rotates logs read from stdin.")
    (license license:bsd-2)))

(define-public go-github-com-decred-dcrd
  (package
    (name "go-github-com-decred-dcrd")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/decred/dcrd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0pmm6ahk0s0qgswvchw60vi0qx9ji3dry8dyd85lhav7xlhpy2mx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/decred/dcrd"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-jrick-logrotate" ,go-github-com-jrick-logrotate)
        ("go-github-com-jrick-bitset" ,go-github-com-jrick-bitset)
        ("go-github-com-jessevdk-go-flags" ,go-github-com-jessevdk-go-flags)
        ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)
        ("go-github-com-decred-slog" ,go-github-com-decred-slog)
        ("go-github-com-decred-base58" ,go-github-com-decred-base58)
        ("go-github-com-dchest-siphash" ,go-github-com-dchest-siphash)
        ("go-github-com-dchest-blake256" ,go-github-com-dchest-blake256)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-btcsuite-winsvc" ,go-github-com-btcsuite-winsvc)
        ("go-github-com-btcsuite-snappy-go" ,go-github-com-btcsuite-snappy-go)
        ("go-github-com-btcsuite-goleveldb" ,go-github-com-btcsuite-goleveldb)
        ("go-github-com-btcsuite-go-socks" ,go-github-com-btcsuite-go-socks)
        ("go-github-com-agl-ed25519" ,go-github-com-agl-ed25519)))
    (home-page "https://github.com/decred/dcrd")
    (synopsis "dcrd")
    (description "dcrd is a full-node Decred implementation written in Go.")
    (license license:isc)))

(define-public go-gopkg-in-tomb-v1
  (package
    (name "go-gopkg-in-tomb-v1")
    (version "1.0.0-20141024135613-dd632973f1e7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/tomb.v1")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1lqmq1ag7s4b3gc3ddvr792c5xb5k6sfn0cchr3i2s7f1c231zjv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gopkg.in/tomb.v1"
                     #:unpack-path "gopkg.in/tomb.v1"
                     #:tests? #f))
    (home-page "https://gopkg.in/tomb.v1")
    (synopsis "Installation and usage")
    (description
      "The tomb package offers a conventional API for clean goroutine termination.")
    (license license:bsd-3)))


(define-public go-github-com-go-task-slim-sprig
  (package
    (name "go-github-com-go-task-slim-sprig")
    (version "2.20.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-task/slim-sprig")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0p14axjkiznjrhl7gbmlc1fliq125xkckn1y9vy2jalslzrgprvv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-task/slim-sprig"))
    (propagated-inputs (list go-github-com-masterminds-semver
                             go-github-com-masterminds-goutils
                             go-github-com-google-uuid
                             go-github-com-stretchr-testify
                             go-github-com-huandu-xstrings
                             go-github-com-imdario-mergo
                             go-golang-org-x-crypto))
    (home-page "https://github.com/go-task/slim-sprig")
    (synopsis "Sprig: Template functions for Go templates")
    (description "Sprig: Template functions for Go.")
    (license license:expat)))


(define-public go-github-com-protonmail-go-crypto
  (package
    (name "go-github-com-protonmail-go-crypto")
    (version "0.0.0-20220113124808-70ae35bab23f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ProtonMail/go-crypto")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dpalb7cd2xs0hji80zv378vf8gx4ph7b8xdd05kvbskyw67fb7n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ProtonMail/go-crypto"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   ;; Source-only package
                   (delete 'build))))
      
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)))
    (home-page "https://github.com/ProtonMail/go-crypto")
    (synopsis #f)
    (description
      "This module is backwards compatible with x/crypto/openpgp, so you can simply
replace all imports of @code{golang.org/x/crypto/openpgp} with
@code{github.com/ProtonMail/go-crypto/openpgp}.")
    (license license:bsd-3)))

(define-public go-github-com-emersion-go-textwrapper
  (package
    (name "go-github-com-emersion-go-textwrapper")
    (version "0.0.0-20200911093747-65d896831594")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emersion/go-textwrapper")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1lh9d7zvj6gm1rr6sv5xlagklgx9d666hq5srd37a4sdcjkbiqmq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/emersion/go-textwrapper"))
    (home-page "https://github.com/emersion/go-textwrapper")
    (synopsis "go-textwrapper")
    (description
      "This package provides a writer that wraps long text lines to a specified length.")
    (license license:expat)))

(define-public go-github-com-emersion-go-message
  (package
    (name "go-github-com-emersion-go-message")
    (version "0.15.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emersion/go-message")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0h4zzllz6d68b9dqcaqqyrl346jqsv0saavgi2nqi8a5fjpgwjh5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/emersion/go-message"))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-github-com-emersion-go-textwrapper"
         ,go-github-com-emersion-go-textwrapper)))
    (home-page "https://github.com/emersion/go-message")
    (synopsis "go-message")
    (description
      "Package message implements reading and writing multipurpose messages.")
    (license license:expat)))

(define-public go-github-com-emersion-go-pgpmail
  (package
    (name "go-github-com-emersion-go-pgpmail")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emersion/go-pgpmail")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ar26b0apw5bxn58qfn1a79cxigbmrqm1irh1rb7x57fydihc7wm"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-pgpmail"
                     #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-emersion-go-message"
         ,go-github-com-emersion-go-message)
        ("go-github-com-protonmail-go-crypto"
         ,go-github-com-protonmail-go-crypto)))
    (home-page "https://github.com/emersion/go-pgpmail")
    (synopsis "go-pgpmail")
    (description
      "Package pgpmail implements PGP encryption for e-mail messages.")
    (license license:expat)))

(define-public go-github-com-emersion-go-smtp
  (package
    (name "go-github-com-emersion-go-smtp")
    (version "0.15.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/emersion/go-smtp")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vhc0vpjd4yhxk6wrh01sdpi7nprjn98s46yy82xwlkm0cskl0h7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/emersion/go-smtp"))
    (propagated-inputs
      `(("go-github-com-emersion-go-sasl" ,go-github-com-emersion-go-sasl)))
    (home-page "https://github.com/emersion/go-smtp")
    (synopsis "go-smtp")
    (description
      "Package smtp implements the Simple Mail Transfer Protocol as defined in
@url{https://rfc-editor.org/rfc/rfc5321.html,RFC 5321}.")
    (license license:expat)))

(define-public go-github-com-fernet-fernet-go
  (package
    (name "go-github-com-fernet-fernet-go")
    (version "0.0.0-20211208181803-9f70042a33ee")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fernet/fernet-go")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "040hcdq0ajmh26w2kxhalz36rzbikxf8a01azrvblx1xhmspdpir"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/fernet/fernet-go"))
    (home-page "https://github.com/fernet/fernet-go")
    (synopsis #f)
    (description
      "Package fernet takes a user-provided message (an arbitrary sequence of bytes), a
key (256 bits), and the current time, and produces a token, which contains the
message in a form that can't be read or altered without the key.")
    (license license:expat)))

(define-public go-github-com-go-chi-chi
  (package
    (name "go-github-com-go-chi-chi")
    (version "1.5.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-chi/chi")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jpa4r5h15gkpfmb6xq1hamv0q20i8bdpw3kh7dw4n1v7pshjsr8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-chi/chi"))
    (home-page "https://github.com/go-chi/chi")
    (synopsis #f)
    (description
      "Package chi is a small, idiomatic and composable router for building HTTP
services.")
    (license license:expat)))


(define-public go-github-com-dgryski-go-rendezvous
  (package
    (name "go-github-com-dgryski-go-rendezvous")
    (version "0.0.0-20200823014737-9f7001d12a5f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dgryski/go-rendezvous")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hhdbsm5k19kh1fyxs4aibza9jylils4p3555lr8xalhj2iz3zlz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dgryski/go-rendezvous"))
    (home-page "https://github.com/dgryski/go-rendezvous")
    (synopsis #f)
    (description #f)
    (license license:expat)))

(define-public go-github-com-chzyer-logex
  (package
    (name "go-github-com-chzyer-logex")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/chzyer/logex")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07ksz43a8kvx0hm8qji6kb1xm7fbwmwapcvcq9zpc8v337jggs4g"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/chzyer/logex"
                 #:tests? #f))
    (home-page "https://github.com/chzyer/logex")
    (synopsis "Logex")
    (description
      "An golang log lib, supports tracing and level, wrap by standard log lib")
    (license license:expat)))

(define-public go-github-com-chzyer-readline
  (package
    (name "go-github-com-chzyer-readline")
    (version "0.0.0-20180603132655-2972be24d48e")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/chzyer/readline")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "104q8dazj8yf6b089jjr82fy9h1g80zyyzvp3g8b44a7d8ngjj6r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/chzyer/readline"))
    (home-page "https://github.com/chzyer/readline")
    (synopsis "Guide")
    (description
      "Readline is a pure go implementation for GNU-Readline kind library.")
    (license license:expat)))

(define-public go-github-com-chzyer-test
  (package
    (name "go-github-com-chzyer-test")
    (version "0.0.0-20210722231415-061457976a23")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/chzyer/test")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jjskijacwzz0qxzrbwsglpg5vil7v4xaq8l40r2fhd2icl9hz7a"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/chzyer/test"
                               #:tests? #f))
    (propagated-inputs (list go-github-com-chzyer-logex))
    (home-page "https://github.com/chzyer/test")
    (synopsis "test")
    (description #f)
    (license license:expat)))

(define-public go-github-com-ianlancetaylor-demangle
  (package
    (name "go-github-com-ianlancetaylor-demangle")
    (version "0.0.0-20220319035150-800ac71e25c2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ianlancetaylor/demangle")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1zzwaiqcm889ldybh26nfs75czs1iwb3k7gp8ng10dhs40nvlk27"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ianlancetaylor/demangle"))
    (home-page "https://github.com/ianlancetaylor/demangle")
    (synopsis "github.com/ianlancetaylor/demangle")
    (description
      "Package demangle defines functions that demangle GCC/LLVM C++ and Rust symbol
names.  This package recognizes names that were mangled according to the C++ ABI
defined at
@url{http://codesourcery.com/cxx-abi/,http://codesourcery.com/cxx-abi/} and the
Rust ABI defined at
@url{https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html,https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html}")
    (license license:bsd-3)))

(define-public go-github-com-google-pprof
  (package
    (name "go-github-com-google-pprof")
    (version "0.0.0-20220401020641-b5a4dc8f4f2a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/pprof")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "010hi6wc1n295hwkxwzzikkrjl3xnbm8f1nvxfhh3nlqlnv7n0bp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/pprof"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-ianlancetaylor-demangle"
         ,go-github-com-ianlancetaylor-demangle)
        ("go-github-com-chzyer-test" ,go-github-com-chzyer-test)
        ("go-github-com-chzyer-readline" ,go-github-com-chzyer-readline)
        ("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)))
    (home-page "https://github.com/google/pprof")
    (synopsis "Introduction")
    (description
      "pprof is a tool for collection, manipulation and visualization of performance
profiles.")
    (license license:asl2.0)))

(define-public go-github-com-nxadm-tail
  (package
    (name "go-github-com-nxadm-tail")
    (version "1.4.8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nxadm/tail")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1j2gi485fhwdpmyzn42wk62103fclwbfywg42p275z1qv2bsz1rc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nxadm/tail"))
    (propagated-inputs
      `(("go-gopkg-in-tomb-v1" ,go-gopkg-in-tomb-v1)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)))
    (home-page "https://github.com/nxadm/tail")
    (synopsis "tail functionality in Go")
    (description
      "nxadm/tail provides a Go library that emulates the features of the BSD `tail`
program.  The library comes with full support for truncation/move detection as
it is designed to work with log rotation tools.  The library works on all
operating systems supported by Go, including POSIX systems like Linux and *BSD,
and MS Windows.  Go 1.9 is the oldest compiler release supported.")
    (license license:expat)))

(define-public go-github-com-go-redis-redis-v8
  (package
    (name "go-github-com-go-redis-redis-v8")
    (version "8.11.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-redis/redis")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0a126m4k8mjavxxyqwmhkyvh54sn113l85mx5zmjph6hlnwzn9cm"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-redis/redis/v8"
       ;;dial tcp 127.0.0.1:6379: connect: connection refused
       #:tests? #f))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-gopkg-in-tomb-v1" ,go-gopkg-in-tomb-v1)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-nxadm-tail" ,go-github-com-nxadm-tail)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)
        ("go-github-com-onsi-gomega" ,go-github-com-onsi-gomega)
        ("go-github-com-onsi-ginkgo" ,go-github-com-onsi-ginkgo)
        ("go-github-com-dgryski-go-rendezvous"
         ,go-github-com-dgryski-go-rendezvous)
        ("go-github-com-cespare-xxhash-v2" ,go-github-com-cespare-xxhash-v2)))
    (home-page "https://github.com/go-redis/redis")
    (synopsis "Redis client for Go")
    (description "Package redis implements a Redis client.")
    (license license:bsd-2)))

(define-public go-github-com-hashicorp-golang-lru
  (package
    (name "go-github-com-hashicorp-golang-lru")
    (version "0.5.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hashicorp/golang-lru")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1sdbymypp9vrnzp8ashw0idlxvaq0rb0alwxx3x8g27yjlqi9jfn"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/hashicorp/golang-lru"))
    (home-page "https://github.com/hashicorp/golang-lru")
    (synopsis "golang-lru")
    (description
      "Package lru provides three different LRU caches of varying sophistication.")
    (license license:mpl2.0)))

(define-public go-github-com-kavu-go-reuseport
  (package
    (name "go-github-com-kavu-go-reuseport")
    (version "1.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kavu/go_reuseport")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07sqycqcbw2vn60z2qs5vdjlkzpjp1yigypgy670mn0gq6qycd35"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/kavu/go_reuseport"))
    (home-page "https://github.com/kavu/go_reuseport")
    (synopsis "GO_REUSEPORT")
    (description
      "Package reuseport provides a function that returns a net.Listener powered by a
net.FileListener with a SO_REUSEPORT option set to the socket.")
    (license license:expat)))

(define-public go-github-com-vaughan0-go-ini
  (package
    (name "go-github-com-vaughan0-go-ini")
    (version "0.0.0-20130923145212-a98ad7ee00ec")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vaughan0/go-ini")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1l1isi3czis009d9k5awsj4xdxgbxn4n9yqjc1ac7f724x6jacfa"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/vaughan0/go-ini"))
    (home-page "https://github.com/vaughan0/go-ini")
    (synopsis "go-ini")
    (description
      "Package ini provides functions for parsing INI configuration files.")
    (license license:expat)))

(define-public go-github-com-vektah-gqlparser
  (package
    (name "go-github-com-vektah-gqlparser")
    (version "1.3.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vektah/gqlparser")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19xp6gpswfi24023jpjvmwijvflshl0jvwxd2xyf4c2914qcaw0r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/vektah/gqlparser"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-sergi-go-diff" ,go-github-com-sergi-go-diff)
        ("github-com-andreyvit-diff" ,go-github-com-andreyvit-diff)
        ("go-github-com-agnivade-levenshtein"
         ,go-github-com-agnivade-levenshtein)))
    (home-page "https://github.com/vektah/gqlparser")
    (synopsis "gqlparser")
    (description
      "This is a parser for graphql, written to mirror the graphql-js reference
implementation as closely while remaining idiomatic and easy to use.")
    (license license:expat)))

(define-public go-github-com-arbovm-levenshtein
  (package
    (name "go-github-com-arbovm-levenshtein")
    (version "0.0.0-20160628152529-48b4e1c0c4d0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/arbovm/levenshtein")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0nmx2iip8xpnbmy6gvqpc9ikizr33dr40xgv746h0b0by8n7rv7y"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/arbovm/levenshtein"))
    (home-page "https://github.com/arbovm/levenshtein")
    (synopsis "Levenshtein Distance")
    (description
      "@url{http://golang.org,Go} package to calculate the
@url{http://en.wikipedia.org/wiki/Levenshtein_distance,Levenshtein Distance}")
    (license license:bsd-3)))

(define-public go-github-com-dgryski-trifles-leven
  (package
    (name "go-github-com-dgryski-trifles")
    (version "0.0.0-20211222222045-8cbc5c9974ec")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dgryski/trifles")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ra8kicd30dd8i0hd4jfici75z5c0bh3fb73w0p9j06ybq4lw13n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dgryski/trifles/leven"
                 #:unpack-path "github.com/dgryski/trifles"))
    (home-page "https://github.com/dgryski/trifles")
    (synopsis #f)
    (description #f)
    (license license:expat)))

(define-public go-github-com-agnivade-levenshtein
  (package
    (name "go-github-com-agnivade-levenshtein")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/agnivade/levenshtein")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0cq6jc032gxssiqnlkq3y3i1b19qsg73ysgxfbn6hwf8qnrzc2xn"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/agnivade/levenshtein"))
    (propagated-inputs
      `(("go-github-com-dgryski-trifles-leven" ,go-github-com-dgryski-trifles-leven)
        ("go-github-com-arbovm-levenshtein"
         ,go-github-com-arbovm-levenshtein)))
    (home-page "https://github.com/agnivade/levenshtein")
    (synopsis "levenshtein")
    (description
      "Package levenshtein is a Go implementation to calculate Levenshtein Distance.")
    (license license:expat)))

(define-public go-github-com-andreyvit-diff
  (package
    (name "go-github-com-andreyvit-diff")
    (version "0.0.0-20170406064948-c7f18ee00883")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/andreyvit/diff")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1s4qjkxig5yqahpzfl4xqh4kzi9mymdpkzq6kj3f4dr5dl3hlynr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/andreyvit/diff"))
    (propagated-inputs
     `(("go-github-com-sergi-go-diff" ,go-github-com-sergi-go-diff)))
    (home-page "https://github.com/andreyvit/diff")
    (synopsis "diff")
    (description
      "diff provides quick and easy string diffing functions based on
github.com/sergi/go-diff, mainly for diffing strings in tests")
    (license license:expat)))

(define-public go-github-com-vektah-gqlparser-v2
  (package
    (name "go-github-com-vektah-gqlparser-v2")
    (version "2.4.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vektah/gqlparser")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nwcfjllxx5694gky123nyx2jwpb7bl39ck4yk2ah1j0gb1z64yr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/vektah/gqlparser/v2"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-sergi-go-diff" ,go-github-com-sergi-go-diff)
        ("go-github-com-andreyvit-diff" ,go-github-com-andreyvit-diff)
        ("go-github-com-agnivade-levenshtein"
         ,go-github-com-agnivade-levenshtein)))
    (home-page "https://github.com/vektah/gqlparser")
    (synopsis "gqlparser")
    (description
      "This is a parser for graphql, written to mirror the graphql-js reference
implementation as closely while remaining idiomatic and easy to use.")
    (license license:expat)))


(define-public go-github-com-golang-protobuf
  (package
    (name "go-github-com-golang-protobuf")
    (version "1.5.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/protobuf")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1mh5fyim42dn821nsd3afnmgscrzzhn3h8rag635d2jnr23r1zhk"))))
    (build-system go-build-system)
    (arguments
     `(#:unpack-path "github.com/golang/protobuf"
       #:import-path "github.com/golang/protobuf/proto"
       ;;#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda arguments
             (for-each
              (lambda (directory)
                (apply (assoc-ref %standard-phases 'install)
                       `(,@arguments #:import-path ,directory)))
              (list "github.com/golang/protobuf")))))))
    (native-inputs (list go-golang-org-x-sync
                         go-github-com-google-go-cmp-cmp))
    (propagated-inputs
     (list go-google-golang-org-protobuf-1.26))
    (synopsis "Go support for Protocol Buffers")
    (description "This package provides Go support for the Protocol Buffers
data serialization format.")
    (home-page "https://github.com/golang/protobuf")
    (license license:bsd-3)))


(define-public go-github-com-dgrijalva-jwt-go
  (package
    (name "go-github-com-dgrijalva-jwt-go")
    (version "3.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dgrijalva/jwt-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "08m27vlms74pfy5z79w67f9lk9zkx6a9jd68k3c4msxy75ry36mp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dgrijalva/jwt-go"))
    (home-page "https://github.com/dgrijalva/jwt-go")
    (synopsis "jwt-go")
    (description
      "Package jwt is a Go implementation of JSON Web Tokens:
@url{http://self-issued.info/docs/draft-jones-json-web-token.html,http://self-issued.info/docs/draft-jones-json-web-token.html}")
    (license license:expat)))

(define-public go-github-com-azure-go-autorest
  (package
    (name "go-github-com-azure-go-autorest")
    (version "14.2.0")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/Azure/go-autorest")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0z3j6jj2f9r208vgrg4byaczxbygiw97k2cm83qd64bl82vhyjix"))))
    (build-system go-build-system)
    (propagated-inputs (list go-github-com-dgrijalva-jwt-go)) 
    (arguments
     '(#:import-path "github.com/Azure/go-autorest"))
    (home-page "https://github.com/azure/go-autorest")
    (synopsis "go-autorest")
    (description
      "Package go-autorest provides an HTTP request client for use with
Autorest-generated API client packages.")
    (license license:asl2.0)))

(define-public go-github-com-kevinmbeaulieu-eq-go
  (package
    (name "go-github-com-kevinmbeaulieu-eq-go")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kevinmbeaulieu/eq-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0gc2r1yrjg206swlx1vrvb92s2m1y6752yhbh869h2m47q2p4y38"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/kevinmbeaulieu/eq-go/eq-go"
                               #:unpack-path "github.com/kevinmbeaulieu/eq-go"))
    (home-page "https://github.com/kevinmbeaulieu/eq-go")
    (synopsis "eq[uivalent]-go")
    (description
      "Check whether two Go source directories contain equivalent code.")
    (license license:asl2.0)))

(define-public go-github-com-logrusorgru-aurora-v3
  (package
    (name "go-github-com-logrusorgru-aurora-v3")
    (version "3.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/logrusorgru/aurora")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0z7cgj8gl69271d0ag4f4yjbsvbrnfibc96cs01spqf5krv2rzjc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/logrusorgru/aurora/v3"))
    (home-page "https://github.com/logrusorgru/aurora")
    (synopsis "Aurora")
    (description "Package aurora implements ANSI-colors")
    (license license:unlicense)))

(define-public go-github-com-matryer-moq
  (package
    (name "go-github-com-matryer-moq")
    (version "0.2.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/matryer/moq")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0syfa716bqjl075ji1kaf8i56xphcjlpraxmafahzk70qq7wmvwr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/matryer/moq"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-pmezard-go-difflib"
         ,go-github-com-pmezard-go-difflib)))
    (home-page "https://github.com/matryer/moq")
    (synopsis "What is Moq?")
    (description "Interface mocking tool for go generate.")
    (license license:expat)))

(define-public go-github-com-99designs-gqlgen
  (package
    (name "go-github-com-99designs-gqlgen")
    (version "0.17.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/99designs/gqlgen")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ap3pf4g1z6gn42rrf9qsab1n764mjr727la5x10cvaffsmlira7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/99designs/gqlgen"
                               #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   ;; Source-only package
                   (delete 'build))))
    (propagated-inputs
      `(("go-github-com-cpuguy83-go-md2man-v2"
         ,go-github-com-cpuguy83-go-md2man-v2)
        ("go-github-com-agnivade-levenshtein"
         ,go-github-com-agnivade-levenshtein)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        
        ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-vektah-gqlparser-v2"
         ,go-github-com-vektah-gqlparser-v2)
        ("go-github-com-urfave-cli-v2" ,go-github-com-urfave-cli-v2)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-mitchellh-mapstructure"
         ,go-github-com-mitchellh-mapstructure)
        ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
        ("go-github-com-mattn-go-colorable" ,go-github-com-mattn-go-colorable)
        ("go-github-com-matryer-moq" ,go-github-com-matryer-moq)
        ("go-github-com-logrusorgru-aurora-v3"
         ,go-github-com-logrusorgru-aurora-v3)
        ("go-github-com-kevinmbeaulieu-eq-go"
         ,go-github-com-kevinmbeaulieu-eq-go)
        ("go-github-com-hashicorp-golang-lru"
         ,go-github-com-hashicorp-golang-lru)
        ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)))
    (home-page "https://github.com/99designs/gqlgen")
    (synopsis "gqlgen")
    (description
      "@url{https://github.com/99designs/gqlgen,gqlgen} is a Go library for building
GraphQL servers without any fuss.")
    (license license:expat)))

(define-public go-github-com-data-dog-go-sqlmock
  (package
    (name "go-github-com-data-dog-go-sqlmock")
    (version "1.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/DATA-DOG/go-sqlmock")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19vn6xf3wqam312g30f7qdcrh8km3wzqsa43qipyz2y5ma2l7pd4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/DATA-DOG/go-sqlmock"))
    (home-page "https://github.com/DATA-DOG/go-sqlmock")
    (synopsis "Sql driver mock for Golang")
    (description
      "Package sqlmock is a mock library implementing sql driver.  Which has one and
only purpose - to simulate any sql driver behavior in tests, without needing a
real database connection.  It helps to maintain correct **TDD** workflow.")
    (license license:bsd-3)))

(define-public go-github-com-lann-builder
  (package
    (name "go-github-com-lann-builder")
    (version "0.0.0-20180802200727-47ae307949d0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/lann/builder")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1kg9jy1rciznj627hafpq2mi7hr5d3ssgqcpwrm3bnlk9sqnydil"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/lann/builder"))
    (propagated-inputs (list go-github-com-lann-ps))
    (home-page "https://github.com/lann/builder")
    (synopsis "Builder - fluent immutable builders for Go")
    (description
      "Package builder provides a method for writing fluent immutable builders.")
    (license license:expat)))

(define-public go-github-com-lann-ps
  (package
    (name "go-github-com-lann-ps")
    (version "0.0.0-20150810152359-62de8c46ede0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/lann/ps")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10yhcyymypvdiiipchsp80jbglk8c4r7lq7h54v9f4mxmvz6xgf7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/lann/ps"))
    (home-page "https://github.com/lann/ps")
    (synopsis "ps")
    (description
      "Fully persistent data structures.  A persistent data structure is a data
structure that always preserves the previous version of itself when it is
modified.  Such data structures are effectively immutable, as their operations
do not update the structure in-place, but instead always yield a new structure.")
    (license license:expat)))

(define-public go-github-com-masterminds-squirrel
  (package
    (name "go-github-com-masterminds-squirrel")
    (version "1.5.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Masterminds/squirrel")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01md7nvjq8mqigid4jzh8hszqn9vgxki55bvpddkdbr6yf6mniwb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Masterminds/squirrel"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-lann-ps" ,go-github-com-lann-ps)
        ("go-github-com-lann-builder" ,go-github-com-lann-builder)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/Masterminds/squirrel")
    (synopsis "Squirrel is \"complete\".")
    (description "Package squirrel provides a fluent SQL generator.")
    (license license:expat)))



(define-public go-cloud-google-com-go-storage
  (package
    (name "go-cloud-google-com-go-storage")
    (version "1.22.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googleapis/google-cloud-go")
             (commit (string-append "storage/v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1y2nl7ilzcckffgcxh8kn45h0w111x1i2crrifcc2zfcp4p9dwr4"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "cloud.google.com/go"
       #:import-path "cloud.google.com/go/storage"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'build))
       ))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-google-golang-org-api" ,go-google-golang-org-api)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
        ("go-go-opencensus-io" ,go-go-opencensus-io)
        ("go-github-com-googleapis-gax-go-v2" ,go-github-com-googleapis-gax-go-v2)
        ("go-github-com-googleapis-go-type-adapters"
         ,go-github-com-googleapis-go-type-adapters)
        
        ("go-github-com-google-martian-v3" ,go-github-com-google-martian-v3)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page
     "https://pkg.go.dev/cloud.google.com/go/storage")
    (synopsis
     "Go wrapper for Google Compute Engine metadata service")
    (description
     "This package provides access to Google Compute Engine (GCE) metadata and
API service accounts for Go.")
    (license license:asl2.0)))

(define-public go-cloud-google-com-go-compute
  (package
    (name "go-cloud-google-com-go-compute")
    (version "1.6.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googleapis/google-cloud-go")
             (commit (string-append "compute/v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "09rz1rxqxxb60i9qgs91gpyc1pj06vniv1pa2s8z8ayb0f90vg2w"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "cloud.google.com/go"
       #:import-path "cloud.google.com/go/compute"
       #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))              ))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-google-golang-org-api" ,go-google-golang-org-api)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
        ("go-go-opencensus-io" ,go-go-opencensus-io)
        ("go-github-com-googleapis-gax-go"
         ,go-github-com-googleapis-gax-go)
        ("go-github-com-google-martian-v3" ,go-github-com-google-martian-v3)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page
     "https://pkg.go.dev/cloud.google.com/go/compute")
    (synopsis
     "Go wrapper for Google Compute Engine metadata service")
    (description
     "This package provides access to Google Compute Engine (GCE) metadata and
API service accounts for Go.")
    (license license:asl2.0)))

(define-public go-github-com-golang-groupcache
  (package
    (name "go-github-com-golang-groupcache")
    (version "0.0.0-20210331224755-41bb18bfe9da")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/groupcache")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "07amgr8ji4mnq91qbsw2jlcmw6hqiwdf4kzfdrj8c4b05w4knszc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/groupcache"))
    (propagated-inputs (list go-github-com-golang-protobuf-proto))
    (home-page "https://github.com/golang/groupcache")
    (synopsis "groupcache")
    (description
      "Package groupcache provides a data loading mechanism with caching and
de-duplication that works across a set of peer processes.")
    (license license:asl2.0)))

(define-public go-github-com-cespare-xxhash-v2
  (package
    (name "go-github-com-cespare-xxhash-v2")
    (version "2.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cespare/xxhash")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1f3wyr9msnnz94szrkmnfps9wm40s5sp9i4ak0kl92zcrkmpy29a"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cespare/xxhash/v2"))
    (home-page "https://github.com/cespare/xxhash")
    (synopsis "xxhash")
    (description
      "Package xxhash implements the 64-bit variant of xxHash (XXH64) as described at
@url{http://cyan4973.github.io/xxHash/,http://cyan4973.github.io/xxHash/}.")
    (license license:expat)))

(define-public go-github-com-cncf-udpa-go
  (package
    (name "go-github-com-cncf-udpa-go")
    (version "0.0.0-20220112060539-c52dc94e7fbe")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cncf/udpa")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1i3nm1hxc4am3a7d9wxvf0ia420qg1xpwflcc0z19lxn952bdbnd"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "github.com/cncf/udpa/go"
        #:unpack-path
        "github.com/cncf/udpa"
        #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    
    (propagated-inputs
      `(("go-github-com-cncf-xds-go" ,go-github-com-cncf-xds-go)))
    (home-page "https://github.com/cncf/udpa")
    (synopsis "Description")
    (description
      "This library has been deprecated in favor of @code{github.com/cncf/xds/go}.  All
users are recommended to switch their imports.")
    (license license:asl2.0)))

(define-public go-github-com-census-instrumentation-opencensus-proto
  (package
    (name "go-github-com-census-instrumentation-opencensus-proto")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/census-instrumentation/opencensus-proto")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ngp6jb345xahsijjpwwlcy2giymyzsy7kdhkrvgjafqssk6aw6f"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/census-instrumentation/opencensus-proto/gen-go/metrics/v1"
                     #:unpack-path "github.com/census-instrumentation/opencensus-proto"))
    (propagated-inputs (list go-github-com-golang-protobuf-proto
                             go-google-golang-org-protobuf))
    (home-page "https://github.com/census-instrumentation/opencensus-proto")
    (synopsis
      "OpenCensus Proto - Language Independent Interface Types For OpenCensus")
    (description
      "Census provides a framework to define and collect stats against metrics and to
break those stats down across user-defined dimensions.")
    (license license:asl2.0)))

(define-public go-github-com-cncf-xds-go
  (package
    (name "go-github-com-cncf-xds-go")
    (version "0.0.0-20220330162227-eded343319d0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cncf/xds")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0lrd3bm30n19hvc65h6kdqhckjhfpij4dahz5kxn2bs8sqp2xxy0"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "github.com/cncf/xds/go/xds/core/v3"
        #:unpack-path
        "github.com/cncf/xds"
        #:phases
        (modify-phases %standard-phases
        (replace 'install
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'install)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/cncf/xds/go")))))))
    (propagated-inputs
      `(;;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ;;("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ("go-github-com-envoyproxy-protoc-gen-validate"
         ,go-github-com-envoyproxy-protoc-gen-validate)))
    (home-page "https://github.com/cncf/xds")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-github-com-iancoleman-strcase
  (package
    (name "go-github-com-iancoleman-strcase")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/iancoleman/strcase")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0rgfn6zz1r9h7yic3b0dcqq900bi638d6qgcyy9jhvk00f4dlg5j"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/iancoleman/strcase"))
    (home-page "https://github.com/iancoleman/strcase")
    (synopsis "strcase")
    (description
      "Package strcase converts strings to various cases.  See the conversion table
below:")
    (license license:expat)))

(define-public go-github-com-lyft-protoc-gen-star
  (package
    (name "go-github-com-lyft-protoc-gen-star")
    (version "0.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/lyft/protoc-gen-star")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0mbgwnd3nhafx9hvjbyyl38x1ch1b4nmk03pisybqfq1qyadx93q"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/lyft/protoc-gen-star"
                 #:tests? #f))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-spf13-afero" ,go-github-com-spf13-afero)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page "https://github.com/lyft/protoc-gen-star")
    (synopsis "protoc-gen-star (PG*)")
    (description "Package pgs provides a library for building protoc plugins")
    (license license:asl2.0)))

(define-public go-github-com-envoyproxy-protoc-gen-validate
  (package
    (name "go-github-com-envoyproxy-protoc-gen-validate")
    (version "0.6.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/envoyproxy/protoc-gen-validate")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1licayw2yww4b1zq84pp6vpsr5731n8x8642cfp0rgavlfnkmsm2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/envoyproxy/protoc-gen-validate"))
    (propagated-inputs
      `(;;("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)
        ("go-golang-org-x-lint" ,go-golang-org-x-lint)
        ;;("go-github-com-spf13-afero" ,go-github-com-spf13-afero)
        ("go-github-com-lyft-protoc-gen-star"
         ,go-github-com-lyft-protoc-gen-star)
        ("go-github-com-iancoleman-strcase"
          ,go-github-com-iancoleman-strcase)
      ))
    (home-page "https://github.com/envoyproxy/protoc-gen-validate")
    (synopsis "protoc-gen-validate (PGV)")
    (description
      "PGV is a protoc plugin to generate polyglot message validators.  While protocol
buffers effectively guarantee the types of structured data, they cannot enforce
semantic rules for values.  This plugin adds support to protoc-generated code to
validate such constraints.")
    (license license:asl2.0)))

(define-public go-github-com-antihax-optional
  (package
    (name "go-github-com-antihax-optional")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/antihax/optional")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ix08vl49qxr58rc6201cl97g1yznhhkwvqldslawind99js4rj0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/antihax/optional"))
    (home-page "https://github.com/antihax/optional")
    (synopsis #f)
    (description #f)
    (license license:expat)))

(define-public go-github-com-rogpeppe-fastuuid
  (package
    (name "go-github-com-rogpeppe-fastuuid")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rogpeppe/fastuuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "028acdg63zkxpjz3l639nlhki2l0canr2v5jglrmwa1wpjqcfff8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rogpeppe/fastuuid"))
    (home-page "https://github.com/rogpeppe/fastuuid")
    (synopsis "fastuuid")
    (description
      "Package fastuuid provides fast UUID generation of 192 bit universally unique
identifiers.")
    (license license:bsd-3)))

(define-public go-sigs-k8s-io-yaml
  (package
    (name "go-sigs-k8s-io-yaml")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kubernetes-sigs/yaml")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qxs0ppqwqrfqs4aywyn1h28xh1qlj5ds4drmygaz1plrxj02dqn"))))
    (build-system go-build-system)
    (arguments '(#:import-path "sigs.k8s.io/yaml"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://sigs.k8s.io/yaml")
    (synopsis "YAML marshaling and unmarshaling support for Go")
    (description
      "kubernetes-sigs/yaml is a permanent fork of
@url{https://github.com/ghodss/yaml,ghodss/yaml}.")
    (license #f)))

(define-public go-google-golang-org-appengine
  (package
    (name "go-google-golang-org-appengine")
    (version "1.6.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/appengine")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1wkipg7xxc0ha5p6c3bj0vpgq38l18441n5l6zxdhx0gzvz5z1hs"))))
    (build-system go-build-system)
    (arguments '(#:import-path "google.golang.org/appengine"))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page "https://google.golang.org/appengine")
    (synopsis "Go App Engine packages")
    (description
      "Package appengine provides basic functionality for Google App Engine.")
    (license license:asl2.0)))

(define-public go-github-com-grpc-ecosystem-grpc-gateway-v2
  (package
    (name "go-github-com-grpc-ecosystem-grpc-gateway-v2")
    (version "2.10.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/grpc-ecosystem/grpc-gateway")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0zffgbqgdwxa4x0l1p24kh06yx5saqs5gia8izzrzpd0mm9q26zy"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/grpc-ecosystem/grpc-gateway/v2"))
    (propagated-inputs
      `(("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-sigs-k8s-io-yaml" ,go-sigs-k8s-io-yaml)
        ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
        ("go-github-com-rogpeppe-fastuuid" ,go-github-com-rogpeppe-fastuuid)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ("go-github-com-golang-glog" ,go-github-com-golang-glog)
        ("go-github-com-antihax-optional" ,go-github-com-antihax-optional)))
    (home-page "https://github.com/grpc-ecosystem/grpc-gateway")
    (synopsis "About")
    (description
      "The gRPC-Gateway is a plugin of the Google protocol buffers compiler
@url{https://github.com/protocolbuffers/protobuf,protoc}.  It reads protobuf
service definitions and generates a reverse-proxy server which translates a
RESTful HTTP API into gRPC.  This server is generated according to the
@url{https://github.com/googleapis/googleapis/raw/master/google/api/http.proto#L46,(code
google.api.http)} annotations in your service definitions.")
    (license license:bsd-3)))

(define-public go-go-opentelemetry-io-proto-otlp
  (package
    (name "go-go-opentelemetry-io-proto-otlp")
    (version "0.16.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/open-telemetry/opentelemetry-proto-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jqpzz9zxia8fj14q7gzc04qcbq8z2716gd9186n4m4fwd0jp2ma"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "go.opentelemetry.io/proto/otlp/common/v1"
       #:unpack-path "go.opentelemetry.io/proto"))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ;;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ;;("go-github-com-grpc-ecosystem-grpc-gateway-v2" ,go-github-com-grpc-ecosystem-grpc-gateway-v2
        ))
    (home-page "https://go.opentelemetry.io/proto/otlp")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-github-com-envoyproxy-go-control-plane
  (package
    (name "go-github-com-envoyproxy-go-control-plane")
    (version "0.10.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/envoyproxy/go-control-plane")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0amjw4x1904r14ps07l3wi5vdph5v2m9c97kkrr567kxr5xpjsv3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/envoyproxy/go-control-plane/envoy/api/v2"
                               #:unpack-path "github.com/envoyproxy/go-control-plane"
               ;;#:phases 
               ))
    (propagated-inputs
      `(;;("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-go-opentelemetry-io-proto-otlp"
         ,go-go-opentelemetry-io-proto-otlp)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-prometheus-client-model"
         ,go-github-com-prometheus-client-model)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ;;("go-github-com-envoyproxy-protoc-gen-validate"  ,go-github-com-envoyproxy-protoc-gen-validate)
        ("go-github-com-cncf-xds-go" ,go-github-com-cncf-xds-go)
        ("go-github-com-census-instrumentation-opencensus-proto"
         ,go-github-com-census-instrumentation-opencensus-proto)))
    (home-page "https://github.com/envoyproxy/go-control-plane")
    (synopsis "control-plane")
    (description
      "This repository contains a Go-based implementation of an API server that
implements the discovery service APIs defined in
@url{https://github.com/envoyproxy/data-plane-api,data-plane-api}.")
    (license license:asl2.0)))

(define-public go-github-com-golang-glog
  (package
    (name "go-github-com-golang-glog")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/glog")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vm206qrvhn3d571bqcman6fnavw4y3a31ffrmv2xkk0li74h2bf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/glog"))
    (home-page "https://github.com/golang/glog")
    (synopsis "glog")
    (description
      "Package glog implements logging analogous to the Google-internal C++
INFO/ERROR/V setup.  It provides functions Info, Warning, Error, Fatal, plus
formatting variants such as Infof.  It also provides V-style logging controlled
by the -v and -vmodule=file=2 flags.")
    (license license:asl2.0)))


(define-public go-go-opencensus-io
  (package
    (name "go-go-opencensus-io")
    (version "0.23.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/census-instrumentation/opencensus-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0gw4f7inf8y2ik00yfb36xganiq9rl4w2d1a41bsjqsh83ajz2km"))))
    (build-system go-build-system)
    (arguments '(#:import-path "go.opencensus.io"))
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs
      `(;;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ;;("go-golang-org-x-net" ,go-golang-org-x-net)
        ;;("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ;;("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
        ;;("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
        ;;("go-github-com-golang-groupcache" ,go-github-com-golang-groupcache)
        ))
    (home-page "https://go.opencensus.io")
    (synopsis "OpenCensus Libraries for Go")
    (description "Package opencensus containsgo-github-com-stretchr-testif Go support for OpenCensus.")
    (license license:asl2.0)))

(define-public go-github-com-googleapis-gax-go-v2
  (package
    (name "go-github-com-googleapis-gax-go-v2")
    (version "2.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/googleapis/gax-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "14rh4y7inavsl7dv2a18hmyh806gxqixpxh8agzqqv8rmc9516xk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/googleapis/gax-go/v2"
                 #:unpack-path "github.com/googleapis/gax-go"              
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(;;("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ;;("go-google-golang-org-api" ,go-google-golang-org-api)
        ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
        ))
    (home-page "https://github.com/googleapis/gax-go")
    (synopsis #f)
    (description
      "Package gax contains a set of modules which aid the development of APIs for
clients and servers based on gRPC and Google API conventions.")
    (license license:bsd-3)))
(define-public go-golang-org-x-exp
  (package
    (name "go-golang-org-x-exp")
    (version "0.0.0-20220414153411-bcd21879b8fd")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/exp")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1dp0plzwwid7q1qrmi42hiyyhfinbby1kaf0vxbp9ybkmrqdc9f2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/exp"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)))
    (home-page "https://golang.org/x/exp")
    (synopsis "exp")
    (description
      "This subrepository holds experimental and deprecated (in the @code{old}
directory) packages.")
    (license license:bsd-3)))

(define-public go-github-com-pelletier-go-toml-v2
  (package
    (name "go-github-com-pelletier-go-toml-v2")
    (version "2.0.0-beta.8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pelletier/go-toml")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0w68awiwz1vf9i6c2xglwxwla0r9fr97rlrhxzyay865pqskdh3m"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pelletier/go-toml/v2"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/pelletier/go-toml")
    (synopsis "go-toml v2")
    (description "Package toml is a library to read and write TOML documents.")
    (license license:expat)))

(define-public go-github-com-bep-gowebp
  (package
    (name "go-github-com-bep-gowebp")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/gowebp")
               (commit (string-append "v" version))
               (recursive? #t)))
        (file-name (git-file-name name version))
        (sha256
         (base32 "1y2cjdws7962r3rp6w3cy4dwpra5s7vb061cfx92djn06fm7cdbx"))
        (modules '((guix build utils)))
       (snippet
        '(begin
           (delete-file-recursively "libwebp_src")
           #t))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/bep/gowebp/libwebp"
       #:unpack-path "github.com/bep/gowebp/"
       #:build-flags '("-tags" "dev")
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'generate-bindings
           ;; Generate bindings for system libwebp, replacing the
           ;; pre-generated bindings.
           (lambda* (#:key inputs unpack-path #:allow-other-keys)
             (mkdir-p
              (string-append "src/" unpack-path "/internal/libwebp"))
             (let ((libwebp-src
                    (string-append (assoc-ref inputs "libwebp-src") "/src")))
               (substitute* (string-append "src/" unpack-path "/gen/main.go")
                 (("filepath.Join\\(rootDir, \"libwebp_src\", \"src\"\\)")
                  (string-append "\"" libwebp-src "\""))
                 (("../../libwebp_src/src/")
                  libwebp-src)))
             (invoke "go" "generate" (string-append unpack-path "/gen"))
             #t))
         (replace 'install          
             (lambda arguments                                      
               (for-each                                             
                (lambda (directory)                                  
                  (apply (assoc-ref %standard-phases 'install)       
                         `(,@arguments #:import-path ,directory)))   
                (list "github.com/bep/gowebp/libwebp"
                      "github.com/bep/gowebp/internal"))))
         ;; tests fail so far bc permission for opening test images missing
         (replace 'check
           (lambda* (#:key tests? import-path #:allow-other-keys)
             (if tests?
                 
                 (invoke "go" "test" import-path "-tags" "dev"))
             #t)))))              
    (propagated-inputs
     (list libwebp go-golang-org-x-image))
    (native-inputs
     `(("go-github-com-frankban-quicktest" ,go-github-com-frankban-quicktest)
       ("libwebp-src" ,(package-source libwebp))))
    (home-page "https://github.com/bep/gowebp")
    (synopsis "Update libwebp version")
    (description
      "This library provides C bindings and an API for @strong{encoding} Webp images
using Google's @url{https://github.com/webmproject/libwebp,libwebp}.")
    (license license:expat)))

(define-public go-github-com-bep-debounce
  (package
    (name "go-github-com-bep-debounce")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/debounce")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vxa9cz2z0cmjixz3gjfp18fzliy9d2q7q6cz0zqs7yqbpjn5f55"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/debounce"))
    (home-page "https://github.com/bep/debounce")
    (synopsis "Go Debounce")
    (description
      "Package debounce provides a debouncer func.  The most typical use case would be
the user typing a text into a form; the UI needs an update, but let's wait for a
break.")
    (license license:expat)))

(define-public go-github-com-bep-overlayfs
  (package
    (name "go-github-com-bep-overlayfs")
    (version "0.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/overlayfs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "09c19an97hcqqhrkqk0rp2wp9lzlmzdcs0j2vg4ay17nfw642hz3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/overlayfs"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ;;("go-github-com-rogpeppe-go-internal"
        ;; ,go-github-com-rogpeppe-go-internal)
        ("go-github-com-kr-text" ,go-github-com-kr-text)
        ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
        ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ;;("go-github-com-spf13-afero" ,go-github-com-spf13-afero)
        ("go-github-com-frankban-quicktest"
         ,go-github-com-frankban-quicktest)))
    (home-page "https://github.com/bep/overlayfs")
    (synopsis #f)
    (description
      "@strong{overlayfs} is a composite filesystem (currently only) for
@url{https://github.com/spf13/afero,Afero} with similar but different semantics
compared to Afero's
@url{https://github.com/spf13/afero/raw/master/copyOnWriteFs.go,copyOnWriteFs}.
See the @url{https://godoc.org/github.com/bep/overlayfs,GoDoc} for more
information.")
    (license license:expat)))

(define-public go-github-com-bep-godartsass
  (package
    (name "go-github-com-bep-godartsass")
    (version "0.14.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/godartsass")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nfgwkhivilg2f4v39x294i998aisg6vffs7az8zmzn179v5i95c"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/godartsass"
                               #:tests? #f))
    (native-inputs (list go-github-com-frankban-quicktest))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ;;("go-github-com-frankban-quicktest" ,go-github-com-frankban-quicktest)
        ("go-github-com-cli-safeexec" ,go-github-com-cli-safeexec)
        ))
    (home-page "https://github.com/bep/godartsass")
    (synopsis #f)
    (description
      "Package godartsass provides a Go API for the Dass Sass Embedded protocol.")
    (license license:expat)))

(define-public go-github-com-cli-safeexec
  (package
    (name "go-github-com-cli-safeexec")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cli/safeexec")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1q80va3721dyw33lrnv7x3gd66kcnbsm38dv3lk7xqhii2adawmk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cli/safeexec"))
    (home-page "https://github.com/cli/safeexec")
    (synopsis "safeexec")
    (description
      "This package provides a Go module that provides a safer alternative to
@code{exec.LookPath()} on Windows.")
    (license license:bsd-2)))

(define-public go-github-com-bep-gitmap
  (package
    (name "go-github-com-bep-gitmap")
    (version "1.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/gitmap")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hvynpflvbn6g1vgrv37njn8005qxdq8l1289ymr37a2p5c1jncm"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/gitmap"
                               ;; tests need git-repository
                               #:tests? #f))
    
    (home-page "https://github.com/bep/gitmap")
    (synopsis "GitMap")
    (description
      "This package provides a fairly fast way to create a map from all the filenames
to info objects for a given revision of a Git repo.")
    (license license:expat)))

(define-public go-github-com-bep-goat
  (package
    (name "go-github-com-bep-goat")
    (version "0.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/goat")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0aziqkkvdrn6bj9ycwdknx0m5kvacajm4w96k15crv9f5ly9lny7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/goat"))
    (propagated-inputs
      `(("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-github-com-rogpeppe-go-internal"
         ,go-github-com-rogpeppe-go-internal)
        ("go-github-com-kr-text" ,go-github-com-kr-text)
        ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-frankban-quicktest"
         ,go-github-com-frankban-quicktest)))
    (home-page "https://github.com/bep/goat")
    (synopsis "GoAT: Go ASCII Tool")
    (description
      "This is a Go implementation of
@url{http://casual-effects.com/markdeep/,markdeep.mini.js}'s ASCII diagram
generation.")
    (license license:expat)))

(define-public go-github-com-nfnt-resize
  (package
    (name "go-github-com-nfnt-resize")
    (version "0.0.0-20180221191011-83c6a9932646")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nfnt/resize")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "005cpiwq28krbjf0zjwpfh63rp4s4is58700idn24fs3g7wdbwya"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nfnt/resize"))
    (home-page "https://github.com/nfnt/resize")
    (synopsis
      "This package is no longer being updated! Please look for alternatives if that bothers you.")
    (description "Package resize implements various image resizing methods.")
    (license license:isc)))

(define-public go-github-com-bep-tmc
  (package
    (name "go-github-com-bep-tmc")
    (version "0.5.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/bep/tmc")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jn3j7mh77a5bbwh3hx3r0blzbdj2fvk5wvcfa8fr45qp2zlpbhx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/bep/tmc"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-frankban-quicktest" ,go-github-com-frankban-quicktest)
        ("go-github-com-bep-debounce" ,go-github-com-bep-debounce)))
    (home-page "https://github.com/bep/tmc")
    (synopsis "How to Use")
    (description
      "This package provides round-trip serialization of typed Go maps.")
    (license license:expat)))

(define-public go-github-com-inconshreveable-mousetrap
  (package
    (name "go-github-com-inconshreveable-mousetrap")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/inconshreveable/mousetrap")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1mn0kg48xkd74brf48qf5hzp0bc6g8cf5a77w895rl3qnlpfw152"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/inconshreveable/mousetrap"))
    (home-page "https://github.com/inconshreveable/mousetrap")
    (synopsis "mousetrap")
    (description "mousetrap is a tiny library that answers a single question.")
    (license license:asl2.0)))

(define-public go-github-com-mattn-go-ieproxy
  (package
    (name "go-github-com-mattn-go-ieproxy")
    (version "0.0.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mattn/go-ieproxy")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "13pip5q07097isjp8mllwny06z8fbr36k6ikzjdvzb8ci73pmfgn"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mattn/go-ieproxy"))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)))
    (home-page "https://github.com/mattn/go-ieproxy")
    (synopsis "ieproxy")
    (description
      "Package ieproxy is a utility to retrieve the proxy parameters (especially of
Internet Explorer on windows)")
    (license license:expat)))

(define-public go-github-com-google-subcommands
  (package
    (name "go-github-com-google-subcommands")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/subcommands")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "00w7fx92696z5p3isvpg71b4023g8f686xnhy56k08vc2q1r2hhw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/subcommands"))
    (home-page "https://github.com/google/subcommands")
    (synopsis "subcommands")
    (description
      "Package subcommands implements a simple way for a single command to have many
subcommands, each of which takes arguments and so forth.")
    (license license:asl2.0)))

(define-public go-github-com-google-wire
  (package
    (name "go-github-com-google-wire")
    (version "0.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/wire")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "038c7ss7fgihh76b4lnsm7dhhhq4kjwm06f8radw454g5jdg467p"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/wire"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-google-subcommands" ,go-github-com-google-subcommands)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))
    (home-page "https://github.com/google/wire")
    (synopsis "Wire: Automated Initialization in Go")
    (description
      "Package wire contains directives for Wire code generation.  For an overview of
working with Wire, see the user guide at
@url{https://github.com/google/wire/blob/master/docs/guide.md,https://github.com/google/wire/blob/master/docs/guide.md}")
    (license license:asl2.0)))

(define-public go-github-com-puerkitobio-urlesc
  (package
    (name "go-github-com-puerkitobio-urlesc")
    (version "0.0.0-20170810143723-de5bf2ad4578")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/PuerkitoBio/urlesc")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n0srpqwbaan1wrhh2b7ysz543pjs1xw2rghvqyffg9l0g8kzgcw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/PuerkitoBio/urlesc"))
    (home-page "https://github.com/puerkitobio/urlesc")
    (synopsis "urlesc")
    (description
      "Package urlesc implements query escaping as per
@url{https://rfc-editor.org/rfc/rfc3986.html,RFC 3986}.  It contains some parts
of the net/url package, modified so as to allow some reserved characters
incorrectly escaped by net/url.  See
@url{https://github.com/golang/go/issues/5684,https://github.com/golang/go/issues/5684}")
    (license license:bsd-3)))

(define-public go-github-com-muesli-smartcrop
  (package
    (name "go-github-com-muesli-smartcrop")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/muesli/smartcrop")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10ns8nvxjpykgh1rapg1pn0p3r9qvrjifw2p23yha85d9wnk8i1x"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/muesli/smartcrop"))
    (native-inputs (list go-github-com-nfnt-resize))
    
    (propagated-inputs (list go-golang-org-x-image))
    (home-page "https://github.com/muesli/smartcrop")
    (synopsis "smartcrop")
    (description
      "Package smartcrop implements a content aware image cropping library based on
Jonas Wagner's smartcrop.js
@url{https://github.com/jwagner/smartcrop.js,https://github.com/jwagner/smartcrop.js}")
    (license license:expat)))

(define-public go-github-com-puerkitobio-purell
  (package
    (name "go-github-com-puerkitobio-purell")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/PuerkitoBio/purell")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0c525frsxmalrn55hzzsxy17ng8avkd40ga0wxfw9haxsdjgqdqy"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/PuerkitoBio/purell"))
    (home-page "https://github.com/puerkitobio/purell")
    (propagated-inputs (list go-github-com-puerkitobio-urlesc
                             go-golang-org-x-text
                             go-golang-org-x-net))
    (synopsis "Purell")
    (description
      "Package purell offers URL normalization as described on the wikipedia page:
@url{http://en.wikipedia.org/wiki/URL_normalization,http://en.wikipedia.org/wiki/URL_normalization}")
    (license license:bsd-3)))

(define-public go-github-com-armon-go-radix
  (package
    (name "go-github-com-armon-go-radix")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/armon/go-radix")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1m1k0jz9gjfrk4m7hjm7p03qmviamfgxwm2ghakqxw3hdds8v503"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/armon/go-radix"))
    (home-page "https://github.com/armon/go-radix")
    (synopsis "go-radix")
    (description
      "This package provides the @code{radix} package that implements a
@url{http://en.wikipedia.org/wiki/Radix_tree,radix tree}.  The package only
provides a single @code{Tree} implementation, optimized for sparse nodes.")
    (license license:expat)))

(define-public go-github-com-evanw-esbuild
  (package
    (name "go-github-com-evanw-esbuild")
    (version "0.14.38")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/evanw/esbuild")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "00dxl96rk5fy3176a94bxsv2ghq899pswcrfpsfnia5vh3b25wxf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/evanw/esbuild"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/evanw/esbuild")
    (synopsis "Why?")
    (description
      "

     @url{https://esbuild.github.io/,Website} |
@url{https://esbuild.github.io/getting-started/,Getting started} |
@url{https://esbuild.github.io/api/,Documentation} |
@url{https://esbuild.github.io/plugins/,Plugins} |
@url{https://esbuild.github.io/faq/,FAQ}")
    (license license:expat)))

(define-public go-github-com-disintegration-gift
  (package
    (name "go-github-com-disintegration-gift")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/disintegration/gift")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0zdki6ydqgxl7lg23f4885w8ij34sdg8xv7b7yp6c7ffi2ikk07f"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/disintegration/gift"))
    (home-page "https://github.com/disintegration/gift")
    (synopsis "GO IMAGE FILTERING TOOLKIT (GIFT)")
    (description
      "Package gift provides a set of useful image processing filters.")
    (license license:expat)))

(define-public go-github-com-clbanning-mxj-v2
  (package
    (name "go-github-com-clbanning-mxj-v2")
    (version "2.5.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/clbanning/mxj")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0kkfxrzn1pa8nafw831avghi7id57xhq4j4yilc2wm1m9gqiw20i"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/clbanning/mxj/v2"
                 #:tests? #f))
    (home-page "https://github.com/clbanning/mxj")
    (synopsis #f)
    (description
      "Marshal/Unmarshal XML to/from map[string]interface{} values (and JSON);
extract/modify values from maps by key or key-path, including wildcards.")
    (license license:expat)))

(define-public go-github-com-fortytw2-leaktest
  (package
    (name "go-github-com-fortytw2-leaktest")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fortytw2/leaktest")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0487zghyxqzk6zdbhd2j074pcc2l15l4sfg5clrjqwfbql7519wx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/fortytw2/leaktest"))
    (home-page "https://github.com/fortytw2/leaktest")
    (synopsis "Leaktest")
    (description
      "Package leaktest provides tools to detect leaked goroutines in tests.  To use
it, call \"defer leaktest.Check(t)()\" at the beginning of each test that may use
goroutines.  copied out of the cockroachdb source tree with slight modifications
to be more re-useable")
    (license license:bsd-3)))
(define-public go-github-com-ghodss-yaml
  (package
    (name "go-github-com-ghodss-yaml")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ghodss/yaml")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0skwmimpy7hlh7pva2slpcplnm912rp3igs98xnqmn859kwa5v8g"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ghodss/yaml"))
    (propagated-inputs (list go-gopkg-in-yaml-v2))
    (home-page "https://github.com/ghodss/yaml")
    (synopsis "YAML marshaling and unmarshaling support for Go")
    (description
      "Copyright 2013 The Go Authors.  All rights reserved.  Use of this source code is
governed by a BSD-style license that can be found in the LICENSE file.")
    (license #f)))


(define-public go-github-com-go-openapi-swag
  (package
    (name "go-github-com-go-openapi-swag")
    (version "0.21.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-openapi/swag")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1gndhgfgwlcqqynn1rizp9nmqs9di22jmjhr6djb9794dkzr4kv5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-openapi/swag"))
    (propagated-inputs
      `(("go-github-com-sourcegraph-go-diff"
         ,go-github-com-sourcegraph-go-diff)
        ("go-golang-org-x-lint" ,go-golang-org-x-lint)
        ("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
        ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-niemeyer-pretty" ,go-github-com-niemeyer-pretty)
        ("go-github-com-mailru-easyjson" ,go-github-com-mailru-easyjson)
        ("go-github-com-kr-text" ,go-github-com-kr-text)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/go-openapi/swag")
    (synopsis "Swag")
    (description
      "Package swag contains a bunch of helper functions for go-openapi and go-swagger
projects.")
    (license license:asl2.0)))

(define-public go-github-com-gobuffalo-flect
  (package
    (name "go-github-com-gobuffalo-flect")
    (version "0.2.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gobuffalo/flect")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0h9c5pvlms809m90h30vsgmvrg46p3jkkhcxr4lb34mqax75jarr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/gobuffalo/flect"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/gobuffalo/flect")
    (synopsis "Flect")
    (description
      "Package flect is a new inflection engine to replace
[@url{https://github.com/markbates/inflect,https://github.com/markbates/inflect}](@url{https://github.com/markbates/inflect,https://github.com/markbates/inflect})
designed to be more modular, more readable, and easier to fix issues on than the
original.")
    (license license:expat)))

(define-public go-github-com-kyokomi-emoji-v2  (package
    (name "go-github-com-kyokomi-emoji-v2")
    (version "2.2.9")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kyokomi/emoji")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0d6i4bcrfnx9gv0dabalm2sngg451kh1wd2blml280lijd0vqk3m"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/kyokomi/emoji/v2"))
    (home-page "https://github.com/kyokomi/emoji")
    (synopsis "Emoji")
    (description "Package emoji terminal output.")
    (license license:expat)))

(define-public go-github-com-magefile-mage
  (package
    (name "go-github-com-magefile-mage")
    (version "1.13.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/magefile/mage")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1fdlay96j8aadix2bzcv1ki43cz5ik72j3an15ai4dlpkxbnds7q"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/magefile/mage"))
    (home-page "https://github.com/magefile/mage")
    (synopsis "About")
    (description
      "Mage is a make-like build tool using Go.  You write plain-old go functions, and
Mage automatically uses them as Makefile-like runnable targets.")
    (license license:asl2.0)))

(define-public go-github-com-montanaflynn-stats
  (package
    (name "go-github-com-montanaflynn-stats")
    (version "0.6.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/montanaflynn/stats")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0r0ad2275saw79kgh3ywafii8f6rja2z6mzm9izs11k2lvkqpz6z"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/montanaflynn/stats"))
    (home-page "https://github.com/montanaflynn/stats")
    (synopsis "Stats - Golang Statistics Package")
    (description
      "Package stats is a well tested and comprehensive statistics library package with
no dependencies.")
    (license license:expat)))

(define-public go-github-com-neurosnap-sentences
  (package
    (name "go-github-com-neurosnap-sentences")
    (version "1.0.9")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/neurosnap/sentences")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1gx3rp1v2mbiag6q33j5hpmaq885cmpqph4qc8cxl50hyhg67ww3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/neurosnap/sentences"
                 #:tests? #f))
    (propagated-inputs
      `(("go-github-com-spf13-cobra" ,go-github-com-spf13-cobra)))
    (home-page "https://github.com/neurosnap/sentences")
    (synopsis "Sentences - A command line sentence tokenizer")
    (description
      "Package sentences is a golang package that will convert a blob of text into a
list of sentences.")
    (license license:expat)))

(define-public go-github-com-shogo82148-go-shuffle
  (package
    (name "go-github-com-shogo82148-go-shuffle")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shogo82148/go-shuffle")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hzrj29xlvd9j807n27bpafbmvkg8cx3zxjb3h6hdxg1kr2c8mam"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shogo82148/go-shuffle"))
    (home-page "https://github.com/shogo82148/go-shuffle")
    (synopsis "go-shuffle")
    (description
      "Package shuffle provides primitives for shuffling slices and user-defined
collections.")
    (license license:expat)))

(define-public go-gopkg-in-neurosnap-sentences-v1
  (package
    (name "go-gopkg-in-neurosnap-sentences-v1")
    (version "1.0.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/neurosnap/sentences.v1")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0lpxx9xa2yg824ksmvbjhp6l6jy9knc9ad6iw7jbrdjc1sln5801"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path
        "gopkg.in/neurosnap/sentences.v1"
        #:unpack-path
        "gopkg.in/neurosnap/sentences.v1"
        #:tests? #f))
    (home-page "https://gopkg.in/neurosnap/sentences.v1")
    (synopsis "Sentences - A command line sentence tokenizer")
    (description
      "Package sentences is a golang package that will convert a blob of text into a
list of sentences.")
    (license license:expat)))

(define-public go-github-com-jdkato-prose
  (package
    (name "go-github-com-jdkato-prose")
    (version "1.2.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jdkato/prose")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1rwxk7s4mz86r32gqn8slih1cms60rz41cwa76ck84x4b5yqhlly"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jdkato/prose"))
    (propagated-inputs
      `(("go-gopkg-in-neurosnap-sentences-v1"
         ,go-gopkg-in-neurosnap-sentences-v1)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-shogo82148-go-shuffle"
         ,go-github-com-shogo82148-go-shuffle)
        ("go-github-com-neurosnap-sentences"
         ,go-github-com-neurosnap-sentences)
        ("go-github-com-montanaflynn-stats"
         ,go-github-com-montanaflynn-stats)))
    (home-page "https://github.com/jdkato/prose")
    (synopsis "prose")
    (description
      "Package prose is a repository of packages related to text processing, including
tokenization, part-of-speech tagging, and named-entity extraction.")
    (license license:expat)))

(define-public go-github-com-hairyhenderson-go-codeowners
  (package
    (name "go-github-com-hairyhenderson-go-codeowners")
    (version "0.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/hairyhenderson/go-codeowners")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1zylvqdp314y17iv694iyaklj95xcr73cs2nnsarhwhiq0hcaj3x"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/hairyhenderson/go-codeowners"))
    (propagated-inputs
      `(("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-spf13-afero" ,go-github-com-spf13-afero)))
    (home-page "https://github.com/hairyhenderson/go-codeowners")
    (synopsis "go-codeowners")
    (description
      "This package provides a package that finds and parses
@url{https://help.github.com/articles/about-codeowners/,(code CODEOWNERS)}
files.")
    (license license:expat)))

(define-public go-github-com-niemeyer-pretty
  (package
    (name "go-github-com-niemeyer-pretty")
    (version "0.0.0-20200227124842-a10e7caefd8e")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/niemeyer/pretty")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jmazh4xzaa3v6g46hz60q2z7nmqs9l9cxdzmmscn3kbcs2znq4v"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/niemeyer/pretty"
                               #:tests? #f))
    (propagated-inputs `(("go-github-com-kr-text" ,go-github-com-kr-text)))
    (home-page "https://github.com/niemeyer/pretty")
    (synopsis #f)
    (description
      "Package pretty provides pretty-printing for Go values.  This is useful during
debugging, to avoid wrapping long output lines in the terminal.")
    (license license:expat)))

(define-public go-github-com-shurcool-go
  (package
    (name "go-github-com-shurcool-go")
    (version "0.0.0-20200502201357-93f07166e636")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shurcooL/go")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wgwlhsgx1c2v650xvf099hgrd4av18gfb0kha09klmsh0p0hc5r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/shurcooL/go"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/shurcooL/go")
    (synopsis "go")
    (description "Common Go code.")
    (license license:expat)))

(define-public go-github-com-shurcool-go-goon
  (package
    (name "go-github-com-shurcool-go-goon")
    (version "0.0.0-20210110234559-7585751d9a17")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/shurcooL/go-goon")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1c0gkv255kjbbcx3ab26driihnq685vp08axrp5ls8vq7g67rrwl"))))
    (build-system go-build-system)
    
    (arguments '(#:import-path "github.com/shurcooL/go-goon"))
    (propagated-inputs (list go-github-com-shurcool-go))
    (home-page "https://github.com/shurcooL/go-goon")
    (synopsis "goon")
    (description
      "Package goon is a deep pretty printer with Go-like notation.  It implements the
goon specification.")
    (license license:expat)))

(define-public go-github-com-sourcegraph-go-diff
  (package
    (name "go-github-com-sourcegraph-go-diff")
    (version "0.6.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sourcegraph/go-diff")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ic58wi4cac61kh5sasn9iylcbzbqawlzva964rk0y0nifsyjcmc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/sourcegraph/go-diff"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-github-com-shurcool-go-goon" ,go-github-com-shurcool-go-goon)
        ("go-github-com-shurcool-go" ,go-github-com-shurcool-go)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)))
    (home-page "https://github.com/sourcegraph/go-diff")
    (synopsis "go-diff")
    (description "Diff parser and printer for Go.")
    (license license:expat)))



(define-public go-github-com-getkin-kin-openapi
  (package
    (name "go-github-com-getkin-kin-openapi")
    (version "0.94.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/getkin/kin-openapi")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "04266ajs6wvawsjgniqcbxz49ggq43gy2gk2ka130lgx5fr9psbv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/getkin/kin-openapi"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-gorilla-mux" ,go-github-com-gorilla-mux)
        ("go-github-com-go-openapi-jsonpointer"
         ,go-github-com-go-openapi-jsonpointer)
        ("go-github-com-ghodss-yaml" ,go-github-com-ghodss-yaml)))
    (home-page "https://github.com/getkin/kin-openapi")
    (synopsis "Introduction")
    (description
      "This package provides a @url{https://golang.org,Go} project for handling
@url{https://www.openapis.org/,OpenAPI} files.  We target the latest OpenAPI
version (currently 3), but the project contains support for older OpenAPI
versions too.")
    (license license:expat)))

(define-public go-github-com-josharian-intern
  (package
    (name "go-github-com-josharian-intern")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/josharian/intern")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1za48ppvwd5vg8vv25ldmwz1biwpb3p6qhf8vazhsfdg9m07951c"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/josharian/intern"))
    (home-page "https://github.com/josharian/intern")
    (synopsis #f)
    (description
      "Package intern interns strings.  Interning is best effort only.  Interned
strings may be removed automatically at any time without notification.  All
functions may be called concurrently with themselves and each other.")
    (license license:expat)))

(define-public go-github-com-mailru-easyjson
  (package
    (name "go-github-com-mailru-easyjson")
    (version "0.7.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mailru/easyjson")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0clifkvvy8f45rv3cdyv58dglzagyvfcqb63wl6rij30c5j2pzc1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mailru/easyjson"))
    (propagated-inputs
      `(("go-github-com-josharian-intern" ,go-github-com-josharian-intern)))
    (home-page "https://github.com/mailru/easyjson")
    (synopsis "easyjson")
    (description
      "Package easyjson contains marshaler/unmarshaler interfaces and helper functions.")
    (license license:expat)))

(define-public go-github-com-go-openapi-jsonpointer
  (package
    (name "go-github-com-go-openapi-jsonpointer")
    (version "0.19.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-openapi/jsonpointer")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0azic9nfwywlz4qxvacyi4g668fbbrkcyv15bag02yfcsi8szg5c"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-openapi/jsonpointer"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-mailru-easyjson" ,go-github-com-mailru-easyjson)
        ("go-github-com-go-openapi-swag" ,go-github-com-go-openapi-swag)))
    (home-page "https://github.com/go-openapi/jsonpointer")
    (synopsis "gojsonpointer")
    (description
      "@url{https://raw.githubusercontent.com/go-openapi/jsonpointer/master/LICENSE,(img
(@ (src http://img.shields.io/badge/license-Apache%20v2-orange.svg) (alt
license)))} @url{http://godoc.org/github.com/go-openapi/jsonpointer,(img (@ (src
https://godoc.org/github.com/go-openapi/jsonpointer?status.svg=) (alt GoDoc)))}
An implementation of JSON Pointer - Go language")
    (license license:asl2.0)))

(define-public go-github-com-azure-azure-pipeline-go
  (package
    (name "go-github-com-azure-azure-pipeline-go")
    (version "0.2.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Azure/azure-pipeline-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "154qwr7v1q2wjp263jhkvrygi728q568zc930h3fxp75v32laqwb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Azure/azure-pipeline-go"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
        ("go-github-com-mattn-go-ieproxy" ,go-github-com-mattn-go-ieproxy)))
    (home-page "https://github.com/azure/azure-pipeline-go")
    (synopsis "Contributing")
    (description
      "This project welcomes contributions and suggestions.  Most contributions require
you to agree to a Contributor License Agreement (CLA) declaring that you have
the right to, and actually do, grant us the rights to use your contribution.
For details, visit @url{https://cla.microsoft.com,https://cla.microsoft.com}.")
    (license license:expat)))

(define-public go-github-com-azure-azure-storage-blob-go
  (package
    (name "go-github-com-azure-azure-storage-blob-go")
    (version "0.14.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Azure/azure-storage-blob-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1z35hnzyrj0q9bigq5dx7gnx11w2zkpkv4150rh43ccj78ncsldb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Azure/azure-storage-blob-go"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-google-uuid" ,go-github-com-google-uuid)
        ("go-github-com-azure-go-autorest-autorest"
         ,go-github-com-azure-go-autorest)
        ("go-github-com-azure-azure-pipeline-go"
         ,go-github-com-azure-azure-pipeline-go)))
    (home-page "https://github.com/azure/azure-storage-blob-go")
    (synopsis "Azure Storage Blob SDK for Go")
    (description
      "The Microsoft Azure Storage SDK for Go allows you to build applications that
takes advantage of Azure's scalable cloud storage.")
    (license license:expat)))

(define-public go-github-com-sanity-io-litter
  (package
    (name "go-github-com-sanity-io-litter")
    (version "1.5.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sanity-io/litter")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jcjzi9fyiwls9jfxzvwaj7ip54nnnza9qjsz0wswx681x5bvcfg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/sanity-io/litter"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/sanity-io/litter")
    (synopsis "Litter")
    (description "Litter is provided by")
    (license license:expat)))

(define-public go-github-com-rwcarlsen-goexif
  (package
    (name "go-github-com-rwcarlsen-goexif")
    (version "0.0.0-20190401172101-9e8deecbddbd")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rwcarlsen/goexif")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1drqhzplg72lvrf3qmb9awbggnjqp23hwn2pgvksi3spv17kc9h2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rwcarlsen/goexif"
                 #:tests? #f              
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (home-page "https://github.com/rwcarlsen/goexif")
    (synopsis "goexif")
    (description
      "This package provides decoding of basic exif and tiff encoded data.  Still in
alpha - no guarantees.  Suggestions and pull requests are welcome.
Functionality is split into two packages - \"exif\" and \"tiff\" The exif package
depends on the tiff package.")
    (license license:bsd-2)))

(define-public go-github-com-djherbis-atime
  (package
    (name "go-github-com-djherbis-atime")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/djherbis/atime")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0xsz55zpihd9wyrj6qvm3miqzb6x3mnp5apzs0dx1byndhb8adpq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/djherbis/atime"))
    (home-page "https://github.com/djherbis/atime")
    (synopsis "atime")
    (description
      "Package atime provides a platform-independent way to get atimes for files.")
    (license license:expat)))

(define-public go-github-com-matryer-try
  (package
    (name "go-github-com-matryer-try")
    (version "0.0.0-20161228173917-9ac251b645a2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/matryer/try")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19fnqmpl3p54vmxgm1hmqvdc87brqx754wf3cdhq1bj04fcbb5h9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/matryer/try"
                 #:tests? #f))
    (home-page "https://github.com/matryer/try")
    (synopsis "try")
    (description "Package try provides retry functionality.")
    (license license:expat)))

(define-public go-github-com-tdewolff-parse-v2
  (package
    (name "go-github-com-tdewolff-parse-v2")
    (version "2.5.29")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tdewolff/parse")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hc9cqivq4jfs9j9wpzndzvk6fx4yl59vcxx4c0d9zkfsr8ik05z"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tdewolff/parse/v2"))
    (propagated-inputs
      `(("go-github-com-tdewolff-test" ,go-github-com-tdewolff-test)))
    (home-page "https://github.com/tdewolff/parse")
    (synopsis "Parse")
    (description
      "Package parse contains a collection of parsers for various formats in its
subpackages.")
    (license license:expat)))

(define-public go-github-com-tdewolff-test
  (package
    (name "go-github-com-tdewolff-test")
    (version "1.0.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tdewolff/test")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "12glhjb4cwp6yxwd17rwa6b4gxna3lm01bgc7yn9di58chc7lyh3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tdewolff/test"))
    (home-page "https://github.com/tdewolff/test")
    (synopsis "Test")
    (description
      "Test is a helper package written in @url{http://golang.org/,Go}.  It implements
a few functions that are useful for io testing, such as readers and writers that
fail after N consecutive reads/writes.")
    (license license:expat)))

(define-public go-github-com-tdewolff-minify-v2
  (package
    (name "go-github-com-tdewolff-minify-v2")
    (version "2.11.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tdewolff/minify")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "02v3fid3745jrlb5qvzwk7b9s2smzyr0sjpl6s5i2y9qwsxvcxma"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tdewolff/minify/v2"))
    (propagated-inputs
      `(("go-github-com-tdewolff-test" ,go-github-com-tdewolff-test)
        ("go-github-com-tdewolff-parse-v2" ,go-github-com-tdewolff-parse-v2)
        ("go-github-com-spf13-pflag" ,go-github-com-spf13-pflag)
        ("go-github-com-matryer-try" ,go-github-com-matryer-try)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)
        ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
        ("go-github-com-djherbis-atime" ,go-github-com-djherbis-atime)
        ("go-github-com-cheekybits-is" ,go-github-com-cheekybits-is)))
    (home-page "https://github.com/tdewolff/minify")
    (synopsis "Minify")
    (description
      "Package minify relates MIME type to minifiers.  Several minifiers are provided
in the subpackages.")
    (license license:expat)))

(define-public go-github-com-dlclark-regexp2
  (package
    (name "go-github-com-dlclark-regexp2")
    (version "1.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/dlclark/regexp2")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1irfv89b7lfkn7k3zgx610ssil6k61qs1wjj31kvqpxb3pdx4kry"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/dlclark/regexp2"))
    (home-page "https://github.com/dlclark/regexp2")
    (synopsis "regexp2 - full featured regular expressions for Go")
    (description
      "Package regexp2 is a regexp package that has an interface similar to Go's
framework regexp engine but uses a more feature full regex engine behind the
scenes.")
    (license license:expat)))

(define-public go-github-com-googleapis-gax-go
  (package
    (name "go-github-com-googleapis-gax-go")
    (version "1.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/googleapis/gax-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1lxawwngv6miaqd25s3ba0didfzylbwisd2nz7r4gmbmin6jsjrx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/googleapis/gax-go"))
    (propagated-inputs
      `(("go-honnef-co-go-tools" ,go-honnef-co-go-tools)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-lint" ,go-golang-org-x-lint)
        ("go-golang-org-x-exp" ,go-golang-org-x-exp)
        ;;("go-github-com-googleapis-gax-go-v2" ,go-github-com-googleapis-gax-go-v2)
        ;;("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
        ))
    (home-page "https://github.com/googleapis/gax-go")
    (synopsis "Google API Extensions for Go")
    (description
      "Package gax contains a set of modules which aid the development of APIs for
clients and servers based on gRPC and Google API conventions.")
    (license license:bsd-3)))

(define-public go-github-com-googleapis-go-type-adapters
  (package
    (name "go-github-com-googleapis-go-type-adapters")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/googleapis/go-type-adapters")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "11y2a2xg3wb02ywq1jy03acz7vd1yggpm2jm2dv47z2mwjpa6xmv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/googleapis/go-type-adapters"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page "https://github.com/googleapis/go-type-adapters")
    (synopsis "Go google.type Adapters")
    (description
      "This library provides helper functions for converting between the Go proto
messages in @code{google.type} (as found in
@url{https://pkg.go.dev/google.golang.org/genproto,genproto}) and Golang native
types.")
    (license license:asl2.0)))

(define-public go-google-golang-org-api
  (package
    (name "go-google-golang-org-api")
    (version "0.70.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/googleapis/google-api-go-client")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0skycrjhvvmalg993j458mdr28sxj8igcgyxrd3aryg9kxh41wpn"))))
    ;;(version "0.75.0")
    ;;(source
    ;;  (origin
    ;;    (method git-fetch)
    ;;    (uri (git-reference
    ;;           (url "https://github.com/googleapis/google-api-go-client")
    ;;           (commit (string-append "v" version))))
    ;;    (file-name (git-file-name name version))
    ;;    (sha256
    ;;      (base32 "1y5ib358f34888bj49zis148rwh93q4n0sckq0xqkls79a7hak4n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "google.golang.org/api"))
    (propagated-inputs
     `(("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-go-opencensus-io" ,go-go-opencensus-io)
        ;;("go-github-com-googleapis-gax-go-v2" ,go-github-com-googleapis-gax-go-v2)
        ("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ;;("go-cloud-google-com-go-compute" ,go-cloud-google-com-go-compute)
        ))
    (home-page "https://google.golang.org/api")
    (synopsis "Google APIs Client Library for Go")
    (description
      "Package api is the root of the packages used to access Google Cloud Services.
See
@url{https://godoc.org/google.golang.org/api,https://godoc.org/google.golang.org/api}
for a full list of sub-packages.")
    (license license:bsd-3)))

(define-public go-google-golang-org-grpc-cmd-protoc-gen-go-grpc
  (package
    (name "go-google-golang-org-grpc-cmd-protoc-gen-go-grpc")
    (version "1.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/grpc/grpc-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1f3zr5a49pdy531aznjapslf8z821wdnk9xrdq564jvva0i8k7m4"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
       #:unpack-path "google.golang.org/grpc"              
             ))
    (propagated-inputs
     `(;;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
       
       ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)))
    (home-page "https://google.golang.org/grpc/cmd/protoc-gen-go-grpc")
    (synopsis "protoc-gen-go-grpc")
    (description
      "protoc-gen-go-grpc is a plugin for the Google protocol buffer compiler to
generate Go code.  Install it by building this program and making it accessible
within your PATH with the name:")
    (license license:asl2.0)))

(define-public go-google-golang-org-genproto
  (package
    (name "go-google-golang-org-genproto")
    (version "0.0.0-20220422154200-b37d22cd5731")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/googleapis/go-genproto")
               (commit "b37d22cd57311a8ca0db7cfca637fe391426baa2")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qxmb6cc79b5pzvsxfa8bibmgfinp48gs15p1ibklj4sp5fxnfld"))))
    (build-system go-build-system)
    (arguments `(#:import-path "google.golang.org/genproto"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'build))))
    (propagated-inputs
      `(;;("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ;;("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
        ))
    (home-page "https://google.golang.org/genproto")
    (synopsis "Go generated proto packages")
    (description
      "This repository contains the generated Go packages for common protocol buffer
types, and the generated @url{http://grpc.io,gRPC} code necessary for
interacting with Google's gRPC APIs.")
    (license license:asl2.0)))

(define-public go-google-golang-org-grpc
  (package
    (name "go-google-golang-org-grpc")
    (version "1.45.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/grpc/grpc-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0xxyhijjbc0gd8bpqjwa9k8g479hangpamm7afnh3ka024wsnadw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "google.golang.org/grpc"
                 ;;#:tests? #f
                 ;;#:phases
                 ;;(modify-phases %standard-phases
                 ;;  (delete 'build))
                 ))
    (native-inputs (list go-github-com-google-go-cmp))
    (propagated-inputs
     `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
       ("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
       ("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
       ("go-golang-org-x-sys" ,go-golang-org-x-sys)
       ("go-golang-org-x-net" ,go-golang-org-x-net)
       ("go-golang-org-x-text" ,go-golang-org-x-text)))
    (home-page "https://google.golang.org/grpc")
    (synopsis "gRPC-Go")
    (description "Package grpc implements an RPC system called gRPC.")
    (license license:asl2.0)))

(define-public go-github-com-google-martian-v3
  (package
    (name "go-github-com-google-martian-v3")
    (version "3.3.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/martian")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "16ag4vb2q7qcq6j5n5pj204gdd4g22bjbk2mbagmrpx3h1r3hfnl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/martian/v3"))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ;;("go-google-golang-org-grpc-cmd-protoc-gen-go-grpc" ,go-google-golang-org-grpc-cmd-protoc-gen-go-grpc)
        ("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-golang-snappy" ,go-github-com-golang-snappy)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page "https://github.com/google/martian")
    (synopsis "Martian Proxy")
    (description
      "Package martian provides an HTTP/1.1 proxy with an API for configurable request
and response modifiers.")
    (license license:asl2.0)))

(define-public go-cloud-google-com-go
  (package
    (name "go-cloud-google-com-go")
    (version "0.101.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/googleapis/google-cloud-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0d0qkl4h9zpjzp3vvj3j1fa9naaaklhcld0j4mliqm5h0903a4ly"))))
    (build-system go-build-system)
    (arguments '(#:import-path "cloud.google.com/go"
               ;;                #:unpack-path "cloud.google.com/go"
               #:phases
       (modify-phases %standard-phases
         (delete 'build))))
    (propagated-inputs
      `(;;("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ;("go-google-golang-org-grpc" ,go-google-golang-org-grpc)
        ;;("go-google-golang-org-genproto" ,go-google-golang-org-genproto)
        ("go-google-golang-org-api" ,go-google-golang-org-api)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
        ("go-github-com-googleapis-go-type-adapters"
         ,go-github-com-googleapis-go-type-adapters)
        
        ;;("go-go-opencensus-io" ,go-go-opencensus-io)
        ;;("go-github-com-googleapis-gax-go-v2"
        ;; ,go-github-com-googleapis-gax-go-v2)
        ;;("go-github-com-google-martian-v3" ,go-github-com-google-martian-v3)
        ;;("go-github-com-google-go-cmp-cmp" ,go-github-com-google-go-cmp-cmp)
        ;;("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
        ;;("go-cloud-google-com-go-storage" ,go-cloud-google-com-go-storage)
        ;;("go-cloud-google-com-go-compute" ,go-cloud-google-com-go-compute)
        ))
    (home-page "https://cloud.google.com/go")
    (synopsis "Google Cloud Client Libraries for Go")
    (description
      "Package cloud is the root of the packages used to access Google Cloud Services.
See
@url{https://godoc.org/cloud.google.com/go,https://godoc.org/cloud.google.com/go}
for a full list of sub-packages.")
    (license license:asl2.0)))


(define-public go-github-com-aws-aws-sdk-go-v2
  (package
    (name "go-github-com-aws-aws-sdk-go-v2")
    (version "1.16.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aws/aws-sdk-go-v2")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n2r5vsamdifpircrd0bxlrzvicvs5x2n3syd2r28yhcsy1bk856"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aws/aws-sdk-go-v2"))
    (propagated-inputs
      `(("go-github-com-jmespath-go-jmespath"
         ,go-github-com-jmespath-go-jmespath)
        ;;("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
        ("go-github-com-aws-smithy-go" ,go-github-com-aws-smithy-go)))
    (home-page "https://github.com/aws/aws-sdk-go-v2")
    (synopsis "AWS SDK for Go v2")
    (description
      "Package sdk is the official AWS SDK v2 for the Go programming language.")
    (license license:asl2.0)))

(define-public go-github-com-aws-smithy-go
  (package
    (name "go-github-com-aws-smithy-go")
    (version "1.11.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aws/smithy-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1sy7jwibmxlixklawfn6bfwvhnfzaw2lcm6lm47h27gzc7nif78f"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aws/smithy-go"))
    (propagated-inputs
      `(("go-github-com-jmespath-go-jmespath"
         ,go-github-com-jmespath-go-jmespath)
        ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)))
    (home-page "https://github.com/aws/smithy-go")
    (synopsis "Smithy Go")
    (description
      "Package smithy provides the core components for a Smithy SDK.")
    (license license:asl2.0)))

(define-public go-github-com-mitchellh-hashstructure
  (package
    (name "go-github-com-mitchellh-hashstructure")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mitchellh/hashstructure")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0inqc2k0mkaklhf8l779r9pnl5d4chdlahkrcjbss2bcnjjdblvl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mitchellh/hashstructure"))
    (home-page "https://github.com/mitchellh/hashstructure")
    (synopsis "hashstructure")
    (description
      "hashstructure is a Go library for creating a unique hash value for arbitrary
values in Go.")
    (license license:expat)))

(define-public go-github-com-spf13-fsync
  (package
    (name "go-github-com-spf13-fsync")
    (version "0.9.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/spf13/fsync")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1z6v7pvlqphq9lqk5sa80ynj3x21yk6s84i41hivgda73lzffg5n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/spf13/fsync"))
    (propagated-inputs (list go-github-com-spf13-afero))
    (home-page "https://github.com/spf13/fsync")
    (synopsis #f)
    (description
      "Package @code{fsync} keeps files and directories in sync.  Read the
documentation on @url{http://godoc.org/github.com/mostafah/fsync,GoDoc}.")
    (license license:expat)))

(define-public go-cloud-google-com-go-iam
  (package
    (name "go-cloud-google-com-go-iam")
    (version "0.3.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googleapis/google-cloud-go")
             (commit (string-append "iam/v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1qcdyvamdg4l9947xnysl27nwp1js1yqfi996ly3psy3ykcgsgdc"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "cloud.google.com/go"
       #:import-path "cloud.google.com/go/iam"
       #:phases
       (modify-phases %standard-phases
         (delete 'build))))
    (propagated-inputs (list go-google-golang-org-grpc go-github-com-googleapis-gax-go-v2))
    (home-page
     "https://pkg.go.dev/cloud.google.com/go/iam")
    (synopsis
     "Go wrapper for Google Compute Engine metadata service")
    (description
     "This package provides access to Google Compute Engine (GCE) metadata and
API service accounts for Go.")
    (license license:asl2.0)))


(define-public go-storj-io-drpc
  (package
    (name "go-storj-io-drpc")
    (version "0.0.30")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/storj/drpc")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jc7s46mz5jncsn95pwy6pyzrsqg72mh74sgzzq80blcpjz3l4b3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "storj.io/drpc"))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-github-com-zeebo-errs" ,go-github-com-zeebo-errs)
        ("go-github-com-zeebo-assert" ,go-github-com-zeebo-assert)))
    (home-page "https://storj.io/drpc")
    (synopsis #f)
    (description "Package drpc is a light replacement for gprc.")
    (license license:expat)))

(define-public go-github-com-calebcase-tmpfile
  (package
    (name "go-github-com-calebcase-tmpfile")
    (version "1.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/calebcase/tmpfile")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0q1bvxyrzlzlaz7rvba1h2pdkv1c9nb18zhns06sz73k2z6h53y8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/calebcase/tmpfile"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/calebcase/tmpfile")
    (synopsis "Cross Platform Temporary Files")
    (description
      "Package tmpfile provides a cross platform facility for creating temporary files
that are automatically cleaned up (even in the event of an unexpected process
exit).")
    (license license:expat)))

(define-public go-github-com-spacemonkeygo-monkit-v3
  (package
    (name "go-github-com-spacemonkeygo-monkit-v3")
    (version "3.0.17")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/spacemonkeygo/monkit")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dwjkzjamw7d2m28x8b2fv6vyf0mynm7331wbz0bz4vlixcal89f"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/spacemonkeygo/monkit/v3"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/spacemonkeygo/monkit")
    (synopsis #f)
    (description
      "Package monkit is a flexible code instrumenting and data collection library.")
    (license license:asl2.0)))

(define-public go-github-com-zeebo-admission-v3
  (package
    (name "go-github-com-zeebo-admission-v3")
    (version "3.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/zeebo/admission")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0lvgpdnz7iapq5gzm2xbr6pz05wd1ackmy0z102sh72fzv9d5w6x"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/zeebo/admission/v3"))
    (propagated-inputs
      `(("go-github-com-zeebo-incenc" ,go-github-com-zeebo-incenc)
        ("go-github-com-zeebo-float16" ,go-github-com-zeebo-float16)
        ("go-github-com-zeebo-errs" ,go-github-com-zeebo-errs)
        ("go-github-com-zeebo-assert" ,go-github-com-zeebo-assert)
        ("go-github-com-spacemonkeygo-monkit-v3"
         ,go-github-com-spacemonkeygo-monkit-v3)))
    (home-page "https://github.com/zeebo/admission")
    (synopsis #f)
    (description "package admission is a fast way to ingest/send metrics.")
    (license license:asl2.0)))

(define-public go-github-com-zeebo-assert
  (package
    (name "go-github-com-zeebo-assert")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/zeebo/assert")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1982wdqf97qg0y0659lf631jv4gcp2sqx58f2q36ddppnhwn5m6l"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/zeebo/assert"))
    (home-page "https://github.com/zeebo/assert")
    (synopsis "package assert")
    (description
      "See the api docs.  There's not a lot of surface area, and that's the goal.")
    (license license:cc0)))

(define-public go-github-com-zeebo-errs
  (package
    (name "go-github-com-zeebo-errs")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/zeebo/errs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0l5xd5bmri99ldqi844358gvp0kvw6lb98msjh9r25lyw05zlzkf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/zeebo/errs"))
    (home-page "https://github.com/zeebo/errs")
    (synopsis "errs")
    (description
      "Package errs provides a simple error package with stack traces.")
    (license license:expat)))

(define-public go-github-com-marten-seemann-qtls-go1-16
  (package
    (name "go-github-com-marten-seemann-qtls-go1-16")
    (version "0.1.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/marten-seemann/qtls-go1-16")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "042d0mbzijq0k7dsm4nhyvb3wrahi696ihkq5lfas61lwqzpxxzn"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/marten-seemann/qtls-go1-16"
                 #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-golang-mock" ,go-github-com-golang-mock)))
    (home-page "https://github.com/marten-seemann/qtls-go1-16")
    (synopsis "qtls")
    (description
      "package qtls partially implements TLS 1.2, as specified in
@url{https://rfc-editor.org/rfc/rfc5246.html,RFC 5246}, and TLS 1.3, as
specified in @url{https://rfc-editor.org/rfc/rfc8446.html,RFC 8446}.")
    (license license:bsd-3)))

(define-public go-github-com-marten-seemann-qtls-go1-17
  (package
    (name "go-github-com-marten-seemann-qtls-go1-17")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/marten-seemann/qtls-go1-17")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "070763lnas3m6haaiq55zdl6cig9598cd4d0ac8xknyc9i4grky8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/marten-seemann/qtls-go1-17"
                #:tests? #f))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-golang-mock" ,go-github-com-golang-mock)))
    (home-page "https://github.com/marten-seemann/qtls-go1-17")
    (synopsis "qtls")
    (description
      "package qtls partially implements TLS 1.2, as specified in
@url{https://rfc-editor.org/rfc/rfc5246.html,RFC 5246}, and TLS 1.3, as
specified in @url{https://rfc-editor.org/rfc/rfc8446.html,RFC 8446}.")
    (license license:bsd-3)))

(define-public go-github-com-golang-mock
  (package
    (name "go-github-com-golang-mock")
    (version "1.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/mock")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1hara8j0x431njjhqxfrg1png7xa1gbrpwza6ya4mwlx76hppap4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/mock/gomock"
                #:unpack-path "github.com/golang/mock"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)))
    (home-page "https://github.com/golang/mock")
    (synopsis "gomock")
    (description
      "gomock is a mocking framework for the @url{http://golang.org/,Go programming
language}.  It integrates well with Go's built-in @code{testing} package, but
can be used in other contexts too.")
    (license license:asl2.0)))

(define-public go-github-com-marten-seemann-qtls-go1-18
  (package
    (name "go-github-com-marten-seemann-qtls-go1-18")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/marten-seemann/qtls-go1-18")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0f6n74ficlpxjz8x7fpl0fpv65w2abcbjbksdnvk8p4f67l8lyya"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/marten-seemann/qtls-go1-18"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-golang-mock" ,go-github-com-golang-mock)))
    (home-page "https://github.com/marten-seemann/qtls-go1-18")
    (synopsis "qtls")
    (description
      "package qtls partially implements TLS 1.2, as specified in
@url{https://rfc-editor.org/rfc/rfc5246.html,RFC 5246}, and TLS 1.3, as
specified in @url{https://rfc-editor.org/rfc/rfc8446.html,RFC 8446}.")
    (license license:bsd-3)))

(define-public go-github-com-nxadm-tail
  (package
    (name "go-github-com-nxadm-tail")
    (version "1.4.8")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/nxadm/tail")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1j2gi485fhwdpmyzn42wk62103fclwbfywg42p275z1qv2bsz1rc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/nxadm/tail"))
    (propagated-inputs
      `(("go-gopkg-in-tomb-v1" ,go-gopkg-in-tomb-v1)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)))
    (home-page "https://github.com/nxadm/tail")
    (synopsis "tail functionality in Go")
    (description
      "nxadm/tail provides a Go library that emulates the features of the BSD `tail`
program.  The library comes with full support for truncation/move detection as
it is designed to work with log rotation tools.  The library works on all
operating systems supported by Go, including POSIX systems like Linux and *BSD,
and MS Windows.  Go 1.9 is the oldest compiler release supported.")
    (license license:expat)))


(define-public go-github-com-chzyer-readline
  (package
    (name "go-github-com-chzyer-readline")
    (version "1.5.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/chzyer/readline")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n3fsjyf8r90qrp09hli6gny3b68lv8qlpgyh9xppja6y6wbbrqd"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/chzyer/readline"))
    (propagated-inputs
      `(("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-chzyer-test" ,go-github-com-chzyer-test)))
    (home-page "https://github.com/chzyer/readline")
    (synopsis "Guide")
    (description
      "Readline is a pure go implementation for GNU-Readline kind library.")
    (license license:expat)))


(define-public go-github-com-ianlancetaylor-demangle
  (package
    (name "go-github-com-ianlancetaylor-demangle")
    (version "0.0.0-20220319035150-800ac71e25c2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ianlancetaylor/demangle")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1zzwaiqcm889ldybh26nfs75czs1iwb3k7gp8ng10dhs40nvlk27"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ianlancetaylor/demangle"))
    (home-page "https://github.com/ianlancetaylor/demangle")
    (synopsis "github.com/ianlancetaylor/demangle")
    (description
      "Package demangle defines functions that demangle GCC/LLVM C++ and Rust symbol
names.  This package recognizes names that were mangled according to the C++ ABI
defined at
@url{http://codesourcery.com/cxx-abi/,http://codesourcery.com/cxx-abi/} and the
Rust ABI defined at
@url{https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html,https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html}")
    (license license:bsd-3)))

(define-public go-github-com-google-pprof
  (package
    (name "go-github-com-google-pprof")
    (version "0.0.0-20220412212628-83db2b799d1f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/pprof")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qpqxqh0m8lrmbhl5h3kgaqq80mxjk4qnv5bdrjraqi9w85s5kk3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/pprof"))
    (propagated-inputs
      `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-ianlancetaylor-demangle"
         ,go-github-com-ianlancetaylor-demangle)
        ("go-github-com-chzyer-test" ,go-github-com-chzyer-test)
        ("go-github-com-chzyer-readline" ,go-github-com-chzyer-readline)
        ("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)))
    (home-page "https://github.com/google/pprof")
    (synopsis "Introduction")
    (description
      "pprof is a tool for collection, manipulation and visualization of performance
profiles.")
    (license license:asl2.0)))

(define-public go-github-com-onsi-ginkgo-v2
  (package
    (name "go-github-com-onsi-ginkgo-v2")
    (version "2.1.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/onsi/ginkgo")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1973axfmywz2fxzqr3m7xgfjh2sv8dr9rz8f4hirl47054k4xig4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/onsi/ginkgo/v2"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ;;("go-github-com-onsi-gomega" ,go-github-com-onsi-gomega)
        ("go-github-com-google-pprof" ,go-github-com-google-pprof)
        ("go-github-com-go-task-slim-sprig"
         ,go-github-com-go-task-slim-sprig)))
    (home-page "https://github.com/onsi/ginkgo")
    (synopsis "Ginkgo 2.0 is now Generally Available!")
    (description
      "Ginkgo is a testing framework for Go designed to help you write expressive
tests.  @url{https://github.com/onsi/ginkgo,https://github.com/onsi/ginkgo}
MIT-Licensed")
    (license license:expat)))


(define-public go-github-com-google-go-cmp
  (package
    (name "go-github-com-google-go-cmp")
    (version "0.5.9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/google/go-cmp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a13m7l1jrysa7mrlmra8y7n83zcnb23yjyg3a609p8i9lxkh1wm"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/go-cmp"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   ;; Source-only package
                   (delete 'build))
                 ))
    (home-page "https://github.com/google/go-cmp")
    (synopsis "Package for equality of Go values")
    (description
      "This package is intended to be a more powerful and safer alternative to
@code{reflect.DeepEqual} for comparing whether two values are semantically
equal.")
    (license license:bsd-3)))

(define-public go-github-com-google-go-cmp-0.5.8
  (package/inherit go-github-com-google-go-cmp
                   (name "go-github-com-google-go-cmp-0.5.8")
                   (version "0.5.8")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url "https://github.com/google/go-cmp")
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0563bczyrmv9ps2p6n8af0m1jsszwdmkdkrxkv6dbm5bwjihhfgk"))))))
    
(define-public go-google-golang-org-protobuf
  (package
   (name "go-google-golang-org-protobuf")
   (version "1.28.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://go.googlesource.com/protobuf")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0qy9wy36wr1vj8lhmzi26hfc14y3rfbsi0p4vkbxhiwb3iy3na7c"))))
   (build-system go-build-system)
    (arguments '(#:import-path "google.golang.org/protobuf"
                 #:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   ;; Source-only package
                   (delete 'build))
                 ))
    (propagated-inputs
      `(;;("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
        ;;("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
        ))
    (home-page "https://google.golang.org/protobuf")
    (synopsis "Go support for Protocol Buffers")
    (description
      "This project hosts the Go implementation for
@url{https://developers.google.com/protocol-buffers,protocol buffers}, which is
a language-neutral, platform-neutral, extensible mechanism for serializing
structured data.  The protocol buffer language is a language for specifying the
schema for structured data.  This schema is compiled into language specific
bindings.  This project provides both a tool to generate Go code for the
protocol buffer language, and also the runtime implementation to handle
serialization of messages in Go.  See the
@url{https://developers.google.com/protocol-buffers/docs/overview,protocol
buffer developer guide} for more information about protocol buffers themselves.")
    (license license:bsd-3)))

(define-public go-google-golang-org-protobuf-1.26
  (package/inherit go-google-golang-org-protobuf
    (name "go-google-golang-org-protobuf-1.26")
    (version "1.26.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://go.googlesource.com/protobuf")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0xq6phaps6d0vcv13ga59gzj4306l0ki9kikhmb52h6pq0iwfqlz"))))))




(define-public go-github-com-onsi-gomega
  (package
    (name "go-github-com-onsi-gomega")
    (version "1.19.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/onsi/gomega")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "092phwk97sk4sv0nbx5pfhqs6x3x1lnrjwyda1m6b6zwrfmq5c6i"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/onsi/gomega"))
    (propagated-inputs
      `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
        ("go-golang-org-x-text" ,go-golang-org-x-text)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-github-com-onsi-ginkgo-v2" ,go-github-com-onsi-ginkgo-v2)
        ("go-github-com-golang-protobuf-proto" ,go-github-com-golang-protobuf-proto)))
    (home-page "https://github.com/onsi/gomega")
    (synopsis ": a BDD Testing Framework for Golang")
    (description
      "Gomega is the Ginkgo BDD-style testing framework's preferred matcher library.")
    (license license:expat)))

(define-public go-github-com-onsi-ginkgo
  (package
    (name "go-github-com-onsi-ginkgo")
    (version "1.16.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/onsi/ginkgo")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1hh6n7q92y0ai8k6rj2yzw6wwxikhyiyk4j92zgvf1zad0gmqqmz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/onsi/ginkgo"))
    (propagated-inputs
      `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-github-com-onsi-gomega" ,go-github-com-onsi-gomega)
        ("go-github-com-nxadm-tail" ,go-github-com-nxadm-tail)
        ("go-github-com-go-task-slim-sprig"
         ,go-github-com-go-task-slim-sprig)))
    (home-page "https://github.com/onsi/ginkgo")
    (synopsis "Ginkgo 2.0 Release Candidate is available!")
    (description "Ginkgo is a BDD-style testing framework for Golang")
    (license license:expat)))

(define-public go-github-com-zeebo-float16
  (package
    (name "go-github-com-zeebo-float16")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/zeebo/float16")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "06qh3fchm5a12lx8kq7nlmfjfvla9smh3zz8s7w083rjlgpyanyl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/zeebo/float16"))
    (home-page "https://github.com/zeebo/float16")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))


(define-public go-storj-io-common
  (package
    (name "go-storj-io-common")
    (version "0.0.0-20220426072541-39d01a8209ed")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/storj/common")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1myrbb4vsk8892vx5a03qyqkqcmv9yjcyc8qblk7wzz8a4n7lrkf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "storj.io/common"
      #:tests? #f
       #:phases
       (modify-phases %standard-phases
         ;; Source-only package
         
         (delete 'build)
         )))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
        ("go-gopkg-in-tomb-v1" ,go-gopkg-in-tomb-v1)
        ("go-golang-org-x-xerrors" ,go-golang-org-x-xerrors)
        ("go-golang-org-x-tools" ,go-golang-org-x-tools)
        ("go-golang-org-x-net" ,go-golang-org-x-net)
        ("go-golang-org-x-mod" ,go-golang-org-x-mod)
        ("go-github-com-zeebo-incenc" ,go-github-com-zeebo-incenc)
        ("go-github-com-zeebo-float16" ,go-github-com-zeebo-float16)
        ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
        ("go-github-com-onsi-ginkgo" ,go-github-com-onsi-ginkgo)
        ("go-github-com-nxadm-tail" ,go-github-com-nxadm-tail)
        ;;("go-github-com-marten-seemann-qtls-go1-18" ,go-github-com-marten-seemann-qtls-go1-18)
        ("go-github-com-marten-seemann-qtls-go1-17"
         ,go-github-com-marten-seemann-qtls-go1-17)
        ("go-github-com-marten-seemann-qtls-go1-16"
         ,go-github-com-marten-seemann-qtls-go1-16)
        ("go-github-com-go-task-slim-sprig" ,go-github-com-go-task-slim-sprig)
        ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)
        ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
        ("go-github-com-cheekybits-genny" ,go-github-com-cheekybits-genny)
        ("go-storj-io-drpc" ,go-storj-io-drpc)
        ("go-golang-org-x-sys" ,go-golang-org-x-sys)
        ("go-golang-org-x-sync" ,go-golang-org-x-sync)
        ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
        ("go-github-com-zeebo-errs" ,go-github-com-zeebo-errs)
        ("go-github-com-zeebo-admission-v3" ,go-github-com-zeebo-admission-v3)
        ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
        ("go-github-com-spacemonkeygo-monkit-v3"
         ,go-github-com-spacemonkeygo-monkit-v3)
        ("go-github-com-lucas-clemente-quic-go"
         ,go-github-com-lucas-clemente-quic-go)
        ("go-github-com-google-pprof" ,go-github-com-google-pprof)
        ("go-github-com-gogo-protobuf" ,go-github-com-gogo-protobuf)
        ("go-github-com-calebcase-tmpfile" ,go-github-com-calebcase-tmpfile)))
    (home-page "https://storj.io/common")
    (synopsis #f)
    (description #f)
    (license license:expat)))

