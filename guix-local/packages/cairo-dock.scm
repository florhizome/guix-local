(define-module (guix-local packages  cairo-dock)
#:use-module (gnu)
#:use-module (guix build-system cmake)
#:use-module (guix packages)
#:use-module (guix build utils)
#:use-module (guix download)
#:use-module (guix git-download)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (gnu packages)
#:use-module (gnu packages base)
#:use-module (gnu packages gettext)
#:use-module (gnu packages pkg-config)
#:use-module (gnu packages wm)
#:use-module (gnu packages gl)
#:use-module (gnu packages xml)
#:use-module (gnu packages gcc)
#:use-module (gnu packages gtk)
#:use-module (gnu packages xorg)
#:use-module (gnu packages curl)
#:use-module (gnu packages build-tools)
#:use-module (gnu packages maths)
#:use-module (gnu packages glib)
#:use-module (gnu packages gnome)
#:use-module (gnu packages python)
#:use-module (gnu packages ruby)
#:use-module (gnu packages photo)
#:use-module (gnu packages linux)
#:use-module (gnu packages mail)
#:use-module (gnu packages gtk)
#:use-module (gnu packages wget)
#:use-module (gnu packages calendar)
#:use-module (gnu packages webkit)
#:use-module (gnu packages gnupg)
#:use-module (gnu packages tls)
#:use-module (gnu packages algebra)
#:use-module (gnu packages pulseaudio) 
#:use-module (gnu packages freedesktop)
#:use-module (srfi srfi-1))

(define-public cairo-dock-core
    (let ((commit "6c569e67a2a366e7634224a0133ede51755629cb")
          (revision "0")
          (version "3.4.1"))
  (package
    (name "cairo-dock-core")
    (version "3.4.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference 
         (url "https://github.com/Cairo-Dock/cairo-dock-core")
         (commit commit)))
       (sha256
        (base32 "1sxm3191hv15mm0m21xwr37xw5ln1wvqyfpdvwp64f597rf3jrr3"))))
    (native-inputs `(("gettext-minimal" ,gettext-minimal)
                     ("gobject-introspection" ,gobject-introspection)
                     ("pkg-config" ,pkg-config) 
                     ("build" ,build))) ;build-tools
    (build-system  cmake-build-system)
    (inputs
     (list xrandr libxcb cairo pango wayland
           glm curl glu xcb-util-cursor xcb-util mesa
           libgcrypt
           libx11 libxcomposite libxrender
           dbus dbus-glib libxtst glib
           libxml2 librsvg gtk+ gdk-pixbuf))
    (arguments
     (list #:tests? #f
           #:configure-flags '("-Denable-desktop-manager=ON")
           #:phases
           #~(modify-phases %standard-phases
               (add-after
                   'patch-source-shebangs 'patch-sessions-destination
                 (lambda* (#:key outputs #:allow-other-keys)
                   (let* ((out (assoc-ref outputs "out")))
                     (substitute*
                         "data/desktop-manager/CMakeLists.txt"
                       (("DESTINATION \\$\\{bindir\\}")
                        (string-append "DESTINATION " out "/share/xsessions")))
                     (substitute*
                         "data/desktop-manager/gnome-session-3.36/CMakeLists.txt"
                       (("DESTINATION /usr/share")
                        (string-append "DESTINATION " out "/share")))
                     (("DESTINATION /usr/lib")
                        (string-append "DESTINATION " out "/lib"))))))
       ;;  (add-after 'unpack 'preserve-file-permissions
       ;;    (lambda _
       ;;      ;; Installing the man page read-only breaks 'reset-gzip-timestamps.
       ;;      (substitute* (find-files "." "^CMakeLists\\.txt$")
       ;;        (("PERMISSIONS OWNER_READ GROUP_READ WORLD_READ") "")))))
       ))
    (home-page "https://www.github.com/cairodock/cairo-dock-core")
    (synopsis "cairo-dock")
    (description
     "Cairo-Dock is a alternative desktop panel, that can use hardware
accelaration. It features multi-docks, taskbar, launchers and applets, that
can be detached from the dock to act as desktop
widgets.")
    (license license:gpl3+))))

 
(define-public cairo-dock-core-wayland
    (let ((commit "ab5594d621f6a6302b404d9ca960be3c42bab0f1")
          (revision "0")
          (version "3.4.1"))
  (package
    (name "cairo-dock-core-wayland")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference 
         (url "https://github.com/dkondor/cairo-dock-core")
         (commit commit)))
       (sha256
        (base32 "1vgwcjhibbbk35i3gfsl5jxwfwzan7z9da8a6y1syj5xpga1cyp9"))))
    (build-system  cmake-build-system)
    (native-inputs
     (list build gettext-minimal gobject-introspection pkg-config wayland-protocols))
    (inputs
     (list xrandr libxcb cairo pango wayland
           glm curl glu xcb-util-cursor xcb-util mesa
           libgcrypt
           libxcomposite libxrender dbus dbus-glib libxtst glib
           libxml2 librsvg gtk+ gtk-layer-shell gdk-pixbuf wlroots))
    (arguments
     (list #:tests? #f
       #:phases
       #~(modify-phases %standard-phases
         (add-after
             'patch-source-shebangs 'patch-sessions-destination
           (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out")))
                (substitute*
                    "data/desktop-manager/CMakeLists.txt"
                  (("DESTINATION \\$\\{bindir\\}")
                   (string-append "DESTINATION " out "/share/xsessions")))
           (substitute*
                    "data/desktop-manager/gnome-session-3.36/CMakeLists.txt"
                  (("DESTINATION /usr/share")
                   (string-append "DESTINATION " out "/share")))
           (substitute*
                    "data/desktop-manager/gnome-session-3.36/CMakeLists.txt"
                  (("DESTINATION /usr/lib") 
                   (string-append "DESTINATION " out "/lib")))))))
       #:configure-flags '("-Denable-desktop-manager=ON")))
    (home-page "https://www.github.com/dkondor/cairo-dock-core")
    (synopsis "Wayland-capable fork of cairo-dock-core")
    (description "")
    (license license:gpl3+))))

(define-public cairo-dock-plug-ins
  (let ((commit "f24f7699e69b0e25e5e7674c4841dcefc4e6b61d")
        (revision "0")
        (version "3.4.1"))
  (package
    (name "cairo-dock-plug-ins")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference 
         (url "https://github.com/Cairo-Dock/cairo-dock-plug-ins")
         (commit commit)))
       (sha256
        (base32 "0virb29918nr4ppgsiipqrdhfgk5rfnhggl4awixhxqzrlcxlb8q"))
       (patches
        (parameterize
            ((%patch-path
              (map
               (lambda (directory)
                 (string-append directory "/packages/patches"))
               %load-path)))
          (search-patches
           ;;patches are from debian, to fix some plugins
           "cairo-dock-plug-ins.patch"
           "0006-Fix-support-libetpan-pkgconfig.patch"
           "cairo-dock-plugins-port-webkit2.patch")))))
    (build-system  cmake-build-system)
    (arguments
     ;; no rule to make target test
     `(#:tests? #f
       ;; bindings are installed in wrong dir, leave them offscreen
       ;; for now. (vala package seems to be imported by others and
       ;;triggers build)
       #:configure-flags '("-Denable-vala-interface=OFF")         
       #:modules ((guix build cmake-build-system)
                  (guix build utils)
                  (srfi srfi-1))         
       #:phases (modify-phases %standard-phases
                (add-after
             'patch-source-shebangs 'patch-sessions-destination
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (substitute*
                (find-files "." "CMakeLists.txt")
                (("DESTINATION \\$\\{pluginsdir\\}")
                 (string-append "DESTINATION " out "/lib/cairo-dock")))
               (substitute*
                (find-files "." "CMakeLists.txt")
                (("DESTINATION \\$\\{gaugesdir\\}")
                 (string-append "DESTINATION " out "/share/cairo-dock/gauges")))
               (substitute* (find-files "." "CMakeLists.txt")
                  (("DESTINATION \\$\\{localedir\\}")
                   (string-append "DESTINATION " out "/share/locale")))
                (let* ((cmake-files
                        (append-map
                         (lambda (dir)
                           (find-files dir "CMakeLists.txt"))
                         (find-files "." "data$" #:directories? #t))))          
                  (for-each
                   (lambda (dir-in)
                     (let* ((slash-index (string-index dir-in #\/)) 
                            (dir-tail (substring/copy dir-in 0 slash-index))
                            (dir-out (string-append out "/share/cairo-dock/plugins/" dir-tail)))
                       (substitute* dir-in                      
                         (("\\$\\{.*datadir\\}") dir-out))))
                   cmake-files))))))))
    
    (native-inputs
     `(("gettext-minimal" ,gettext-minimal)
       ("gobject-introspection" ,gobject-introspection)
       ("pkg-config" ,pkg-config)
       ("build" ,build)))
    (inputs 
     (list libxtst xrandr libxcb mesa cairo pango
           xcb-util-cursor xcb-util libxcomposite
           cairo-dock-core gvfs wireless-tools
           libdbusmenu libxxf86vm vte
           webkitgtk-with-libsoup2 libindicator
           alsa-lib pulseaudio  upower xdg-utils
           libexif libetpan libxklavier libxml2
           glm wget curl glm glu 
           dbus dbus-glib glib
           libxml2 librsvg gtk+ zeitgeist
           lm-sensors gdk-pixbuf libical openssl fftw))
    (home-page "https://www.glx-dock.org/")
    (synopsis "")
    (description "")
    (license license:gpl3+))))
