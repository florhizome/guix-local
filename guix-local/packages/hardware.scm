(define-module (guix-local packages hardware)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system python)
  #:use-module (guix build-system linux-module)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)  
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages c)
  #:use-module (gnu packages web)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages flex))

(define-public rot8
  (package
    (name "rot8")
    (version "0.1.5")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "rot8" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "088h57ppch3i3gqnad67fimz24js0mjn8v1pb85ysl1gic130q2i"))))
    (build-system cargo-build-system)
    (propagated-inputs (list xrandr))
    (arguments
     `(#:cargo-inputs (("rust-clap" ,rust-clap-3)
                       ("rust-glob" ,rust-glob-0.3)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/efernau/rot8")
    (synopsis "Screen rotation daemon")
    (description
     "rot8 is an automatic display rotation daemon using the built-in accelerometer.
It's compatible with sway and X11.")
    (license license:expat)))

(define-public gamemode
  (package
   (name "gamemode")
   (version "1.7")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/FeralInteractive/gamemode")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1dbakycp1if1paya0n1kk0cgdacp0a3pjr4djsj978b4c6cmr08c"))))
   (build-system meson-build-system)
   (arguments
    (list #:configure-flags
          #~(list
	     (string-append "--libexecdir=" #$output "/libexec")
	     "-Dwith-systemd-user-unit-dir=false"
             ;;"-Dwith-systemd-group=false"
             (string-append "-Dwith-dbus-service-dir=" #$output "share/dbus-1")
             ;;(string-append "-Dwith-pam-limits-dir=" #$output "/etc/security/limits.d")
             "-Dwith-sd-bus-provider=elogind")))
   (native-inputs
    (list appstream dbus libinih pkg-config))
   (inputs
    (list elogind))
   (home-page "https://gitlab.freedesktop.org/hadess/iio-sensor-proxy")
   (synopsis "Optimise Linux system performance on demand")
   (description "Optimise Linux system performance on demand")
   (license license:bsd-3)))

(define-public fwts
  (package
   (name "fwts")
   (version "23.01.00")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/ColinIanKing/fwts")
           (commit (string-append "V" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0irz7vxz0jr9y99z53wdvd33agvvzjhh0wxnhxzflnkfhi7j7q0y"))))
   (build-system gnu-build-system)
   (arguments
    (list #:phases
          #~(modify-phases
	     %standard-phases
	     (add-before
                 'configure 'patch-paths
	       (lambda* (#:key inputs #:allow-other-keys)
		 (substitute* "./src/lib/include/fwts_binpaths.h"
		   (("/usr/bin/lspci")		    
                    (search-input-file inputs "sbin/lspci"))
                   (("/usr/bin/dmidecode")
		    (search-input-file inputs "bin/dmidecode"))
                   (("/usr/bin/iasl")
		    (search-input-file inputs "bin/iasl"))))))))
;;   (arguments
;;    `(#:tests? #f)) ;;doc-tests fail
   (native-inputs
    (list autoconf ;;
          automake
          byacc
          ;;autogen
          ;;gettext-minimal
          libtool
          pkg-config))
   (inputs
    (list acpica bison dmidecode dtc json-c flex glib   libbsd pciutils pcre ))
   (home-page "https://wiki.ubuntu.com/FirmwareTestSuite")
   (synopsis "Firmware test suite")
   (description "")
   (license license:expat)))


(define-public hid-tools
  (package
    (name "hid-tools")
    (version "0.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "hid-tools" version))
              
              (sha256
               (base32
                "17wihbbdbk9gfs0k6i59ci8g2m9wz8p69hisism10994amdp9wl0"))))
    (build-system python-build-system)
    (propagated-inputs (list python-parse python-pyudev))
    (home-page "http://gitlab.freedesktop.org/libevdev/hid-tools")
    (synopsis "HID tools")
    (description "HID tools")
    (license license:gpl3)))

(define-public libgudev-237
  (package/inherit libgudev
    (name "libgudev-237")
    (version "237")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnome/sources/" name "/"
                                  version "/" name "-" version ".tar.xz"))
              (sha256
               (base32
                "1al6nr492nzbm8ql02xhzwci2kwb1advnkaky3j9636jf08v41hd"))))
    (build-system meson-build-system)
    (native-inputs
     `(("glib:bin" ,glib "bin") ; for glib-genmarshal, etc.
       ("gobject-introspection" ,gobject-introspection)
       ("pkg-config" ,pkg-config)))
    (propagated-inputs
     (list glib ; in Requires of gudev-1.0.pc
           eudev))               ; in Requires.private of gudev-1.0.pc
    (inputs
     `(("udev" ,eudev)))))

(define-public iio-sensor-proxy-3.1
  (package
   (name "iio-sensor-proxy-3.1")
   (version "3.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.freedesktop.org/hadess/iio-sensor-proxy")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1dpcc3i53aw5illfhfkwxy0hsjsnya5iw4iv4al46vgyvcnvjc8z"))))
   (build-system meson-build-system)
   (arguments
    (list #:configure-flags
          #~(list
	     (string-append "--sysconfdir=" #$output "/share")
	     "-Dsystemdsystemunitdir=false"
             (string-append "-Dudevrulesdir="
                            #$output "/lib/udev"))
          #:glib-or-gtk? #t
          #:phases
          #~(modify-phases
	     %standard-phases
	     (add-before 'configure 'fake-pkexec
			 (lambda _
			   (setenv "PKEXEC_UID" "-1")))
	     (add-before 'configure 'correct-polkit-dir
			 (lambda _
			   (substitute* "meson.build"
					(("polkit_gobject_dep\\..*")
					 (string-append "'" #$output "/share/polkit-1/actions'"))))))))
   (native-inputs
    (list dbus
          (list glib "bin")
          gobject-introspection
          python
          python-dbusmock
          python-psutil
          pkg-config
          umockdev))
   (inputs
    (list glib libgudev polkit))
   (home-page "https://gitlab.freedesktop.org/hadess/iio-sensor-proxy")
   (synopsis "IIO accelerometer sensor to input device proxy")
   (description "Proxies sensor output like orientation changes and ambient light to userspace.")
   (license license:gpl3)))

(define-public iio-sensor-proxy
  (package/inherit iio-sensor-proxy-3.1
    (name "iio-sensor-proxy")
    (version "3.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.freedesktop.org/hadess/iio-sensor-proxy")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0zf9kjhngs8k8vpwfiyra60vp4zic6gzs7axw1cd736my9pyhniv"))))
    (inputs (list glib libgudev-237 polkit))))


(define-public low-memory-monitor
 (package
    (name "low-memory-monitor")
    (version "2.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.freedesktop.org/hadess/low-memory-monitor")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0431wzn9q2hnx0mwai8w3bcmbln8g76r7wyyhjbkamnl2sccl8jn"))))
    (build-system meson-build-system)
    (arguments
     (list #:configure-flags
           #~(list "-Dsystemdsystemunitdir=false"
                   "-Dtrigger_kernel_oom=true")
           #:glib-or-gtk? #t))
    (native-inputs
     (list (list glib "bin")
           gobject-introspection
           gtk-doc
           libxml2
	   pkg-config))
   (inputs
    (list glib libgudev eudev))
    (synopsis "Daemon to send information about memory pressure to userspace")
    (description
     "Low Memory Monitor is an early boot daemon that will monitor memory
pressure information coming from the kernel, and, when memory pressure means
that memory isn't as readily available and would cause interactivity problems,
 would:

@itemize
@item send D-Bus signals to user-space applications when memory is running low,

@item if configured to do so and memory availability worsens, activate the kernel's
OOM killer.
@end itemize

It is designed for use on traditional Linux systems, with interactive user interfaces
and D-Bus communication.")
    (home-page "https://gitlab.freedesktop.org/hadess/low-memory-monitor")
    (license license:gpl3)))

(define-public rtkit
 (package
    (name "rtkit")
    (version "0.13")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/heftig/rtkit")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "14z09cxahpwvn229vhg1a7fqp65fdjx6rlis2jlb44i10y9fyjyk"))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags
       (let* ((out (assoc-ref %outputs "out"))
              (sharedir (string-append out "/share")))
         (list (string-append  "-Ddatadir=" sharedir)
               (string-append  "-Ddbus_interfacedir="
                               sharedir "/dbus-1/interfaces")
               (string-append  "-Ddbus_systemservicedir="
                               sharedir "/dbus-1/system-services")
               (string-append  "-Dpolkit_actiondir="
                               sharedir "/polkit-1/actions")))
       #:tests? #f
       #:phases                  
       (modify-phases %standard-phases
         (add-before 'configure 'set-useful-envvars
	   (lambda* (#:key outputs #:allow-other-keys)
	     (let ((out (assoc-ref outputs "out")))
	       (setenv "PKEXEC_UID" "-1")))))))
    (native-inputs
     (list pkg-config))
   (inputs
    (list polkit dbus libcap xxd))
   (synopsis "REALTIMEKIT Realtime Policy and Watchdog Daemon")
   (description "RealtimeKit is a D-Bus system service that changes the
        scheduling policy of user processes/threads to SCHED_RR
        (i.e. realtime scheduling mode) on request. It is intended to
        be used as a secure mechanism to allow real-time scheduling to
        be used by normal user processes.

        RealtimeKit enforces strict policies when handing out
        real-time security to user threads:

        * Only clients with RLIMIT_RTTIME set will get RT scheduling

        * RT scheduling will only be handed out to processes with
          SCHED_RESET_ON_FORK set to guarantee that the scheduling
          settings cannot 'leak' to child processes, thus making sure
          that 'RT fork bombs' cannot be used to bypass RLIMIT_RTTIME
          and take the system down.

        * Limits are enforced on all user controllable resources, only
          a maximum number of users, processes, threads can request RT
          scheduling at the same time.

        * Only a limited number of threads may be made RT in a
          specific time frame.

        * Client authorization is verified with PolicyKit

        RealtimeKit can also be used to hand outh high priority
        scheduling (i.e. negative nice level) to user processes.

        In addition to this a-priori policy enforcement, RealtimeKit
        also provides a-posteriori policy enforcement, i.e. it
        includes a canary-based watchdog that automatically demotes
        all real-time threads to SCHED_OTHER should the system
        overload despite the logic pointed out above. For more
        information regarding canary-based RT watchdogs, see the
        Acknowledgments section below.

        In its duty to manage real-time scheduling *securely*
        RealtimeKit runs as unpriviliged user, and uses capabalities,
        resource limits and chroot() to minimize its security impact.

        RealtimeKit probably has little use in embedded or server use
        cases, use RLIMIT_RTPRIO there instead.")
   (home-page "https://github.com/heftig/rtkit")
   ;;some files seem to be bsd licensed?
   (license license:gpl3+)))


(define-public asus-numpad
  (let ((version "0.0")
	(commit "d172acfe7b64e52c008362aba52f0cd635b7458b")
	(revision "0"))
  (package
   (name "asus-numpad")
   (version (git-version version revision commit))
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/xytovl/asus-numpad")
       (commit commit)))
     (sha256
      (base32 "1mghllnv66gsrhlsvy3xmfg4qax6aj23rpmf4hh1n845y5kpg4k8"))))
   (build-system meson-build-system)
   (arguments
    `(#:tests? #f
      #:phases                  
       (modify-phases %standard-phases
         (add-before 'configure 'patch-device
	   (lambda _
             (with-output-to-file "layouts.dat"
               (lambda _
                 (display
                  (string-append
                   "[B5402FEA]\n"
                   "match = B5402FEA\n"
                   "row = KEY_KP7 KEY_KP8 KEY_KP9   KEY_KPSLASH    KEY_BACKSPACE\n"
                   "row = KEY_KP4 KEY_KP5 KEY_KP6   KEY_KPASTERISK KEY_BACKSPACE\n"
                   "row = KEY_KP1 KEY_KP2 KEY_KP3   KEY_KPMINUS    KEY_5\n"
                   "row = KEY_KP0 KEY_KPDOT KEY_KPENTER KEY_KPPLUS KEY_KPEQUAL\n"
                   ))
                 #t))))))) ;;doc-tests fail
   (native-inputs
    (list pkg-config))
;;hardcode libevdev?
   (propagated-inputs
    (list libevdev i2c-tools))
   (home-page "")
   (synopsis "")
   (description "")
   (license license:expat))))

(define-public asusfan
  (package
   (name "asusfan")
   (version "0.9.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/daringer/asus-fan")
       (commit "01ec2213cd89ca2de2c9c18c820d4c07ff661da0")))
     (sha256
      (base32 "0d4j5fkz5935p5z2701il4a30fadgiqymifbqpfxizmfv5bjg0jr"))))
   (build-system linux-module-build-system)
   (arguments
    (list
     #:tests? #f
     #:make-flags #~(list "DEBUG=1")
     #:phases
     #~(modify-phases %standard-phases
         (add-before 'install 'install-script
           (lambda* (#:key outputs #:allow-other-keys)
             (mkdir-p (string-append (assoc-ref outputs "out") "/bin"))
             (copy-file "./misc/asus-fan-create-symlinks.sh"
                        (string-append
                         (assoc-ref outputs "out") "/bin/asus-fan-create-symlinks.sh")))))))
   (native-inputs
    (list  pkg-config))
   (inputs
    (list eudev))
   (home-page "ASUS (Zenbook) fan(s) control and monitor kernel module.")
   (synopsis "")
   (description "")
   (license license:expat)))

(define-public asus-nb-wmi
  (package
   (name "asus-nb-wmi")
   (version "0.0.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/KastB/asus-wmi")
       (commit "7ba2a5c80d0ca0e3d17e7affdd99bd4f9e1d2442")))
     (sha256
      (base32 "0wsx8iirhgyhhcg9gb0pkhq83fq7xfgp3jki9v8mxjd99sir51lc"))))
   (build-system linux-module-build-system)
   (arguments
    `(#:tests? #f))      
   (native-inputs
    (list pkg-config))
   (home-page "")
   (synopsis "")
   (description "")
   (license license:expat)))
