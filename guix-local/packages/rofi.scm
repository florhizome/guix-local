(define-module (guix-local packages rofi)
;;; routine imports
  #:use-module  (guix download)
  #:use-module  (guix git-download)
  #:use-module  ((guix licenses) #:prefix license:)
  #:use-module  (guix utils)
  #:use-module  (guix build utils)
  #:use-module  (guix gexp)
  #:use-module  (guix packages)
  #:use-module  (guix build-system gnu)
  #:use-module  (guix build-system meson)
  #:use-module  (gnu packages)
  #:use-module  (gnu packages base)
  #:use-module  (gnu packages xdisorg)
  #:use-module  (gnu packages gawk)
  #:use-module  (gnu packages web)
  #:use-module  (gnu packages pkg-config)
  #:use-module  (gnu packages gtk)
  #:use-module  (gnu packages freedesktop))

(define-public rofi-wayland
  (package
    (inherit rofi)
    (name "rofi-wayland")
    (version "1.7.5")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/lbonn/rofi")
         (commit
          (string-append version "+wayland1"))
         (recursive? #t)))
       (sha256
         (base32 "0d2h1ys05sixwf508b3cqaailjibh56009cnjz809akgndbqplkm"))
        (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     (list #:tests? #f ;;last test fails
           #:parallel-tests? #f             ; fails in some circumstances
           #:phases
           #~(modify-phases %standard-phases
               (add-before 'configure 'adjust-tests
                 (lambda _
                   (substitute* '("test/helper-expand.c")
                     (("~root") "/root")
                     (("~") "")
                     (("g_get_home_dir \\(\\)") "\"/\"")))))))
    (inputs (modify-inputs
             (package-inputs rofi)
             (append wayland-protocols gdk-pixbuf wayland)))
    (synopsis "Rofi fork that's wayland-ready")
    (description "This is a fork of Rofi with added support for Wayland via the
 layer shell protocol.
Differences with rofi (in wayland mode):
@itemize
@item native window selection mode when running under Wayland.
@item -normal-window flag in Wayland mode.
@item selecting which monitor to run rofi on in Wayland mode,
 rofi only shows up on the currently focused monitor
@item some window locations parameters work partially, x-offset
 and y-offset are only working from screen edges
@item some X11-specific options like -dpi or fake transparency
@end itemize")))


(define-public buku-rofi
  (package
    (name "buku-rofi")
    (version "0.0.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://gitlab.com/danisztls/buku-rofi")
         (commit "095351dc055aecedc428a289549b540cd022c23b")))
       (sha256
        (base32 "1bmj3sv7z7vyiaf6a0xp0dxsd43d4fjkn9ijyrlfr6fprrl5j2ib"))))
    (native-inputs
     (list pkg-config)) 
    (inputs (list sed gawk rofi))
    (propagated-inputs (list buku))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-before 'build 'patch-makefile-paths
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (setenv "PREFIX" out)
               (substitute* "./Makefile"
                 (("/usr/local/bin/buku-rofi")
                  (string-append out "/bin/buku-rofi")))))))))
  (synopsis "Rofi plugin interfacing with buku")
  (description "This rofi plugin tries to provide an interface to
the buku bookmark manager, improving on the (outdated) buku_run script.")
  (home-page "https://gitlab.com/danisztls/buku-rofi")
  (license license:gpl3)))
