(define-module (guix-local packages  initware)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages base)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages glib)
  
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages javascript)
  
  #:use-module (gnu packages bash))

(define-public iw-cjson
  (package
   (inherit cjson)
   (name "iw-cjson")
   (version "1.7.15")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/InitWare/iw-cjson")
                  (commit "f5827ba03cce31504f6dd701cc751448522da1dd"))
                 ;;(file-name (git-file-name name version)
                 ;;https://github.com/InitWare/iw-cjson/commit/f5827ba03cce31504f6dd701cc751448522da1dd
                 )
            (sha256
             (base32 "1rq99kfxs32r240smhgwsdkql0mhnidv18x3ish59c16lip5pdj6"))))
   (arguments
     `(#:tests? #f))))


(define-public initware
  (package
   (name "initware")
   (version "0.8")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference (url "https://github.com/InitWare/InitWare")
                     (commit "9d808d459fa31d28a3984fea31e66afa33015a08")))
     (sha256
      (base32 "076wkj269fspchhk728a43gmrf95rnm56rl8l5vircvznj6j5dpl"))))
   (build-system cmake-build-system)
   (arguments
     `(#:tests? #f))
   (native-inputs
    (list
     ;;gettext-minimal
     m4
     pkg-config))
   (inputs
    (list
     iw-cjson
     ;;blibxml2
     libdrm
     dbus
     libevdev
     eudev
     libx11
     ;; libxslt
     linux-pam
     polkit
     gperf
     ;;pm-utils
     inotify-tools
     ))
   (home-page "https://consolekit2.github.io/ConsoleKit2/")
   (synopsis "")
   (description "")
   (license license:expat)))

initware
