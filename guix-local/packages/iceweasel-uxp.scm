(define-module (guix-local packages  iceweasel-uxp)
  #:use-module (gnu packages base)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages libreoffice)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages video)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages assembly)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  
  #:use-module (gnu packages python))



(define-public iceweasel-uxp
  (package
   (name "iceweasel-uxp")
   (version "3.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://git.hyperbola.info:50100/software/iceweasel-uxp.git")
           (commit (string-append "v" version))))
     (sha256
      (base32 "0bhiiybgz864gm0af2d738sl2p119l2z3gjqhz8pkjp7npjb1zyn"))))
   (build-system gnu-build-system)
   (native-inputs (list pkg-config sed findutils grep xvfb-run python-2.7 imake autoconf-2.13 automake libtool yasm  diffutils))
   (inputs (list jack-2 pulseaudio hunspell gtk+ libevent libxt mesa ffmpeg libvpx))
   
   ;;(arguments '() ;tests not found
   (home-page "")
   (description "")
   (synopsis "")
   (license license:expat)))

iceweasel-uxp 


