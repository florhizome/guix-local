(define-module  (guix-local packages wayland-updates)
  #:use-module  (guix download)
  #:use-module  (guix git-download)
  #:use-module  (guix build-system gnu)
  #:use-module  (guix build-system meson)
  #:use-module  (guix build-system cmake)
  #:use-module  ((guix licenses) #:prefix license:)
  #:use-module  (guix utils)
  #:use-module  (guix build utils)
  #:use-module  (guix gexp)
  #:use-module  (guix packages)
  #:use-module  (gnu packages)
  #:use-module  (gnu packages base)
  #:use-module  (gnu packages check)
  #:use-module  (gnu packages bash)
  #:use-module  (gnu packages autotools)
  #:use-module  (gnu packages gtk)
  #:use-module  (gnu packages gnome)
  #:use-module  (gnu packages glib)
  #:use-module  (gnu packages image)
  #:use-module  (gnu packages maths)
  #:use-module  (gnu packages build-tools)
  #:use-module  (gnu packages pulseaudio)
  #:use-module  (gnu packages linux)
  #:use-module  (gnu packages vulkan)
  #:use-module  (gnu packages pkg-config)
  #:use-module  (gnu packages freedesktop)
  #:use-module  (gnu packages xdisorg)
  #:use-module  (gnu packages gl)
  #:use-module  (gnu packages xorg) 
  #:use-module  (gnu packages xml)
  #:use-module  (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages version-control)
  #:use-module  (gnu packages build-tools)
  #:use-module  (gnu packages video)
  #:use-module  (gnu packages wm)
  #:use-module  (gnu packages fontutils)
  #:use-module  (gnu packages cmake)
  #:use-module  (gnu packages ninja)
  #:use-module  (gnu packages admin)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages autotools))

(define-public libdrm-next
  (package
   (inherit libdrm)
   (name "libdrm-next")
   (version "2.4.114")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://cgit.freedesktop.org/drm/libdrm")
        (commit "b9ca37b3134861048986b75896c0915cbf2e97f9")))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1vxfha0anvxf2i1lirh0m35avkv9rdmhd5c3s6fp6g432963vgrh"))))))

(define-public wayland-next
  (package
   (inherit wayland)
   (name "wayland-next")
   (version "1.21.0")
   (source
    (origin
     (method git-fetch)
      (uri
       (git-reference
        (url "https://gitlab.freedesktop.org/wayland/wayland")
        (commit version)))
      (file-name (git-file-name name version))
       (sha256
        (base32 "0fwad6w5jm32c04wh4gca7d1ixdj4b9dnsiy1h6qd9nxs0w47wwy"))))))

(define-public wayland-protocols-latest
  (package
   (inherit wayland-protocols)
   (name "wayland-protocols-latest")
   (version "1.31")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://gitlab.freedesktop.org/wayland/wayland-protocols")
        (commit version)))
      (file-name (git-file-name name version))
       (sha256
        (base32 "0kpyvnzlwfj9d57v43z5fhk7fliz6224m4hw1xj425c8vrjbw0nx"))))))

(define-public hwdata
  (package
    (name "hwdata")
    (version "0.365")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/vcrhonek/hwdata")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "00gqx24dyy9l98ygnvx8i087xq8pl9d2393h4d2cm4d5nnvr51d4"))))
    (build-system gnu-build-system)
    
    (arguments (list #:tests? #f
                     #:configure-flags
                     #~(list (string-append "--prefix=" #$output)
                             (string-append "--datadir=" #$output "/share")))) ;lspci not found
    (native-inputs (list pkg-config))
    (home-page "https://gitlab.gnome.com/jadahl/libdecor")
    (synopsis "")
    (description "")
    (license license:expat))) ;;?

(define-public wlroots-0.16
  (package
   (inherit wlroots)
   (name "wlroots-0.16")
   (version "0.16.0")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://gitlab.freedesktop.org/wlroots/wlroots")
        (commit version)))
       (sha256
        (base32 "18rfr3wfm61dv9w8m4xjz4gzq2v3k5vx35ymbi1cggkgbk3lbc4k"))))
   (propagated-inputs
    `(("libxcb" ,libxcb)
      ("pixman" ,pixman)
      ("eudev" ,eudev)
      ("seatd" ,seatd)
      ("elogind" ,elogind)
      ("mesa" ,mesa)
      ("libinput" ,libinput)
      ("libxkbcommon" ,libxkbcommon)
      ("xcb-util-renderutil" ,xcb-util-renderutil)
      ("xcb-util-errors" ,xcb-util-errors)
      ("xcb-util-wm" ,xcb-util-wm)
      ("xcb-util" ,xcb-util)))
   (inputs
    (modify-inputs
     (package-propagated-inputs wlroots)
     (replace "wayland-protocols" wayland-protocols-latest)
     (replace "wayland" wayland-next)
     (append libdrm-next)
     (append hwdata)))
   (arguments '())))


(define-public wlroots-0.15
  (package
   (inherit wlroots)
   (name "wlroots-0.15")
   (version "0.15.0")
   (source
    (origin
      (method url-fetch)
      (uri
       (string-append
        "https://gitlab.freedesktop.org/wlroots/wlroots/-/releases/" version "/downloads/wlroots-" version ".tar.gz"))
       (sha256
        (base32 "0y225shwnk6rv9l2831czzbsinxf3yz4snl9vbqvdnvrxa6a6899"))))
   (propagated-inputs
    `(("libxcb" ,libxcb)
      ("pixman" ,pixman)
      ("eudev" ,eudev)
      ("seatd" ,seatd)
      ("elogind" ,elogind)
      ("mesa" ,mesa)
      ("libinput" ,libinput)
      ("libxkbcommon" ,libxkbcommon)
      ("xcb-util-renderutil" ,xcb-util-renderutil)
      ("xcb-util-errors" ,xcb-util-errors)
      ("xcb-util-wm" ,xcb-util-wm)
      ("xcb-util" ,xcb-util)))
   (inputs
    (modify-inputs
     (package-propagated-inputs wlroots)
     (replace "wayland-protocols" wayland-protocols-next)
     (append libdrm-next)))
   (arguments '())))
   
