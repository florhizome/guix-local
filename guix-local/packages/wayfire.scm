(define-module  (guix-local packages wayfire)
  #:use-module  (guix download)
  #:use-module  (guix git-download)
  #:use-module  (guix build-system gnu)
  #:use-module  (guix build-system meson)
  #:use-module  (guix build-system cmake)
  #:use-module  ((guix licenses) #:prefix license:)
  #:use-module  (guix utils)
  #:use-module  (guix build utils)
  #:use-module  (guix gexp)
  #:use-module  (guix packages)
  #:use-module  (gnu packages)
  #:use-module  (gnu packages base)
  #:use-module  (gnu packages check)
  #:use-module  (gnu packages bash)
  #:use-module  (gnu packages autotools)
  #:use-module  (gnu packages gtk)
  #:use-module  (gnu packages cpp)
  #:use-module  (gnu packages gnome)
  #:use-module  (gnu packages admin)
  #:use-module  (gnu packages glib)
  #:use-module  (gnu packages image)
  #:use-module  (gnu packages maths)
  #:use-module  (gnu packages build-tools)
  #:use-module  (gnu packages pulseaudio)
  #:use-module  (gnu packages linux)
  #:use-module  (gnu packages vulkan)
  #:use-module  (gnu packages pkg-config)
  #:use-module  (gnu packages freedesktop)
  #:use-module  (gnu packages xdisorg)
  #:use-module  (gnu packages gl)
  #:use-module  (gnu packages xorg)
  #:use-module  (gnu packages xml)
  #:use-module  (gnu packages gcc)
  #:use-module  (gnu packages boost)
  #:use-module  (gnu packages version-control)
  #:use-module  (gnu packages build-tools)
  #:use-module  (gnu packages video)
  #:use-module  (gnu packages wm)
  #:use-module  (gnu packages fontutils)
  #:use-module  (gnu packages cmake)
  #:use-module  (gnu packages ninja)
  #:use-module  (gnu packages python-xyz)
  #:use-module  (gnu packages autotools)
  ;;local packages
  #:use-module (guix-local packages wlroots-xyz)
  #:use-module (guix-local packages wayland-updates))

;;cmake and doctest are only inputs for testing
(define-public wf-config
  (package
    (name "wf-config")
    (version "0.7.1")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/WayfireWM/wf-config")
         (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1l1yqhmdpwvzb416i29hcvasisbx2f419nlvyl0q2gaw6ay02d80"))))
      (build-system meson-build-system)
      (arguments '(#:build-type "release"))
      (native-inputs (list pkg-config wayland cmake doctest))
      (propagated-inputs (list glm libevdev libxml2))
      (native-search-paths
       (list (search-path-specification
              (variable "WAYFIRE_PLUGIN_PATH")
              (files '("lib/wayfire")))
             (search-path-specification
              (variable "WAYFIRE_PLUGIN_XML_PATH")
              (files '("share/wayfire/metadata")))))
    (search-paths native-search-paths)
    (home-page "https://wayfire.org")
    (synopsis "Config library for Wayfire")
    (description "synopsis")
    (license license:expat)))

(define wf-touch-src
  (let ((commit "8974eb0f6a65464b63dd03b842795cb441fb6403"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/WayfireWM/wf-touch")
            (commit commit)))
      (sha256
       (base32 "08g1xy9g6206hzh4s4hfhnak6ysrf772iva4536apmwblmw1hfrj"))
      (file-name (git-file-name "wf-touch" commit)))))

(define wf-utils-src
  (let ((commit "25ed62f35c0b7810beee2009c6a419847f8f89fe"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/WayfireWM/wf-utils")
            (commit commit)))
      (sha256
       (base32 "15kwsay92qwqffpjd8zsfrc90hw71g97hh2rr8dy1a8vlni8868q"))
      (file-name (git-file-name "wf-utils" commit)))))

(define-public wayfire
  (package
    (name "wayfire")
    (version "0.7.5")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/WayfireWM/wayfire")
         (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0c9iqkixxmbjjz0dicn8ffdd41nwh812zgf7ydsi9sva90br51lk"))))
    (build-system meson-build-system)
    (arguments
     (list #:build-type "release"
           #:configure-flags
           #~(list "-Duse_system_wfconfig=enabled"
                   "-Duse_system_wlroots=enabled")
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'add-external-sources
                 (lambda* (#:key inputs #:allow-other-keys)
                   (copy-recursively #$wf-touch-src "subprojects/wf-touch")
                   (copy-recursively #$wf-utils-src "subprojects/wf-utils")))
               (add-after 'unpack 'patch-shell-path
                 (lambda*
                     (#:key inputs #:allow-other-keys)
                   (substitute* "src/meson.build"
                     (("/bin/sh")
                      (string-append
                       (assoc-ref inputs "bash")
                       "/bin/bash")))
                   (substitute* "src/core/core.cpp"
                     (("/bin/sh")
                      (string-append
                       (assoc-ref inputs "bash")
                       "/bin/bash"))))))))
    (native-inputs
     (list doctest pkg-config cmake bash)) ;;doctest
    (inputs
     (list cairo
           glm
           freetype
           libdrm-next
           json-modern-cxx
           libseat
           mesa
           libinput
           libxkbcommon libevdev freetype libjpeg-turbo libpng
           pango pixman
           wf-config
           wayland-next
           wayland-protocols-latest))
    (propagated-inputs (list pango wlroots-0.16))
    (native-search-paths
     (list (search-path-specification
            (variable "WAYFIRE_PLUGIN_PATH")
            (files '("lib/wayfire")))
           (search-path-specification
            (variable "WAYFIRE_PLUGIN_XML_PATH")
            (files '("share/wayfire/metadata")))))
    (search-paths native-search-paths)
    (home-page "https://wayfire.org")
    (synopsis "Modular and extensible wayland compositor")
    (description "Wayfire is a 3D Wayland compositor, inspired by
 Compiz and based on wlroots.
It aims to create a customizable, extendable and lightweight
 environment without sacrificing its appearance.")
    (license license:expat)))

(define wf-shell-gvc-src
  (let ((commit "468022b708fc1a56154f3b0cc5af3b938fb3e9fb"))
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/GNOME/libgnome-volume-control")
            (commit commit)))
      (file-name (git-file-name "libgnome-volume-control" commit))
      (sha256
       (base32 "01wrl875r52v7f12yrx3qdn2d2s3h2xz7jx1dswydypd9b6qss0d")))))

(define-public wf-shell
  (let ((commit "c9639087aca3ad69bbd8f56f4213768639b4c8d0")
        (version "0.7.0")
        (revision "0"))
    (package
      (name "wf-shell")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/WayfireWM/wf-shell")
               (commit commit)))
         (sha256
          (base32 "1ibaw944frbwszsmvcflw04izj612viyc7pw1jqw5fk5jkcmigwh"))))
      (build-system meson-build-system)
      (arguments
       (list #:glib-or-gtk? #t
             #:configure-flags #~(list "-Dwayland-logout=false")
             #:phases
             #~(modify-phases
                %standard-phases
                (add-after
                 'unpack 'add-external-sources
                 (lambda* (#:key inputs #:allow-other-keys)
                   (copy-recursively #$wf-shell-gvc-src "subprojects/gvc"))))))
      (native-inputs (list pkg-config gobject-introspection))
      (inputs (list alsa-lib
                    gtk-layer-shell
                    gtkmm-3
                    glib
                    libdrm-next
                    libseat
                    pulseaudio
                    wayfire
                    wf-config
                    wayland-next
                    wlroots-0.16))
      (native-search-paths
       (package-native-search-paths wayfire))
      (home-page "https://wayfire.org")
      (synopsis "GTK3-based graphical shell for wayfire")
      (description "Wf-shell is a repository which contains the
various components needed to built a fully functional DE based
around wayfire. It has a GTK-based panel, dock and a background wallpaper client.")
      (license license:expat))))

(define-public wcm
  (let ((commit "5a82f0a0c76c7878f8a5e78dd02dc563202547a0")
        (version "0.7.0")
        (revision "0"))
    (package
      (name "wcm")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/WayfireWM/wcm.git")
           (commit commit)))
         (sha256
          (base32 "0pbi1mf60is2bq25vj7idvzdvcla7py9icdqbilqryfmww555b29"))))
      (build-system meson-build-system)
      (arguments
       (list #:configure-flags
         #~(list "-Denable_wdisplays=false")))
      (native-inputs
       (list pkg-config `(,glib "bin") wayland-protocols))
      (inputs (list gtk+
                    libdrm-next
                    libseat
                    wayfire
                    wayland-next
                    wf-config
                    wlroots-0.16))
      (propagated-inputs (list wdisplays))
      (native-search-paths (package-native-search-paths wayfire))
      (search-paths  native-search-paths)
      (home-page "https://wayfire.org")
      (synopsis "Wayfire Config Manager")
      (description "Wayfire Config Manager is a Gtk3 application to
configure wayfire. It writes the config file that wayfire reads to
update option values.")
      (license license:expat))))

(define-public wayfire-plugins-extra
  (let
      ((commit "0rqz72gbgwbj558n6ja31lqdv5js28pnggwfrwjbxfig41401d02")
       (version "0.7.5")
       (revision "0"))
    (package
      (name "wayfire-plugins-extra")
      (version version)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/WayfireWM/wayfire-plugins-extra")
               (commit (string-append "v" version))))

         (sha256
          (base32 "0ac01yial2zazqhrqaa59aj83diw71jsmc634b8gnvm811nwp9qg"))))
      (build-system meson-build-system)
      (arguments
       (list #:glib-or-gtk? #t
            #:phases
         #~(modify-phases %standard-phases
           (add-after
               'unpack 'alter-target-dirs
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out")))
                 ;;subprojects are in ./subprojects/projectname/meson.build
                 (substitute* (list "./metadata/meson.build"
                                    ;;"./subprojects/dbus/meson.build"
                                    ;;"./subprojects/windecor/meson.build"
                                    ;;"./subprojects/wayfire-shadows/meson.build"
                                    )
                   (("wayfire\\.get_variable\\(pkgconfig: 'metadatadir'\\)")
                    (string-append "'" out "/share/wayfire/metadata'")))

                 #t))))))

      (native-inputs (list pkg-config gobject-introspection))
    (inputs (list hwdata wayfire gtkmm-3 wf-config librsvg
                  pango wayland-next wlroots-0.16 libdrm-next libseat))
    (native-search-paths (package-native-search-paths wayfire))
    (home-page "https://wayfire.org")
    (synopsis "Additional plugins for Wayfire")
    (description "This is a package with additional wayfire plugins that have
 external dependencies, debugging purposes or otherwise will not be included
 with wayfire core plugins.")
    (license license:expat))))
