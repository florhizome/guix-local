;;; Copyright © 2021 florhizome <florhizome@posteo.net>
;;; Copyright © 2020 Ryan Prior <rprior@protonmail.com>
;;; Copyright © 2020 L  p R n  d n <guix@lprndn.info>
;;; Copyright © 2020 pkill9 <pkill9@runbox.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-local packages  pantheon-switchboard-plugs)
  #:use-module (gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages file)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages firmware)  
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pantheon)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages text-editors)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages code)
  #:use-module (gnu packages ibus)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages iso-codes)
  #:use-module (gnu packages language)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages backup)
  #:use-module (ice-9 optargs)
  #:use-module (guix-local packages pantheon))

(define-public switchboard
  (package
    (name "switchboard")
    (version "6.0.2")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/elementary/switchboard")
         (commit version)))
       (sha256
        (base32 "0j3x46xaq6xwcqkfwjqwhp2hb0gp3238w6z7rd2vg5kv1nfrmkyr"))
       (patches
        ;;search switchboard plugs via env var (from nixos)
        (parameterize
            ((%patch-path
              (map
               (lambda (directory)
                 (string-append directory "/guix-local/packages/patches"))
               %load-path)))
          (search-patches "plugs-path-env.patch")))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("vala" ,vala)
       ("glib:bin" ,glib "bin")
       ("gettext" ,gettext-minimal)
       ("gobject-introspection"  ,gobject-introspection)))
    (inputs (list gtk+ granite libgee libhandy))   
    (native-search-paths
     (list (search-path-specification
            (variable "SWITCHBOARD_PLUGS_PATH")
            (files '("/lib/switchboard")))))
    (home-page "https://github.com/elementary/switchboard")
    (synopsis "Extensible System Settings app designed for elementary OS")
    (description "Switchboard is just the container application for Switchboard
 Plugs, which provide the actual settings for various hardware and software.")
    (license license:lgpl2.1)))

(define make-switchboard-plug
  (lambda*
      (name version hash
            #:key (more-inputs '()) (synopsis "") (description ""))
    (package
     (name name)
     (version version)
     (source
      (origin
       (method git-fetch)
       (uri
        (git-reference
         (url (string-append "https://github.com/elementary/" name))
         (commit version)))
       (sha256
        (base32 hash))))
     (build-system meson-build-system)
     (arguments
      `(#:glib-or-gtk? #t))
     (native-inputs 
      (list pkg-config vala `(,glib "bin") gettext-minimal
            gobject-introspection))
     (inputs (cons* libgee gtk+ libhandy granite switchboard more-inputs))
     (native-search-paths
      (list (search-path-specification
             (variable "SWITCHBOARD_PLUGS_PATH")
             (files '("/lib/switchboard")))))
     (home-page "https://elementary.io")
     (synopsis synopsis)
     (description description)
     (license license:gpl2+))))

(define-public switchboard-plug-a11y
  (make-switchboard-plug
   "switchboard-plug-a11y"
   "2.3.0"
   "0dc5jv335j443rg08cb7p8wvmcg36wrf1vlcfg9r20cksdis9v4l"))

(define-public switchboard-plug-about
  (make-switchboard-plug
   "switchboard-plug-about"
   "6.1.0"
   "0bjdcl3myix2xa47vy02rwzlji0hf1rpbisc9w7ai5gk4v2vghpz"
   ;;FIXME appstream not found by gobject
   #:more-inputs (list fwupd
                       libjcat
                       json-glib-minimal
                       gnutls
                       curl
                       libgtop
                       appstream
                       appstream-glib)))

(define-public switchboard-plug-applications
  (make-switchboard-plug
   "switchboard-plug-applications"
   "6.0.1"
   "18izmzhqp6x5ivha9yl8gyz9adyrsylw7w5p0cwm1bndgqbi7yh5"
   #:more-inputs (list libostree flatpak)))

(define-public switchboard-plug-bluetooth
  (make-switchboard-plug
   "switchboard-plug-bluetooth"
   "2.3.6"
   "0n9fhi9g0ww341bjk6lpc5ppnl7qj9b3d63j9a7iqnap57bgks9y"
   #:synopsis  "Switchboard Bluetooth Plug"
   #:description "Switchboard Bluetooth Plug."))

(define-public switchboard-plug-display
  (make-switchboard-plug
   "switchboard-plug-display"
   "2.3.3"
   "05z9fab1zks5bzzxvhfsqagw7v01mfwrfv34hdfjysdcg7xpwvkp"))

(define-public switchboard-plug-network
  (make-switchboard-plug
   "switchboard-plug-network"
   "2.4.4"
   "0n9fhi9g0ww341bjk6lpc5ppnl7qj9b3d63j9a7iqnap57bgks9y"
   #:synopsis  "Switchboard Network Plug"
   #:description "Switchboard Network Plug.")) 

(define-public switchboard-plug-printers
  (make-switchboard-plug
   "switchboard-plug-printers"
   "2.2.1"
   "19pg9gliyy40sbcwgyir9120mjk9ih6awh86hz6j2knwlh4355j4"
   #:more-inputs (list cups)))

(define-public switchboard-plug-security-privacy
  (make-switchboard-plug
   "switchboard-plug-security-privacy"
   "2.3.0"
   "1yjl3acxab8h1hc5z783x7j4amzq2680nxb5mls70ybz2kn54jm6"
   #:more-inputs (list polkit zeitgeist)))

(define-public switchboard-plug-sound
  (make-switchboard-plug
   "switchboard-plug-sound"
   "2.3.2"
   "1spb236xyhqlmn31x98njn3fqvzk7di789vwmrh8zy0zbnsrhwbb"
   #:more-inputs (list pulseaudio libcanberra)))

(define-public switchboard-plug-sharing
  (make-switchboard-plug
   "switchboard-plug-sharing"
   "2.1.6"
   "0vsa52b08c6mqnkgb8b1mqg3i0cq9pv7xnqgx18rp4fgk383fflb"
   #:synopsis  "Switchboard Sharing Plug"
   #:description "Switchboard Sharing Plug."))

(define-public switchboard-plug-onlineaccounts
  (make-switchboard-plug
   "switchboard-plug-onlineaccounts"
   "6.5.1"
   "02gd2g9mmx18k97h5hp6lmmfl28vsv9r6d4pk7q6q3v5zqwrpqpd"
   #:more-inputs
   (list evolution-data-server-3.44)
   #:synopsis  "Switchboard Online Accounts Plug"
   #:description "Switchboard Online Accounts Plug"))

(define-public switchboard-plug-pantheon-shell
  (make-switchboard-plug
     "switchboard-plug-pantheon-shell"
     "6.3.1"
     "1zdf4rjxczhla484y8l6qx7wjlimryfxzic460x0546vlcnq0alp"
     #:more-inputs
     (list gexiv2 gnome-desktop gnome-settings-daemon plank bamf)
    #:synopsis  "Switchboard Desktop Plug"
    #:description "Switchboard Desktop Plug."))

(define-public switchboard-plug-mouse-touchpad
  (make-switchboard-plug
    "switchboard-plug-mouse-touchpad"
    "6.1.0"
    "0nqgbpk1knvbj5xa078i0ka6lzqmaaa873gwj3mhjr5q2gzkw7y5"
    #:more-inputs (list libxml2)
    #:synopsis  "Switchboard Mouse & Touchpad Plug"
    #:description "Switchboard Mouse & Touchpad Plug."))  

;;FIXME need to turn off config flag for systemd
(define-public switchboard-plug-parental-controls
  (make-switchboard-plug
   "switchboard-plug-parental-controls"
   "6.0.1"
   "0cga4mncbw8qx5w7vf07vqxl04a7s4rs8q9123p6m1bgj6arj9jl"
   #:more-inputs (list libostree accountsservice polkit malcontent dbus flatpak)))

(define-public switchboard-plug-wacom
  (make-switchboard-plug
   "switchboard-plug-wacom"
   "1.0.1"
   "0qj9zl63szqqg2szwm7wqc6sb2kqklxi7n7w9vxzn3mni168qkzq"
   #:more-inputs (list libxi libx11 libwacom libgudev)))

(define-public switchboard-plug-wallet
  (make-switchboard-plug
   "switchboard-plug-wallet"
   ;;FIXME find a better way to do version
   "bb971cada851d8eaeaf611a81b240e14de7a8b39"
   "12sy09hv8km3y52cnjnya18rxri6qz57qbyx85r4rbjaj8w5q0jk"
   #:more-inputs (list libsecret)))

;;FIXME other licenses - lgpl2.1 lgpl3 gpl2
(define-public switchboard-plug-power
  (make-switchboard-plug
   "switchboard-plug-power"
   "2.7.0"
   "05z9fab1zks5bzzxvhfsqagw7v01mfwrfv34hdfjysdcg7xpwvkp"
   #:more-inputs (list dbus polkit)))

(define-public switchboard-plug-notifications
  (make-switchboard-plug "switchboard-plug-notifications" "2.2.0"
                         "0zzhgs8m1y7ab31hbn7v8g8k7rx51gqajl243zmysn86lfqk8iay"
   #:synopsis "Switchboard Notifications Plug"
   #:description "Switchboard Notifications Plug."))

;;FIXME need to turn off config flag for systemduserunitdir
(define-public switchboard-plug-parental-controls
  (make-switchboard-plug
   "switchboard-plug-parental-controls"
   "6.0.0"
   "0cga4mncbw8qx5w7vf07vqxl04a7s4rs8q9123p6m1bgj6arj9jl"
   #:more-inputs (list libostree accountsservice polkit malcontent dbus flatpak)))


(define-public switchboard-plug-useraccounts
  (make-switchboard-plug
   "switchboard-plug-useraccounts"
   "2.4.2"
   "1y2zjjkl829hxa5x6bylsph28zqak8r5v5zim0mnzkw6883y7dym"
   #:more-inputs
   (list accountsservice
         gnome-desktop
         libpwquality 
         libxml2
         polkit
         polkit-gnome)
   #:synopsis "Switchboard useraccounts Plug"
   #:description "Switchboard useraccounts Plug."))

(define-public switchboard-plug-pantheon-tweaks
  (package
   (name "switchboard-plug-pantheon-tweaks")
   (version "1.0.3")
   (source
    (origin
      (method git-fetch)
      (uri
       (git-reference
        (url
         "https://github.com/pantheon-tweaks/pantheon-tweaks")
         (commit version)))
      (sha256
       (base32
        "0scli5n4nmsx7bmlf3b5ahl39cfzq6z379mpwpcg8jynkwhcqzry"))))
   (build-system meson-build-system)
   (arguments
    `(#:glib-or-gtk? #t))
   (native-inputs (list python pkg-config vala `(,glib "bin") `(,gtk+ "bin")
                        gettext-minimal gobject-introspection))
   (inputs (list libgee gtk+ granite switchboard))
   (home-page "https://github.com/pantheon-tweaks/pantheon-tweaks")
   (synopsis "Switchboard Notifications Plug")
   (description "Switchboard Notifications Plug.")
   (license license:gpl2+)))
