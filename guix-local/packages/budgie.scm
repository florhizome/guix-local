(define-module (guix-local packages  budgie)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages python)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages ibus)  
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages video)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages web)
  
  #:use-module (gnu packages glib)  
  #:use-module (gnu packages virtualization)  
  #:use-module (gnu packages iso-codes))

(define-public libwnck-40.1
  (package
   (inherit libwnck)
   (name "libwnck")
   (version "40.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/GNOME/libwnck")
       (commit version)))
     (sha256
      (base32 "06vws3w3aj913gfmf251bc9jf9hp439jvqil31imid3d0caf4igm"))))
    (build-system meson-build-system)
    ))


(define-public budgie-desktop
  (package
   (name "budgie-desktop")
   (version "10.6.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/BuddiesOfBudgie/budgie-desktop")
       (commit (string-append "v" version))
       (recursive? #t)))
     (sha256
      (base32 "0im36q5q5lyfhs0dk68293fsxw5kcm4y4x2j6czi818wll9f9b04"))))
   (build-system meson-build-system)
   (native-inputs
    (list `(,glib "bin") vala sassc `(,gtk+ "bin") ;;desktop-file-utils
          gobject-introspection pkg-config intltool gtk-doc))
   (inputs
    (list libnotify mutter gnome-menus gnome-bluetooth upower
          gsettings-desktop-schemas libhandy dbus dbus-glib libgudev
          adwaita-icon-theme budgie-screensaver libpeas polkit polkit-gnome
          pulseaudio  gobject-introspection  alsa-lib glib atk
          libxml2 gtk+ gnome-desktop network-manager gtk
          gnome-settings-daemon libwnck-40.1 accountsservice ibus))
   (home-page "https://wayfire.org")
   (synopsis "")
   (description "")
   (license license:expat)))

(define-public budgie-screensaver
  (package
   (name "budgie-screensaver")
   (version "5.0.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/BuddiesOfBudgie/budgie-screensaver")
       (commit (string-append "v" version))))
     (sha256
      (base32 "1j1vgmvkqlcsqx58bkqdpsh7wghdmwv7h8qcs7di7g91znziqmj5"))))
   (build-system meson-build-system)
   (arguments `(#:configure-flags '("-Dwith-systemd=false")))
   (native-inputs
    (list `(,glib "bin")
          gobject-introspection pkg-config cmake intltool))
   (inputs
    (list gsettings-desktop-schemas libhandy dbus dbus-glib
          polkit polkit-gnome libxml2 glib linux-pam
          libdazzle gtk+ gnome-desktop))
   (home-page "")
   (synopsis "")
   (description "")
   (license license:expat)))

(define-public budgie-control-center
  (package
    (name "budgie-control-center")
   (version "1.0.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/BuddiesOfBudgie/budgie-control-center")
       (commit (string-append "v" version))
       (recursive? #t)))
     (sha256
      (base32 "0lzvwpnv0hg8m3pnyvs027ivz98jm596l93f9jpcjkrad14ppbl5"))))
   (build-system meson-build-system)
   (arguments `(#:configure-flags '("-Dwayland=true")))
   (native-inputs
    (list `(,glib "bin") python
          gobject-introspection pkg-config cmake intltool))
   (inputs
    (list gsettings-desktop-schemas libhandy dbus dbus-glib
          polkit polkit-gnome libxml2 glib linux-pam
          gtk+ gnome-desktop ibus))
   (home-page "")
   (synopsis "")
   (description "")
   (license license:expat)))
