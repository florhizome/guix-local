(define-module (guix-local home services desktop)
  #:use-module (guix gexp)
  #:use-module (gnu packages freedesktop)
  ;;#:use-module (guix-local packages pulseaudio)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd))

(define (home-xdg-desktop-portal-shepherd-service _)
  (list
   (shepherd-service
    ;;(requirement '(dbus))
    (provision '(xdg-desktop-portal))
    (stop  #~(make-kill-destructor))
    (start #~(make-forkexec-constructor
              (list #$(file-append xdg-desktop-portal "/libexec/xdg-desktop-portal")))))
   (shepherd-service
    (requirement '(xdg-desktop-portal))
    (provision '(xdg-desktop-portal-gtk))
    (stop  #~(make-kill-destructor))
    (start #~(make-forkexec-constructor
              (list #$(file-append xdg-desktop-portal-gtk
                                   "/libexec/xdg-desktop-portal-gtk")))))))

(define-public home-xdg-desktop-portal-service-type
  (service-type
   (name 'home-xdg-desktop-portal)
   (extensions
    (list (service-extension
           home-shepherd-service-type
           home-xdg-desktop-portal-shepherd-service)
          (service-extension
           home-profile-service-type
           (const (list xdg-desktop-portal xdg-desktop-portal-gtk)))))
   (default-value #f)
   (description "")))

(define (home-rot8-shepherd-service _)
  (list
   (shepherd-service
    (provision '(rotation-daemon))
    (stop  #~(make-kill-destructor))
    (start #~(make-forkexec-constructor
              (list #$(file-append rust-rot8 "/bin/rot8") "--touch-screen=eDP-1"))))))

(define-public home-rot8-service-type
  (service-type
   (name 'home-rot8)
   (extensions
    (list (service-extension
           home-shepherd-service-type
           home-rot8-shepherd-service)))
   (default-value #f)
   (description "")))
