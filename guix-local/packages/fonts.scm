(define-module (guix-local packages fonts)
  #:use-module (ice-9 regex)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system font)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages xorg)) 

;;TODO: Roboto Slab, Josefin Slab/Sans, Inter (signal font) 
;;https://designalot.net/10-best-free-alternatives-to-popular-fonts/
;;https://www.typewolf.com/google-fonts
;;https://www.webdesignerdepot.com/2020/08/17-open-source-fonts-youll-actually-love/

;; license is proprietary ?
;;(define-public font-ubuntu
;;  (package
;;    (name "font-ubuntu")
;;    (version "0.83")
;;    (source
;;     (origin
;;       (method url-fetch)
;;       (uri "https://assets.ubuntu.com/v1/0cef8205-ubuntu-font-family-0.83.zip")
;;       (sha256
;;        (base32 "1kwfsvqkkh0928mf75md37g150hs46wqnhzgkzqm5mbga91b78k1"))))
;;    (build-system font-build-system)
;;    (home-page "https://design.ubuntu.com/font/")
;;    (synopsis "")
;;    (description "")
;;    (license license:silofl1.1)))
;;


(define-public font-iosevka-mayukai-serif
  (package
    (name "font-iosevka-mayukai-serif")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri
        (string-append
         "https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases/download/v"
         version "/iosevka-mayukai-serif-editor-v601.zip"))
       (sha256
        (base32 "0i56gciwczc6p6sdjfv3id89ppnz3haxv1y7d8a87b8jpxrlhfsw"))))
    (build-system font-build-system)
    (home-page "")
    (synopsis "")
    (description "")
    (license license:silofl1.1)))

(define-public font-iosevka-mayukai-original
  (package
    (name "font-iosevka-mayukai-original")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri
        (string-append
         "https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases/download/v"
         version "/iosevka-mayukai-original-editor-v601.zip"))
       (sha256
        (base32 "0fnch388bk281pyy91vkchk62yy7knzncgxz96vdz6dajazaj64x"))))
    (build-system font-build-system)
    (home-page "")
    (synopsis "")
    (description "")
    (license license:silofl1.1))) ;oder #f?

(define-public font-iosevka-mayukai-sonata
  (package
    (name "font-iosevka-mayukai-sonata")
    (version "6.0.1")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri
        (string-append
         "https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases/download/v"
         version "/iosevka-mayukai-sonata-editor-v601.zip"))
       (sha256
        (base32 "0a82j4kdkrc9ax97xmjal12gjyqyd87a77ww1j32xrr3pdkn7x3k"))))
    (build-system font-build-system)
    (home-page "https://design.ubuntu.com/font/")
    (synopsis "")
    (description "")
    (license license:silofl1.1)))

(define-public font-iosevka-mayukai
  (package
    (name "font-iosevka-mayukai")
    (version (package-version font-iosevka-mayukai-original))
    (source #f)
    (build-system trivial-build-system)
    (arguments '(#:builder (begin (mkdir %output) #t)))
    (propagated-inputs
     (list font-iosevka-mayukai-original
           font-iosevka-mayukai-sonata
           font-iosevka-mayukai-serif))
    (home-page "https://github.com/Iosevka-Mayukai")
    (synopsis "Meta-package for the Iosevka Mayukai fonts")
    (description "")
    (license license:silofl1.1)))


;;https://github.com/googlefonts/nunito
;;https://github.com/googlefonts/nunito/commit/0243cb7fe470b9f4114a8460a26443fb3d805beb
;;https://github.com/googlefonts/NunitoSans/commit/19beed7a8f441263413f59c8a98478680fd5cf55
;; https://github.com/etunni/glegoo/commit/a6b0a10abfaf1b88feb4a9f9eb731beefbb4bbb8
;; https://github.com/googlefonts/robotoslab/commit/56bcfeddeae694babdb34c19ebe3956b17829699
;;https://githugb.com/googlefonts/josefinsans
;;https://sourceforge.net/projects/inter.mirror/files/latest/download
;;https://github.com/akveo/eva-icons/releases/tag/v1.1.3
;;https://sourceforge.net/projects/heuristica/files/latest/download

(define-public font-google-roboto-mono
  (let ((commit "8f651634e746da6df6c2c0be73255721d24f2372")
        (version "0.0")
        (revision "0"))                 
  (package
    (name "font-google-roboto-mono")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/googlefonts/RobotoMono")
         (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "00ddmr7yvb9isakfvgv6g74m80fmg81dmh1hrrdyswapaa7858a5"))))
    (build-system font-build-system)
    (home-page "https://github.com/googlefonts/RobotoMono")
    (synopsis "Monospaced version of Roboto")
    (description "Roboto Mono is a monospaced addition to the Roboto type
 family. Like the other members of the Roboto family, the fonts are
 optimized for readability on screens across a wide variety of devices
 and reading environments.")
    (license license:asl2.0))))


(define-public font-google-roboto-slab
  (let ((commit "56bcfeddeae694babdb34c19ebe3956b17829699")
        (version "0.0")
        (revision "0"))                 
  (package
    (name "font-google-roboto-slab")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/googlefonts/robotoslab")
         (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0qrlxslqp3z1kwzfbb0fqvplmlmh2psqm1kwvdrvggww4hy0708m"))))
    (build-system font-build-system)
    (home-page "https://github.com/googlefonts/robotoslab")
    (synopsis "Slab variant of Google's Roboto")
    (description "This is the Roboto Slab family, which can be used alongside
the normal Roboto family and the Roboto Condensed family.")
    (license license:silofl1.1))))

(define-public font-nunito
  (let ((commit "0243cb7fe470b9f4114a8460a26443fb3d805beb")
        (version "0.83")
        (revision "0"))                 
  (package
    (name "font-nunito")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googlefonts/nunito")
             (commit commit)))
       (sha256
        (base32 "07ndbn9gjd7g8mw25lm9j33pphj1rbwrr6kbdlmhhg4axd4p4jzf"))))
    (build-system font-build-system)
    (home-page "https://github.com/googlefonts/nunito")
    (synopsis "Well balanced sans serif typeface")
    (description "Nunito is a well balanced sans serif typeface superfamily,
 with 2 versions: The project began with Nunito, created by Vernon Adams
 as a rounded terminal sans serif for display typography. ")
    (license license:silofl1.1))))

(define-public font-nunito-sans
  (let ((commit "19beed7a8f441263413f59c8a98478680fd5cf55")
        (version "0.83")
        (revision "0"))                 
  (package
    (name "font-nunito-sans")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/googlefonts/NunitoSans")
         (commit commit)))
       (sha256
        (base32 "0ys9h5hpq9ssaws70dmavl08xbs1xpw4qhb6l3fjzdz6di7wjyn5"))))
    (build-system font-build-system)
    (home-page "https://github.com/googlefonts/NunitoSans")
    (synopsis "Nunito Sans Version")
    (description "Jacques Le Bailly extended Nunito to a full set of weights, and
 an accompanying regular non-rounded terminal version, Nunito Sans.")
    (license license:silofl1.1))))


;;FIXME

(define-public font-glegoo
  (let ((commit "a6b0a10abfaf1b88feb4a9f9eb731beefbb4bbb8")
        (version "1.1")
        (revision "0"))                 
    (package
     (name "font-glegoo")
     (version (git-version version revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/etunni/glegoo")
             (commit commit)))
       (sha256
        (base32 "11awx0zdz14x8hrm214fa8hxfrq18sq3q47sgvc4kidvy2v83qbf"))))
     (build-system font-build-system)
     (home-page "https://design.ubuntu.com/font/")
     (synopsis "")
     (description "")
     (license license:silofl1.1))))

;; FIXME

(define-public font-josefin-sans
  (let ((commit "132fdfd997a62411375d15e20ef81285923750c6")
        (version "0.83")
        (revision "0"))                 
  (package
    (name "font-josefin-sans")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googlefonts/josefinsans")
             (commit commit)))
       (sha256
        (base32 "1ndlf54gjix7pl62g7jja25kfcinfyl8bbg724qn7nasp77ihy56"))))
    (native-inputs `(("bash" ,bash)))
    (build-system font-build-system)
    (home-page "https://github.com/googlefonts/josefinsans")
    (synopsis "")
    (description "The idea of this typeface is to be geometric, elegant, with
 a vintage feeling, for use at larger sizes. It is inspired by geometric sans
 serif designs from the 1920s. The x-height is half way from baseline to cap
 height, an unusual proportion. ")
    (license license:silofl1.1))))

(define-public font-bitter
  (package
   (name "font-bitter")
   (version "2.1.1")
   (source
    (origin
     (method url-fetch)
     (uri "https://github.com/solmatas/Bitter/archive/refs/tags/v.2.110.tar.gz")
     (sha256
      (base32 "14wczn4dhqa5d8sd693m8352xwdhf5viik9959yqz264nvq3vr1c"))))
   (build-system font-build-system)
   (home-page "http://huertatipografica.com/en/fonts/bitter-ht")   
   (synopsis "Serif font designed for reading on screen")
   (description "People read and interact with text on screens more and more each day.
 What happens on screen ends up being more important than what comes out of the
 printer. With the accelerating popularity of electronic books, type designers
 are working hard to seek out the ideal designs for reading on screen.")
   (license license:silofl1.1)))

(define-public font-inter
  (package
   (name "font-inter")
   (version "3.19")
   (source
    (origin
     (method url-fetch/zipbomb)
     (uri  "https://github.com/rsms/inter/releases/download/v3.19/Inter-3.19.zip")
     (sha256
            (base32 "0ch0rk6nwd80y7vqbmrii9cr3zq6sq2gqpgkxdxsaqhp1livc2hm"))))
   (build-system font-build-system)   
   (home-page "https://rsms.me/inter/")   
   (synopsis "Sans-serif Font with good accessibility")
   (description "Inter is a typeface carefully crafted & designed
for computer screens. It features a tall x-height to aid in
readability of mixed-case and lower-case text.")
   (license license:silofl1.1)))
