(define-module (guix-local packages icons)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)  
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages web)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk))

;; some TODOs
;; tela-circle-icons
;; miya-icons
;; beautyline
;; la-capitaine
;; solus, matrilineare..

;;some gtk themes


(define-public zafiro-icon-theme
  (package
    (name "zafiro-icon-theme")
    (version "1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/zayronxio/Zafiro-icons")
             (commit version)))
       (sha256
        (base32
         "11cycfa6rz9n61m61mymp50mrp4x9zj4dvy76h63c04j8fangc91"))
       (file-name (git-file-name name version))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           (("./Dark" "share/icons/Zafiro-Dark")
            ("./Light" "share/icons/Zafiro-Light"))))
    (home-page "https://github.com/zayronxio/Zafiro-icons")
    (synopsis "Flat icon pack with light colors")
    (description
     "Minimalist icons created with the flat-desing technique,
 utilizing washed out colors and always accompanied by white.")
    (license license:gpl3)))


(define-public oranchelo-icon-theme
  (package
    (name "oranchelo-icon-theme")
    (version "0.9.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OrancheloTeam/oranchelo-icon-theme")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "087ryfvlkwg0nm7wpfj6xr2w4hrwi2z6rdwn1sfm9b7xyn7ijfr0"))
       (file-name (git-file-name name version))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan
           `(("." "share/icons"
              #:exclude ("oranchelo-installer.sh" "LICENSE"
                         "Author" "README.md" "Changelog"
                         "Makefile")))))
    (home-page "https://github.com/OrancheloTeam/oranchelo-icon-them")
    (synopsis "Oranchelo Icon Theme")
    (description
     "Oranchelo is a flat-design icon theme for XFCE4 based on Super
 Flat Remix and inspired by \"Corny icons\" by Patryk Goworowski.")
    (license license:gpl3)))


(define-public suru-icon-theme
  (package
    (name "suru-icon-theme")
    (version "0.01")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/snwh/suru-icon-theme")
             (commit "2d8102084eaf194f04076ec6949feacb0eb4a1ba")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0r5xzvifdzlajjn57k7zs00hgsbp7025rq78pnlbnfrnr7l8agvv"))))
    (build-system meson-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'disable-post-install
           (lambda _
             (substitute* "meson.build"
               (("meson.add_install_script.*") "")))))))
    (native-inputs
     (list autoconf automake))
    (synopsis "A desktop adaptation of the Ubuntu mobile icons.")
    (description
     "This project is a revitalization of the Suru icon set that was designed
 for Ubuntu Touch. The principles and styles created for Suru now serve as
 the basis for a new FreeDesktop icon theme.")
    (home-page "https://snwh.org/suru")
    ;; icons are cc-by-sa-4 ; scripts are gpl3 
    (license (list license:lgpl3+
                   license:cc-by-sa4.0))))

;;inherits adwaita, gnome, hicolor could be added to propagated-inputs
(define-public paper-icon-theme
  (package
    (name "paper-icon-theme")
    (version "1.5.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/snwh/paper-icon-theme")
             (commit (string-append "v." version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1k4pqnxq21lizhlhpmprdkkaabiw7jg4zr0bxrkkwnhwgfkr924l"))))
    (build-system meson-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'disable-post-install
           (lambda _
             (substitute* "meson.build"
               (("meson.add_install_script.*") "")))))))
    (native-inputs
     (list autoconf automake))
    (synopsis "A simple and modern icon theme.")
    (description
     "Paper is a modern freedesktop icon theme whose design is based around the
 use of bold colours and simple geometric shapes to compose icons. Each icon
 has been designed for pixel-perfect viewing.

While it does take some inspiration from the icons in Google's Material Design,
 some aspects have been adjusted to better suit a desktop environment.")
    (home-page "https://snwh.org/paper")
    (license (list license:lgpl3+
                   license:cc-by-sa4.0))))


(define-public fluent-icon-theme
  (package
    (name "fluent-icon-theme")
    (version "2021-12-20")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/vinceliuice/fluent-icon-theme")
             (commit version)))
       (sha256
        (base32
         "1j78j1j3ajbyccpbxlgy42calwlqzh51anz19v0c0d7dccch810j"))
       (file-name (git-file-name name version))))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (gtk (assoc-ref %build-inputs "gtk+"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-join
                    (map
                     (lambda (x)
                       (string-append x "/bin:"))
                     '(coreutils bash sed gtk)) ""))
           (copy-recursively source (getcwd))
           (patch-shebang "install.sh")
           (mkdir-p iconsdir)
           (invoke "./install.sh" "-d" iconsdir "-b" "-r") ;;-a
           #t))))
    (native-inputs
     (list bash coreutils sed `(,gtk+ "bin") ))
    (home-page "https://github.com/vinceliuice/fluent-icon-theme")
    (synopsis "Fluent icon theme for linux desktops")
    (description
     "Flat and colorful personality icon theme")
    (license license:gpl3+)))


(define-public tela-icon-theme
  (package
    (name "tela-icon-theme")
    (version "2021-12-25")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/vinceliuice/tela-icon-theme")
             (commit version)))
       (sha256
        (base32
         "18dzw75xsdca6aphcn9ih38l9lxgg03njy730m2za275zn77snxa"))
       (file-name (git-file-name name version))))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (gtk-bin (assoc-ref %build-inputs "gtk+"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-append coreutils "/bin:"
                                  (string-append bash "/bin:"
                                                 (string-append sed "/bin:"
                                                                (string-append gtk-bin "/bin:")))))
           (copy-recursively source (getcwd))
           (patch-shebang "install.sh")
           (mkdir-p iconsdir)
           (invoke "./install.sh" "-a" "-d" iconsdir)
           #t))))
    (native-inputs
     (list bash sed `(,gtk+ "bin") coreutils))
    (home-page "https://github.com/vinceliuice/tela-icon-theme")
    (synopsis "")
    (description
     "Flat and colorful personality icon theme")
    (license license:gpl3+)))

(define-public tela-circle-icon-theme
  (package
    (name "tela-circle-icon-theme")
    (version "2022-03-07")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/vinceliuice/tela-circle-icon-theme")
             (commit version)))
       (sha256
        (base32
         "1j6p66pc60qh3kfx9asfkms8m5zrmlg7fx82a7zz9bmfk4crc1xx"))
       (file-name (git-file-name name version))))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (gtk-bin (assoc-ref %build-inputs "gtk+"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-append coreutils "/bin:"
                                  (string-append bash "/bin:"
                                                 (string-append sed "/bin:"
                                                                (string-append gtk-bin "/bin:")))))
           (copy-recursively source (getcwd))
           (patch-shebang "install.sh")
           (mkdir-p iconsdir)
           (invoke "./install.sh" "-a" "-c" "-d" iconsdir)
           #t))))
    (native-inputs
     (list bash sed `(,gtk+ "bin") coreutils))
    (home-page "https://github.com/vinceliuice/tela-icon-theme")
    (synopsis "Tela Icon Theme")
    (description "Version of Tela Icon Theme with circled icons")
    (license license:gpl3+)))


(define-public ketsa-icon-theme
  (package
    (name "ketsa-icon-theme")
    (version "0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/zayronxio/ketsa-icon-theme")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "1w8pl3n2brmiahpqiszc0hi9kayhjlcimz1hnvm2wwf42ajsbsjz"))
       (file-name (git-file-name name version))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("." "share/icons/Ketsa" #:exclude ("LICENSE")))))
    (home-page "")
    (synopsis "Ketsa Icon Theme")
    (description "Beautiful icons designed to detail with lots of
light and elaborate shadows, Original Designs")
    (license license:gpl3)))


(define-public material-cursors
  (let ((version "0.01")
        (revision "0")
        (commit "6fe67106b60522122bd4253725c7bdc60b9fcd65"))
    (package
      (name "material-cursors")
      (version (git-version version revision commit))
      (source
       (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/varlesh/material-cursors")
             (commit commit)))
       (sha256
        (base32
         "0q3gvalja4f9l76gydcw7hb8j5i4s92dja78wp1pkvrn5lpd4vs9"))
       (file-name (git-file-name name version))))
       (build-system gnu-build-system)
       (arguments
        `(#:tests? #f
          #:phases
          (modify-phases %standard-phases
            (delete 'configure)
            (add-before 'build 'setenvvars
              (lambda _
                (setenv "HOME" (getcwd))
                (setenv "PWD" (getcwd))
                (setenv "PREFIX" (assoc-ref %outputs "out")))))))
       (native-inputs
        (list inkscape xcursorgen fontconfig))
       (synopsis "Material Cursors Theme")
       (description "")
       (home-page "https://github.com/varlesh/material-cursors")
       (license license:gpl3+))))

(define-public volantes-cursors
  (let ((version "0.0")
        (revision "0")
        (commit "b43f0987b96f27823a03522fe1020b6c7de63c4f"))
    (package
      (name "volantes-cursors")
      (version (git-version version revision commit))
      (source
       (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/varlesh/volantes-cursors")
             (commit commit)))
       (sha256
        (base32
         "0sga6k6gszdy1pz9sx80yidliwg84k27v769691k7s425nwywc4d"))
       (file-name (git-file-name name version))))
       (build-system gnu-build-system)
       (arguments
        `(#:tests? #f
          #:phases
          (modify-phases %standard-phases
            (delete 'configure)
            (add-before 'build 'setenvvars
              (lambda _
                (setenv "HOME" (getcwd))
                (setenv "PWD" (getcwd))
                (setenv "PREFIX" (assoc-ref %outputs "out")))))))
       (native-inputs
        (list inkscape xcursorgen fontconfig))
       (synopsis "Volantes Cursors")
       (description "")
       (home-page "https://github.com/varlesh/volantes-cursors")
       (license license:gpl3+))))

(define-public oreo-cursors
  (let ((version "0.0")
        (revision "0")
        (commit "ffe91dfac2afe3b8b74f50e1de13ff959c76665c"))
    (package
      (name "oreo-cursors")
      (version (git-version version revision commit))
      (source
       (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/varlesh/oreo-cursors")
             (commit commit)))
       (sha256
        (base32
         "1p00ly2m8hif1jj58nssvy5cipw58p2asirx0agvlp8jlq5gbchd"))
       (file-name (git-file-name name version))))
       (build-system gnu-build-system)
       (arguments
        `(#:tests? #f
          #:phases
          (modify-phases %standard-phases
            (delete 'configure)
            (add-before 'build 'setenvvars
              (lambda _
                (invoke "ruby" "generators/convert.rb")
                (setenv "HOME" (getcwd))
                (setenv "PWD" (getcwd))
                (setenv "PREFIX" (assoc-ref %outputs "out")))))))
       (native-inputs
        (list inkscape ruby xcursorgen fontconfig))
       (synopsis "Oreo Cursors")
       (description "")
       (home-page "https://github.com/varlesh/oreo-cursors")
       (license license:gpl3+))))


;;more nice cursors
;; openzone
;; xcursor pro
;; deepin/cutefish ;o



(define-public capitaine-cursors
  (package
    (name "capitaine-cursors")
    (version "r4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/keeferrourke/capitaine-cursors")
             (commit "06c88433662a4004cf56a6e471b523a0a8880be0")))
       (sha256
        (base32
         "0cmp5l9x4r4994wqyv6dd61b1fhrnq656gmbxibynmxj5z3x7kf5"))
       (file-name (git-file-name name version))))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (bc (assoc-ref %build-inputs "bc"))
                (inkscape (assoc-ref %build-inputs "inkscape"))
                (xcursorgen (assoc-ref %build-inputs "xcursorgen"))
                             (findutils (assoc-ref %build-inputs  "findutils"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-append coreutils "/bin:"
                                  (string-append bash "/bin:"
                                                 (string-append bc "/bin:"
                                                                (string-append findutils "/bin:"
                                                                               (string-append xcursorgen "/bin:"
                                                                                              (string-append inkscape "/bin:")))))))
           (copy-recursively source (getcwd))
           (patch-shebang "./build.sh")
           (mkdir-p iconsdir)
           (setenv "HOME" (getcwd))
           (setenv "PWD" (getcwd))
           ;;(invoke "./build.sh" "-d xhd" "-t dark")
           (invoke "./build.sh" "-d xhd")
           ;;(display (find-files "./dist"))
           (copy-recursively "./dist/dark"
                             (string-append iconsdir "/capitaine-cursors"))
           ;;(copy-recursively "./dist/light"
           ;;                  (string-append iconsdir "/capitaine-cursors-light"))           
           #t))))
    (native-inputs
     (list bash coreutils inkscape bc findutils xcursorgen))
    (synopsis "")
    (description "")
    (home-page "https://github.com/vinceliuice/capitaine-cursors")
    (license license:gpl3+)))


(define-public lyra-cursors
  (package
    (name "lyra-cursors")
    (version "0.8.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/yeyushengfan258/Lyra-Cursors")
             (commit "c096c54034f95bd35699b3226250e5c5ec015d9a")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "123rgcfp5p6d7r5222dbkm0k309c5dmh0sahq689dl44igr9gxlm"))
       (patches
        (search-patches "lyra-cursors.patch"))))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (bc (assoc-ref %build-inputs "bc"))
                (inkscape (assoc-ref %build-inputs "inkscape"))
                (xcursorgen (assoc-ref %build-inputs "xcursorgen"))                
                (fontconfig (assoc-ref %build-inputs "fontconfig"))
                (findutils (assoc-ref %build-inputs  "findutils"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))                
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-append coreutils "/bin:"
                                  (string-append bash "/bin:"
                                                 (string-append bc "/bin:"
                                                                (string-append findutils "/bin:"                                                                                                                                                                                                                          (string-append xcursorgen "/bin:"
                                                                                                                                                                                                                                                                                                                                         (string-append inkscape "/bin:")))))))
           (copy-recursively source (getcwd))
           (patch-shebang "./build.sh")
           (mkdir-p iconsdir)
           (setenv "HOME" (getcwd))
           (setenv "PWD" (getcwd))
           (invoke "./build.sh" "LyraR")
           (copy-recursively "./dist/LyraR-cursors"
                             (string-append iconsdir "/LyraR-cursors"))
           #t))))
    (native-inputs
     (list bash coreutils fontconfig inkscape bc findutils xcursorgen))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public future-cursors
  (package
    (name "future-cursors")
    (version "0.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/yeyushengfan258/Future-cursors")
             (commit "587c14d2f5bd2dc34095a4efbb1a729eb72a1d36")))
       ;;(file-name (git-file-name name version))
       (sha256
        (base32
         "09hpgc21v3c31fx65w74h5w42pnn9a18s4g5m7rifmhdmcqj08ff"))
       ;;(patches
       ;; (search-patches "future-cursors.patch"))
       ))
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (source (assoc-ref %build-inputs "source"))
                (bash (assoc-ref %build-inputs "bash"))
                (sed (assoc-ref %build-inputs "sed"))
                (bc (assoc-ref %build-inputs "bc"))
                (inkscape (assoc-ref %build-inputs "inkscape"))
                (xcursorgen (assoc-ref %build-inputs "xcursorgen"))                
                (fontconfig (assoc-ref %build-inputs "fontconfig"))
                (findutils (assoc-ref %build-inputs  "findutils"))
                (coreutils (assoc-ref %build-inputs  "coreutils"))                
                (iconsdir (string-append out "/share/icons")))
           (setenv "PATH"
                   (string-append coreutils "/bin:"
                                  (string-append bash "/bin:"
                                                 (string-append bc "/bin:"
                                                                (string-append findutils "/bin:"                                                                                                                                                                                                                          (string-append xcursorgen "/bin:"
                                                                                                                                                                                                                                                                                                                                         (string-append inkscape "/bin:")))))))
           (copy-recursively source (getcwd))
           (patch-shebang "./build.sh")
           (mkdir-p iconsdir)
           (setenv "HOME" (getcwd))
           (setenv "PWD" (getcwd))
           (invoke "./build.sh" "cyan")
           (copy-recursively "./dist"
                             (string-append iconsdir "/future-cursors"))
           #t))))
    (native-inputs
     (list bash coreutils fontconfig inkscape bc findutils xcursorgen))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public icon-slicer
  (package
    (name "icon-slicer")
    (version "0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://freedesktop.org/software/icon-slicer/releases/icon-slicer-" version ".tar.gz"))
       (sha256
        (base32
         "0kdnc08k2rs8llfg7xgvnrsk60x59pv5fqz6kn2ifnn2s1nj3w05"))
       (patches (search-patches "hotspotfix-icon-slicer.patch"))))
    (build-system gnu-build-system)
    (native-inputs
     (list `(,gtk+-2 "bin") pkg-config popt))
    (inputs (list gdk-pixbuf))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public openzone-cursors
  (package
    (name "openzone-cursors")
    (version "1.2.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ducakar/openzone-cursors")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "02c536mc17ccsrzgma366k3wlm02ivklvr30fafxl981zgghlii4"))
       (file-name (git-file-name name version))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:make-flags
       (list (string-append "DESTDIR=" (assoc-ref %outputs "out")))         
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (native-inputs
     (list inkscape xcursorgen icon-slicer))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:gpl3+)))

(define-public python-clickgen
  (package
  (name "python-clickgen")
  (version "1.1.9")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "clickgen" version))
      (sha256
        (base32 "17abhldc30ya7kag7rpb1399wlajcavmpssl34wf8icaq3iblrr5"))))
  (build-system python-build-system)
  (propagated-inputs (list python-pillow))
  (home-page "https://github.com/ful1e5/clickgen")
  (synopsis "The hassle-free cursor building toolbox \U01f9f0")
  (description "The hassle-free cursor building toolbox \U01f9f0")
  (license #f)))
