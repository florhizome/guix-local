(define-module (guix-local packages goblog)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system go)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (gnu packages)
  #:use-module (gnu packages sqlite)  
  #:use-module (gnu packages golang)
  #:use-module (gnu packages syncthing)
  #:use-module (guix-local packages golang)
  #:use-module (guix-local packages hugo)
  #:use-module (guix-local packages writefreely))

(define-public go-git-jlel-se-jlelse-go-geouri
  (package
    (name "go-git-jlel-se-jlelse-go-geouri")
    (version "0.0.0-20210525190615-a9c1d50f42d6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.jlel.se/jlelse/go-geouri.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1s9anyzbcpsfgj0hjj126kjs21xba1mvc86gb7hk2qfwprw0vyl8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.jlel.se/jlelse/go-geouri"))
    (home-page "https://git.jlel.se/jlelse/go-geouri")
    (synopsis "go-geouri")
    (description "Parse geo URI (RFC 5870) with Go")
    (license license:expat)))

(define-public go-git-jlel-se-jlelse-template-strings
  (package
    (name "go-git-jlel-se-jlelse-template-strings")
    (version "0.0.0-20220211095702-c012e3b5045b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.jlel.se/jlelse/template-strings.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0c7mikhcazhpncv5czswlni5xcppnkd1ns5zh0lgv5hh4j2j02kg"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.jlel.se/jlelse/template-strings"))
    (propagated-inputs `(("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://git.jlel.se/jlelse/template-strings")
    (synopsis "template-strings")
    (description "Simple Go module to manage template translations")
    (license license:expat)))

(define-public go-github-com-jlelse-feeds
  (package
    (name "go-github-com-jlelse-feeds")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jlelse/feeds")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hsjszh0hyjfksmx5ynqqm48zgnx396yl7jd0iizx9fbyq6lnpvq"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jlelse/feeds"))
    (propagated-inputs `(("go-github-com-kr-text" ,go-github-com-kr-text)
                         ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)))
    (home-page "https://github.com/jlelse/feeds")
    (synopsis "gorilla/feeds")
    (description "Syndication (feed) generator library for golang.")
    (license license:bsd-2)))

(define-public go-git-jlel-se-jlelse-goldmark-mark
  (package
    (name "go-git-jlel-se-jlelse-goldmark-mark")
    (version "0.0.0-20210522162520-9788c89266a4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.jlel.se/jlelse/goldmark-mark.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0spsdslqyyph7l4fgiv1psaqszfp9xva99ch2xs2h2kbz3p21x6d"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.jlel.se/jlelse/goldmark-mark"))
    (propagated-inputs `(("go-github-com-yuin-goldmark" ,go-github-com-yuin-goldmark)))
    (home-page "https://git.jlel.se/jlelse/goldmark-mark")
    (synopsis "goldmark-mark")
    (description "Goldmark extension for HTML mark tags")
    (license license:expat)))

(define-public go-github-com-dgryski-go-farm
  (package
    (name "go-github-com-dgryski-go-farm")
    (version "0.0.0-20200201041132-a6ae2369ad13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/dgryski/go-farm")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1qbz4a4fv3853ix974x02q1129kc4xxf0c92ib5sdpsq04zjbqv8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dgryski/go-farm"))
    (home-page "https://github.com/dgryski/go-farm")
    (synopsis "go-farm")
    (description "FarmHash, a family of hash functions.")
    (license license:expat)))

(define-public go-github-com-snabb-diagio
  (package
    (name "go-github-com-snabb-diagio")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/snabb/diagio")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0g4swgx30gaq0a0l71qd7c1q3dq6q8xcdnwp8063lrv8vqf3xplg"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/snabb/diagio"))
    (home-page "https://github.com/snabb/diagio")
    (synopsis "diagio")
    (description
     "Package diagio implements I/O wrappers which can be useful for debugging and
other diagnostics purposes.")
    (license license:expat)))

(define-public go-github-com-snabb-sitemap
  (package
    (name "go-github-com-snabb-sitemap")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/snabb/sitemap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0mb8r4r7dqqwdi3f9brcsqp469rsn621x9h2ahc601arjiv1zk0c"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/snabb/sitemap"))
    (propagated-inputs `(("go-github-com-snabb-diagio" ,go-github-com-snabb-diagio)))
    (home-page "https://github.com/snabb/sitemap")
    (synopsis "sitemap")
    (description
     "Package sitemap provides tools for creating XML sitemaps and sitemap indexes and
writing them to io.Writer (such as http.ResponseWriter).")
    (license license:expat)))

(define-public go-github-com-dgraph-io-ristretto
  (package
    (name "go-github-com-dgraph-io-ristretto")
    (version "0.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/dgraph-io/ristretto")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0mjni3zaxvjvw5c7nh4sij13sslg92x9xi3ykxzbv2s6g2ynigss"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dgraph-io/ristretto"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-golang-glog" ,go-github-com-golang-glog)
                         ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
                         ("go-github-com-dgryski-go-farm" ,go-github-com-dgryski-go-farm)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-github-com-cespare-xxhash-v2" ,go-github-com-cespare-xxhash-v2)))
    (home-page "https://github.com/dgraph-io/ristretto")
    (synopsis "Ristretto")
    (description
     "Ristretto is a fast, fixed size, in-memory cache with a dual focus on throughput
and hit ratio performance.  You can easily add Ristretto to an existing system
and keep the most valuable data where you need it.")
    (license license:asl2.0)))

(define-public go-github-com-hacdias-indieauth-v2
  (package
    (name "go-github-com-hacdias-indieauth-v2")
    (version "2.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hacdias/indieauth")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0s2qpmgp2dv86dz26cfsgrnavdm46j5h6mdi3vxbj47aspai0pkf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hacdias/indieauth/v2"))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
                         ("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-willnorris-com-go-webmention" ,go-willnorris-com-go-webmention)
                         ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/hacdias/indieauth")
    (synopsis "IndieAuth Toolkit for Go")
    (description
     "This package provides a set of tools to help you implement IndieAuth, both as a
Server or as a Client, according to the spec:
@@url{https://indieauth.spec.indieweb.org,https://indieauth.spec.indieweb.org}")
    (license license:expat)))

(define-public go-git-jlel-se-jlelse-go-shutdowner
  (package
    (name "go-git-jlel-se-jlelse-go-shutdowner")
    (version "0.0.0-20210707065515-773db8099c30")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.jlel.se/jlelse/go-shutdowner.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0iy8qdq4a3z3d39l8lvi3i99yhy3l8bl262dzgll7jkig3r73j5z"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.jlel.se/jlelse/go-shutdowner"))
    (home-page "https://git.jlel.se/jlelse/go-shutdowner")
    (synopsis "go-shutdowner")
    (description
     "This package provides a simple library for graceful shutdowns")
    (license license:expat)))

(define-public go-git-jlel-se-jlelse-go-geouri
  (package
    (name "go-git-jlel-se-jlelse-go-geouri")
    (version "0.0.0-20210525190615-a9c1d50f42d6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.jlel.se/jlelse/go-geouri.git")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1s9anyzbcpsfgj0hjj126kjs21xba1mvc86gb7hk2qfwprw0vyl8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "git.jlel.se/jlelse/go-geouri"))
    (home-page "https://git.jlel.se/jlelse/go-geouri")
    (synopsis "go-geouri")
    (description "Parse geo URI (RFC 5870) with Go")
    (license license:expat)))

(define-public go-github-com-wsxiaoys-terminal
  (package
    (name "go-github-com-wsxiaoys-terminal")
    (version "0.0.0-20160513160801-0940f3fc43a0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/wsxiaoys/terminal")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0prryvh99xyhf32wmnspr68j3wiy4jvz00ygwlkpfvwk84dyykli"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/wsxiaoys/terminal"
       #:tests? #f))
    (home-page "https://github.com/wsxiaoys/terminal")
    (synopsis "Terminal")
    (description
     "Terminal is a simple golang package that provides basic terminal handling.")
    (license license:bsd-3)))

(define-public go-willnorris-com-go-webmention
  (package
    (name "go-willnorris-com-go-webmention")
    (version "0.0.0-20220108183051-4a23794272f0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/willnorris/webmention")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "07yr21h7qwvplqk6ij71wlnc8fcdvdgr0q345qq0wlxpd7qzrnaz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "willnorris.com/go/webmention"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-wsxiaoys-terminal" ,go-github-com-wsxiaoys-terminal)
                         ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
                         ("go-github-com-andybalholm-cascadia" ,go-github-com-andybalholm-cascadia)))
    (home-page "https://willnorris.com/go/webmention")
    (synopsis "webmention")
    (description
     "Package webmention provides functions for discovering the webmention endpoint
for URLs, and sending webmentions according to
@@url{http://webmention.org/,http://webmention.org/}.")
    (license license:bsd-3)))


(define-public go-github-com-kylelemons-godebug
  (package
    (name "go-github-com-kylelemons-godebug")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kylelemons/godebug")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0dkk3friykg8p6wgqryx6745ahhb9z1j740k7px9dac6v5xjp78c"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/kylelemons/godebug/"
       #:unpack-path "github.com/kylelemons/godebug"
       #:phases
        (modify-phases %standard-phases
          (replace 'build
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'build)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/kylelemons/godebug/pretty"
                   "github.com/kylelemons/godebug/diff"))))
          (replace 'check
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'check)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/kylelemons/godebug/pretty"
                   "github.com/kylelemons/godebug/diff")))))))
    (home-page "https://github.com/kylelemons/godebug")
    (synopsis "Pretty Printing for Go")
    (description
     "Have you ever wanted to get a pretty-printed version of a Go data structure,
complete with indentation? I have found this especially useful in unit tests and
in debugging my code, and thus godebug was born!")
    (license license:asl2.0)))

(define-public go-willnorris-com-go-microformats
  (package
    (name "go-willnorris-com-go-microformats")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/willnorris/microformats")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "01a5q3w7dyfkiggwww421a7cb1n64l5kfzvrzqzbaxrk47aahx2r"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "willnorris.com/go/microformats"
       #:tests? #f)) ;FIXME
    (native-inputs (list go-github-com-stretchr-testify
                         go-github-com-kylelemons-godebug))

    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-kylelemons-godebug" ,go-github-com-kylelemons-godebug)
                         ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)))
    (home-page "https://willnorris.com/go/microformats")
    (synopsis "microformats")
    (description
     "Package microformats provides a microformats parser, supporting both v1 and v2
syntax.")
    (license license:expat)))

(define-public go-github-com-boombuler-barcode
  (package
    (name "go-github-com-boombuler-barcode")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/boombuler/barcode")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0v4ypgh3xarzfpgys838mgkfabqacbjklhf4kfqnycs0v0anvnlr"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/boombuler/barcode"))
    (home-page "https://github.com/boombuler/barcode")
    (synopsis "Introduction")
    (description
     "This is a package for GO which can be used to create different types of
barcodes.")
    (license license:expat)))

(define-public go-github-com-carlmjohnson-requests
  (package
    (name "go-github-com-carlmjohnson-requests")
    (version "0.22.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/carlmjohnson/requests")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ircmg9qcgs2y12mn99y7vb1dcj6nf9yi8nlkps0jg1mpc5xq8sh"))))
    (build-system go-build-system)
    (arguments
     (list #:import-path "github.com/carlmjohnson/requests"
           #:go go-1.18
           #:tests? #f))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/carlmjohnson/requests")
    (synopsis "Requests")
    (description
     "Package requests is a convenience wrapper around net/http to make it faster and
easier to build requests and custom transports.")
    (license license:expat)))

(define-public go-github-com-kaorimatz-go-opml
  (package
    (name "go-github-com-kaorimatz-go-opml")
    (version "0.0.0-20210201121027-bc8e2852d7f9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kaorimatz/go-opml")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13yarggdliqk7xb26bicn5qxidfj9mz26ic3cibd6xqplhgy4zql"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/kaorimatz/go-opml"))
    (native-inputs (list go-golang-org-x-text))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/kaorimatz/go-opml")
    (synopsis "go-opml")
    (description "Go library for parsing and rendering OPML")
    (license license:expat)))

(define-public go-github-com-justinas-alice
  (package
    (name "go-github-com-justinas-alice")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/justinas/alice")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "19l88vi13rqyhjl100zd5z26ghy4iln74kqfd3hsmd2nydz7manz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/justinas/alice"))
    (home-page "https://github.com/justinas/alice")
    (synopsis "Alice")
    (description
     "Package alice provides a convenient way to chain http handlers.")
    (license license:expat)))


(define-public go-github-com-lestrrat-go-envload
  (package
    (name "go-github-com-lestrrat-go-envload")
    (version "0.0.0-20180220120943-6ed08b54a570")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lestrrat-go/envload")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0zm47hbpql3xqar9kpahaps3qydsrn7s8y6kpwhffxvln8mlbv1v"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/lestrrat/go-envload"))
    (native-inputs (list go-github-com-stretchr-testify))

    (propagated-inputs (list go-golang-org-x-net))
    (home-page "https://github.com/lestrrat-go/envload")
    (synopsis "go-envload")
    (description "Restore environment variables, so you can break em")
    (license license:expat)))

(define-public go-github-com-lestrrat-go-strftime
  (package
    (name "go-github-com-lestrrat-go-strftime")
    (version "0.0.0-20180220042222-ba3bf9c1d042")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lestrrat/go-strftime")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15g4hylks5kd5chjhkq254z1lbzbdl3s5slng4qsbdbqimicdpar"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/lestrrat/go-strftime"))
    (native-inputs (list go-github-com-stretchr-testify
                         go-github-com-lestrrat-go-envload))
    
    (propagated-inputs (list go-github-com-pkg-errors))
    (home-page "https://github.com/lestrrat-go/strftime")
    (synopsis "go-strftime")
    (description "strftime for Go")
    (license license:expat)))


(define-public go-github-com-vcraescu-go-paginator
  (package
    (name "go-github-com-vcraescu-go-paginator")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/vcraescu/go-paginator")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "183q0vksxvwcvan0pqf1yx8pan8hmq9d12mi6gadyvc7mqfyyl6h"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/vcraescu/go-paginator"))
    (propagated-inputs `(("go-gorm-io-gorm" ,go-gorm-io-gorm)
                         ("go-gorm-io-driver-sqlite" ,go-gorm-io-driver-sqlite)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-lib-pq" ,go-github-com-lib-pq)
                         ("go-github-com-jinzhu-gorm" ,go-github-com-jinzhu-gorm)
                         ("go-github-com-gofrs-uuid" ,go-github-com-gofrs-uuid)
                         ("go-github-com-go-sql-driver-mysql" ,go-github-com-go-sql-driver-mysql)
                         ("go-github-com-erikstmartin-go-testdb" ,go-github-com-erikstmartin-go-testdb)
                         ("go-github-com-denisenkom-go-mssqldb" ,go-github-com-denisenkom-go-mssqldb)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/vcraescu/go-paginator")
    (synopsis "Paginator")
    (description
     "This package provides a simple way to implement pagination in Golang.")
    (license license:expat)))


(define-public go-github-com-coreos-go-systemd-v22
  (package
    (name "go-github-com-coreos-go-systemd-v22")
    (version "22.5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/coreos/go-systemd")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vhb4cw8nw9nx8mprx829xv8w4jnwhc2lcyjljzlfafsn8nx5nyf"))))
    (build-system go-build-system)
    (arguments
     '(#:unpack-path "github.com/coreos/go-systemd/v22"
       #:import-path "github.com/coreos/go-systemd/v22/util"              
       #:tests? #f
       #:phases
        (modify-phases %standard-phases
          (replace 'install
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'install)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/coreos/go-systemd/v22"))))
          (replace 'check
               (lambda arguments
                 (for-each
                  (lambda (directory)
                    (apply (assoc-ref %standard-phases 'check)
                           `(,@arguments #:import-path ,directory)))
                  (list
                   "github.com/coreos/go-systemd/v22")))))))
    (propagated-inputs `(("go-github-com-godbus-dbus-v5" ,go-github-com-godbus-dbus-v5)))
    (home-page "https://github.com/coreos/go-systemd")
    (synopsis "go-systemd")
    (description "Go bindings to systemd.  The project has several packages:")
    (license license:asl2.0)))

(define-public go-github-com-jinzhu-inflection
  (package
    (name "go-github-com-jinzhu-inflection")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jinzhu/inflection")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "165i20d11s03771gi43skl66salxj36212r25fbs0cgr4qgfj7fy"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jinzhu/inflection"))
    (home-page "https://github.com/jinzhu/inflection")
    (synopsis "Inflection")
    (description
     "Package inflection pluralizes and singularizes English nouns.")
    (license license:expat)))

(define-public go-github-com-jinzhu-now
  (package
    (name "go-github-com-jinzhu-now")
    (version "1.1.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jinzhu/now")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "10ywpaxs6d3y8gqlzx6rh3yw4ya83bnx0hrs0k0wq5bxbjhfmlil"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jinzhu/now"))
    (home-page "https://github.com/jinzhu/now")
    (synopsis "Now")
    (description "Package now is a time toolkit for golang.")
    (license license:expat)))

(define-public go-gorm-io-gorm
  (package
    (name "go-gorm-io-gorm")
    (version "1.24.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-gorm/gorm")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1bkgqrky53jvgyrv2f0imdlqr9xg1639dmjya2ifcadd8xvf314v"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gorm.io/gorm"))
    (propagated-inputs `(("go-github-com-jinzhu-now" ,go-github-com-jinzhu-now)
                         ("go-github-com-jinzhu-inflection" ,go-github-com-jinzhu-inflection)))
    (home-page "https://gorm.io/gorm")
    (synopsis "GORM")
    (description
     "The fantastic ORM library for Golang, aims to be developer friendly.")
    (license license:expat)))

(define-public go-gorm-io-driver-sqlite
  (package
    (name "go-gorm-io-driver-sqlite")
    (version "1.4.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-gorm/sqlite")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15kjaxzp8n8bv3hzgdk82ph12iz33k3mpsvzm5ihvpw71wz5dzmj"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gorm.io/driver/sqlite"))
    (propagated-inputs `(("go-gorm-io-gorm" ,go-gorm-io-gorm)
                         ("go-github-com-mattn-go-sqlite3" ,go-github-com-mattn-go-sqlite3)
                         ("go-github-com-jinzhu-now" ,go-github-com-jinzhu-now)))
    (home-page "https://gorm.io/driver/sqlite")
    (synopsis "GORM Sqlite Driver")
    (description
     "Checkout @@url{https://gorm.io,https://gorm.io} for details.")
    (license license:expat)))

(define-public go-github-com-alecthomas-assert-v2
  (package
    (name "go-github-com-alecthomas-assert-v2")
    (version "2.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/alecthomas/assert")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1zz7p5pjnaxy352ibj67hm5yj1vspkdzwbjvxgd3ibzxgrl55rxi"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/alecthomas/assert/v2"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'build))))
    (propagated-inputs `(("go-github-com-hexops-gotextdiff" ,go-github-com-hexops-gotextdiff)
                         ("go-github-com-alecthomas-repr" ,go-github-com-alecthomas-repr)))
    (home-page "https://github.com/alecthomas/assert")
    (synopsis "A simple assertion library using Go generics")
    (description
     "Package assert provides type-safe assertions with clean error messages.")
    (license license:expat)))

(define-public go-github-com-hexops-gotextdiff
  (package
    (name "go-github-com-hexops-gotextdiff")
    (version "1.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hexops/gotextdiff")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vgq6w0cfhr76qlczgm5khsj1wnjkva0vhkh3qspaa1nkfw3jny1"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hexops/gotextdiff"))
    (home-page "https://github.com/hexops/gotextdiff")
    (synopsis "gotextdiff - unified text diffing in Go")
    (description "package gotextdiff supports a pluggable diff algorithm.")
    (license license:bsd-3)))

(define-public go-github-com-alecthomas-kong
  (package
    (name "go-github-com-alecthomas-kong")
    (version "0.7.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/alecthomas/kong")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ygki45a8clkgyh9mwal4wg1cq9v7m4w3jclrzzak9rqfnbl02s8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/alecthomas/kong"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-hexops-gotextdiff" ,go-github-com-hexops-gotextdiff)
                         ("go-github-com-alecthomas-repr" ,go-github-com-alecthomas-repr)
                         ("go-github-com-alecthomas-assert-v2" ,go-github-com-alecthomas-assert-v2)))
    (home-page "https://github.com/alecthomas/kong")
    (synopsis "Kong is a command-line parser for Go")
    (description
     "Package kong aims to support arbitrarily complex command-line structures with as
little developer effort as possible.")
    (license license:expat)))

(define-public go-github-com-alecthomas-chroma-v2
  (package
    (name "go-github-com-alecthomas-chroma-v2")
    (version "2.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/alecthomas/chroma")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1h2i4hqn0h4iplhid0x6rv6l7a2zglivv6w33gg8z1cvd6acjzcl"))))
    (build-system go-build-system)
    (arguments 
     (list #:import-path "github.com/alecthomas/chroma/v2"
           #:unpack-path "github.com/alecthomas/chroma/v2"))
    (native-inputs (list go-github-com-stretchr-testify))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-github-com-lestrrat-go-file-rotatelogs" ,go-github-com-lestrrat-go-file-rotatelogs)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-mattn-go-colorable" ,go-github-com-mattn-go-colorable)
                         ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
                         ("go-github-com-alecthomas-kong" ,go-github-com-alecthomas-kong)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-github-com-dlclark-regexp2" ,go-github-com-dlclark-regexp2)
                         ("go-github-com-alecthomas-repr" ,go-github-com-alecthomas-repr)))
    (home-page "https://github.com/alecthomas/chroma")
    (synopsis "Chroma — A general purpose syntax highlighter in pure Go")
    (description
     "Package chroma takes source code and other structured text and converts it into
syntax highlighted HTML, ANSI- coloured text, etc.")
    (license license:expat)))

(define-public go-github-com-lestrrat-go-file-rotatelogs
  (package
    (name "go-github-com-lestrrat-go-file-rotatelogs")
    (version "0.0.0-20180223000712-d3151e2a480f")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lestrrat-go/file-rotatelogs")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12mswdhxs1gyb6llyh43n8rhqvrwyypqdn1ylbcgmjjc73ylzkq3"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/lestrrat-go/file-rotatelogs"
       #:tests? #f))
    (native-inputs (list go-github-com-jonboulle-clockwork
                         go-github-com-stretchr-testify))
    (propagated-inputs (list go-github-com-pkg-errors go-github-com-lestrrat-go-strftime))
    (home-page "https://github.com/lestrrat-go/file-rotatelogs")
    (synopsis "go-file-rotatelogs")
    (description
     "package rotatelogs is a port of File-RotateLogs from Perl
(@@url{https://metacpan.org/release/File-RotateLogs,https://metacpan.org/release/File-RotateLogs}),
and it allows you to automatically rotate output files when you write to them
according to the filename pattern that you can specify.")
    (license license:expat)))

(define-public go-github-com-scylladb-termtables
  (package
    (name "go-github-com-scylladb-termtables")
    (version "0.0.0-20191203121021-c4c0b6d42ff4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/scylladb/termtables")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12qjh5gjw2hvrjdh99d4ng8sxicjgdf5bbadrlp4sbd86rwskr54"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/scylladb/termtables"))
    (home-page "https://github.com/scylladb/termtables")
    (synopsis "Termtables")
    (description
     "This package provides a @@url{http://golang.org,Go} port of the Ruby library
@@url{https://github.com/visionmedia/terminal-table,terminal-tables} for fast
and simple ASCII table generation.")
    (license license:asl2.0)))

(define-public go-github-com-araddon-dateparse
  (package
    (name "go-github-com-araddon-dateparse")
    (version "0.0.0-20210429162001-6b43995a97de")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/araddon/dateparse")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0p60rdbfk7d97hb1kk225lvnqvhw04d822782hn66i4yfvigrraj"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/araddon/dateparse"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-scylladb-termtables" ,go-github-com-scylladb-termtables)
                         ("go-github-com-mattn-go-runewidth" ,go-github-com-mattn-go-runewidth)))
    (home-page "https://github.com/araddon/dateparse")
    (synopsis "Go Date Parser")
    (description
     "Package dateparse parses date-strings without knowing the format in advance,
using a fast lex based approach to eliminate shotgun attempts.  It leans towards
US style dates when there is a conflict.")
    (license license:expat)))

(define-public go-github-com-dmulholl-mp3lib
  (package
    (name "go-github-com-dmulholl-mp3lib")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/dmulholl/mp3lib")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ph3k85qzjhamchngchy947dv95jiscq6j2xk8p3vdb94cbi34vq"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dmulholl/mp3lib"))
    (home-page "https://github.com/dmulholl/mp3lib")
    (synopsis "MP3Lib")
    (description "Package mp3lib is a simple library for parsing MP3 files.")
    (license license:unlicense)))

(define-public go-github-com-tomnomnom-linkheader
  (package
    (name "go-github-com-tomnomnom-linkheader")
    (version "0.0.0-20180905144013-02ca5825eb80")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tomnomnom/linkheader")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ghrv28vrvvrpyr4d4q817yby8g1j04mid8ql00sds1pvfv67d32"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/tomnomnom/linkheader"))
    (home-page "https://github.com/tomnomnom/linkheader")
    (synopsis #f)
    (description
     "Package linkheader provides functions for parsing HTTP Link headers")
    (license license:expat)))

(define-public go-github-com-elnormous-contenttype
  (package
    (name "go-github-com-elnormous-contenttype")
    (version "1.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/elnormous/contenttype")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0kviyv0lsypndr1z5xf5lzsag5rh122vl7k03hwi4aiqrv3dhz4a"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/elnormous/contenttype"))
    (home-page "https://github.com/elnormous/contenttype")
    (synopsis "Content-Type support library for Go")
    (description
     "Package contenttype implements HTTP Content-Type and Accept header parsers.")
    (license license:unlicense)))

(define-public go-github-com-paulmach-go-geojson
  (package
    (name "go-github-com-paulmach-go-geojson")
    (version "1.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/paulmach/go.geojson")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1gn4lgd1n9dvz408f2bda8qddpdi391jnwaivl1kgyr6y5abh85f"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/paulmach/go.geojson"))
    (home-page "https://github.com/paulmach/go.geojson")
    (synopsis "go.geojson")
    (description
     "Package geojson is a library for encoding and decoding GeoJSON into Go structs.
Supports both the json.Marshaler and json.Unmarshaler interfaces as well as
helper functions such as `UnmarshalFeatureCollection`, `UnmarshalFeature` and
`UnmarshalGeometry`.")
    (license license:expat)))

(define-public go-github-com-denisenkom-go-mssqldb
  (package
    (name "go-github-com-denisenkom-go-mssqldb")
    (version "0.12.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/denisenkom/go-mssqldb")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12zd98r3p9nvjd1cnlzfzdifk4smkq8zffw7qidw1ni90zl26gy2"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/denisenkom/go-mssqldb"
       #:tests? #f)) ;FIXME
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-golang-sql-sqlexp" ,go-github-com-golang-sql-sqlexp)
                         ("go-github-com-golang-sql-civil" ,go-github-com-golang-sql-civil)
                         ("go-github-com-azure-azure-sdk-for-go" ,go-github-com-azure-azure-sdk-for-go)))
    (home-page "https://github.com/denisenkom/go-mssqldb")
    (synopsis "A pure Go MSSQL driver for Go's database/sql package")
    (description
     "package mssql implements the TDS protocol used to connect to MS SQL Server
(sqlserver) database servers.")
    (license license:bsd-3)))

(define-public go-github-com-jinzhu-gorm
  (package
    (name "go-github-com-jinzhu-gorm")
    (version "1.9.16")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jinzhu/gorm")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1nnbxs337238gx0d9rg643m5jwpz005ki91g5did7rwvk7bbsly9"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jinzhu/gorm"))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-mattn-go-sqlite3" ,go-github-com-mattn-go-sqlite3)
                         ("go-github-com-lib-pq" ,go-github-com-lib-pq)
                         ("go-github-com-jinzhu-now" ,go-github-com-jinzhu-now)
                         ("go-github-com-jinzhu-inflection" ,go-github-com-jinzhu-inflection)
                         ("go-github-com-go-sql-driver-mysql" ,go-github-com-go-sql-driver-mysql)
                         ("go-github-com-erikstmartin-go-testdb" ,go-github-com-erikstmartin-go-testdb)
                         ("go-github-com-denisenkom-go-mssqldb" ,go-github-com-denisenkom-go-mssqldb)))
    (home-page "https://github.com/jinzhu/gorm")
    (synopsis "GORM")
    (description
     "GORM V2 moved to
@@url{https://github.com/go-gorm/gorm,https://github.com/go-gorm/gorm}")
    (license license:expat)))

(define-public go-github-com-erikstmartin-go-testdb
  (package
    (name "go-github-com-erikstmartin-go-testdb")
    (version "0.0.0-20160219214506-8d10e4a1bae5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/erikstmartin/go-testdb")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1fhrqcpv8x74qwxx9gpnhgqbz5wkp2bnsq92w418l1fnrgh4ppmq"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/erikstmartin/go-testdb"))
    (home-page "https://github.com/erikstmartin/go-testdb")
    (synopsis "go-testdb")
    (description
     "Framework for stubbing responses from go's driver.Driver interface.")
    (license license:bsd-2)))

(define-public go-github-com-golang-sql-sqlexp
  (package
    (name "go-github-com-golang-sql-sqlexp")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang-sql/sqlexp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0g7apf8mfzrzh1dzkhskbiabb8xc6ghbnxnc4bdk9hzgvrv9c4hn"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/golang-sql/sqlexp"))
    (home-page "https://github.com/golang-sql/sqlexp")
    (synopsis "golang-sql exp")
    (description
     "Package sqlexp provides interfaces and functions that may be adopted into the
database/sql package in the future.  All features may change or be removed in
the future.")
    (license license:bsd-3)))

(define-public go-github-com-golang-sql-civil
  (package
    (name "go-github-com-golang-sql-civil")
    (version "0.0.0-20220223132316-b832511892a9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang-sql/civil")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "152smf33psdad1222jrabpkl7yvkzw8k66hyypn5gj07943gsk10"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/golang-sql/civil"))
    (home-page "https://github.com/golang-sql/civil")
    (synopsis "Civil Date and Time")
    (description
     "Package civil implements types for civil time, a time-zone-independent
representation of time that follows the rules of the proleptic Gregorian
calendar with exactly 24-hour days, 60-minute hours, and 60-second minutes.")
    (license license:asl2.0)))

(define-public go-github-com-joeshaw-gengen
  (package
    (name "go-github-com-joeshaw-gengen")
    (version "0.0.0-20220404160307-14f6c878dcab")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/joeshaw/gengen")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1rbb8agh7w4p7pmrwcy5ppla9nsz0j3a9mbklyb9g4qj1ml5jjss"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/joeshaw/gengen"))
    (propagated-inputs `(("go-golang-org-x-tools" ,go-golang-org-x-tools)))
    (home-page "https://github.com/joeshaw/gengen")
    (synopsis "gengen - A generics code generator for Go")
    (description
     "🎉🎉 @@strong{Go 1.18 includes native support for generics!} 🎉🎉")
    (license license:expat)))

(define-public go-github-com-tkrajina-gpxgo
  (package
    (name "go-github-com-tkrajina-gpxgo")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tkrajina/gpxgo")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1zykkdb5xiwh5ap201lz7gxmgkxr6glyyn6ckjj476r83ydaafam"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/tkrajina/gpxgo"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-joeshaw-gengen" ,go-github-com-joeshaw-gengen)))
    (home-page "https://github.com/tkrajina/gpxgo")
    (synopsis "Go GPX library")
    (description
     "gpxgo is a golang library for parsing and manipulating GPX files.  GPX (GPS
eXchange Format) is a XML based file format for GPS track logs.")
    (license license:asl2.0)))

(define-public go-github-com-traefik-yaegi
  (package
    (name "go-github-com-traefik-yaegi")
    (version "0.14.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/traefik/yaegi")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "19rkx7dz00n3yfppvyanwqkccyh352kxb5kbrp14qw9qxhcy1rb0"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/traefik/yaegi"))
    (home-page "https://github.com/traefik/yaegi")
    (synopsis "Features")
    (description "Package yaegi provides a Go interpreter.")
    (license license:asl2.0)))

(define-public go-github-com-cretz-bine
  (package
    (name "go-github-com-cretz-bine")
    (version "0.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/cretz/bine")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "16h7j7v4qbwb7zjsbc1p3b67xji7hgis95znz9cj8fw3rqxwvkcs"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/cretz/bine"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/cretz/bine")
    (synopsis #f)
    (description
     "Bine is a toolkit to assist in creating Tor clients and servers.  Features:")
    (license license:expat)))

(define-public go-github-com-go-telegram-bot-api-telegram-bot-api-v5
  (package
    (name "go-github-com-go-telegram-bot-api-telegram-bot-api-v5")
    (version "5.5.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     "https://github.com/go-telegram-bot-api/telegram-bot-api")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "18bar3gkl714fr3ziwyncgmdygwhci01rzg41i010j6q119cph69"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-telegram-bot-api/telegram-bot-api/v5"
       #:tests? #f))
    (home-page "https://github.com/go-telegram-bot-api/telegram-bot-api")
    (synopsis "Golang bindings for the Telegram Bot API")
    (description
     "Package tgbotapi has functions and types used for interacting with the Telegram
Bot API.")
    (license license:expat)))

(define-public go-github-com-mergestat-timediff
  (package
    (name "go-github-com-mergestat-timediff")
    (version "0.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mergestat/timediff")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "06zjcifywk6y0k8gvc32znyfadg5h06grrjdajhl59l58rpgjd1d"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/mergestat/timediff"))
    (home-page "https://github.com/mergestat/timediff")
    (synopsis "timediff")
    (description
     "@@code{timediff} is a Go package for printing human readable, relative time
differences.  Output is based on
@@url{https://day.js.org/docs/en/display/from-now,ranges defined in the Day.js}
JavaScript library, and can be customized if needed.  It's currently used by the
.")
    (license license:expat)))

(define-public go-github-com-azure-azure-sdk-for-go
  (package
    (name "go-github-com-azure-azure-sdk-for-go")
    (version "67.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Azure/azure-sdk-for-go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1w21sp8z5x3s1vlvamj1xca0ff51v68wln48sdrkpr9vp6vd8pfv"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/azure/azure-sdk-for-go"))
    (home-page "https://github.com/azure/azure-sdk-for-go")
    (synopsis "Azure SDK for Go")
    (description
     "Package sdk provides Go packages for managing and using Azure services.")
    (license license:expat)))

(define-public go-github-com-posener-wstest
  (package
    (name "go-github-com-posener-wstest")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/posener/wstest")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "16wpwg82ph0acy7qzz579qv91gfbkac9nlh31cjg8hisv3mpfxjz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/posener/wstest"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)))
    (home-page "https://github.com/posener/wstest")
    (synopsis "wstest")
    (description
     "Package wstest provides a NewDialer function to test just the `http.Handler`
that upgrades the connection to a websocket session.  It runs the handler
function in a goroutine without listening on any port.  The returned
`websocket.Dialer` then can be used to dial and communicate with the given
handler.")
    (license license:asl2.0)))

(define-public go-github-com-thoas-go-funk
  (package
    (name "go-github-com-thoas-go-funk")
    (version "0.9.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/thoas/go-funk")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1j2m9g4yspr3d2xkbda76a2qf2pmk0399y85ij8nkqn4fzhg7pq2"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/thoas/go-funk"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/thoas/go-funk")
    (synopsis #f)
    (description #f)
    (license license:expat)))


(define-public go-github-com-samber-lo
  (package
    (name "go-github-com-samber-lo")
    (version "1.33.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/samber/lo")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1m67kypdd21369mddrvsnq3z2qpiwxxcc4ca4cwpmxxmmqy983nl"))))
    (build-system go-build-system)
    (arguments
     (list #:import-path "github.com/samber/lo"
           #:go go-1.19))
    ;;(native-inputs (list go-1.19))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-niemeyer-pretty" ,go-github-com-niemeyer-pretty)
                         ("go-github-com-kr-text" ,go-github-com-kr-text)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-golang-org-x-exp" ,go-golang-org-x-exp)
                         ("go-github-com-thoas-go-funk" ,go-github-com-thoas-go-funk)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/samber/lo")
    (synopsis "lo")
    (description "✨")
    (license license:expat)))


(define-public go-github-com-c2h5oh-datasize
  (package
    (name "go-github-com-c2h5oh-datasize")
    (version "0.0.0-20220606134207-859f65c6625b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/c2h5oh/datasize")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wvcd7pq42mqizdw96iyhx5zid0say7fgrl5lrvz0qrlgh7zxqfn"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/c2h5oh/datasize"))
    (home-page "https://github.com/c2h5oh/datasize")
    (synopsis "datasize")
    (description "Golang helpers for data sizes")
    (license license:expat)))

(define-public go-github-com-yuin-goldmark-emoji
  (package
    (name "go-github-com-yuin-goldmark-emoji")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/yuin/goldmark-emoji")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0jl0w0hnfhfm4wvlxbbkyzfxyr9sk8q0nxaydql6wq64my5h49ln"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/yuin/goldmark-emoji"))
    (propagated-inputs `(("go-github-com-yuin-goldmark" ,go-github-com-yuin-goldmark)))
    (home-page "https://github.com/yuin/goldmark-emoji")
    (synopsis "goldmark-emoji")
    (description
     "package emoji is a extension for the
goldmark(@@url{http://github.com/yuin/goldmark,http://github.com/yuin/goldmark}).")
    (license license:expat)))

(define-public go-github-com-masterminds-semver-v3
  (package
    (name "go-github-com-masterminds-semver-v3")
    (version "3.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Masterminds/semver")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0dsqa585ixz6pbff60p0pk709kp3kksh668mjwrlxgqiammxa1p8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/Masterminds/semver/v3"))
    (home-page "https://github.com/Masterminds/semver")
    (synopsis "SemVer")
    (description
     "Package semver provides the ability to work with Semantic Versions
(@@url{http://semver.org,http://semver.org}) in Go.")
    (license license:expat)))

(define-public go-github-com-cockroachdb-apd
  (package
    (name "go-github-com-cockroachdb-apd")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/cockroachdb/apd")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "14jnnqpdsa3vxh2zpznd2dpnychcrlkljppfplrigrs245slyh72"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/cockroachdb/apd"))
    (home-page "https://github.com/cockroachdb/apd")
    (propagated-inputs (list go-github-com-pkg-errors))
    (synopsis "apd")
    (description "Package apd implements arbitrary-precision decimals.")
    (license license:asl2.0)))

(define-public go-github-com-go-logfmt-logfmt
  (package
    (name "go-github-com-go-logfmt-logfmt")
    (version "0.5.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-logfmt/logfmt")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "01fs4x2aqw2qcsz18s4nfvyqv3rcwz5xmgpk3bic6nzgyzsjd7dp"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-logfmt/logfmt"))
    (home-page "https://github.com/go-logfmt/logfmt")
    (synopsis "logfmt")
    (description
     "Package logfmt implements utilities to marshal and unmarshal data in the logfmt
format.  The logfmt format records key/value pairs in a way that balances
readability for humans and simplicity of computer parsing.  It is most commonly
used as a more human friendly alternative to JSON for structured logging.")
    (license license:expat)))

(define-public go-github-com-go-kit-log
  (package
    (name "go-github-com-go-kit-log")
    (version "0.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-kit/log")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1xjv2g1cd1iaghhm1c1zw0lcz89a9zq5xradyjipvrbqxbxckqm6"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-kit/log"))
    (propagated-inputs `(("go-github-com-go-logfmt-logfmt" ,go-github-com-go-logfmt-logfmt)))
    (home-page "https://github.com/go-kit/log")
    (synopsis "package log")
    (description "Package log provides a structured logger.")
    (license license:expat)))

(define-public go-github-com-jackc-pgmock
  (package
    (name "go-github-com-jackc-pgmock")
    (version "0.0.0-20210724152146-4ad1a8207f65")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgmock")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "189hp5fkvavwgg7z0z9b9xj88ypsphvb7s4dpwa5aj42jm39nqha"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgmock"))
    (native-inputs (list go-github-com-jackc-pgconn go-github-com-stretchr-testify))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ;;("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-jackc-pgproto3-v2" ,go-github-com-jackc-pgproto3-v2)
                         ;;("go-github-com-jackc-pgconn" ,go-github-com-jackc-pgconn)
                         ))
    (home-page "https://github.com/jackc/pgmock")
    (synopsis "pgmock")
    (description
     "Package pgmock provides the ability to mock a PostgreSQL server.")
    (license license:expat)))

(define-public go-github-com-jackc-pgpassfile
  (package
    (name "go-github-com-jackc-pgpassfile")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgpassfile")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1crw06lzksgimbmr1a3sr00azg2v7l4qkvjra1cpmzzq5mncaj8z"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgpassfile"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jackc/pgpassfile")
    (synopsis "pgpassfile")
    (description "Package pgpassfile is a parser PostgreSQL .pgpass files.")
    (license license:expat)))

(define-public go-github-com-jackc-pgservicefile
  (package
    (name "go-github-com-jackc-pgservicefile")
    (version "0.0.0-20200714003250-2b9c44734f2b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgservicefile")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13gbi0ad58rm3rcgj8bssc7hgrqwva0q015fw57vx5cxb4rcrmxh"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgservicefile"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jackc/pgservicefile")
    (synopsis "pgservicefile")
    (description
     "Package pgservicefile is a parser for PostgreSQL service files (e.g.
.pg_service.conf).")
    (license license:expat)))

(define-public go-github-com-jackc-pgconn
  (package
    (name "go-github-com-jackc-pgconn")
    (version "1.13.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgconn")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "14pgrbnc6p2fqjfcan8l73iy3nr12fmva9p9vss0r1fh9skwlw1x"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgconn"
       #:tests? #f))
    ;;(native-inputs (list go-github-com-jackc-pgmock
    ;;                     go-github-com-stretchr-testify))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-jackc-pgservicefile" ,go-github-com-jackc-pgservicefile)
                         ("go-github-com-jackc-pgproto3-v2" ,go-github-com-jackc-pgproto3-v2)
                         ("go-github-com-jackc-pgpassfile" ,go-github-com-jackc-pgpassfile)
                         ("go-github-com-jackc-pgio" ,go-github-com-jackc-pgio)
                         ("go-github-com-jackc-chunkreader-v2" ,go-github-com-jackc-chunkreader-v2)))
    (home-page "https://github.com/jackc/pgconn")
    (synopsis "pgconn")
    (description "Package pgconn is a low-level PostgreSQL database driver.")
    (license license:expat)))

(define-public go-github-com-jackc-chunkreader-v2
  (package
    (name "go-github-com-jackc-chunkreader-v2")
    (version "2.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/chunkreader")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0fj585hp3s4cjfzncr5gmim96p0b956pqdf4nm7yan1ipfch9l1c"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/chunkreader/v2"))
    (home-page "https://github.com/jackc/chunkreader")
    (synopsis "chunkreader")
    (description
     "Package chunkreader provides an io.Reader wrapper that minimizes IO reads and
memory allocations.")
    (license license:expat)))

(define-public go-github-com-jackc-pgio
  (package
    (name "go-github-com-jackc-pgio")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgio")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0l17gpn11wf6jm5kbfmxh8j00n5zpmwck3wr91f1cv34k4chyvg1"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgio"))
    (home-page "https://github.com/jackc/pgio")
    (synopsis "pgio")
    (description
     "Package pgio is a low-level toolkit building messages in the PostgreSQL wire
protocol.")
    (license license:expat)))

(define-public go-github-com-jackc-pgproto3-v2
  (package
    (name "go-github-com-jackc-pgproto3-v2")
    (version "2.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgproto3")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0flyzzbkg1zw12dcfwn9dc9g2s9z56bqg92m3whb5iyxypa3rpy9"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgproto3/v2"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-jackc-pgio" ,go-github-com-jackc-pgio)
                         ("go-github-com-jackc-chunkreader-v2" ,go-github-com-jackc-chunkreader-v2)))
    (home-page "https://github.com/jackc/pgproto3")
    (synopsis "pgproto3")
    (description
     "Package pgproto3 is a encoder and decoder of the PostgreSQL wire protocol
version 3.")
    (license license:expat)))

(define-public go-github-com-jackc-puddle
  (package
    (name "go-github-com-jackc-puddle")
    (version "1.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/puddle")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0382q7xjdw5wx6174i2sf4gnc5ppgj9snvrvh3rcnwg02yd0np38"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/puddle"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/jackc/puddle")
    (synopsis "Puddle")
    (description "Package puddle is a generic resource pool.")
    (license license:expat)))

(define-public go-github-com-rs-xid
  (package
    (name "go-github-com-rs-xid")
    (version "1.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/rs/xid")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1j1dcrq9napvdfl6g2vd631iv12myirhlmn6kgw8f1jnrkaqgdaz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/rs/xid"))
    (home-page "https://github.com/rs/xid")
    (synopsis "Globally Unique ID Generator")
    (description
     "Package xid is a globally unique id generator suited for web scale")
    (license license:expat)))

(define-public go-github-com-rs-zerolog
  (package
    (name "go-github-com-rs-zerolog")
    (version "1.28.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/rs/zerolog")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "123wsr1cbyza15k50md2lq5si5gccb2ah7ykcggsmlr60xm485q9"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/rs/zerolog"))
    (propagated-inputs `(("go-github-com-rs-xid" ,go-github-com-rs-xid)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-mattn-go-colorable" ,go-github-com-mattn-go-colorable)
                         ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
    
                         ("go-github-com-coreos-go-systemd-v22" ,go-github-com-coreos-go-systemd-v22)))
    (home-page "https://github.com/rs/zerolog")
    (synopsis "Zero Allocation JSON Logger")
    (description
     "Package zerolog provides a lightweight logging library dedicated to JSON
logging.")
    (license license:expat)))

(define-public go-gopkg-in-inconshreveable-log15-v2
  (package
    (name "go-gopkg-in-inconshreveable-log15-v2")
    (version "2.0.0-20200109203555-b30bc20e4fd1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gopkg.in/inconshreveable/log15.v2")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "03frzx2ar9fsvdrlq7d1k8askaxkyvml2im39ipcmnz760rv52hx"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "gopkg.in/inconshreveable/log15.v2"
       #:unpack-path "gopkg.in/inconshreveable/log15.v2"))
    (propagated-inputs (list go-github-com-go-stack-stack
                             go-github-com-mattn-go-colorable
                             go-github-com-mattn-go-isatty))
    (home-page "https://gopkg.in/inconshreveable/log15.v2")
    (synopsis "log15")
    (description
     "Package log15 provides an opinionated, simple toolkit for best-practice logging
that is both human and machine readable.  It is modeled after the standard
library's io and net/http packages.")
    (license license:asl2.0)))

(define-public go-github-com-go-stack-stack
  (package
    (name "go-github-com-go-stack-stack")
    (version "1.8.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-stack/stack")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "01m6l9w84yq2yyly8bdfsgc386hla1gn9431c7vr3mfa3bchj5wb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-stack/stack"))
    (home-page "https://github.com/go-stack/stack")
    (synopsis "stack")
    (description
     "Package stack implements utilities to capture, manipulate, and format call
stacks.  It provides a simpler API than package runtime.")
    (license license:expat)))

(define-public go-github-com-jackc-pgx-v4
  (package
    (name "go-github-com-jackc-pgx-v4")
    (version "4.17.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgx")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1n7p1bail3m3hx13rym308skjp8aiqic8l6p6y0s7i4kymx0zwmk"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgx/v4"
       #:tests? #f))
    (propagated-inputs `(("go-gopkg-in-inconshreveable-log15-v2" ,go-gopkg-in-inconshreveable-log15-v2)
                         ("go-go-uber-org-zap" ,go-go-uber-org-zap)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
                         ("go-github-com-shopspring-decimal" ,go-github-com-shopspring-decimal)
                         ("go-github-com-rs-zerolog" ,go-github-com-rs-zerolog)
                         ("go-github-com-jackc-puddle" ,go-github-com-jackc-puddle)
                         ("go-github-com-jackc-pgtype" ,go-github-com-jackc-pgtype)
                         ("go-github-com-jackc-pgproto3-v2" ,go-github-com-jackc-pgproto3-v2)
                         ("go-github-com-jackc-pgio" ,go-github-com-jackc-pgio)
                         ("go-github-com-jackc-pgconn" ,go-github-com-jackc-pgconn)
                         ("go-github-com-gofrs-uuid" ,go-github-com-gofrs-uuid)
                         ("go-github-com-go-kit-log" ,go-github-com-go-kit-log)
                         ("go-github-com-cockroachdb-apd" ,go-github-com-cockroachdb-apd)
                         ("go-github-com-masterminds-semver-v3" ,go-github-com-masterminds-semver-v3)))
    (home-page "https://github.com/jackc/pgx")
    (synopsis "pgx - PostgreSQL Driver and Toolkit")
    (description "Package pgx is a PostgreSQL database driver.")
    (license license:expat)))

(define-public go-github-com-shopspring-decimal
  (package
    (name "go-github-com-shopspring-decimal")
    (version "1.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/shopspring/decimal")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1w1wjv2aqyqp22s8gc2nxp8gk4h0dxvp15xsn5lblghaqjcd239h"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/shopspring/decimal"))
    (home-page "https://github.com/shopspring/decimal")
    (synopsis "decimal")
    (description
     "Package decimal implements an arbitrary precision fixed-point decimal.")
    (license license:expat)))

(define-public go-github-com-jackc-pgtype
  (package
    (name "go-github-com-jackc-pgtype")
    (version "1.12.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jackc/pgtype")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "19h3x3k3wsahpch48dqa5zkzjvqwly6x6d80w5jmvrwcczp526qw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jackc/pgtype"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-shopspring-decimal" ,go-github-com-shopspring-decimal)
                         ("go-github-com-lib-pq" ,go-github-com-lib-pq)
                         ;;("go-github-com-jackc-pgx-v4" ,go-github-com-jackc-pgx-v4)
                         ("go-github-com-jackc-pgio" ,go-github-com-jackc-pgio)
                         ("go-github-com-jackc-pgconn" ,go-github-com-jackc-pgconn)
                         ("go-github-com-gofrs-uuid" ,go-github-com-gofrs-uuid)))
    (home-page "https://github.com/jackc/pgtype")
    (synopsis "pgtype")
    (description
     "pgtype implements Go types for over 70 PostgreSQL types.  pgtype is the type
system underlying the
@@url{https://github.com/jackc/pgx,https://github.com/jackc/pgx} PostgreSQL
driver.  These types support the binary format for enhanced performance with
pgx.  They also support the database/sql @@code{Scan} and @@code{Value}
interfaces and can be used with
@@url{https://github.com/lib/pq,https://github.com/lib/pq}.")
    (license license:expat)))

(define-public go-github-com-lopezator-migrator
  (package
    (name "go-github-com-lopezator-migrator")
    (version "0.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lopezator/migrator")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0x3hsf5b0rbc4fjilbsbk7cswnfr0vgcb21gicza2b3k2m65xbcp"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/lopezator/migrator"))
    (propagated-inputs `(("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-jackc-pgtype" ,go-github-com-jackc-pgtype)
                         ("go-github-com-jackc-pgservicefile" ,go-github-com-jackc-pgservicefile)
                         ("go-github-com-jackc-pgproto3-v2" ,go-github-com-jackc-pgproto3-v2)
                         ("go-github-com-jackc-pgpassfile" ,go-github-com-jackc-pgpassfile)
                         ("go-github-com-jackc-pgio" ,go-github-com-jackc-pgio)
                         ("go-github-com-jackc-pgconn" ,go-github-com-jackc-pgconn)
                         ("go-github-com-jackc-chunkreader-v2" ,go-github-com-jackc-chunkreader-v2)
                         ("go-github-com-jackc-pgx-v4" ,go-github-com-jackc-pgx-v4)
                         ("go-github-com-go-sql-driver-mysql" ,go-github-com-go-sql-driver-mysql)))
    (home-page "https://github.com/lopezator/migrator")
    (synopsis "migrator")
    (description "Dead simple Go database migration library.")
    (license license:asl2.0)))


(define-public go-github-com-dchest-captcha
  (package
    (name "go-github-com-dchest-captcha")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/dchest/captcha")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15b8aiiipzai362c6ingm8yznj0hv33z6bi47r79k9kj59bqq9xa"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dchest/captcha"))
    (home-page "https://github.com/dchest/captcha")
    (synopsis "Package captcha")
    (description
     "Package captcha implements generation and verification of image and audio
CAPTCHAs.")
    (license license:expat)))
(define-public go-github-com-gin-contrib-sse
  (package
    (name "go-github-com-gin-contrib-sse")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gin-contrib/sse")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "072nq91a65n5xvwslqjyvydfd0mfpnvb3vwjyfvmzm1ym96wr1nd"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gin-contrib/sse"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/gin-contrib/sse")
    (synopsis "Server-Sent Events")
    (description
     "Server-sent events (SSE) is a technology where a browser receives automatic
updates from a server via HTTP connection.  The Server-Sent Events EventSource
API is @@url{http://www.w3.org/TR/2009/WD-eventsource-20091029/,standardized as
part of HTML5[1] by the W3C}.")
    (license license:expat)))

(define-public go-github-com-go-playground-assert-v2
  (package
    (name "go-github-com-go-playground-assert-v2")
    (version "2.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-playground/assert")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13mb07dxhcy9ydqbracnrpfj682g6sazjpm56yrlbn2jc1yfy44c"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-playground/assert/v2"))
    (home-page "https://github.com/go-playground/assert")
    (synopsis "Package assert")
    (description
     "Package assert provides some basic assertion functions for testing and also
provides the building blocks for creating your own more complex validations.")
    (license license:expat)))

(define-public go-github-com-go-playground-validator-v10
  (package
    (name "go-github-com-go-playground-validator-v10")
    (version "10.11.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-playground/validator")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "03sd3gd3pl2bv07ivrixp3h5hrw1d1llrpjdalh9jbgby0x2f8ig"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-playground/validator/v10"))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-rogpeppe-go-internal" ,go-github-com-rogpeppe-go-internal)
                         ("go-github-com-leodido-go-urn" ,go-github-com-leodido-go-urn)
                         ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
                         ("go-github-com-go-playground-universal-translator" ,go-github-com-go-playground-universal-translator)
                         ("go-github-com-go-playground-locales" ,go-github-com-go-playground-locales)
                         ("go-github-com-go-playground-assert-v2" ,go-github-com-go-playground-assert-v2)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/go-playground/validator")
    (synopsis "Package validator")
    (description
     "Package validator implements value validations for structs and individual fields
based on tags.")
    (license license:expat)))


(define-public go-github-com-ugorji-go
  (package
    (name "go-github-com-ugorji-go")
    (version "1.2.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ugorji/go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0h4mbxp8zyhash342l00fd5726yhfmgrlk6v7sl6k8fd6mkb10l4"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ugorji/go"))
    (propagated-inputs `(("go-github-com-ugorji-go-codec" ,go-github-com-ugorji-go-codec)))
    (home-page "https://github.com/ugorji/go")
    (synopsis "go-codec")
    (description
     "This repository contains the @@code{go-codec} library, the @@code{codecgen} tool
and benchmarks for comparing against other libraries.")
    (license license:expat)))

(define-public go-github-com-ugorji-go-codec
  (package
    (name "go-github-com-ugorji-go-codec")
    (version "1.2.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ugorji/go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0h4mbxp8zyhash342l00fd5726yhfmgrlk6v7sl6k8fd6mkb10l4"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ugorji/go/codec"
       #:unpack-path "github.com/ugorji/go"))
    ;;(propagated-inputs `(("go-github-com-ugorji-go" ,go-github-com-ugorji-go)))
    (home-page "https://github.com/ugorji/go")
    (synopsis "Package Documentation for github.com/ugorji/go/codec")
    (description
     "Package codec provides a High Performance, Feature-Rich Idiomatic Go 1.4+
codec/encoding library for binc, msgpack, cbor, json.")
    (license license:expat)))


(define-public go-github-com-gin-gonic-gin
  (package
    (name "go-github-com-gin-gonic-gin")
    (version "1.8.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gin-gonic/gin")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0fbs44q2w1cx891l3i1jxfrvf9v3ar3hlb8vnqlg7nbkch04jkdf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gin-gonic/gin"))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-modern-go-reflect2" ,go-github-com-modern-go-reflect2)
                         ("go-github-com-modern-go-concurrent" ,go-github-com-modern-go-concurrent)
                         ("go-github-com-leodido-go-urn" ,go-github-com-leodido-go-urn)
                         ("go-github-com-go-playground-universal-translator" ,go-github-com-go-playground-universal-translator)
                         ("go-github-com-go-playground-locales" ,go-github-com-go-playground-locales)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
                         ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-ugorji-go-codec" ,go-github-com-ugorji-go-codec)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-pelletier-go-toml-v2" ,go-github-com-pelletier-go-toml-v2)
                         ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
                         ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
                         ("go-github-com-goccy-go-json" ,go-github-com-goccy-go-json)
                         ("go-github-com-go-playground-validator-v10" ,go-github-com-go-playground-validator-v10)
                         ("go-github-com-gin-contrib-sse" ,go-github-com-gin-contrib-sse)))
    (home-page "https://github.com/gin-gonic/gin")
    (synopsis "Gin Web Framework")
    (description "Package gin implements a HTTP web framework called gin.")
    (license license:expat)))

(define-public go-github-com-gobwas-httphead
  (package
    (name "go-github-com-gobwas-httphead")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gobwas/httphead")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "106l8ml5yihld3rrf45q5fhlsx64hrpj2dsvnnm62av4ya5nf0gb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gobwas/httphead"))
    (home-page "https://github.com/gobwas/httphead")
    (synopsis "httphead.")
    (description
     "Package httphead contains utils for parsing HTTP and HTTP-grammar compatible
text protocols headers.")
    (license license:expat)))

(define-public go-github-com-gobwas-pool
  (package
    (name "go-github-com-gobwas-pool")
    (version "0.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gobwas/pool")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0imipsf8nslc78in78wcri2ir2zzajp2h543dp2cshrrdbwkybx7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gobwas/pool"))
    (home-page "https://github.com/gobwas/pool")
    (synopsis "pool")
    (description
     "Package pool contains helpers for pooling structures distinguishable by size.")
    (license license:expat)))

(define-public go-github-com-gobwas-ws
  (package
    (name "go-github-com-gobwas-ws")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gobwas/ws")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1nv7kksbm3swyp0z85zskdjqchr49bwkhk99cfgdpm3hx8516b87"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gobwas/ws"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-gobwas-pool" ,go-github-com-gobwas-pool)
                         ("go-github-com-gobwas-httphead" ,go-github-com-gobwas-httphead)))
    (home-page "https://github.com/gobwas/ws")
    (synopsis "ws")
    (description
     "Package ws implements a client and server for the WebSocket protocol as
specified in @@url{https://rfc-editor.org/rfc/rfc6455.html,RFC 6455}.")
    (license license:expat)))


(define-public go-nhooyr-io-websocket
  (package
    (name "go-nhooyr-io-websocket")
    (version "1.8.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/nhooyr/websocket")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "11bz96vh0nkw6f8kczzcs07ixdhjy8s7bl398j0cf1hh40zxvass"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "nhooyr.io/websocket"
       #:tests? #f)) ;FIXME wasmbrowsertest not found
    (propagated-inputs `(("go-golang-org-x-time" ,go-golang-org-x-time)
                         ("go-github-com-klauspost-compress" ,go-github-com-klauspost-compress)
                         ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)
                         ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
                         ("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
                         ("go-github-com-gobwas-ws" ,go-github-com-gobwas-ws)
                         ("go-github-com-gobwas-pool" ,go-github-com-gobwas-pool)
                         ("go-github-com-gobwas-httphead" ,go-github-com-gobwas-httphead)
                         ("go-github-com-gin-gonic-gin" ,go-github-com-gin-gonic-gin)
                         ))
    (home-page "https://nhooyr.io/websocket")
    (synopsis "websocket")
    (description
     "Package websocket implements the
@@url{https://rfc-editor.org/rfc/rfc6455.html,RFC 6455} WebSocket protocol.")
    (license license:expat)))

(define-public go-github-com-pquerna-otp
  (package
    (name "go-github-com-pquerna-otp")
    (version "1.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/pquerna/otp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1cvac28nchyi9l6iagdsq8mqm59498n4sfjvw88pf0rxhzmbxfcf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/pquerna/otp"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-boombuler-barcode" ,go-github-com-boombuler-barcode)))
    (home-page "https://github.com/pquerna/otp")
    (synopsis "otp: One Time Password utilities Go / Golang")
    (description
     "Package otp implements both HOTP and TOTP based one time passcodes in a Google
Authenticator compatible manner.")
    (license license:asl2.0)))

(define-public go-github-com-modern-go-concurrent
  (package
    (name "go-github-com-modern-go-concurrent")
    (version "0.0.0-20180306012644-bacd9c7ef1dd")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/modern-go/concurrent")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0s0fxccsyb8icjmiym5k7prcqx36hvgdwl588y0491gi18k5i4zs"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/modern-go/concurrent"))
    (home-page "https://github.com/modern-go/concurrent")
    (synopsis "concurrent")
    (description
     "because sync.Map is only available in go 1.9, we can use concurrent.Map to make
code portable")
    (license license:asl2.0)))

(define-public go-github-com-modern-go-reflect2
  (package
    (name "go-github-com-modern-go-reflect2")
    (version "1.0.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/modern-go/reflect2")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "05a89f9j4nj8v1bchfkv2sy8piz746ikj831ilbp54g8dqhl8vzr"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/modern-go/reflect2"))
    (home-page "https://github.com/modern-go/reflect2")
    (synopsis "reflect2")
    (description "reflect api that avoids runtime reflect.Value cost")
    (license license:asl2.0)))

(define-public go-github-com-json-iterator-go
  (package
    (name "go-github-com-json-iterator-go")
    (version "1.1.12")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/json-iterator/go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c8f0hxm18wivx31bs615x3vxs2j3ba0v6vxchsjhldc8kl311bz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/json-iterator/go"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-modern-go-reflect2" ,go-github-com-modern-go-reflect2)
                         ("go-github-com-modern-go-concurrent" ,go-github-com-modern-go-concurrent)
                         ("go-github-com-google-gofuzz" ,go-github-com-google-gofuzz)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/json-iterator/go")
    (synopsis "Benchmark")
    (description
     "Package jsoniter implements encoding and decoding of JSON as defined in
@@url{https://rfc-editor.org/rfc/rfc4627.html,RFC 4627} and provides interfaces
with identical syntax of standard lib encoding/json.  Converting from
encoding/json to jsoniter is no more than replacing the package with jsoniter
and variable type declarations (if any).  jsoniter interfaces gives 100%
compatibility with code using standard lib.")
    (license license:expat)))

(define-public go-github-com-mmcdole-goxpp
  (package
    (name "go-github-com-mmcdole-goxpp")
    (version "0.0.0-20200921145534-2f3784f67354")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mmcdole/goxpp")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "17xn1ffbj31v73gi79bgp7blrbwkv59pbscncasldliz96hdxxg3"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/mmcdole/goxpp"))
    (native-inputs (list go-github-com-stretchr-testify))

    (home-page "https://github.com/mmcdole/goxpp")
    (synopsis "goxpp")
    (description
     "The @@code{goxpp} library is an XML parser library that is loosely based on the
@@url{http://www.xmlpull.org/v1/download/unpacked/doc/quick_intro.html,Java
XMLPullParser}.  This library allows you to easily parse arbitrary XML content
using a pull parser.  You can think of @@code{goxpp} as a lightweight wrapper
around Go's XML @@code{Decoder} that provides a set of functions that make it
easier to parse XML content than using the raw decoder itself.")
    (license license:expat)))

(define-public go-github-com-mmcdole-gofeed
  (package
    (name "go-github-com-mmcdole-gofeed")
    (version "1.1.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mmcdole/gofeed")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04nbhx1arnmxirmhlb9ij7x7wb2pqyzf6ij4nrspizfdbnwx54f7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/mmcdole/gofeed"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-urfave-cli" ,go-github-com-urfave-cli)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-mmcdole-goxpp" ,go-github-com-mmcdole-goxpp)
                         ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)))
    (home-page "https://github.com/mmcdole/gofeed")
    (synopsis "gofeed")
    (description
     "The @@code{gofeed} library is a robust feed parser that supports parsing both
@@url{https://en.wikipedia.org/wiki/RSS,RSS},
@@url{https://en.wikipedia.org/wiki/Atom_(standard),Atom} and
@@url{https://jsonfeed.org/version/1,JSON} feeds.  The library provides a
universal @@code{gofeed.Parser} that will parse and convert all feed types into
a hybrid @@code{gofeed.Feed} model.  You also have the option of utilizing the
feed specific @@code{atom.Parser} or @@code{rss.Parser} or @@code{json.Parser}
parsers which generate @@code{atom.Feed}, @@code{rss.Feed} and @@code{json.Feed}
respectively.")
    (license license:expat)))

(define-public go-github-com-schollz-sqlite3dump
  (package
    (name "go-github-com-schollz-sqlite3dump")
    (version "1.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/schollz/sqlite3dump")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ix4a2brxybbjy0kf9v6bpppx8m48hh3f7aynwvxn6387s8961vr"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/schollz/sqlite3dump"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-mattn-go-sqlite3" ,go-github-com-mattn-go-sqlite3)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/schollz/sqlite3dump")
    (synopsis #f)
    (description #f)
    (license license:expat)))

(define-public goblog
  (package
    (name "goblog")
    (version "0.0.0-20221108192331-c242e2c7b3a2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jlelse/GoBlog")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1z25kng4j3y6hallrfr4wq4563vmqc81dm5s2q67n0rpp0fz1rx0"))))
    (build-system go-build-system)
    (arguments
     (list #:import-path "go.goblog.app/app"
           #:unpack-path "go.goblog.app/app"
           #:build-flags #~(list "-tags=linux,sqlite_fts5")
           #:go go-1.19
           ))
    (native-inputs
     `(("go-github-com-lestrrat-go-file-rotatelogs" ,go-github-com-lestrrat-go-file-rotatelogs)))
    (inputs (list sqlite))
    (propagated-inputs `(("go-willnorris-com-go-webmention" ,go-willnorris-com-go-webmention)
                         ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
                         ("go-gopkg-in-ini-v1" ,go-gopkg-in-ini-v1)
                         ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
                         ("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-oauth2" ,go-golang-org-x-oauth2)
                         ("go-golang-org-x-image" ,go-golang-org-x-image)
                         ("go-golang-org-x-exp" ,go-golang-org-x-exp)
                         ("go-github-com-tdewolff-parse-v2" ,go-github-com-tdewolff-parse-v2)
                         ("go-github-com-subosito-gotenv" ,go-github-com-subosito-gotenv)
                         ("go-github-com-spf13-pflag" ,go-github-com-spf13-pflag)
                         ("go-github-com-spf13-jwalterweatherman" ,go-github-com-spf13-jwalterweatherman)
                         ("go-github-com-spf13-afero" ,go-github-com-spf13-afero)
                         ("go-github-com-snabb-diagio" ,go-github-com-snabb-diagio)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-pelletier-go-toml-v2" ,go-github-com-pelletier-go-toml-v2)
                         ("go-github-com-pelletier-go-toml" ,go-github-com-pelletier-go-toml)
                         ("go-github-com-modern-go-reflect2" ,go-github-com-modern-go-reflect2)
                         ("go-github-com-modern-go-concurrent" ,go-github-com-modern-go-concurrent)
                         ("go-github-com-mmcdole-goxpp" ,go-github-com-mmcdole-goxpp)
                         ("go-github-com-mitchellh-mapstructure" ,go-github-com-mitchellh-mapstructure)
                         ("go-github-com-magiconair-properties" ,go-github-com-magiconair-properties)
                         ("go-github-com-lestrrat-go-strftime" ,go-github-com-lestrrat-go-strftime)
                         ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
                         ("go-github-com-jonboulle-clockwork" ,go-github-com-jonboulle-clockwork)
                         ("go-github-com-hashicorp-hcl" ,go-github-com-hashicorp-hcl)
                         ("go-github-com-hashicorp-go-multierror" ,go-github-com-hashicorp-go-multierror)
                         ("go-github-com-hashicorp-errwrap" ,go-github-com-hashicorp-errwrap)
                         ("go-github-com-gorilla-securecookie" ,go-github-com-gorilla-securecookie)
                         ("go-github-com-gorilla-css" ,go-github-com-gorilla-css)
                         ("go-github-com-golang-protobuf" ,go-github-com-golang-protobuf)
                         ("go-github-com-golang-glog" ,go-github-com-golang-glog)
                         ("go-github-com-fsnotify-fsnotify" ,go-github-com-fsnotify-fsnotify)
                         ("go-github-com-felixge-httpsnoop" ,go-github-com-felixge-httpsnoop)
                         ("go-github-com-dustin-go-humanize" ,go-github-com-dustin-go-humanize)
                         ("go-github-com-dlclark-regexp2" ,go-github-com-dlclark-regexp2)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-github-com-cespare-xxhash-v2" ,go-github-com-cespare-xxhash-v2)
                         ("go-github-com-boombuler-barcode" ,go-github-com-boombuler-barcode)
                         ("go-github-com-aymerick-douceur" ,go-github-com-aymerick-douceur)
                         ("go-github-com-andybalholm-cascadia" ,go-github-com-andybalholm-cascadia)
                         ("go-willnorris-com-go-microformats" ,go-willnorris-com-go-microformats)
                         ;;("go---" ,go---)
                         ("go-nhooyr-io-websocket" ,go-nhooyr-io-websocket)
                         ("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-sync" ,go-golang-org-x-sync)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-yuin-goldmark-emoji" ,go-github-com-yuin-goldmark-emoji)
                         ;;("go---" ,go---)
                         ("go-github-com-yuin-goldmark" ,go-github-com-yuin-goldmark)
                         ("go-github-com-vcraescu-go-paginator" ,go-github-com-vcraescu-go-paginator)
                         ("go-github-com-traefik-yaegi" ,go-github-com-traefik-yaegi)
                         ("go-github-com-tomnomnom-linkheader" ,go-github-com-tomnomnom-linkheader)
                         ("go-github-com-tkrajina-gpxgo" ,go-github-com-tkrajina-gpxgo)
                         ;;("go---" ,go---)
                         ("go-github-com-tdewolff-minify-v2" ,go-github-com-tdewolff-minify-v2)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-spf13-viper" ,go-github-com-spf13-viper)
                         ("go-github-com-spf13-cast" ,go-github-com-spf13-cast)
                         ("go-github-com-snabb-sitemap" ,go-github-com-snabb-sitemap)
                         ("go-github-com-schollz-sqlite3dump" ,go-github-com-schollz-sqlite3dump)
                         ("go-github-com-samber-lo" ,go-github-com-samber-lo)
                         ("go-github-com-pquerna-otp" ,go-github-com-pquerna-otp)
                         ("go-github-com-posener-wstest" ,go-github-com-posener-wstest)
                         ("go-github-com-paulmach-go-geojson" ,go-github-com-paulmach-go-geojson)
                         ("go-github-com-mmcdole-gofeed" ,go-github-com-mmcdole-gofeed)
                         ("go-github-com-microcosm-cc-bluemonday" ,go-github-com-microcosm-cc-bluemonday)
                         ("go-github-com-mergestat-timediff" ,go-github-com-mergestat-timediff)
                         ("go-github-com-mattn-go-sqlite3" ,go-github-com-mattn-go-sqlite3)
                         ("go-github-com-lopezator-migrator" ,go-github-com-lopezator-migrator)
                         ("go-github-com-lestrrat-go-file-rotatelogs" ,go-github-com-lestrrat-go-file-rotatelogs)
                         ("go-github-com-klauspost-compress" ,go-github-com-klauspost-compress)
                         ("go-github-com-kaorimatz-go-opml" ,go-github-com-kaorimatz-go-opml)
                         ("go-github-com-justinas-alice" ,go-github-com-justinas-alice)
                         ("go-github-com-jlelse-feeds" ,go-github-com-jlelse-feeds)
                         ;;("go---" ,go---)
                         ("go-github-com-jlaffaye-ftp" ,go-github-com-jlaffaye-ftp)
                         ("go-github-com-hacdias-indieauth-v2" ,go-github-com-hacdias-indieauth-v2)
                         ("go-github-com-gorilla-websocket" ,go-github-com-gorilla-websocket)
                         ("go-github-com-gorilla-sessions" ,go-github-com-gorilla-sessions)
                         ("go-github-com-gorilla-handlers" ,go-github-com-gorilla-handlers)
                         ("go-github-com-google-uuid" ,go-github-com-google-uuid)
                         ("go-github-com-go-telegram-bot-api-telegram-bot-api-v5" ,go-github-com-go-telegram-bot-api-telegram-bot-api-v5)
                         ("go-github-com-go-fed-httpsig" ,go-github-com-go-fed-httpsig)
                         ("go-github-com-go-chi-chi-v5" ,go-github-com-go-chi-chi-v5)
                         ("go-github-com-emersion-go-smtp" ,go-github-com-emersion-go-smtp)
                         ("go-github-com-emersion-go-sasl" ,go-github-com-emersion-go-sasl)
                         ("go-github-com-elnormous-contenttype" ,go-github-com-elnormous-contenttype)
                         ("go-github-com-dmulholl-mp3lib" ,go-github-com-dmulholl-mp3lib)
                         ("go-github-com-disintegration-imaging" ,go-github-com-disintegration-imaging)
                         ("go-github-com-dgraph-io-ristretto" ,go-github-com-dgraph-io-ristretto)
                         ("go-github-com-dchest-captcha" ,go-github-com-dchest-captcha)
                         ("go-github-com-cretz-bine" ,go-github-com-cretz-bine)
                         ;;("go---" ,go---)
                         ("go-github-com-carlmjohnson-requests" ,go-github-com-carlmjohnson-requests)
                         ("go-github-com-c2h5oh-datasize" ,go-github-com-c2h5oh-datasize)
                         ("go-github-com-araddon-dateparse" ,go-github-com-araddon-dateparse)
                         ("go-github-com-alecthomas-chroma-v2" ,go-github-com-alecthomas-chroma-v2)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)
                         ("go-git-jlel-se-jlelse-template-strings" ,go-git-jlel-se-jlelse-template-strings)
                         ("go-git-jlel-se-jlelse-goldmark-mark" ,go-git-jlel-se-jlelse-goldmark-mark)
                         ("go-git-jlel-se-jlelse-go-shutdowner" ,go-git-jlel-se-jlelse-go-shutdowner)
                         ("go-git-jlel-se-jlelse-go-geouri" ,go-git-jlel-se-jlelse-go-geouri)))
    (home-page "https://go.goblog.app/app")
    (synopsis "GoBlog")
    (description
     "How to install and run (and other useful information about) GoBlog is explained
in the @@url{https://docs.goblog.app/,docs}.")
    (license license:expat)))
