(define-module (guix-local packages  gde)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages video)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages glib)  
  #:use-module (gnu packages virtualization)  
  #:use-module (gnu packages iso-codes))

(define-public gnome-desktop-42
  (package
    (name "gnome-desktop-42")
    (version "42.0")
    (source
     (origin
      (method git-fetch)
      (uri
       (git-reference
        (url "https://gitlab.gnome.org/GNOME/gnome-desktop")
        (commit version)))
      (sha256
       (base32
        "06654hi5n6jyv6wlnd02hm2sbbhwijcirswgpcdnr7xrxmdc4d41"))))
    (build-system meson-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'patch-path
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((libc   (assoc-ref inputs "libc")))
               (substitute* "libgnome-desktop/gnome-languages.c"
                 (("\"locale\"")
                  (string-append "\"" libc "/bin/locale\"")))
               #t)))
         (add-before 'configure 'patch-bubblewrap
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "libgnome-desktop/gnome-desktop-thumbnail-script.c"
               (("\"bwrap\",")
                (string-append "\"" (which "bwrap") "\","))
               (("\"--ro-bind\", \"/usr\", \"/usr\",")
                (string-append "\"--ro-bind\", \""
                               (%store-directory)
                               "\", \""
                               (%store-directory)
                               "\","))
               (("\"--ro-bind\", \"/etc/ld.so.cache\", \"/etc/ld.so.cache\",") ""))
             #t))
         (add-before 'check 'pre-check
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Tests require a running X server and locales.
             (system "Xvfb :1 &")
             (setenv "DISPLAY" ":1")
             (setenv "XDG_CACHE_HOME" "/tmp/xdg-cache")
             (setenv "XDG_CONFIG_HOME" "/tmp")
             (setenv "GUIX_LOCPATH"
                     (search-input-directory inputs
                                             "lib/locale")))))))
    (native-inputs
     `(("glib:bin" ,glib "bin") ; for gdbus-codegen
       ("glibc-locales" ,glibc-locales) ; for tests
       ("gobject-introspection" ,gobject-introspection)
       ("itstool" ,itstool)
       ("intltool" ,intltool)
       ("pkg-config" ,pkg-config)
       ("xmllint" ,libxml2)
       ("xorg-server" ,xorg-server-for-tests)))
    (propagated-inputs
     ;; Required by gnome-desktop-3.0.pc.
     (list gsettings-desktop-schemas
           gtk+
           iso-codes
           libseccomp
           libx11
           xkeyboard-config))
    (inputs
     (list gdk-pixbuf
           glib
           gtk
           bubblewrap
           libxext
           libxkbfile
           libxrandr))
    (home-page "https://www.gnome.org/")
    (synopsis
     "Libgnome-desktop, gnome-about, and desktop-wide documents")
    (description
     "The libgnome-desktop library provides API shared by several applications
on the desktop, but that cannot live in the platform for various reasons.
There is no API or ABI guarantee, although we are doing our best to provide
stability.  Documentation for the API is available with gtk-doc.

The gnome-about program helps find which version of GNOME is installed.")
    ; Some bits under the LGPL.
    (license license:gpl2+)))

(define-public gtk-layer-shell-0.7
  (let
      ((commit "617744ccb51c6a30c5e63547182e75991b5c1d43")
       (version "0.7")
       (revision "0"))
  (package
    (name "gtk-layer-shell")
    (version (git-version version revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/wmww/gtk-layer-shell")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1njr4syc4g187w4w28srch3z6rbcfgl5h81kka3lar0pd67jlnx2"))))
    (build-system meson-build-system)
    (arguments `(#:configure-flags (list "-Dtests=true")))
    (native-inputs (list pkg-config gobject-introspection))
    (inputs (list wayland vala gtk+))
    (home-page "https://github.com/wmww/gtk-layer-shell")
    (synopsis "Library to create Wayland desktop components using the Layer
Shell protocol")
    (description "Layer Shell is a Wayland protocol for desktop shell
components, such as panels, notifications and wallpapers.  It can be used to
anchor windows to a corner or edge of the output, or stretch them across the
entire output.  It supports all Layer Shell features including popups and
popovers.")
    (license license:expat))))


(define-public gde-panel
  (package
   (name "gde-panel")
   (version "0.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/carbonOS/gde/gde-panel")
       (commit "7d91d108ce6ab0798a693b7b7b6c557a31ca3edf")
       (recursive? #t)))
     (sha256
      (base32 "0gf7inhlq5mk1h4yjlifp4gv6i3ipq1565l5zzsq9r5q08kbf0d2"))))
   (build-system meson-build-system)
   (native-inputs
    (list
     `(,glib "bin")
     vala
     `(,gtk+ "bin")
     desktop-file-utils
     gobject-introspection
     pkg-config))
   (inputs
    (list gsettings-desktop-schemas libhandy vala dbus  dbus-glib   libgudev
          adwaita-icon-theme
          pulseaudio  gobject-introspection  alsa-lib glib atk colord-gtk 
          libdazzle gtk+ libxml2 gnome-desktop-42 network-manager  gtk-layer-shell-0.7
          gtk wayfire     wlroots-for-wayfire  wayland     wf-config))
   (native-search-paths
    (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build a
fully functional DE based around wayfire.  Currently it has only a GTK-based
panel and background client.")
   (license license:expat)))

(define-public gde-dialogs
  (package
   (name "gde-dialogs")
   (version "0.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/carbonOS/gde/gde-dialogs")
       (commit "0206844bcb60fdd384259f01107d93d7c315bd90")))
     (sha256
      (base32 "0qyqrqghyfrwfwaq4chyay4hq4pbd0pcvwp8in50q1710dgilk85"))))
   (build-system meson-build-system)
   (native-inputs
    (list
     `(,glib "bin")
     vala
     gobject-introspection
     pkg-config))
   (inputs
    (list
     polkit-gnome
     polkit
     libhandy
     alsa-lib
     glib
     libdazzle
     libgudev
     gnome-desktop
     network-manager
     gtk-layer-shell
     wayfire
     wlroots-for-wayfire
     wayland
     wf-config))
   (native-search-paths
    (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build a
fully functional DE based around wayfire.  Currently it has only a GTK-based
panel and background client.")
   (license license:expat)))

(define-public gde-gnome-bridge
  (package
   (name "gde-gnome-bridge")
   (version "0.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/carbonOS/gde/gde-gnome-bridge")
       (commit "716a5263e5694b641c34b1714cf886083d3517db")))
     (sha256
      (base32 "1kyy3jxn94fbhdidrmrah2pzka9fhiwpkxzzyzb4m3ayl5v5291h"))))
   (build-system meson-build-system)
   (native-inputs
    (list
     `(,glib "bin")
     vala
     wayland
     gobject-introspection
     pkg-config))
   (inputs
    (list wayland
          glib
          gnome-desktop
         gtk-layer-shell))
   (native-search-paths
    (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build a
fully functional DE based around wayfire.  Currently it has only a GTK-based
panel and background client.")
   (license license:expat)))

(define-public gde-background
  (package
   (name "gde-background")
   (version "0.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/carbonOS/gde/gde-background")
       (commit "9549d3dbc15694e05ea3261c76d06ef3bd8f3c98")))
     (sha256
      (base32 "0jl0ws2hf072mg32wp5gmqnh787lfgn2qcij2j38gxjrfabab7df"))))
   (build-system meson-build-system)
   ;(arguments
   ; `(#:configure-flags '("-Dgvc=false")))
   (native-inputs
    (list
     `(,glib "bin")
     vala
     gobject-introspection
     pkg-config))
   (inputs
    (list
     geoclue
     polkit-gnome
     polkit
     libhandy
     alsa-lib
     glib
     libdazzle
     libgudev
     gnome-desktop
     network-manager
     gtk-layer-shell
     ;;gtkmm-3
     ;;wayfire
     ;;wlroots-for-wayfire
     wayland
     ;;wf-config
     ))
     
   (native-search-paths
    (package-native-search-paths wayfire))
   (search-paths  native-search-paths)
   (home-page "https://wayfire.org")
   (synopsis "GTK-based panel for wayfire")
   (description "WF-Shell contains various components needed to build
 a fully functional DE based around wayfire.  Currently it has only a
 GTK-based panel and background client.")
   (license license:expat)))
