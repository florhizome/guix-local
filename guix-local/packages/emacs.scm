;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021, 2022 Aleksandr Vityazev <avityazev@posteo.org>
;;; Copyright © 2021, 2022 florhizome <florhizome@posteo.net>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-local packages  emacs)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages image)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages pkg-config)
  ;;  #:use-module (rrr packages emacs)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

(define-public emacs-flo
  (let ((commit "20da50619fd5b65e6d0c18c48d10ab1ec9067b63")
        (revision "16")
        (version "29.0.50"))
    (package
      (inherit emacs-native-comp)
      (name "emacs-flo")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/emacs.git/")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0h3a7yyyj1kzadgjarcnsmy4dmbrqvlsq5fjfwjzf9iw5ll9nnxh"))))
      (arguments
       (substitute-keyword-arguments (package-arguments emacs-native-comp)
         ((#:configure-flags flags ''())
          #~(cons* "--with-pgtk"
                  "--with-gsettings"
                  "--with-cairo"
                  "--with-rsvg"
                  "--with-json"
                  "--with-xpm"
                  "--without-libsystemd"
                  "--with-native-compilation"
                  #$flags))
         ((#:phases phases)
          #~(modify-phases #$phases
             (replace 'restore-emacs-pdmp
               ;; restore the dump file that Emacs installs somewhere in
               ;; libexec/ to its original state
               (lambda* (#:key outputs target #:allow-other-keys)
                 (let* ((libexec (string-append (assoc-ref outputs "out")
                                                "/libexec"))
                        ;; each of these find-files should return one file
                        (pdmp (find-files libexec "^emacs.*\\.pdmp$"))
                        (pdmp-real (find-files libexec
                                               "^\\.emacs.*\\.pdmp-real$")))
                   (for-each (lambda (wrapper real)
                               (delete-file wrapper)
                               (rename-file real wrapper))
                             pdmp pdmp-real))))))))
      (inputs
       `(("libwebp" ,libwebp)
         ("webkitgtk" ,webkitgtk-with-libsoup2)
         ,@(package-inputs emacs-native-comp))))))

(define-public emacs-next-wo-nc
  (let ((commit "0804b61f824811d28aea4df0ce018b0b0ea2e878")
        (revision "10")
        (version "29.0.50"))
    (package
      (inherit emacs-next)
      (name "emacs-next-wo-nc")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/emacs.git/")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "1k1d7zl91hxpi2najjvazx5sckjw4z9wm9hwbgfcbx5k63n47b1v"))))
      (arguments
       (substitute-keyword-arguments (package-arguments emacs-next)
         ((#:phases phases)
          `(modify-phases ,phases
             (replace 'restore-emacs-pdmp
               ;; restore the dump file that Emacs installs somewhere in
               ;; libexec/ to its original state
               (lambda* (#:key outputs target #:allow-other-keys)
                 (let* ((libexec (string-append (assoc-ref outputs "out")
                                                "/libexec"))
                        ;; each of these find-files should return one file
                        (pdmp (find-files libexec "^emacs.*\\.pdmp$"))
                        (pdmp-real (find-files libexec
                                               "^\\.emacs.*\\.pdmp-real$")))
                   (for-each (lambda (wrapper real)
                               (delete-file wrapper)
                               (rename-file real wrapper))
                             pdmp pdmp-real))))))))
      (inputs
       `(("libwebp" ,libwebp)
         ,@(package-inputs emacs-next))))))

