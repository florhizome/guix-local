(define-module (guix-local packages  lxqt-kcm)
  #:use-module (gnu)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system python)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils) 
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages lxqt)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages kde-frameworks))

(define-public lxqt-kcm-integration
  (let ((commit "f5995a00cdddcbfb37a35a056e4fa7fceeee9dea")
        (version "0.0.1")
        (revision "0"))
    (package
      (name "lxqt-kcm-integration")
      (version (git-version version revision commit))
      (source
       (origin (method git-fetch)
               (uri (git-reference (url "https://github.com/lxqt/lxqt-kcm-integration")
                                   (commit commit)))
               
               (sha256 
                (base32 "0h6l77ld012j5srrjg35wpfm34xddqqy6h4vkjp7w18m1j9i6c57"))))
      
      (arguments
       `(#:tests? #f
                  #:phases
                  (modify-phases %standard-phases
                    (add-after 'unpack 'patch-target
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let* ((out (assoc-ref outputs "out")))
                          (substitute* "./applications/CMakeLists.txt"
                            (("DESTINATION \"/usr/share/applications")
                             (string-append "DESTINATION \"" out "/share/applications")))
                          #t))))))
      (native-inputs `(("pkg-config" ,pkg-config)
                       ("extra-cmake-modules" ,extra-cmake-modules)
                       ("lxqt-build-tools" ,lxqt-build-tools)))
      (inputs `(("kcmutils" ,kcmutils)
                ;;plasma-workspace" ,plasma-workspace)
                ("kglobalaccel" ,kglobalaccel)))      
      (build-system qt-build-system)
      (native-search-paths
       (list
        (search-path-specification
         (variable "QT_PLUGIN_PATH")
         (files '("/lib/qt5/plugins")))))
      (synopsis "")
      (description "")
      (home-page "https://gitlab.com/kwinft")
      (license license:expat))))
