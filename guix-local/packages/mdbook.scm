(define-module (guix-local packages  mdbook)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages web)
  #:use-module (gnu packages tls)  
  #:use-module (gnu packages pkg-config)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guixrus packages common rust)
  )


(define-public rust-select-0.5
  (package
    (name "rust-select")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "select" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dvnb12fqczq0mqgyh7pafkhngv8478x0y3sxy5ngj7w1bwn3q4f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-html5ever" ,rust-html5ever-0.25)
         ("rust-markup5ever-rcdom" ,rust-markup5ever-rcdom-0.1))))
    (home-page "https://github.com/utkarshkukreti/select.rs")
    (synopsis
      "A library to extract useful data from HTML documents, suitable for web scraping.")
    (description
      "This package provides a library to extract useful data from HTML documents,
suitable for web scraping.")
    (license license:expat)))

(define-public rust-topological-sort-0.1
  (package
    (name "rust-topological-sort")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "topological-sort" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g478pajmv620cl2hpppacdlrhdrmqrmcvvq76abkcd4vr17yz5a"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/gifnksm/topological-sort-rs")
    (synopsis "Performs topological sorting.")
    (description "Performs topological sorting.")
    (license (list license:expat license:asl2.0))))

(define-public rust-opener-0.5
  (package
    (name "rust-opener")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opener" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lkrn4fv1h4m8gmp7ll6x7vjvb6kls2ngwa5cgsh2ix5fb6yp8sf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-0.2) ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/Seeker14491/opener")
    (synopsis "Open a file or link using the system default program.")
    (description "Open a file or link using the system default program.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tungstenite-0.14
  (package
    (name "rust-tungstenite")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1db5j4792b197v95y7hr8j9n0qn75rdc1xcd19mjfbmxi9axicm0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rustls" ,rust-rustls-0.19)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.5)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-url" ,rust-url-2)
         ("rust-utf-8" ,rust-utf-8-0.7)
         ("rust-webpki" ,rust-webpki-0.21))))
    (home-page "https://github.com/snapview/tungstenite-rs")
    (synopsis "Lightweight stream-based WebSocket implementation")
    (description "Lightweight stream-based WebSocket implementation")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-tungstenite-0.15
  (package
    (name "rust-tokio-tungstenite")
    (version "0.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1n6b8qgjgnxl5g1fwmsfcgmshpv814yhqja56nc9h75gbkwf67ai"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-rustls" ,rust-rustls-0.19)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tungstenite" ,rust-tungstenite-0.14)
         ("rust-webpki" ,rust-webpki-0.21)
         ("rust-webpki-roots" ,rust-webpki-roots-0.21))))
    (home-page "https://github.com/snapview/tokio-tungstenite")
    (synopsis
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket implementation")
    (description
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket
implementation")
    (license license:expat)))

(define-public rust-warp-0.3
  (package
    (name "rust-warp")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "warp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zjqbg2j1fdpqq74bi80hmvyakf1f771d7vrmkqvg90lj4g4xvrw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-log" ,rust-log-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-multipart" ,rust-multipart-0.18)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.15)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/seanmonstar/warp")
    (synopsis "serve the web at warp speeds")
    (description "serve the web at warp speeds")
    (license license:expat)))

(define-public rust-simple-asn1-0.4
  (package
    (name "rust-simple-asn1")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "simple_asn1" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jxy9as8nj65c2n27j843g4fpb95x4fjz31w6qx63q3wwlys2b39"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-num-bigint" ,rust-num-bigint-0.2)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/acw/simple_asn1")
    (synopsis "A simple DER/ASN.1 encoding/decoding library.")
    (description
      "This package provides a simple DER/ASN.1 encoding/decoding library.")
    (license license:isc)))

(define-public rust-pem-0.8
  (package
    (name "rust-pem")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pem" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sqkzp87j6s79sjxk4n913gcmalzb2fdc75l832d0j7a3z9cnmpx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/jcreekmore/pem-rs.git")
    (synopsis "Parse and encode PEM-encoded data.")
    (description "Parse and encode PEM-encoded data.")
    (license license:expat)))

(define-public rust-jsonwebtoken-7
  (package
    (name "rust-jsonwebtoken")
    (version "7.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jsonwebtoken" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ciz205wcjcn7n6i871zz5xlbzk863b0ybgiqi7li9ipwhawraxg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.12)
         ("rust-pem" ,rust-pem-0.8)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-simple-asn1" ,rust-simple-asn1-0.4))))
    (home-page "https://github.com/Keats/jsonwebtoken")
    (synopsis "Create and decode JWTs in a strongly typed way.")
    (description "Create and decode JWTs in a strongly typed way.")
    (license license:expat)))

(define-public rust-async-lock-2
  (package
    (name "rust-async-lock")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-lock" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dkps300paviz3npwaq38n0sc92fv55b20mr3fizp0hp34fifyp9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-event-listener" ,rust-event-listener-2))))
    (home-page "https://github.com/smol-rs/async-lock")
    (synopsis "Async synchronization primitives")
    (description "Async synchronization primitives")
    (license (list license:asl2.0 license:expat))))

(define-public rust-async-session-3
  (package
    (name "rust-async-session")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-session" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c76vazdlcs2rsxq8gd8a6wnb913vxhnfx1hyfmfpqml4gjlrnh7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-lock" ,rust-async-lock-2)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-blake3" ,rust-blake3-0.3)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-log" ,rust-log-0.4)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha2" ,rust-sha2-0.9))))
    (home-page "https://github.com/http-rs/async-session")
    (synopsis "Async session support with pluggable middleware")
    (description "Async session support with pluggable middleware")
    (license (list license:expat license:asl2.0))))

(define-public rust-salvo-extra-0.16
  (package
    (name "rust-salvo-extra")
    (version "0.16.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "salvo_extra" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "023wagm5mpkp1jnpggllbddqigsy5h4qnw2lk8m3j25fj61fl3iy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-async-session" ,rust-async-session-3)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-cookie" ,rust-cookie-0.16)
         ("rust-csrf" ,rust-csrf-0.4)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-hkdf" ,rust-hkdf-0.12)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-hyper-rustls" ,rust-hyper-rustls-0.23)
         ("rust-jsonwebtoken" ,rust-jsonwebtoken-7)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-salvo-core" ,rust-salvo-core-0.16)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-sha2" ,rust-sha2-0.10)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.16)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://salvo.rs")
    (synopsis
      "Salvo is a powerful and simplest web server framework in Rust world.
")
    (description
      "Salvo is a powerful and simplest web server framework in Rust world.")
    (license (list license:expat license:asl2.0))))

(define-public rust-textnonce-1
  (package
    (name "rust-textnonce")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "textnonce" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10v653sz0305dlzdqh6wh795hxypk24s21iiqcfyv16p1kbzhhvp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.12)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/mikedilger/textnonce")
    (synopsis "Text based random nonce generator")
    (description "Text based random nonce generator")
    (license (list license:expat license:asl2.0))))

(define-public rust-proc-quote-impl-0.3
  (package
    (name "rust-proc-quote-impl")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-quote-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "184ax14pyazv5g6yma60ls7x4hd5q6wah1kf677xng06idifrcvz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1))))
    (home-page "https://github.com/Goncalerta/proc-quote")
    (synopsis "A procedural macro implementation of quote!.")
    (description
      "This package provides a procedural macro implementation of quote!.")
    (license (list license:expat license:asl2.0))))

(define-public rust-proc-quote-0.4
  (package
    (name "rust-proc-quote")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-quote" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0051nax31x1yzr1imbp200l2gpz6pqcmlcna099r33773lbap12y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-quote-impl" ,rust-proc-quote-impl-0.3)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/Goncalerta/proc-quote")
    (synopsis "A procedural macro implementation of quote!.")
    (description
      "This package provides a procedural macro implementation of quote!.")
    (license (list license:expat license:asl2.0))))

(define-public rust-salvo-macros-0.16
  (package
    (name "rust-salvo-macros")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "salvo_macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hdlzvcv2vvbr60w1kmfr9bx8glx4xs9g0ry1pwa7yf7ig987z90"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-quote" ,rust-proc-quote-0.4)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://salvo.rs")
    (synopsis "salvo proc macros")
    (description "salvo proc macros")
    (license (list license:expat license:asl2.0))))

(define-public rust-salvo-core-0.16
  (package
    (name "rust-salvo-core")
    (version "0.16.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "salvo_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01dazprfzmjmvwgcrvqxjd12hgwwlk71mskwyl4cj2y2gm5p80bv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-cookie" ,rust-cookie-0.16)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-fastrand" ,rust-fastrand-1)
         ("rust-form-urlencoded" ,rust-form-urlencoded-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-multer" ,rust-multer-2)
         ("rust-multimap" ,rust-multimap-0.8)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-regex" ,rust-regex-1)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-0.2)
         ("rust-salvo-macros" ,rust-salvo-macros-0.16)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tempdir" ,rust-tempdir-0.3)
         ("rust-textnonce" ,rust-textnonce-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://salvo.rs")
    (synopsis
      "Salvo is a powerful and simplest web server framework in Rust world.
")
    (description
      "Salvo is a powerful and simplest web server framework in Rust world.")
    (license (list license:expat license:asl2.0))))

(define-public rust-salvo-0.16
  (package
    (name "rust-salvo")
    (version "0.16.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "salvo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jw9h9aac4ms9shvssc8mw53q9842f5bfqv1a8aqkpcyd2j23n4b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-salvo-core" ,rust-salvo-core-0.16)
         ("rust-salvo-extra" ,rust-salvo-extra-0.16))))
    (home-page "https://salvo.rs")
    (synopsis
      "Salvo is a powerful and simplest web server framework in Rust world.
")
    (description
      "Salvo is a powerful and simplest web server framework in Rust world.")
    (license (list license:expat license:asl2.0))))

(define-public rust-globset-0.4
  (package
    (name "rust-globset")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "globset" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gdzphnjjc0wdaawsq3n1nnypv9ja4prhca2n66hcahay2gksihh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-bstr" ,rust-bstr-0.2)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page
      "https://github.com/BurntSushi/ripgrep/tree/master/crates/globset")
    (synopsis
      "Cross platform single glob and glob set matching. Glob set matching is the
process of matching one or more glob patterns against a single candidate path
simultaneously, and returning all of the globs that matched.
")
    (description
      "Cross platform single glob and glob set matching.  Glob set matching is the
process of matching one or more glob patterns against a single candidate path
simultaneously, and returning all of the globs that matched.")
    (license (list license:unlicense license:expat))))

(define-public rust-rust-embed-utils-7
  (package
    (name "rust-rust-embed-utils")
    (version "7.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed-utils" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ab02sv9zlinf6gacmac0zm9iv7q9kcyrx013mx4icfbmyiyqvvm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-globset" ,rust-globset-0.4)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis "Utilities for rust-embed")
    (description "Utilities for rust-embed")
    (license license:expat)))

(define-public rust-rust-embed-impl-6
  (package
    (name "rust-rust-embed-impl")
    (version "6.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c6xgqdckydg3nx8c6zxz8cs157pczwq7s3bpir0rgx29gi67rwl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rust-embed-utils" ,rust-rust-embed-utils-7)
         ("rust-shellexpand" ,rust-shellexpand-2)
         ("rust-syn" ,rust-syn-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis
      "Rust Custom Derive Macro which loads files into the rust binary at compile time during release and loads the file from the fs during dev")
    (description
      "Rust Custom Derive Macro which loads files into the rust binary at compile time
during release and loads the file from the fs during dev")
    (license license:expat)))

(define-public rust-tungstenite-0.17
  (package
    (name "rust-tungstenite")
    (version "0.17.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1x848392ihy5mh098sns0lcmb5rdwkxpmdcfya108mz783m2ssnr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.6)
         ("rust-sha-1" ,rust-sha-1-0.10)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-url" ,rust-url-2)
         ("rust-utf-8" ,rust-utf-8-0.7)
         ("rust-webpki" ,rust-webpki-0.22)
         ("rust-webpki-roots" ,rust-webpki-roots-0.22))))
    (home-page "https://github.com/snapview/tungstenite-rs")
    (synopsis "Lightweight stream-based WebSocket implementation")
    (description "Lightweight stream-based WebSocket implementation")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-tungstenite-0.17
  (package
    (name "rust-tokio-tungstenite")
    (version "0.17.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1bi1z1l8392v20mg24gryw5jrm0166wxa155z138qma958is3k86"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.6)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-tungstenite" ,rust-tungstenite-0.17)
         ("rust-webpki" ,rust-webpki-0.22)
         ("rust-webpki-roots" ,rust-webpki-roots-0.22))))
    (home-page "https://github.com/snapview/tokio-tungstenite")
    (synopsis
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket implementation")
    (description
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket
implementation")
    (license license:expat)))

(define-public rust-tokio-metrics-0.1
  (package
    (name "rust-tokio-metrics")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-metrics" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pawfs6gsgv0k950mz84fcf0vhsfk1am4bg2hhb1flwv0sh8bddw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis "Runtime and task level metrics for Tokio applications.
")
    (description "Runtime and task level metrics for Tokio applications.")
    (license license:expat)))

(define-public rust-futures-codec-0.4
  (package
    (name "rust-futures-codec")
    (version "0.4.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "futures-codec" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0nzadpxhdxdlnlk2f0gfn0qbifqc3pbnzm10v4z04x8ciczxcm6f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-bytes" ,rust-bytes-0.5)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-pin-project" ,rust-pin-project-0.4)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-cbor" ,rust-serde-cbor-0.11)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/matthunz/futures-codec")
    (synopsis "Utilities for encoding and decoding frames using `async/await`")
    (description
     "Utilities for encoding and decoding frames using `async/await`")
    (license license:expat)))

(define-public rust-sse-codec-0.3
  (package
    (name "rust-sse-codec")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sse-codec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nh8b1y2k5lsvcva15da4by935bavirfpavs0d54pi2h2f0rz9c4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-bytes" ,rust-bytes-0.5)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-codec" ,rust-futures-codec-0.4)
         ("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/goto-bus-stop/sse-codec")
    (synopsis "async Server-Sent Events protocol encoder/decoder")
    (description "async Server-Sent Events protocol encoder/decoder")
    (license license:mpl2.0)))

(define-public rust-regex-syntax-0.6
  (package
    (name "rust-regex-syntax")
    (version "0.6.26")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r6vplrklxq7yx7x4zqf04apr699swbsn6ipv8bk82nwqngdxcs9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/rust-lang/regex")
    (synopsis "A regular expression parser.")
    (description "This package provides a regular expression parser.")
    (license (list license:expat license:asl2.0))))

(define-public rust-regex-1
  (package
    (name "rust-regex")
    (version "1.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wczbykw6fas7359j9lhkkv13dplhiphzrf2ii6dmg5xjiyi4gyq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex-syntax" ,rust-regex-syntax-0.6))))
    (home-page "https://github.com/rust-lang/regex")
    (synopsis
      "An implementation of regular expressions for Rust. This implementation uses
finite automata and guarantees linear time matching on all inputs.
")
    (description
      "An implementation of regular expressions for Rust.  This implementation uses
finite automata and guarantees linear time matching on all inputs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-crc16-0.4
  (package
    (name "rust-crc16")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crc16" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zzwb5iv51wnh96532cxkk4aa8ys47rhzrjy98wqcys25ks8k01k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/blackbeam/rust-crc16")
    (synopsis "A CRC16 implementation")
    (description "This package provides a CRC16 implementation")
    (license license:expat)))

(define-public rust-redis-0.21
  (package
    (name "rust-redis")
    (version "0.21.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redis" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lh88wg4q4d3v3fc5r7hx25giaygm506xqd0aq404nkziprvb00s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arc-swap" ,rust-arc-swap-1)
         ("rust-async-native-tls" ,rust-async-native-tls-0.3)
         ("rust-async-std" ,rust-async-std-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-combine" ,rust-combine-4)
         ("rust-crc16" ,rust-crc16-0.4)
         ("rust-dtoa" ,rust-dtoa-0.4)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-itoa" ,rust-itoa-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-r2d2" ,rust-r2d2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/mitsuhiko/redis-rs")
    (synopsis "Redis driver for Rust.")
    (description "Redis driver for Rust.")
    (license license:bsd-3)))

(define-public rust-yasna-0.5
  (package
    (name "rust-yasna")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yasna" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0k1gk11hq4rwlppv9f50bz8bnmgr73r66idpp7rybly96si38v9l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-vec" ,rust-bit-vec-0.6)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/qnighy/yasna.rs")
    (synopsis "ASN.1 library for Rust")
    (description "ASN.1 library for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-rcgen-0.9
  (package
    (name "rust-rcgen")
    (version "0.9.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rcgen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ppwfl9g504x2qwk7m7mag8c3l70w9mcfha93013nlzqdlw2vynp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pem" ,rust-pem-1)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-time" ,rust-time-0.3)
         ("rust-x509-parser" ,rust-x509-parser-0.13)
         ("rust-yasna" ,rust-yasna-0.5)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/est31/rcgen")
    (synopsis "Rust X.509 certificate generator")
    (description "Rust X.509 certificate generator")
    (license (list license:expat license:asl2.0))))

(define-public rust-priority-queue-1
  (package
    (name "rust-priority-queue")
    (version "1.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "priority-queue" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07kw6j62738jaibdhvhgkfvmdmvjkagvwx5icg8x5xgmjdsdx76y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/garro95/priority-queue")
    (synopsis
      "A Priority Queue implemented as a heap with a function to efficiently change the priority of an item.")
    (description
      "This package provides a Priority Queue implemented as a heap with a function to
efficiently change the priority of an item.")
    (license (list license:lgpl3 license:mpl2.0))))

(define-public rust-proc-macro-crate-1
  (package
    (name "rust-proc-macro-crate")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-crate" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06pi2jqncn1kscwsp7zm0p04iki3vl70n99j0d2dxx2bj774fzg1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1) ("rust-toml" ,rust-toml-0.5))))
    (home-page "https://github.com/bkchr/proc-macro-crate")
    (synopsis "Replacement for crate (macro_rules keyword) in proc-macros
")
    (description "Replacement for crate (macro_rules keyword) in proc-macros")
    (license (list license:asl2.0 license:expat))))

(define-public rust-poem-derive-1
  (package
    (name "rust-poem-derive")
    (version "1.3.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "poem-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dpvlb0sgix1h8nj49icj1cyzgj8mrk3qm6wmlv0zp29ywg4xmzy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/poem-web/poem")
    (synopsis "Macros for poem")
    (description "Macros for poem")
    (license (list license:expat license:asl2.0))))

(define-public rust-opentelemetry-semantic-conventions-0.9
  (package
    (name "rust-opentelemetry-semantic-conventions")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-semantic-conventions" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gb21vchkkhd11nzggnhlmm9pf5ijj8jzzngn8j24h9dhdfw6p4q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opentelemetry" ,rust-opentelemetry-0.17))))
    (home-page
      "https://github.com/open-telemetry/opentelemetry-rust/tree/main/opentelemetry-semantic-conventions")
    (synopsis "Semantic conventions for OpenTelemetry")
    (description "Semantic conventions for OpenTelemetry")
    (license license:asl2.0)))

(define-public rust-protobuf-codegen-2
  (package
    (name "rust-protobuf-codegen")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01ypnn1194br42c5ign40s4v2irw3zypv6j38c1n4blgghmn7hdf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e. g. from `build.rs`) and `protoc-gen-rust` binary.
")
    (description
      "Code generator for rust-protobuf.

Includes a library to invoke programmatically (e.  g.  from `build.rs`) and
`protoc-gen-rust` binary.")
    (license license:expat)))

(define-public rust-protobuf-codegen-pure-2
  (package
    (name "rust-protobuf-codegen-pure")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen-pure" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1iicacpccn3bg65pm3ylvjndf35pplb8l23bg461jmcfn7yj50cz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen" ,rust-protobuf-codegen-2))))
    (home-page
      "https://github.com/stepancheg/rust-protobuf/tree/master/protobuf-codegen-pure/")
    (synopsis
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP
")
    (description
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15jgq6nkhysicmnb2a998aiqan8jr4rd46hdsc10kkcffcc6szng"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license license:expat)))

(define-public rust-procfs-0.12
  (package
    (name "rust-procfs")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "procfs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "014i9pkhhvcl4q6hwkbnflgq5ssn2ybrlxbp6s5dkqilk5mn0h89"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/eminence/procfs")
    (synopsis "Interface to the linux procfs pseudo-filesystem")
    (description "Interface to the linux procfs pseudo-filesystem")
    (license (list license:expat license:asl2.0))))

(define-public rust-prometheus-0.13
  (package
    (name "rust-prometheus")
    (version "0.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prometheus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0f9wipskgld8x94crgw5s1jj41sbdqbrz0w9qyj5wr3dza6ykb6g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-procfs" ,rust-procfs-0.12)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen-pure" ,rust-protobuf-codegen-pure-2)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/tikv/rust-prometheus")
    (synopsis "Prometheus instrumentation library for Rust applications.")
    (description "Prometheus instrumentation library for Rust applications.")
    (license license:asl2.0)))

(define-public rust-opentelemetry-prometheus-0.10
  (package
    (name "rust-opentelemetry-prometheus")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-prometheus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ijab7q17lfznp2l152si4sd48a7xbdfrz6kw0nf3sww8xz9fa4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opentelemetry" ,rust-opentelemetry-0.17)
         ("rust-prometheus" ,rust-prometheus-0.13)
         ("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis "Prometheus exporter for OpenTelemetry")
    (description "Prometheus exporter for OpenTelemetry")
    (license license:asl2.0)))

(define-public rust-sluice-0.5
  (package
    (name "rust-sluice")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sluice" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1d9ywr5039ibgaby8sc72f8fs5lpp8j5y6p3npya4jplxz000x3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-channel" ,rust-async-channel-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3))))
    (home-page "https://github.com/sagebind/sluice")
    (synopsis
      "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (description
      "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (license license:expat)))

(define-public rust-num-enum-derive-0.5
  (package
    (name "rust-num-enum-derive")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kj6b8f2fx8prlcl6y1k97668s5aiia4f9gjlk0nmpak3rj9h11v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Internal implementation details for ::num_enum (Procedural macros to make inter-operation between primitives and enums easier)")
    (description
      "Internal implementation details for ::num_enum (Procedural macros to make
inter-operation between primitives and enums easier)")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-num-enum-0.5
  (package
    (name "rust-num-enum")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1j8rq7i4xnbzy72z82k41469xlj1bmn4ixagd9wlbvv2ark9alyg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-enum-derive" ,rust-num-enum-derive-0.5))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (description
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-rustls-ffi-0.8
  (package
    (name "rust-rustls-ffi")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls-ffi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06kqrvm1d5ps9pml26zdd2hm8hh20j6svwvqibpnx7m5rh3jg9cx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-enum" ,rust-num-enum-0.5)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-0.2)
         ("rust-sct" ,rust-sct-0.7)
         ("rust-webpki" ,rust-webpki-0.22))))
    (home-page "https://github.com/rustls/rustls-ffi")
    (synopsis "C-to-rustls bindings")
    (description "C-to-rustls bindings")
    (license (list license:asl2.0 license:isc license:expat))))

(define-public rust-curl-sys-0.4
  (package
    (name "rust-curl-sys")
    (version "0.4.55+curl-7.83.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qi77rzdv4bbl1600akdpqxbk62w1q5kzpb15qy5iv38fg3lwwr3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libnghttp2-sys" ,rust-libnghttp2-sys-0.1)
         ("rust-libz-sys" ,rust-libz-sys-1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-rustls-ffi" ,rust-rustls-ffi-0.8)
         ("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Native bindings to the libcurl library")
    (description "Native bindings to the libcurl library")
    (license license:expat)))

(define-public rust-curl-0.4
  (package
    (name "rust-curl")
    (version "0.4.43")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07v5s3qafyl9gnnlzbddgg5fzy41gncy00ahbbv46nr0xyp5bn1p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-curl-sys" ,rust-curl-sys-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-openssl-probe" ,rust-openssl-probe-0.1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-schannel" ,rust-schannel-0.1)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Rust bindings to libcurl for making HTTP requests")
    (description "Rust bindings to libcurl for making HTTP requests")
    (license license:expat)))

(define-public rust-castaway-0.1
  (package
    (name "rust-castaway")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "castaway" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xhspwy477qy5yg9c3jp713asxckjpx0vfrmz5l7r5zg7naqysd2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/sagebind/castaway")
    (synopsis
      "Safe, zero-cost downcasting for limited compile-time specialization.")
    (description
      "Safe, zero-cost downcasting for limited compile-time specialization.")
    (license license:expat)))

(define-public rust-isahc-1
  (package
    (name "rust-isahc")
    (version "1.7.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "isahc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1scfgyv3dpjbkqa9im25cd12cs6rbd8ygcaw67f3dx41sys08kik"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-channel" ,rust-async-channel-1)
         ("rust-castaway" ,rust-castaway-0.1)
         ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8)
         ("rust-curl" ,rust-curl-0.4)
         ("rust-curl-sys" ,rust-curl-sys-0.4)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-event-listener" ,rust-event-listener-2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-polling" ,rust-polling-2)
         ("rust-publicsuffix" ,rust-publicsuffix-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-sluice" ,rust-sluice-0.5)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-futures" ,rust-tracing-futures-0.2)
         ("rust-url" ,rust-url-2)
         ("rust-waker-fn" ,rust-waker-fn-1))))
    (home-page "https://github.com/sagebind/isahc")
    (synopsis "The practical HTTP client that is fun to use.")
    (description "The practical HTTP client that is fun to use.")
    (license license:expat)))

(define-public rust-opentelemetry-http-0.6
  (package
    (name "rust-opentelemetry-http")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y9khrw8bc8jdjyhfx2whllkl7ywxrsrjvmygbsjh7p61qa4i424"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-isahc" ,rust-isahc-1)
         ("rust-opentelemetry" ,rust-opentelemetry-0.17)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-surf" ,rust-surf-2))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis
      "Helper implementations for exchange of traces and metrics over HTTP")
    (description
      "Helper implementations for exchange of traces and metrics over HTTP")
    (license license:asl2.0)))

(define-public rust-opentelemetry-0.17
  (package
    (name "rust-opentelemetry")
    (version "0.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1f5c04yl784bwzksl66q6vjp0fjk7dnn9ms9iksgs4xg0acfh1b1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-std" ,rust-async-std-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
         ("rust-dashmap" ,rust-dashmap-4)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-executor" ,rust-futures-executor-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis "A metrics collection and distributed tracing framework")
    (description
      "This package provides a metrics collection and distributed tracing framework")
    (license license:asl2.0)))

(define-public rust-hyper-rustls-0.23
  (package
    (name "rust-hyper-rustls")
    (version "0.23.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper-rustls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b2wi75qrmwfpw3pqwcg1xjndl4z0aris15296wf7i8d5v04hz6q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-log" ,rust-log-0.4)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.6)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-webpki-roots" ,rust-webpki-roots-0.22))))
    (home-page "https://github.com/ctz/hyper-rustls")
    (synopsis "Rustls+hyper integration for pure rust HTTPS")
    (description "Rustls+hyper integration for pure rust HTTPS")
    (license (list license:asl2.0 license:isc license:expat))))

(define-public rust-httparse-1
  (package
    (name "rust-httparse")
    (version "1.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "httparse" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0k60q1hx96cvmjn6k3yjkff87fz0ga2a4z0g9ss8a9x5nndy4v29"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/seanmonstar/httparse")
    (synopsis "A tiny, safe, speedy, zero-copy HTTP/1.x parser.")
    (description
      "This package provides a tiny, safe, speedy, zero-copy HTTP/1.x parser.")
    (license (list license:expat license:asl2.0))))

(define-public rust-h2-0.3
  (package
    (name "rust-h2")
    (version "0.3.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "h2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0msasdyv0n7avs5i1csjrs0rvdsp4k5z3fwl8rd53jbzcdnjra1p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/hyperium/h2")
    (synopsis "An HTTP/2 client and server")
    (description "An HTTP/2 client and server")
    (license license:expat)))

(define-public rust-hyper-0.14
  (package
    (name "rust-hyper")
    (version "0.14.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyper" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1whf3qqvxcpbp1dfpzy6lhl4yjl1wfcby2nrc4417gpy1alf0smj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-h2" ,rust-h2-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-want" ,rust-want-0.3))))
    (home-page "https://hyper.rs")
    (synopsis "A fast and correct HTTP library.")
    (description "This package provides a fast and correct HTTP library.")
    (license license:expat)))

(define-public rust-headers-0.3
  (package
    (name "rust-headers")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "headers" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pd5i8aywnmx9q7wfzn9bs0jq2fm5rmk0kdhcnmy1qcbg3jpizsc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-headers-core" ,rust-headers-core-0.2)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-sha-1" ,rust-sha-1-0.10))))
    (home-page "https://hyper.rs")
    (synopsis "typed HTTP headers")
    (description "typed HTTP headers")
    (license license:expat)))

(define-public rust-fluent-pseudo-0.3
  (package
    (name "rust-fluent-pseudo")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-pseudo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0byldssmzjdmynbh1yvdrxcj0xmhqznlmmgwnh8a1fhla7wn5vgx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-regex" ,rust-regex-1))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "Pseudolocalization transformation API for use with Project Fluent API.
")
    (description
      "Pseudolocalization transformation API for use with Project Fluent API.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-self-cell-0.10
  (package
    (name "rust-self-cell")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "self_cell" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1by8h3axgpbiph5nbq80z6a41hd4cqlqc66hgnngs57y42j6by8y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustversion" ,rust-rustversion-1))))
    (home-page "https://github.com/Voultapher/self_cell")
    (synopsis
      "Safe-to-use proc-macro-free self-referential structs in stable Rust.")
    (description
      "Safe-to-use proc-macro-free self-referential structs in stable Rust.")
    (license license:asl2.0)))

(define-public rust-intl-pluralrules-7
  (package
    (name "rust-intl-pluralrules")
    (version "7.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intl_pluralrules" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ksy3hxqs8if3nbvcin0a8390lpkzbk2br1brik70z96hj1ri3xi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinystr" ,rust-tinystr-0.3)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "https://github.com/zbraniecki/pluralrules")
    (synopsis "Unicode Plural Rules categorizer for numeric input.")
    (description "Unicode Plural Rules categorizer for numeric input.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-type-map-0.4
  (package
    (name "rust-type-map")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "type-map" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ilsqq7pcl3k9ggxv2x5fbxxfd6x7ljsndrhc38jmjwnbr63dlxn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustc-hash" ,rust-rustc-hash-1))))
    (home-page "https://github.com/kardeiz/type-map")
    (synopsis "Provides a typemap container with FxHashMap")
    (description "This package provides a typemap container with FxHashMap")
    (license (list license:expat license:asl2.0))))

(define-public rust-intl-memoizer-0.5
  (package
    (name "rust-intl-memoizer")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intl-memoizer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vx6cji8ifw77zrgipwmvy1i3v43dcm58hwjxpb1h29i98z46463"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-type-map" ,rust-type-map-0.4)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A memoizer specifically tailored for storing lazy-initialized
intl formatters.
")
    (description
      "This package provides a memoizer specifically tailored for storing
lazy-initialized intl formatters.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fluent-syntax-0.11
  (package
    (name "rust-fluent-syntax")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-syntax" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y6ac7z7sbv51nsa6km5z8rkjj4nvqk91vlghq1ck5c3cjbyvay0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "http://www.projectfluent.org")
    (synopsis "Parser/Serializer tools for Fluent Syntax. 
")
    (description "Parser/Serializer tools for Fluent Syntax. ")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unic-langid-macros-impl-0.9
  (package
    (name "rust-unic-langid-macros-impl")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-macros-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17xaknz6ixhg2cp8x174d54hdlv78akb2s0kw31p8xg2jzynyf99"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-macros-0.9
  (package
    (name "rust-unic-langid-macros")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rfn9pvbypvxyr8iyfx6dycafnn9ih9v8r3dhgr0b23yv3b81y8q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-tinystr" ,rust-tinystr-0.3)
         ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
         ("rust-unic-langid-macros-impl" ,rust-unic-langid-macros-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-tinystr-macros-0.1
  (package
    (name "rust-tinystr-macros")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c5c361iv5h9brfmhrmz1s7456px5acdd5aqjkazcssfs2playn9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-tinystr" ,rust-tinystr-0.3))))
    (home-page "https://github.com/zbraniecki/tinystr")
    (synopsis "Proc macros for TinyStr.
")
    (description "Proc macros for TinyStr.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tinystr-0.3
  (package
    (name "rust-tinystr")
    (version "0.3.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hf74r8qiigddfsxsbkab1pz1hsgi2297azf42k9x39qnknqwwr9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinystr-macros" ,rust-tinystr-macros-0.1))))
    (home-page "https://github.com/unicode-org/icu4x")
    (synopsis "A small ASCII-only bounded length string representation.")
    (description
      "This package provides a small ASCII-only bounded length string representation.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unic-langid-impl-0.9
  (package
    (name "rust-unic-langid-impl")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0kck3fpdvqv5nha47xkna3zsr8ik9hpyr5ac830n4j29y3m8wjhs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tinystr" ,rust-tinystr-0.3))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-0.9
  (package
    (name "rust-unic-langid")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rcw8llr3a120qad7rlbv4fb19l744ckxwnx37dhn0qafg6qyckk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
         ("rust-unic-langid-macros" ,rust-unic-langid-macros-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-fluent-langneg-0.13
  (package
    (name "rust-fluent-langneg")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-langneg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "152yxplc11vmxkslvmaqak9x86xnavnhdqyhrh38ym37jscd0jic"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://projectfluent.org/")
    (synopsis "A library for language and locale negotiation.
")
    (description
      "This package provides a library for language and locale negotiation.")
    (license license:asl2.0)))

(define-public rust-fluent-bundle-0.15
  (package
    (name "rust-fluent-bundle")
    (version "0.15.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-bundle" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zbzm13rfz7fay7bps7jd4j1pdnlxmdzzfymyq2iawf9vq0wchp2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fluent-langneg" ,rust-fluent-langneg-0.13)
         ("rust-fluent-syntax" ,rust-fluent-syntax-0.11)
         ("rust-intl-memoizer" ,rust-intl-memoizer-0.5)
         ("rust-intl-pluralrules" ,rust-intl-pluralrules-7)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-self-cell" ,rust-self-cell-0.10)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A localization system designed to unleash the entire expressive power of
natural language translations.
")
    (description
      "This package provides a localization system designed to unleash the entire
expressive power of natural language translations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fluent-0.16
  (package
    (name "rust-fluent")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19s7z0gw95qdsp9hhc00xcy11nwhnx93kknjmdvdnna435w97xk1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fluent-bundle" ,rust-fluent-bundle-0.15)
         ("rust-fluent-pseudo" ,rust-fluent-pseudo-0.3)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A localization system designed to unleash the entire expressive power of
natural language translations.
")
    (description
      "This package provides a localization system designed to unleash the entire
expressive power of natural language translations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-chacha20-0.7
  (package
    (name "rust-chacha20")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chacha20" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c8h4sp9zh13v8p9arydjcj92xc6j3mccrjc4mizrvq7fzx9717h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-rand-core" ,rust-rand-core-0.6)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/stream-ciphers")
    (synopsis
      "The ChaCha20 stream cipher (RFC 8439) implemented in pure Rust using traits
from the RustCrypto `cipher` crate, with optional architecture-specific
hardware acceleration (AVX2, SSE2). Additionally provides the ChaCha8, ChaCha12,
XChaCha20, XChaCha12 and XChaCha8 stream ciphers, and also optional
rand_core-compatible RNGs based on those ciphers.
")
    (description
      "The ChaCha20 stream cipher (RFC 8439) implemented in pure Rust using traits from
the RustCrypto `cipher` crate, with optional architecture-specific hardware
acceleration (AVX2, SSE2).  Additionally provides the ChaCha8, ChaCha12,
XChaCha20, XChaCha12 and XChaCha8 stream ciphers, and also optional
rand_core-compatible RNGs based on those ciphers.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-chacha20poly1305-0.8
  (package
    (name "rust-chacha20poly1305")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chacha20poly1305" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18mb6k1w71dqv5q50an4rvp19l6yg8ssmvfrmknjfh2z0az7lm5n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-chacha20" ,rust-chacha20-0.7)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-poly1305" ,rust-poly1305-0.7)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/AEADs")
    (synopsis
      "Pure Rust implementation of the ChaCha20Poly1305 Authenticated Encryption
with Additional Data Cipher (RFC 8439) with optional architecture-specific
hardware acceleration. Also contains implementations of the XChaCha20Poly1305
extended nonce variant of ChaCha20Poly1305, and the reduced-round
ChaCha8Poly1305 and ChaCha12Poly1305 lightweight variants.
")
    (description
      "Pure Rust implementation of the ChaCha20Poly1305 Authenticated Encryption with
Additional Data Cipher (RFC 8439) with optional architecture-specific hardware
acceleration.  Also contains implementations of the XChaCha20Poly1305 extended
nonce variant of ChaCha20Poly1305, and the reduced-round ChaCha8Poly1305 and
ChaCha12Poly1305 lightweight variants.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-csrf-0.4
  (package
    (name "rust-csrf")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csrf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q7ixhshj6a7x2vgsr4d4iqa5mgp4fwkr4lx2hgvnj9xcy1py9dh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-aes-gcm" ,rust-aes-gcm-0.9)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-chacha20poly1305" ,rust-chacha20poly1305-0.8)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-generic-array" ,rust-generic-array-0.14)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-log" ,rust-log-0.4)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-typemap" ,rust-typemap-0.3))))
    (home-page "https://github.com/heartsucker/rust-csrf")
    (synopsis "CSRF protection primitives")
    (description "CSRF protection primitives")
    (license license:expat)))

(define-public rust-zstd-sys-2
  (package
    (name "rust-zstd-sys")
    (version "2.0.1+zstd.1.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0azifd7xsyy9yljihx26pr9am85717fzdzdzbladjiiqqnxprl4z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.59)
         ("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Low-level bindings for the zstd compression library.")
    (description "Low-level bindings for the zstd compression library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-zstd-safe-5
  (package
    (name "rust-zstd-safe")
    (version "5.0.2+zstd.1.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd-safe" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nzl4q3xl68pq58g9xlym299bvjdii8cl7ix595ym7jgw22maahx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2) ("rust-zstd-sys" ,rust-zstd-sys-2))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Safe low-level bindings for the zstd compression library.")
    (description "Safe low-level bindings for the zstd compression library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-zstd-0.11
  (package
    (name "rust-zstd")
    (version "0.11.2+zstd.1.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r7xlmgnifhxbfyid8vkcnd5ip16gx9hf89d1l0lzrpc4q1rdk10"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zstd-safe" ,rust-zstd-safe-5))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Binding for the zstd compression library.")
    (description "Binding for the zstd compression library.")
    (license license:expat)))

(define-public rust-async-compression-0.3
  (package
    (name "rust-async-compression")
    (version "0.3.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-compression" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15gnvigh9jaq80ipn04j06k6f2vgnxjp2ddi2z3ldxq1mf9d6prl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-brotli" ,rust-brotli-3)
         ("rust-bytes" ,rust-bytes-0.5)
         ("rust-bzip2" ,rust-bzip2-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-0.2)
         ("rust-tokio" ,rust-tokio-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-xz2" ,rust-xz2-0.1)
         ("rust-zstd" ,rust-zstd-0.11)
         ("rust-zstd-safe" ,rust-zstd-safe-5))))
    (home-page "https://github.com/Nemo157/async-compression")
    (synopsis
      "Adaptors between compression crates and Rust's modern asynchronous IO types.
")
    (description
      "Adaptors between compression crates and Rust's modern asynchronous IO types.")
    (license (list license:expat license:asl2.0))))

(define-public rust-poem-1
  (package
    (name "rust-poem")
    (version "1.3.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "poem" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1y12cp7k75gjc48fm4ayaf8qmwr1x8yn2x32ph3hzm54vgw7r8z4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-cookie" ,rust-cookie-0.16)
         ("rust-csrf" ,rust-csrf-0.4)
         ("rust-eyre" ,rust-eyre-0.6)
         ("rust-fluent" ,rust-fluent-0.16)
         ("rust-fluent-langneg" ,rust-fluent-langneg-0.13)
         ("rust-fluent-syntax" ,rust-fluent-syntax-0.11)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-hyper-rustls" ,rust-hyper-rustls-0.23)
         ("rust-intl-memoizer" ,rust-intl-memoizer-0.5)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-multer" ,rust-multer-2)
         ("rust-opentelemetry" ,rust-opentelemetry-0.17)
         ("rust-opentelemetry-http" ,rust-opentelemetry-http-0.6)
         ("rust-opentelemetry-prometheus" ,rust-opentelemetry-prometheus-0.10)
         ("rust-opentelemetry-semantic-conventions"
          ,rust-opentelemetry-semantic-conventions-0.9)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-poem-derive" ,rust-poem-derive-1)
         ("rust-priority-queue" ,rust-priority-queue-1)
         ("rust-prometheus" ,rust-prometheus-0.13)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rcgen" ,rust-rcgen-0.9)
         ("rust-redis" ,rust-redis-0.21)
         ("rust-regex" ,rust-regex-1)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-rust-embed" ,rust-rust-embed-6)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-sse-codec" ,rust-sse-codec-0.3)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-metrics" ,rust-tokio-metrics-0.1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.17)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-typed-headers" ,rust-typed-headers-0.2)
         ("rust-unic-langid" ,rust-unic-langid-0.9)
         ("rust-x509-parser" ,rust-x509-parser-0.13))))
    (home-page "https://github.com/poem-web/poem")
    (synopsis
      "Poem is a full-featured and easy-to-use web framework with the Rust programming language.")
    (description
      "Poem is a full-featured and easy-to-use web framework with the Rust programming
language.")
    (license (list license:expat license:asl2.0))))

(define-public rust-iri-string-0.4
  (package
    (name "rust-iri-string")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "iri-string" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y2z4f5y87hnff2d5lcl811hp7iv2f5qri7x3fgm48z2q4w7c3wg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nom" ,rust-nom-7) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/lo48576/iri-string")
    (synopsis "IRI as string types")
    (description "IRI as string types")
    (license (list license:expat license:asl2.0))))

(define-public rust-http-range-header-0.3
  (package
    (name "rust-http-range-header")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http-range-header" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0aas8c5dagfhcqpmqq9xw6a8nkl3lfg4g4mpddvyz1cj1bnqxzhb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/MarcusGrass/parse-range-headers")
    (synopsis "No-dep range header parser")
    (description "No-dep range header parser")
    (license license:expat)))

(define-public rust-tower-http-0.2
  (package
    (name "rust-tower-http")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tower-http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1n2cddayd0kczcg48ifb28fds4vhh45c4kskx3x43yzpmgpz78xb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-http-range-header" ,rust-http-range-header-0.3)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-iri-string" ,rust-iri-string-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/tower-rs/tower-http")
    (synopsis "Tower middleware and utilities for HTTP clients and servers")
    (description "Tower middleware and utilities for HTTP clients and servers")
    (license license:expat)))

(define-public rust-tower-service-0.3
  (package
    (name "rust-tower-service")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tower-service" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1iih764s3f6vlkspfmr72fkrs2lw1v3wiqmc6bd5zq1hdlfzs39n"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/tower-rs/tower")
    (synopsis
      "Trait representing an asynchronous, request / response based, client or server.
")
    (description
      "Trait representing an asynchronous, request / response based, client or server.")
    (license license:expat)))

(define-public rust-hdrhistogram-7
  (package
    (name "rust-hdrhistogram")
    (version "7.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hdrhistogram" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h0905yk0pxgxfk4kzlfmnglm6ky1ssbrpf4ars4yb5y25q2nrri"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-nom" ,rust-nom-7)
         ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://github.com/HdrHistogram/HdrHistogram_rust")
    (synopsis "A port of HdrHistogram to Rust")
    (description "This package provides a port of HdrHistogram to Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-tower-0.4
  (package
    (name "rust-tower")
    (version "0.4.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tower" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0gkisq1mcfyw2i9aq7d1d4y52x35506v8pfzh9sp7pvammizv2cs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-hdrhistogram" ,rust-hdrhistogram-7)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/tower-rs/tower")
    (synopsis
      "Tower is a library of modular and reusable components for building robust
clients and servers.
")
    (description
      "Tower is a library of modular and reusable components for building robust
clients and servers.")
    (license license:expat)))

(define-public rust-tungstenite-0.16
  (package
    (name "rust-tungstenite")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l9s7gi9kgl4zynhbyb7737lmwaxaim4b818lwi7y95f2hx73lva"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.6)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-url" ,rust-url-2)
         ("rust-utf-8" ,rust-utf-8-0.7)
         ("rust-webpki" ,rust-webpki-0.22)
         ("rust-webpki-roots" ,rust-webpki-roots-0.22))))
    (home-page "https://github.com/snapview/tungstenite-rs")
    (synopsis "Lightweight stream-based WebSocket implementation")
    (description "Lightweight stream-based WebSocket implementation")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-tungstenite-0.16
  (package
    (name "rust-tokio-tungstenite")
    (version "0.16.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wnadcv9q2yi7bjkdp6z0g4rk7kbdblsv613fpgjrhgwdbgkj2z8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.6)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-tungstenite" ,rust-tungstenite-0.16)
         ("rust-webpki" ,rust-webpki-0.22)
         ("rust-webpki-roots" ,rust-webpki-roots-0.22))))
    (home-page "https://github.com/snapview/tokio-tungstenite")
    (synopsis
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket implementation")
    (description
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket
implementation")
    (license license:expat)))

(define-public rust-sync-wrapper-0.1
  (package
    (name "rust-sync-wrapper")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sync_wrapper" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1a59lwsw52d1a64l2y1m7npfw6xjvrjf96c5014g1b69lkj8yl90"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://docs.rs/sync_wrapper")
    (synopsis
      "A tool for enlisting the compilerâ\x80\x99s help in proving the absence of concurrency")
    (description
      "This package provides a tool for enlisting the compilerâ\x80\x99s help in proving the
absence of concurrency")
    (license license:asl2.0)))

(define-public rust-matchit-0.4
  (package
    (name "rust-matchit")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "matchit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0czq1brp3945f4pcpjbsrmh5zbvz48ci9z0ibmkssr856kqa8xlk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ibraheemdev/matchit")
    (synopsis "A blazing fast URL router.")
    (description "This package provides a blazing fast URL router.")
    (license license:expat)))

(define-public rust-http-0.2
  (package
    (name "rust-http")
    (version "0.2.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fxzyvspr6g8znc6i0kif0bhpih8ibhy7xc6k984j8pm19bp11pz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-itoa" ,rust-itoa-1))))
    (home-page "https://github.com/hyperium/http")
    (synopsis "A set of types for representing HTTP requests and responses.
")
    (description
      "This package provides a set of types for representing HTTP requests and
responses.")
    (license (list license:expat license:asl2.0))))

(define-public rust-axum-core-0.1
  (package
    (name "rust-axum-core")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "axum-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lbsca8nba4zh7b5slr71dspfbgi2vwqxjvr4xbqg77g7cwxmg3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-mime" ,rust-mime-0.3))))
    (home-page "https://github.com/tokio-rs/axum")
    (synopsis "Core types and traits for axum")
    (description "Core types and traits for axum")
    (license license:expat)))

(define-public rust-axum-0.4
  (package
    (name "rust-axum")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "axum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pcxnxp4290mcjv8z3g4wp22r4yrlp1ggap49z8p36hy5k4ldwy9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-axum-core" ,rust-axum-core-0.1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-matchit" ,rust-matchit-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-multer" ,rust-multer-2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-sha-1" ,rust-sha-1-0.10)
         ("rust-sync-wrapper" ,rust-sync-wrapper-0.1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.16)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tower-http" ,rust-tower-http-0.2)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3))))
    (home-page "https://github.com/tokio-rs/axum")
    (synopsis "Web framework that focuses on ergonomics and modularity")
    (description "Web framework that focuses on ergonomics and modularity")
    (license license:expat)))

(define-public rust-rust-embed-6
  (package
    (name "rust-rust-embed")
    (version "6.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hxi8d4nrlqm8xvhn86iinw5dfm05m9lxs9a32bz665kcnnfa5ws"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-actix-web" ,rust-actix-web-3)
         ("rust-axum" ,rust-axum-0.4)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-include-flate" ,rust-include-flate-0.1)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-poem" ,rust-poem-1)
         ("rust-rocket" ,rust-rocket-0.5)
         ("rust-rust-embed-impl" ,rust-rust-embed-impl-6)
         ("rust-rust-embed-utils" ,rust-rust-embed-utils-7)
         ("rust-salvo" ,rust-salvo-0.16)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-warp" ,rust-warp-0.3))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis
      "Rust Custom Derive Macro which loads files into the rust binary at compile time during release and loads the file from the fs during dev")
    (description
      "Rust Custom Derive Macro which loads files into the rust binary at compile time
during release and loads the file from the fs during dev")
    (license license:expat)))

(define-public rust-smartstring-1
  (package
    (name "rust-smartstring")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smartstring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0agf4x0jz79r30aqibyfjm1h9hrjdh0harcqcvb2vapv7rijrdrz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-autocfg" ,rust-autocfg-1)
         ("rust-proptest" ,rust-proptest-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-static-assertions" ,rust-static-assertions-1)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/bodil/smartstring")
    (synopsis "Compact inlined strings")
    (description "Compact inlined strings")
    (license #f)))

(define-public rust-ubyte-0.10
  (package
    (name "rust-ubyte")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ubyte" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "152fs2qpcy2xd16av6wj91z1qfznlpyxfjp1g6xjj6ilcgr2k3m5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/SergioBenitez/ubyte")
    (synopsis
      "A simple, complete, const-everything, saturating, human-friendly, no_std library for byte units.
")
    (description
      "This package provides a simple, complete, const-everything, saturating,
human-friendly, no_std library for byte units.")
    (license (list license:expat license:asl2.0))))

(define-public rust-slab-0.4
  (package
    (name "rust-slab")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slab" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0cmvcy9ppsh3dz8mi6jljx7bxyknvgpas4aid2ayxk1vjpz3qw7b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/tokio-rs/slab")
    (synopsis "Pre-allocated storage for a uniform data type")
    (description "Pre-allocated storage for a uniform data type")
    (license license:expat)))

(define-public rust-tokio-util-0.7
  (package
    (name "rust-tokio-util")
    (version "0.7.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p2frdzx3nr2pv99a2xfjf0p589kv90n9a9aq7wj3yy2mnhs327r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Additional utilities for working with Tokio.
")
    (description "Additional utilities for working with Tokio.")
    (license license:expat)))

(define-public rust-time-macros-0.2
  (package
    (name "rust-time-macros")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14h712p63k121cwi80x8ydn99k703wkcw2ksivd7r0addwd7nra2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/time-rs/time")
    (synopsis "Procedural macros for the time crate.")
    (description "Procedural macros for the time crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-threads-0.1
  (package
    (name "rust-num-threads")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_threads" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i5vmffsv6g79z869flp1sja69g1gapddjagdw1k3q9f3l2cw698"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/jhpratt/num_threads")
    (synopsis
      "A minimal library that determines the number of running threads for the current process.")
    (description
      "This package provides a minimal library that determines the number of running
threads for the current process.")
    (license (list license:expat license:asl2.0))))

(define-public rust-time-0.3
  (package
    (name "rust-time")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1p8dsb0zwa2r9bz2f31kxfsij6qhmkf1as3ch82z0q58lw42ww62"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itoa" ,rust-itoa-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-threads" ,rust-num-threads-0.1)
         ("rust-quickcheck" ,rust-quickcheck-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-time-macros" ,rust-time-macros-0.2))))
    (home-page "https://time-rs.github.io")
    (synopsis
      "Date and time library. Fully interoperable with the standard library. Mostly compatible with #![no_std].")
    (description
      "Date and time library.  Fully interoperable with the standard library.  Mostly
compatible with #![no_std].")
    (license (list license:expat license:asl2.0))))

(define-public rust-oid-registry-0.4
  (package
    (name "rust-oid-registry")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "oid-registry" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0akbah3j8231ayrp2l1y5d9zmvbvqcsj0sa6s6dz6h85z8bhgqiq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-asn1-rs" ,rust-asn1-rs-0.3))))
    (home-page "https://github.com/rusticata/oid-registry")
    (synopsis "Object Identifier (OID) database")
    (description "Object Identifier (OID) database")
    (license (list license:expat license:asl2.0))))

(define-public rust-der-parser-7
  (package
    (name "rust-der-parser")
    (version "7.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "der-parser" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10kfa2gzl3x20mwgrd43cyi79xgkqxyzcyrh0xylv4apa33qlfgy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-asn1-rs" ,rust-asn1-rs-0.3)
         ("rust-cookie-factory" ,rust-cookie-factory-0.3)
         ("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-nom" ,rust-nom-7)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rusticata-macros" ,rust-rusticata-macros-4))))
    (home-page "https://github.com/rusticata/der-parser")
    (synopsis "Parser/encoder for ASN.1 BER/DER data")
    (description "Parser/encoder for ASN.1 BER/DER data")
    (license (list license:expat license:asl2.0))))

(define-public rust-displaydoc-0.2
  (package
    (name "rust-displaydoc")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "displaydoc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11i8p5snlc1hs4g5q3wiyr75dn276l6kr0si5m7xmfa6y31mvy9v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/yaahc/displaydoc")
    (synopsis
      "A derive macro for implementing the display Trait via a doc comment and string interpolation
")
    (description
      "This package provides a derive macro for implementing the display Trait via a
doc comment and string interpolation")
    (license (list license:expat license:asl2.0))))

(define-public rust-wyz-0.5
  (package
    (name "rust-wyz")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wyz" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03ir858jfk3sn98v3vzh33ap8s27sfgbalrv71n069wxyaa1bcrh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-tap" ,rust-tap-1)
         ("rust-typemap" ,rust-typemap-0.3))))
    (home-page "https://myrrlyn.net/crates/wyz")
    (synopsis "myrrlynâ\x80\x99s utility collection")
    (description "myrrlynâ\x80\x99s utility collection")
    (license license:expat)))

(define-public rust-radium-0.7
  (package
    (name "rust-radium")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "radium" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02cxfi3ky3c4yhyqx9axqwhyaca804ws46nn4gc1imbk94nzycyw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/bitvecto-rs/radium")
    (synopsis "Portable interfaces for maybe-atomic types")
    (description "Portable interfaces for maybe-atomic types")
    (license license:expat)))

(define-public rust-funty-2
  (package
    (name "rust-funty")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "funty" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "177w048bm0046qlzvp33ag3ghqkqw4ncpzcm5lq36gxf2lla7mg6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/myrrlyn/funty")
    (synopsis "Trait generalization over the primitive types")
    (description "Trait generalization over the primitive types")
    (license license:expat)))

(define-public rust-bitvec-1
  (package
    (name "rust-bitvec")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sq1kyx32fd88q6x2n6fbhag4smdab93ma9c8sh7vd2v7awzr28l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-funty" ,rust-funty-2)
         ("rust-radium" ,rust-radium-0.7)
         ("rust-serde" ,rust-serde-1)
         ("rust-tap" ,rust-tap-1)
         ("rust-wyz" ,rust-wyz-0.5))))
    (home-page "https://bitvecto-rs.github.io/bitvec")
    (synopsis "Addresses memory by bits, for packed collections and bitfields")
    (description
      "Addresses memory by bits, for packed collections and bitfields")
    (license license:expat)))

(define-public rust-asn1-rs-impl-0.1
  (package
    (name "rust-asn1-rs-impl")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "asn1-rs-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1va27bn7qxqp4wanzjlkagnynv6jnrhnwmcky2ahzb1r405p6xr7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/rusticata/asn1-rs")
    (synopsis "Implementation details for the `asn1-rs` crate")
    (description "Implementation details for the `asn1-rs` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-asn1-rs-derive-0.1
  (package
    (name "rust-asn1-rs-derive")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "asn1-rs-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gzf9vab06lk0zjvbr07axx64fndkng2s28bnj27fnwd548pb2yv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-synstructure" ,rust-synstructure-0.12))))
    (home-page "https://github.com/rusticata/asn1-rs")
    (synopsis "Derive macros for the `asn1-rs` crate")
    (description "Derive macros for the `asn1-rs` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-asn1-rs-0.3
  (package
    (name "rust-asn1-rs")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "asn1-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0czsk1nd4dx2k83f7jzkn8klx05wbmblkx1jh51i4c170akhbzrh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-asn1-rs-derive" ,rust-asn1-rs-derive-0.1)
         ("rust-asn1-rs-impl" ,rust-asn1-rs-impl-0.1)
         ("rust-bitvec" ,rust-bitvec-1)
         ("rust-cookie-factory" ,rust-cookie-factory-0.3)
         ("rust-displaydoc" ,rust-displaydoc-0.2)
         ("rust-nom" ,rust-nom-7)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rusticata-macros" ,rust-rusticata-macros-4)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/rusticata/asn1-rs")
    (synopsis "Parser/encoder for ASN.1 BER/DER data")
    (description "Parser/encoder for ASN.1 BER/DER data")
    (license (list license:expat license:asl2.0))))

(define-public rust-x509-parser-0.13
  (package
    (name "rust-x509-parser")
    (version "0.13.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x509-parser" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "077bi0xyaa8cmrqf3rrw1z6kkzscwd1nxdxgs7mgz2ambg7bmfcz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-asn1-rs" ,rust-asn1-rs-0.3)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-der-parser" ,rust-der-parser-7)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-nom" ,rust-nom-7)
         ("rust-oid-registry" ,rust-oid-registry-0.4)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-rusticata-macros" ,rust-rusticata-macros-4)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/rusticata/x509-parser")
    (synopsis "Parser for the X.509 v3 format (RFC 5280 certificates)")
    (description "Parser for the X.509 v3 format (RFC 5280 certificates)")
    (license (list license:expat license:asl2.0))))

(define-public rust-zerocopy-derive-0.3
  (package
    (name "rust-zerocopy-derive")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17rab2i1vwmxcr7c6r6xv55nhy41wlay0lpfcyl4vqpgh8mwiyx0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-synstructure" ,rust-synstructure-0.12))))
    (home-page
      "https://fuchsia.googlesource.com/fuchsia/+/HEAD/src/lib/zerocopy/zerocopy-derive")
    (synopsis "Custom derive for traits from the zerocopy crate")
    (description "Custom derive for traits from the zerocopy crate")
    (license #f)))

(define-public rust-zerocopy-0.6
  (package
    (name "rust-zerocopy")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zerocopy" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dpj4nd9v56wy93ahjkp95znjzj91waqvidqch8gxwdwq661hbrk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-zerocopy-derive" ,rust-zerocopy-derive-0.3))))
    (home-page
      "https://fuchsia.googlesource.com/fuchsia/+/HEAD/src/lib/zerocopy")
    (synopsis "Utilities for zero-copy parsing and serialization")
    (description "Utilities for zero-copy parsing and serialization")
    (license #f)))

(define-public rust-quote-1
  (package
    (name "rust-quote")
    (version "1.0.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lca4xnwdc2sp76bf4n50kifmi5phhxr9520w623mfcksr7bbzm1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "https://github.com/dtolnay/quote")
    (synopsis "Quasi-quoting macro quote!(...)")
    (description "Quasi-quoting macro quote!(...)")
    (license (list license:expat license:asl2.0))))

(define-public rust-uuid-macro-internal-1
  (package
    (name "rust-uuid-macro-internal")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid-macro-internal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06r6ngqrrr76jlcl904kir76fgbz3xskr1rbriqmfxmgbm77nd8r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "Private implementation details of the uuid! macro.")
    (description "Private implementation details of the uuid! macro.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-sha1-smol-1
  (package
    (name "rust-sha1-smol")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha1_smol" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04nhbhvsk5ms1zbshs80iq5r1vjszp2xnm9f0ivj38q3dhc4f6mf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/mitsuhiko/sha1-smol")
    (synopsis "Minimal dependency free implementation of SHA1 for Rust.")
    (description "Minimal dependency free implementation of SHA1 for Rust.")
    (license license:bsd-3)))

(define-public rust-md5-asm-0.5
  (package
    (name "rust-md5-asm")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "md5-asm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ixmkg8j7sqy9zln6pz9xi2dl2d9zpm8pz6p49za47n1bvradfbk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/RustCrypto/asm-hashes")
    (synopsis "Assembly implementation of MD5 compression function")
    (description "Assembly implementation of MD5 compression function")
    (license license:expat)))

(define-public rust-md-5-0.10
  (package
    (name "rust-md-5")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "md-5" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10h5kna43cpggp9hy1hz4zb1qpixdl4anf3hdj3gfwhb3sr4d1k5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-digest" ,rust-digest-0.10)
         ("rust-md5-asm" ,rust-md5-asm-0.5))))
    (home-page "https://github.com/RustCrypto/hashes")
    (synopsis "MD5 hash function")
    (description "MD5 hash function")
    (license (list license:expat license:asl2.0))))

(define-public rust-uuid-1
  (package
    (name "rust-uuid")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w668dw9jq0dz24smh1v3w3ilzi6fcs45vc702hnwkbc8lcx7z4c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-atomic" ,rust-atomic-0.5)
         ("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-md-5" ,rust-md-5-0.10)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-sha1-smol" ,rust-sha1-smol-1)
         ("rust-slog" ,rust-slog-2)
         ("rust-uuid-macro-internal" ,rust-uuid-macro-internal-1)
         ("rust-zerocopy" ,rust-zerocopy-0.6))))
    (home-page "https://github.com/uuid-rs/uuid")
    (synopsis "A library to generate and parse UUIDs.")
    (description
      "This package provides a library to generate and parse UUIDs.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tokio-rustls-0.23
  (package
    (name "rust-tokio-rustls")
    (version "0.23.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-rustls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nfsmmi8l1lgpbfy6079d5i13984djzcxrdr9jc06ghi0cwyhgn4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustls" ,rust-rustls-0.20)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-webpki" ,rust-webpki-0.22))))
    (home-page "https://github.com/tokio-rs/tls")
    (synopsis "Asynchronous TLS/SSL streams for Tokio using Rustls.")
    (description "Asynchronous TLS/SSL streams for Tokio using Rustls.")
    (license (list license:expat license:asl2.0))))

(define-public rust-state-0.5
  (package
    (name "rust-state")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "state" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fzji31ijbkimbzdy4dln9mp5xp7lm1a0dnqxv4n10hywphnds6v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-loom" ,rust-loom-0.5))))
    (home-page "https://github.com/SergioBenitez/state")
    (synopsis
      "A library for safe and effortless global and thread-local state management.
")
    (description
      "This package provides a library for safe and effortless global and thread-local
state management.")
    (license (list license:expat license:asl2.0))))

(define-public rust-stable-pattern-0.1
  (package
    (name "rust-stable-pattern")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stable-pattern" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i8hq82vm82mqj02qqcsd7caibrih7x5w3a1xpm8hpv30261cr25"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/SergioBenitez/stable-pattern")
    (synopsis "Stable port of std::str::Pattern and friends.")
    (description "Stable port of std::str::Pattern and friends.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rustls-pemfile-1
  (package
    (name "rust-rustls-pemfile")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls-pemfile" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1afrjj5l8gw8qm7njwf55nrgb8whqyfq56pyb0a0dzw7wyfjqlp7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-base64" ,rust-base64-0.13))))
    (home-page "https://github.com/rustls/pemfile")
    (synopsis "Basic .pem file parser for keys and certificates")
    (description "Basic .pem file parser for keys and certificates")
    (license (list license:asl2.0 license:isc license:expat))))

(define-public rust-typenum-1
  (package
    (name "rust-typenum")
    (version "1.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11yrvz1vd43gqv738yw1v75rzngjbs7iwcgzjy3cq5ywkv2imy6w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-scale-info" ,rust-scale-info-1))))
    (home-page "https://github.com/paholg/typenum")
    (synopsis
      "Typenum is a Rust library for type-level numbers evaluated at
    compile time. It currently supports bits, unsigned integers, and signed
    integers. It also provides a type-level array of type-level numbers, but its
    implementation is incomplete.")
    (description
      "Typenum is a Rust library for type-level numbers evaluated at     compile time.
It currently supports bits, unsigned integers, and signed     integers.  It also
provides a type-level array of type-level numbers, but its     implementation is
incomplete.")
    (license (list license:expat license:asl2.0))))

(define-public rust-crypto-common-0.1
  (package
    (name "rust-crypto-common")
    (version "0.1.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "crypto-common" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "1cvby95a6xg7kxdz5ln3rl9xh66nz66w46mm3g56ri1z5x815yqv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-generic-array" ,rust-generic-array-0.14)
                       ("rust-rand-core" ,rust-rand-core-0.6)
                       ("rust-typenum" ,rust-typenum-1))))
    (home-page "https://github.com/RustCrypto/traits")
    (synopsis "Common cryptographic traits")
    (description "Common cryptographic traits")
    (license (list license:expat license:asl2.0))))

(define-public rust-const-oid-0.9
  (package
    (name "rust-const-oid")
    (version "0.9.1")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "const-oid" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0fyxvwnl3x6bxhy08a3g4ryf8mky6wnhwd6ll4g6mjxgfnk1ihyf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/RustCrypto/formats/tree/master/const-oid")
    (synopsis
     "Const-friendly implementation of the ISO/IEC Object Identifier (OID) standard
as defined in ITU X.660, with support for BER/DER encoding/decoding as well as
heapless no_std (i.e. embedded) support
")
    (description
     "Const-friendly implementation of the ISO/IEC Object Identifier (OID) standard as
defined in ITU X.660, with support for BER/DER encoding/decoding as well as
heapless no_std (i.e.  embedded) support")
    (license (list license:asl2.0 license:expat))))

(define-public rust-digest-0.10
  (package
    (name "rust-digest")
    (version "0.10.6")
    (source (origin
              (method url-fetch)
              (uri (crate-uri "digest" version))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0vz74785s96g727vg37iwkjvbkcfzp093j49ihhyf8sh9s7kfs41"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-blobby" ,rust-blobby-0.3)
                       ("rust-block-buffer" ,rust-block-buffer-0.10)
                       ("rust-const-oid" ,rust-const-oid-0.9)
                       ("rust-crypto-common" ,rust-crypto-common-0.1)
                       ("rust-subtle" ,rust-subtle-2))))
    (home-page "https://github.com/RustCrypto/traits")
    (synopsis
     "Traits for cryptographic hash functions and message authentication codes")
    (description
     "Traits for cryptographic hash functions and message authentication codes")
    (license (list license:expat license:asl2.0))))

(define-public rust-hmac-0.12
  (package
    (name "rust-hmac")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hmac" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pmbr069sfg76z7wsssfk5ddcqd9ncp79fyz6zcm6yn115yc6jbc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-digest" ,rust-digest-0.10))))
    (home-page "https://github.com/RustCrypto/MACs")
    (synopsis
      "Generic implementation of Hash-based Message Authentication Code (HMAC)")
    (description
      "Generic implementation of Hash-based Message Authentication Code (HMAC)")
    (license (list license:expat license:asl2.0))))

(define-public rust-hkdf-0.12
  (package
    (name "rust-hkdf")
    (version "0.12.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hkdf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dyl16cf15hka32hv3l7dwgr3xj3brpfr27iyrbpdhlzdfgh46kr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-hmac" ,rust-hmac-0.12))))
    (home-page "https://github.com/RustCrypto/KDFs/")
    (synopsis "HMAC-based Extract-and-Expand Key Derivation Function (HKDF)")
    (description
      "HMAC-based Extract-and-Expand Key Derivation Function (HKDF)")
    (license (list license:expat license:asl2.0))))

(define-public rust-zeroize-1
  (package
    (name "rust-zeroize")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zeroize" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z8yix823b6lz878qwg6bvwhg3lb0cbw3c9yij9p8mbv7zdzfmj7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zeroize-derive" ,rust-zeroize-derive-1))))
    (home-page "https://github.com/RustCrypto/utils/tree/master/zeroize")
    (synopsis
      "Securely clear secrets from memory with a simple trait built on
stable Rust primitives which guarantee memory is zeroed using an
operation will not be 'optimized away' by the compiler.
Uses a portable pure Rust implementation that works everywhere,
even WASM!
")
    (description
      "Securely clear secrets from memory with a simple trait built on stable Rust
primitives which guarantee memory is zeroed using an operation will not be
'optimized away' by the compiler.  Uses a portable pure Rust implementation that
works everywhere, even WASM!")
    (license (list license:asl2.0 license:expat))))

(define-public rust-polyval-0.5
  (package
    (name "rust-polyval")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "polyval" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1890wqvc0csc9y9k9k4gsbz91rgdnhn6xnfmy9pqkh674fvd46c4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-universal-hash" ,rust-universal-hash-0.4)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/universal-hashes")
    (synopsis
      "POLYVAL is a GHASH-like universal hash over GF(2^128) useful for constructing
a Message Authentication Code (MAC)
")
    (description
      "POLYVAL is a GHASH-like universal hash over GF(2^128) useful for constructing a
Message Authentication Code (MAC)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-ghash-0.4
  (package
    (name "rust-ghash")
    (version "0.4.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ghash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "169wvrc2k9lw776x3pmqp76kc0w5717wz01bfg9rz0ypaqbcr0qm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-polyval" ,rust-polyval-0.5)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/universal-hashes")
    (synopsis
      "Universal hash over GF(2^128) useful for constructing a Message Authentication Code (MAC),
as in the AES-GCM authenticated encryption cipher.
")
    (description
      "Universal hash over GF(2^128) useful for constructing a Message Authentication
Code (MAC), as in the AES-GCM authenticated encryption cipher.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-aead-0.4
  (package
    (name "rust-aead")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aead" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xw8kp9j1whfdxhgmr2qf9xgslkg52zh6gzmhsh13y9w3s73nq8b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-blobby" ,rust-blobby-0.3)
         ("rust-generic-array" ,rust-generic-array-0.14)
         ("rust-heapless" ,rust-heapless-0.7)
         ("rust-rand-core" ,rust-rand-core-0.6))))
    (home-page "https://github.com/RustCrypto/traits")
    (synopsis
      "Traits for Authenticated Encryption with Associated Data (AEAD) algorithms,
such as AES-GCM as ChaCha20Poly1305, which provide a high-level API
")
    (description
      "Traits for Authenticated Encryption with Associated Data (AEAD) algorithms, such
as AES-GCM as ChaCha20Poly1305, which provide a high-level API")
    (license (list license:expat license:asl2.0))))

(define-public rust-aes-gcm-0.9
  (package
    (name "rust-aes-gcm")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-gcm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xndncn1phjb7pjam63vl0yp7h8jh95m0yxanr1092vx7al8apyz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-aes" ,rust-aes-0.7)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-ctr" ,rust-ctr-0.8)
         ("rust-ghash" ,rust-ghash-0.4)
         ("rust-subtle" ,rust-subtle-2)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/AEADs")
    (synopsis
      "Pure Rust implementation of the AES-GCM (Galois/Counter Mode)
Authenticated Encryption with Associated Data (AEAD) Cipher
with optional architecture-specific hardware acceleration
")
    (description
      "Pure Rust implementation of the AES-GCM (Galois/Counter Mode) Authenticated
Encryption with Associated Data (AEAD) Cipher with optional
architecture-specific hardware acceleration")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cookie-0.16
  (package
    (name "rust-cookie")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cookie" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01fa6z8sqqg19ya0l9ifh8vn05l5hpxdzkbh489mpymhw5np1m4l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-gcm" ,rust-aes-gcm-0.9)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-hkdf" ,rust-hkdf-0.12)
         ("rust-hmac" ,rust-hmac-0.12)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha2" ,rust-sha2-0.10)
         ("rust-subtle" ,rust-subtle-2)
         ("rust-time" ,rust-time-0.3)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/SergioBenitez/cookie-rs")
    (synopsis
      "HTTP cookie parsing and cookie jar management. Supports signed and private
(encrypted, authenticated) jars.
")
    (description
      "HTTP cookie parsing and cookie jar management.  Supports signed and private
(encrypted, authenticated) jars.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-http-0.5
  (package
    (name "rust-rocket-http")
    (version "0.5.0-rc.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket_http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18hpzjmgvl4ibgk62i4qcpq949qsp3s0nqvi4k0y6kcm4z8nbv9d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cookie" ,rust-cookie-0.16)
         ("rust-either" ,rust-either-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pear" ,rust-pear-0.2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-ref-cast" ,rust-ref-cast-1)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-stable-pattern" ,rust-stable-pattern-0.1)
         ("rust-state" ,rust-state-0.5)
         ("rust-time" ,rust-time-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.23)
         ("rust-uncased" ,rust-uncased-0.9)
         ("rust-uuid" ,rust-uuid-1)
         ("rust-x509-parser" ,rust-x509-parser-0.13))))
    (home-page "https://rocket.rs")
    (synopsis
      "Types, traits, and parsers for HTTP requests, responses, and headers.
")
    (description
      "Types, traits, and parsers for HTTP requests, responses, and headers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-core-0.3
  (package
    (name "rust-devise-core")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l00qiih4z14ai0c3s16nlvw0kv4p07ygi6a0ms0knc78xpz87l4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro2-diagnostics" ,rust-proc-macro2-diagnostics-0.9)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-codegen-0.3
  (package
    (name "rust-devise-codegen")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cp7nnfwvjp6wfq11n0ffjjrwfa1wbsb58g1bz3ha6z5lvkp6g0j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise-core" ,rust-devise-core-0.3)
         ("rust-quote" ,rust-quote-1))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-0.3
  (package
    (name "rust-devise")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15dmibnykic2a1ndi66shyvxmpfysnhf05lg2iv8871g0w5miish"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise-codegen" ,rust-devise-codegen-0.3)
         ("rust-devise-core" ,rust-devise-core-0.3))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-codegen-0.5
  (package
    (name "rust-rocket-codegen")
    (version "0.5.0-rc.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0iwvk69rsbww6j5r1r8mqr66mxrnpxks43np00ncvsb1kjxvdbnn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise" ,rust-devise-0.3)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rocket-http" ,rust-rocket-http-0.5)
         ("rust-syn" ,rust-syn-1)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://rocket.rs")
    (synopsis "Procedural macros for the Rocket web framework.")
    (description "Procedural macros for the Rocket web framework.")
    (license (list license:expat license:asl2.0))))

(define-public rust-serde-derive-1
  (package
    (name "rust-serde-derive")
    (version "1.0.137")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gkqhpw86zvppd0lwa8ljzpglwczxq3d7cnkfwirfn9r1jxgl9hz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://serde.rs")
    (synopsis "Macros 1.1 implementation of #[derive(Serialize, Deserialize)]")
    (description
      "Macros 1.1 implementation of #[derive(Serialize, Deserialize)]")
    (license (list license:expat license:asl2.0))))

(define-public rust-serde-1
  (package
    (name "rust-serde")
    (version "1.0.137")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l8pynxnmld179a33l044yvkigq3fhiwgx0518a1b0vzqxa8vsk1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://serde.rs")
    (synopsis "A generic serialization/deserialization framework")
    (description
      "This package provides a generic serialization/deserialization framework")
    (license (list license:expat license:asl2.0))))

(define-public rust-rmp-0.8
  (package
    (name "rust-rmp")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rmp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17rw803xv84csxgd654g7q64kqf9zgkvhsn8as3dbmlg6mr92la4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-paste" ,rust-paste-1))))
    (home-page "https://github.com/3Hren/msgpack-rust")
    (synopsis "Pure Rust MessagePack serialization implementation")
    (description "Pure Rust MessagePack serialization implementation")
    (license license:expat)))

(define-public rust-rmp-serde-1
  (package
    (name "rust-rmp-serde")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rmp-serde" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zilg9zhx2pmyd889hnnzcfzf34hk49g7wynldgij4314w6nny15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-rmp" ,rust-rmp-0.8)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/3Hren/msgpack-rust")
    (synopsis "Serde bindings for RMP")
    (description "Serde bindings for RMP")
    (license license:expat)))

(define-public rust-multer-2
  (package
    (name "rust-multer")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "multer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0diqknyfg0m131bm19rll4abg34ad7k122arcwb5q7anhzk3b3sz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-spin" ,rust-spin-0.9)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/rousan/multer-rs")
    (synopsis
      "An async parser for `multipart/form-data` content-type in Rust.")
    (description
      "An async parser for `multipart/form-data` content-type in Rust.")
    (license license:expat)))

(define-public rust-proc-macro2-diagnostics-0.9
  (package
    (name "rust-proc-macro2-diagnostics")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2-diagnostics" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nmazlb1dkznjds7qwms7yxhi33ajc3isji2lsgx8r3lsqk9gwjb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "")
    (synopsis "Diagnostics for proc-macro2.")
    (description "Diagnostics for proc-macro2.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pear-codegen-0.2
  (package
    (name "rust-pear-codegen")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pear_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l4209fi1n0wj110l12l4xpy32d1xffm61nm82vyq0r37ijcm9c2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro2-diagnostics" ,rust-proc-macro2-diagnostics-0.9)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "A (codegen) pear is a fruit.")
    (description "This package provides a (codegen) pear is a fruit.")
    (license (list license:expat license:asl2.0))))

(define-public rust-inlinable-string-0.1
  (package
    (name "rust-inlinable-string")
    (version "0.1.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inlinable_string" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ysjci8yfvxgf51z0ny2nnwhxrclhmb3vbngin8v4bznhr3ybyn8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/fitzgen/inlinable_string")
    (synopsis
      "The `inlinable_string` crate provides the `InlinableString` type -- an owned, grow-able UTF-8 string that stores small strings inline and avoids heap-allocation -- and the `StringExt` trait which abstracts string operations over both `std::string::String` and `InlinableString` (or even your own custom string type).")
    (description
      "The `inlinable_string` crate provides the `InlinableString` type -- an owned,
grow-able UTF-8 string that stores small strings inline and avoids
heap-allocation -- and the `StringExt` trait which abstracts string operations
over both `std::string::String` and `InlinableString` (or even your own custom
string type).")
    (license (list license:asl2.0 license:expat))))

(define-public rust-pear-0.2
  (package
    (name "rust-pear")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pear" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00l7llav8cidhclx0m2gxm267pfa90c7r2x7xbinij74qm0l5r0m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-inlinable-string" ,rust-inlinable-string-0.1)
         ("rust-pear-codegen" ,rust-pear-codegen-0.2)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "")
    (synopsis "A pear is a fruit.")
    (description "This package provides a pear is a fruit.")
    (license (list license:expat license:asl2.0))))

(define-public rust-figment-0.10
  (package
    (name "rust-figment")
    (version "0.10.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "figment" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pr2w6pldkkjavj1sacn9xiibzhlf13ply0gnnxan616qy9442vr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atomic" ,rust-atomic-0.5)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-pear" ,rust-pear-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-uncased" ,rust-uncased-0.9)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/SergioBenitez/Figment")
    (synopsis "A configuration library so con-free, it's unreal.")
    (description
      "This package provides a configuration library so con-free, it's unreal.")
    (license (list license:expat license:asl2.0))))

(define-public rust-binascii-0.1
  (package
    (name "rust-binascii")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "binascii" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wnaglgl72pn5ilv61q6y34w76gbg7crb8ifqk6lsxnq2gajjg9q"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/naim94a/binascii-rs")
    (synopsis
      "Useful no-std binascii operations including base64, base32 and base16 (hex)")
    (description
      "Useful no-std binascii operations including base64, base32 and base16 (hex)")
    (license license:expat)))

(define-public rust-atomic-0.5
  (package
    (name "rust-atomic")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atomic" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0k135q1qfmxxyzrlhr47r0j38r5fnd4163rgl552qxyagrk853dq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "https://github.com/Amanieu/atomic-rs")
    (synopsis "Generic Atomic<T> wrapper type")
    (description "Generic Atomic<T> wrapper type")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unicode-ident-1
  (package
    (name "rust-unicode-ident")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ident" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vlksh7rxnkakdc5qiwxix6fng9a5cw9v8dfnkf5xsx1zdlg0anj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/dtolnay/unicode-ident")
    (synopsis
      "Determine whether characters have the XID_Start or XID_Continue properties according to Unicode Standard Annex #31")
    (description
      "Determine whether characters have the XID_Start or XID_Continue properties
according to Unicode Standard Annex #31")
    (license (list license:expat license:asl2.0))))

(define-public rust-proc-macro2-1
  (package
    (name "rust-proc-macro2")
    (version "1.0.39")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vzm2m7rq6sym9w73ca3hpc5m9wkwm500hyya6bgrdr5j1b2ajy5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "https://github.com/dtolnay/proc-macro2")
    (synopsis
      "A substitute implementation of the compiler's `proc_macro` API to decouple
token-based libraries from the procedural macro use case.
")
    (description
      "This package provides a substitute implementation of the compiler's `proc_macro`
API to decouple token-based libraries from the procedural macro use case.")
    (license (list license:expat license:asl2.0))))

(define-public rust-syn-1
  (package
    (name "rust-syn")
    (version "1.0.95")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hprqgqywlv4z9piq6ygjh0shq7xfkxkc8braafz6949mcb63bzv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "https://github.com/dtolnay/syn")
    (synopsis "Parser for Rust source code")
    (description "Parser for Rust source code")
    (license (list license:expat license:asl2.0))))

(define-public rust-async-trait-0.1
  (package
    (name "rust-async-trait")
    (version "0.1.53")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-trait" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "003nzwgwb2apz6nww5wmh66k8f47npifll8c33zgkz1d999a6spd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/dtolnay/async-trait")
    (synopsis "Type erasure for async trait methods")
    (description "Type erasure for async trait methods")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-0.5
  (package
    (name "rust-rocket")
    (version "0.5.0-rc.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05wkp7a91ak4jgjhqkpifxh1qiv4vymhkks9ngz0b974zj1x1slq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-stream" ,rust-async-stream-0.3)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-atomic" ,rust-atomic-0.5)
         ("rust-atty" ,rust-atty-0.2)
         ("rust-binascii" ,rust-binascii-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-either" ,rust-either-1)
         ("rust-figment" ,rust-figment-0.10)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-multer" ,rust-multer-2)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-ref-cast" ,rust-ref-cast-1)
         ("rust-rmp-serde" ,rust-rmp-serde-1)
         ("rust-rocket-codegen" ,rust-rocket-codegen-0.5)
         ("rust-rocket-http" ,rust-rocket-http-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-state" ,rust-state-0.5)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-time" ,rust-time-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.7)
         ("rust-ubyte" ,rust-ubyte-0.10)
         ("rust-uuid" ,rust-uuid-1)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "https://rocket.rs")
    (synopsis
      "Web framework with a focus on usability, security, extensibility, and speed.
")
    (description
      "Web framework with a focus on usability, security, extensibility, and speed.")
    (license (list license:expat license:asl2.0))))

(define-public rust-borsh-schema-derive-internal-0.9
  (package
    (name "rust-borsh-schema-derive-internal")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "borsh-schema-derive-internal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h2i37xrbhxvdl32v94j2k8vlf45l4aaffgyv59iv8mzv2b5dgfd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "http://borsh.io")
    (synopsis "Schema Generator for Borsh
")
    (description "Schema Generator for Borsh")
    (license license:asl2.0)))

(define-public rust-borsh-derive-internal-0.9
  (package
    (name "rust-borsh-derive-internal")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "borsh-derive-internal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ra0qkc3a2ah08y4z8b40zximiwv2fzji2iab4g2sbrmgf5c4jal"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "http://borsh.io")
    (synopsis "Binary Object Representation Serializer for Hashing
")
    (description "Binary Object Representation Serializer for Hashing")
    (license license:asl2.0)))

(define-public rust-borsh-derive-0.9
  (package
    (name "rust-borsh-derive")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "borsh-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xb7wkfa4l2lw6gi4lkfsfqa4b2dj5vpcdycwcc5sdrhy99cahb4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-borsh-derive-internal" ,rust-borsh-derive-internal-0.9)
         ("rust-borsh-schema-derive-internal"
          ,rust-borsh-schema-derive-internal-0.9)
         ("rust-proc-macro-crate" ,rust-proc-macro-crate-0.1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "http://borsh.io")
    (synopsis "Binary Object Representation Serializer for Hashing
")
    (description "Binary Object Representation Serializer for Hashing")
    (license license:asl2.0)))

(define-public rust-borsh-0.9
  (package
    (name "rust-borsh")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "borsh" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ylvjdlyfyfscyq5phmvgbh7vlgvy485wn8mj2lzz2qd4183dgqm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-borsh-derive" ,rust-borsh-derive-0.9)
         ("rust-hashbrown" ,rust-hashbrown-0.11))))
    (home-page "http://borsh.io")
    (synopsis "Binary Object Representation Serializer for Hashing
")
    (description "Binary Object Representation Serializer for Hashing")
    (license (list license:expat license:asl2.0))))

(define-public rust-rust-decimal-1
  (package
    (name "rust-rust-decimal")
    (version "1.23.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust_decimal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1y703zhl8qvvq4yrdi0i53nfzfpdqv01h16jp0823vphvgm6kp12"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-arrayvec" ,rust-arrayvec-0.7)
         ("rust-borsh" ,rust-borsh-0.9)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-diesel" ,rust-diesel-1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-postgres" ,rust-postgres-0.19)
         ("rust-rocket" ,rust-rocket-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio-postgres" ,rust-tokio-postgres-0.7))))
    (home-page "https://github.com/paupino/rust-decimal")
    (synopsis
      "Decimal number implementation written in pure Rust suitable for financial and fixed-precision calculations.")
    (description
      "Decimal number implementation written in pure Rust suitable for financial and
fixed-precision calculations.")
    (license license:expat)))

(define-public rust-rhai-codegen-1
  (package
    (name "rust-rhai-codegen")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rhai_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16m524m6j773qxr2c32p6di9yi7hsdsjl2jipa5rz761klfgz87s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://rhai.rs/book/plugins/index.html")
    (synopsis
      "Procedural macros support package for Rhai, a scripting language and engine for Rust")
    (description
      "Procedural macros support package for Rhai, a scripting language and engine for
Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-hashbrown-0.8
  (package
    (name "rust-hashbrown")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09cckr5l71ypvfdbvv1qsag4222blixwn9300hpbr831j3vn46z9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.3)
         ("rust-autocfg" ,rust-autocfg-1)
         ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-lang/hashbrown")
    (synopsis "A Rust port of Google's SwissTable hash map")
    (description
      "This package provides a Rust port of Google's SwissTable hash map")
    (license (list license:asl2.0 license:expat))))

(define-public rust-no-std-compat-0.4
  (package
    (name "rust-no-std-compat")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "no-std-compat" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "132vrf710zsdp40yp1z3kgc2ss8pi0z4gmihsz3y7hl4dpd56f5r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.8) ("rust-spin" ,rust-spin-0.5))))
    (home-page "https://gitlab.com/jD91mZM2/no-std-compat")
    (synopsis
      "A `#![no_std]` compatibility layer that will make porting your crate to no_std *easy*.")
    (description
      "This package provides a `#![no_std]` compatibility layer that will make porting
your crate to no_std *easy*.")
    (license license:expat)))

(define-public rust-core-error-0.0.0
  (package
    (name "rust-core-error")
    (version "0.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-error" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13wvc7lcpi7f6rr0racns4l52gzpix4xhih6qns30hmn5sbv5kgg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/core-error/core-error")
    (synopsis "std::error::Error for libcore")
    (description "std::error::Error for libcore")
    (license (list license:expat license:asl2.0))))

(define-public rust-rhai-1
  (package
    (name "rust-rhai")
    (version "1.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rhai" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xvifvqyhkyf67v32a6k61dd5pg2338dkh6cnxy31r5rp0xra1lz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.7)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-core-error" ,rust-core-error-0.0.0)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-libm" ,rust-libm-0.2)
         ("rust-no-std-compat" ,rust-no-std-compat-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rhai-codegen" ,rust-rhai-codegen-1)
         ("rust-rust-decimal" ,rust-rust-decimal-1)
         ("rust-rustyline" ,rust-rustyline-9)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-smartstring" ,rust-smartstring-1)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://rhai.rs")
    (synopsis "Embedded scripting for Rust")
    (description "Embedded scripting for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-handlebars-4
  (package
    (name "rust-handlebars")
    (version "4.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "handlebars" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pz6qqlh03gs0fqyh9gkwc3v1yrzassn0d808c7x7k2s7s2sj4yi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-pest" ,rust-pest-2)
         ("rust-pest-derive" ,rust-pest-derive-2)
         ("rust-rhai" ,rust-rhai-1)
         ("rust-rust-embed" ,rust-rust-embed-6)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/sunng87/handlebars-rust")
    (synopsis "Handlebars templating implemented in Rust.")
    (description "Handlebars templating implemented in Rust.")
    (license license:expat)))

(define-public rust-gitignore-1
  (package
    (name "rust-gitignore")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gitignore" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "197rlas99iqc95fhikyspfa5flhva9pbl1jc8fn9h50ccbj91akq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-glob" ,rust-glob-0.3))))
    (home-page "https://github.com/nathankleyn/gitignore.rs")
    (synopsis
      "Implementation of .gitignore file parsing and glob testing in Rust.")
    (description
      "Implementation of .gitignore file parsing and glob testing in Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rust-stemmers-1
  (package
    (name "rust-rust-stemmers")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-stemmers" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m6acgdflrrcm17dj7lp7x4sfqqhga24qynv660qinwz04v20sp4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/CurrySoftware/rust-stemmers")
    (synopsis
      "A rust implementation of some popular snowball stemming algorithms")
    (description
      "This package provides a rust implementation of some popular snowball stemming
algorithms")
    (license (list license:expat license:bsd-3))))

(define-public rust-lindera-ipadic-builder-0.8
  (package
    (name "rust-lindera-ipadic-builder")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-ipadic-builder" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r3cya3l5qsm8rciy3i6k0r1s48q61lcg79bvzaakzdqfkj5dpky"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-yada" ,rust-yada-0.5))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A Japanese morphological dictionary builder for IPADIC.")
    (description
      "This package provides a Japanese morphological dictionary builder for IPADIC.")
    (license license:expat)))

(define-public rust-lindera-ipadic-0.8
  (package
    (name "rust-lindera-ipadic")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-ipadic" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vbwwhkqdyn4da7adafykczi6anvzckg6bngxgb0grkrc49xlvr6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-ipadic-builder" ,rust-lindera-ipadic-builder-0.8)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A Japanese morphological dictionary for IPADIC.")
    (description
      "This package provides a Japanese morphological dictionary for IPADIC.")
    (license license:expat)))

(define-public rust-lindera-dictionary-0.8
  (package
    (name "rust-lindera-dictionary")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-dictionary" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "122qzfkhjrivzp4vjyj709wjcwvzsnvab00jmgj519iw1z34mb38"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-lindera-core" ,rust-lindera-core-0.8))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A Japanese morphological dictionary.")
    (description "This package provides a Japanese morphological dictionary.")
    (license license:expat)))

(define-public rust-yada-0.5
  (package
    (name "rust-yada")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yada" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hc5wl40406fi6d2ffchgxa4i034wfx4b5gdf2v2mgvvlnvjrldn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/takuyaa/yada")
    (synopsis
      "Yada is a yet another double-array trie library aiming for fast search and compact data representation.")
    (description
      "Yada is a yet another double-array trie library aiming for fast search and
compact data representation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lindera-core-0.8
  (package
    (name "rust-lindera-core")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h97pji23gpb481vk354qnqjrhnlxall6myy4ja8rsqz24s43lq9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-yada" ,rust-yada-0.5))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A morphological analysis library.")
    (description "This package provides a morphological analysis library.")
    (license license:expat)))

(define-public rust-lindera-0.8
  (package
    (name "rust-lindera")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10flm145vycvkqb2fxlg6xkiy08vriycl4sibxbyxd1ak5wpn1jf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-dictionary" ,rust-lindera-dictionary-0.8)
         ("rust-lindera-ipadic" ,rust-lindera-ipadic-0.8)
         ("rust-lindera-ipadic-builder" ,rust-lindera-ipadic-builder-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A morphological analysis library.")
    (description "This package provides a morphological analysis library.")
    (license license:expat)))

(define-public rust-cedarwood-0.4
  (package
    (name "rust-cedarwood")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cedarwood" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0z8lipbhd0dv0msrsjd2p2p8cky8hkmkskcqincm457lz6c28cgs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-smallvec" ,rust-smallvec-1))))
    (home-page "https://github.com/MnO2/cedarwood")
    (synopsis
      "efficiently-updatable double-array trie in Rust (ported from cedar)")
    (description
      "efficiently-updatable double-array trie in Rust (ported from cedar)")
    (license license:bsd-2)))

(define-public rust-jieba-rs-0.6
  (package
    (name "rust-jieba-rs")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jieba-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "106x4r931kwyw7s3jnqkfc4f5fj4zwna7762a3g1sh150gsi4zlc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cedarwood" ,rust-cedarwood-0.4)
         ("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-hashbrown" ,rust-hashbrown-0.11)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-ordered-float" ,rust-ordered-float-2)
         ("rust-phf" ,rust-phf-0.10)
         ("rust-phf-codegen" ,rust-phf-codegen-0.10)
         ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/messense/jieba-rs")
    (synopsis "The Jieba Chinese Word Segmentation Implemented in Rust")
    (description "The Jieba Chinese Word Segmentation Implemented in Rust")
    (license license:expat)))

(define-public rust-elasticlunr-rs-2
  (package
    (name "rust-elasticlunr-rs")
    (version "2.3.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "elasticlunr-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s8m510cv3lf6r4h892dfmr6bdjck5wdcfza452iryq0wjdfkvk0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jieba-rs" ,rust-jieba-rs-0.6)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lindera" ,rust-lindera-0.8)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-regex" ,rust-regex-1)
         ("rust-rust-stemmers" ,rust-rust-stemmers-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-strum" ,rust-strum-0.21)
         ("rust-strum-macros" ,rust-strum-macros-0.21))))
    (home-page "https://github.com/mattico/elasticlunr-rs")
    (synopsis
      "A partial port of elasticlunr.js to Rust for generating static document search indexes")
    (description
      "This package provides a partial port of elasticlunr.js to Rust for generating
static document search indexes")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-xid-0.2
  (package
    (name "rust-unicode-xid")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-xid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "015zxvwk0is9ls3v016krn5gpr5rk5smyzg6c9j58439ckrm2zlm"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/unicode-rs/unicode-xid")
    (synopsis
      "Determine whether characters have the XID_Start
or XID_Continue properties according to
Unicode Standard Annex #31.
")
    (description
      "Determine whether characters have the XID_Start or XID_Continue properties
according to Unicode Standard Annex #31.")
    (license (list license:expat license:asl2.0))))

(define-public rust-camino-1
  (package
    (name "rust-camino")
    (version "1.0.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "camino" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04n4wqx52gsgq1lfpfgb6m3bsx6j1dysy8jy1zcpz1lpfzlik4c6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proptest" ,rust-proptest-1) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/camino-rs/camino")
    (synopsis "UTF-8 paths")
    (description "UTF-8 paths")
    (license (list license:expat license:asl2.0))))

(define-public rust-pathdiff-0.2
  (package
    (name "rust-pathdiff")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pathdiff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pa4dcmb7lwir4himg1mnl97a05b2z0svczg62l8940pbim12dc8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-camino" ,rust-camino-1))))
    (home-page "https://github.com/Manishearth/pathdiff")
    (synopsis "Library for diffing paths to obtain relative paths")
    (description "Library for diffing paths to obtain relative paths")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-lex-0.2
  (package
    (name "rust-clap-lex")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04wjgd1d3rxsng70rczfzhc7lj87hmwzznhs1dp5xb9d27qkaz53"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-os-str-bytes" ,rust-os-str-bytes-6))))
    (home-page "https://github.com/clap-rs/clap/tree/master/clap_lex")
    (synopsis "Minimal, flexible command line parser")
    (description "Minimal, flexible command line parser")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-derive-3
  (package
    (name "rust-clap-derive")
    (version "3.1.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g53w6qkqcc122bqh51jzfg51147il643idvq1czxkr2x5306ci5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-heck" ,rust-heck-0.4)
         ("rust-proc-macro-error" ,rust-proc-macro-error-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/clap-rs/clap/tree/master/clap_derive")
    (synopsis
      "Parse command line argument by defining a struct, derive crate.")
    (description
      "Parse command line argument by defining a struct, derive crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-3
  (package
    (name "rust-clap")
    (version "3.1.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02s4hk9hrmm2s1j7dkbwpyd75mfzx3p8ks2chmp4ccybv95xznyj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-clap-derive" ,rust-clap-derive-3)
         ("rust-clap-lex" ,rust-clap-lex-0.2)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-strsim" ,rust-strsim-0.10)
         ("rust-termcolor" ,rust-termcolor-1)
         ("rust-terminal-size" ,rust-terminal-size-0.1)
         ("rust-textwrap" ,rust-textwrap-0.15)
         ("rust-unicase" ,rust-unicase-2)
         ("rust-yaml-rust" ,rust-yaml-rust-0.4))))
    (home-page "https://github.com/clap-rs/clap")
    (synopsis
      "A simple to use, efficient, and full-featured Command Line Argument Parser")
    (description
      "This package provides a simple to use, efficient, and full-featured Command Line
Argument Parser")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-complete-3
  (package
    (name "rust-clap-complete")
    (version "3.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "190lxhgfin17cm8kx4jw4gild3nzkfswplx58lkw4wwdrpxfd4ns"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-3)
         ("rust-clap-lex" ,rust-clap-lex-0.2)
         ("rust-is-executable" ,rust-is-executable-1)
         ("rust-os-str-bytes" ,rust-os-str-bytes-6)
         ("rust-pathdiff" ,rust-pathdiff-0.2)
         ("rust-shlex" ,rust-shlex-1)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://github.com/clap-rs/clap/tree/master/clap_complete")
    (synopsis "Generate shell completion scripts for your clap::Command")
    (description "Generate shell completion scripts for your clap::Command")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-x86-64-msvc-0.36
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "103n3xijm5vr7qxr1dps202ckfnv7njjnnfqmchg8gl5ii5cl4f8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-x86-64-gnu-0.36
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qfrck3jnihymfrd01s8260d4snql8ks2p8yaabipi3nhwdigkad"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-i686-msvc-0.36
  (package
    (name "rust-windows-i686-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "097h2a7wig04wbmpi3rz1akdy4s8gslj5szsx8g2v0dj91qr3rz2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-i686-gnu-0.36
  (package
    (name "rust-windows-i686-gnu")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dm3svxfzamrv6kklyda9c3qylgwn5nwdps6p0kc9x6s077nq3hq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-aarch64-msvc-0.36
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ixaxs2c37ll2smprzh0xq5p238zn8ylzb3lk1zddqmd77yw7f4v"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Code gen support for the windows crate")
    (description "Code gen support for the windows crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-windows-sys-0.36
  (package
    (name "rust-windows-sys")
    (version "0.36.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lmqangv0zg1l46xiq7rfnqwsx8f8m52mqbgg2mrx7x52rd1a17a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-aarch64-msvc" ,rust-windows-aarch64-msvc-0.36)
         ("rust-windows-i686-gnu" ,rust-windows-i686-gnu-0.36)
         ("rust-windows-i686-msvc" ,rust-windows-i686-msvc-0.36)
         ("rust-windows-x86-64-gnu" ,rust-windows-x86-64-gnu-0.36)
         ("rust-windows-x86-64-msvc" ,rust-windows-x86-64-msvc-0.36))))
    (home-page "https://github.com/microsoft/windows-rs")
    (synopsis "Rust for Windows")
    (description "Rust for Windows")
    (license (list license:expat license:asl2.0))))

(define-public rust-parking-lot-core-0.9
  (package
    (name "rust-parking-lot-core")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ab95rljb99rm51wcic16jgbajcr6lgbqkrr21w7bc2wyb5pk8h9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-petgraph" ,rust-petgraph-0.6)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thread-id" ,rust-thread-id-4)
         ("rust-windows-sys" ,rust-windows-sys-0.36))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "An advanced API for creating custom synchronization primitives.")
    (description
      "An advanced API for creating custom synchronization primitives.")
    (license (list license:expat license:asl2.0))))

(define-public rust-once-cell-1
  (package
    (name "rust-once-cell")
    (version "1.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "once_cell" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09a2gb4sls2d3762jsps81s5y270465s0ip7cvv5h7qc7zwcw2bp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atomic-polyfill" ,rust-atomic-polyfill-0.1)
         ("rust-parking-lot-core" ,rust-parking-lot-core-0.9))))
    (home-page "https://github.com/matklad/once_cell")
    (synopsis "Single assignment cells and lazy values.")
    (description "Single assignment cells and lazy values.")
    (license (list license:expat license:asl2.0))))

(define-public rust-markup5ever-0.11
  (package
    (name "rust-markup5ever")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "markup5ever" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05mhzsp6lfxla1fgd0ac283b405s6kyj27wj5r6d7wq42jxjj9ks"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-phf" ,rust-phf-0.10)
         ("rust-phf-codegen" ,rust-phf-codegen-0.10)
         ("rust-string-cache" ,rust-string-cache-0.8)
         ("rust-string-cache-codegen" ,rust-string-cache-codegen-0.5)
         ("rust-tendril" ,rust-tendril-0.4))))
    (home-page "https://github.com/servo/html5ever")
    (synopsis "Common code for xml5ever and html5ever")
    (description "Common code for xml5ever and html5ever")
    (license (list license:expat license:asl2.0))))

(define-public rust-html5ever-0.26
  (package
    (name "rust-html5ever")
    (version "0.26.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "html5ever" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dx3lhfwngi21wa79cpjv5rd4wn5vmklr50wrwbryidq92mqr9my"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-mac" ,rust-mac-0.1)
         ("rust-markup5ever" ,rust-markup5ever-0.11)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/servo/html5ever")
    (synopsis "High-performance browser-grade HTML5 parser")
    (description "High-performance browser-grade HTML5 parser")
    (license (list license:expat license:asl2.0))))

(define-public rust-ammonia-3
  (package
    (name "rust-ammonia")
    (version "3.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ammonia" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0x2a5h675rwhjy3b6j5px07q69jqngxacdzfrhy05k48xq4jbvfm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-html5ever" ,rust-html5ever-0.26)
         ("rust-maplit" ,rust-maplit-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-tendril" ,rust-tendril-0.4)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/rust-ammonia/ammonia")
    (synopsis "HTML Sanitization")
    (description "HTML Sanitization")
    (license (list license:expat license:asl2.0))))

(define-public mdbook
  (package
    (name "mdbook")
    (version "0.4.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mdbook" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17d3w87fdyvxbirw8pvgq0ry0nlpqyjdyja0hl4yapiy3bl2lqbl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs
        (("rust-ammonia" ,rust-ammonia-3)
         ("rust-anyhow" ,rust-anyhow-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-3)
         ("rust-clap-complete" ,rust-clap-complete-3)
         ("rust-elasticlunr-rs" ,rust-elasticlunr-rs-2)
         ("rust-env-logger" ,rust-env-logger-0.7)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-gitignore" ,rust-gitignore-1)
         ("rust-handlebars" ,rust-handlebars-4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-notify" ,rust-notify-4)
         ("rust-opener" ,rust-opener-0.5)
         ("rust-pulldown-cmark" ,rust-pulldown-cmark-0.9)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-shlex" ,rust-shlex-1)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-topological-sort" ,rust-topological-sort-0.1)
         ("rust-warp" ,rust-warp-0.3))
        #:cargo-development-inputs
        (("rust-assert-cmd" ,rust-assert-cmd-1)
         ("rust-predicates" ,rust-predicates-2)
         ("rust-pretty-assertions" ,rust-pretty-assertions-0.6)
         ("rust-select" ,rust-select-0.5)
         ("rust-semver" ,rust-semver-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/rust-lang/mdBook")
    (synopsis "Creates a book from markdown files")
    (description "Creates a book from markdown files")
    (license license:mpl2.0)))

